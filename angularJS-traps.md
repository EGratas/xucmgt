# AngularJS traps and stuff that we wish we knew after debugging for 2 hours.

  Scope.watch takes a string as a first parameter.

    ```js
      $scope.$watch('someVariable', (newValue, oldValue) => {
        // watch stuff
      });
    ```

  When listening on an event using scope.on, the first parameter of the callback is always the event.

    ```js
      $rootScope.$on('someEvent',(event, data)=>{
    ```
  After listening on an event using scope.on, you should call the content of scope.on again, in case the service sent the event before the directive or controller started listening for it. This can happened on log in or after a refresh.  

  
    ```js
      $rootScope.$on('someEvent',(event, data)=>{
        someFunction();
      }
      someFunction();
    ``` 


  You can export a class from your app files and import it inside your specs.
  Note that this is not used to mock it, but to use the real implementation of a class.

    ```js
      //app
      export class SomeClass {
        // class stuff
      }

      //spec
      import { SomeClass } from '../../../../app/assets/javascripts/someModule/someFile.js';
    ```

  To add something at the end of the call stack, for example directly after an ongoing digest, you can use lodash :

    ```js
      _.defer(() => { scope.$apply(); });
    ```

  To test function or chained asynchronous functions, you can control the end of the test with `done` (you should cover all the cases to avoid to wait the default timeout):

    1. Add done in parameter of the test function

    ```js
      it('should cancel token renewal if the user logs out', function(done) {
    ```

    2. Use `done()` and `done.fail()` to make the test pass or not. Don't forget the `rootScope.$digest();` to actually enter in the promise callback

    ```js
      let promise = xucLink.login("jbond", "1234", "1001", false);
      [...]
      promise.then(() => {
      xucLink.getTokenRenewPromise().then(
        () =>  {
          done.fail('Should be canceled when logged out');
        }, () => {
          done();
        }
      ).catch(() => {
        done.fail('Should be resolved');
      });
    });
    rootScope.$digest();
    ```
