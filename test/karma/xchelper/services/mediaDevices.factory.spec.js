describe('media devices factory', () => {

  var $window = {
    navigator: {
      mediaDevices: {
        promise: null,
        enumerateDevices: () => { return $window.navigator.mediaDevices.promise.promise; }
      }
    }
  };

  var mediaDevices;
  var $scope;

  var windowProvider = {
    $get: function ($q) {
      $window.navigator.mediaDevices.promise = $q.defer();
      return $window;
    }
  };

  const devices = [{
    "deviceId": "default",
    "kind": "audioinput",
    "label": "Par défaut",
    "groupId": "3014cc1edeb9c8b745fa3a4a8ea0c0c9471e867e025587bc7895ba8b0f6632e5"
  }, {
    "deviceId": "c964a067e333b6868ef8c6703d19a0013b033ddc4760a62a0df4d13d5bef9b31",
    "kind": "audioinput",
    "label": "Audio interne Stéréo analogique",
    "groupId": "866e0814806f79637c7ff580dd7228ef5aa0ae063dc58660f7b78e703eeff1ad"
  }, {
    "deviceId": "63de9385d4859fd2c60e8460c2781b86aa2e0dd3554035a62c28db9c347eed6e",
    "kind": "videoinput",
    "label": "",
    "groupId": "47ad004ec25ef221d0af70981d1e35434a4e093fa1d4fcf98f259e9f7ed05467"
  }, {
    "deviceId": "defaultoutput",
    "kind": "audiooutput",
    "label": "Par défaut",
    "groupId": "defaultoutput"
  }];

  beforeEach(() => {
    angular.mock.module(function ($provide) {
      $provide.provider('$window', windowProvider);
    });
  });

  beforeEach(angular.mock.module('karma-backend'));
  beforeEach(angular.mock.module('html-templates'));
  beforeEach(angular.mock.module('xcHelper'));
  beforeEach(angular.mock.module('xcCti'));

  beforeEach(angular.mock.inject(function (_mediaDevices_ , _$rootScope_) {
    $scope = _$rootScope_;
    mediaDevices = _mediaDevices_;
  }));

  it('get audioinput devices', (done) => {

    const expected = [{
      "deviceId": "defaultoutput",
      "kind": "audiooutput",
      "label": "Par défaut",
      "groupId": "defaultoutput"
    }];

    $window.navigator.mediaDevices.promise.resolve(devices);
    var promise = mediaDevices.getAudioOutput();
    promise.then((result) => {
      expect(result).toEqual(expected);
      done();
    });
    $scope.$apply();
  });
});