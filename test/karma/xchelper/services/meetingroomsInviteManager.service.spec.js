describe('Meetingrooms invite manager', function() {
    
  var $rootScope;
  var MeetingroomsInviteManager;
  var $window;

  const ack = (type, name, id) => {
    return {
      ackType: type,
      displayName: name,
      requestId: id,
      _type: 'something'
    };
  };

  const response = (type, name, id) => {
    return {
      responseType: type,
      displayName: name,
      requestId: id,
      _type: 'something'
    };
  };
    
  beforeEach(angular.mock.module('html-templates'));
  beforeEach(angular.mock.module('karma-backend'));
  beforeEach(angular.mock.module('xcHelper'));
  beforeEach(angular.mock.module('xcCti'));
    
  beforeEach(angular.mock.inject((_$rootScope_, _MeetingroomsInviteManager_, _$window_) => {
    $rootScope = _$rootScope_;
    MeetingroomsInviteManager = _MeetingroomsInviteManager_;
    $window = _$window_;
  }));
    
  it('can send an invitation', () => {
    spyOn($window.Cti, "inviteToMeetingRoom");
    MeetingroomsInviteManager.sendInvitation('jbond', {raw: 'some.valid.token'});
    MeetingroomsInviteManager.sendInvitation('lluke', {raw: 'some.valid.token'});
    expect($window.Cti.inviteToMeetingRoom).toHaveBeenCalledTimes(2);
  });
    
  it('removes an invitation from the storage if its accepted or refused', () => {
    MeetingroomsInviteManager.sendInvitation('jbond', 'James Bond', {raw: 'some.valid.token'});
    expect(MeetingroomsInviteManager.invitations[0]).toEqual(jasmine.objectContaining({requestId: 1, displayName: "James Bond"}));
    MeetingroomsInviteManager.onResponse(response("Accept", "James Bond", 1));
    expect(MeetingroomsInviteManager.invitations[0]).toBe(undefined);
    MeetingroomsInviteManager.sendInvitation('jluke', 'Jean Luke', {raw: 'some.valid.token'});
    expect(MeetingroomsInviteManager.invitations[0]).toEqual(jasmine.objectContaining({requestId: 2, displayName: "Jean Luke"}));
    MeetingroomsInviteManager.onResponse(response("Reject", "Jean Luke", 2));
    expect(MeetingroomsInviteManager.invitations[0]).toBe(undefined);
  });

  it('counts the number of invitations', () => {
    expect(MeetingroomsInviteManager.invitations.length).toEqual(0);
    MeetingroomsInviteManager.sendInvitation('jbond', 'James Bond', {raw: 'some.valid.token'});
    expect(MeetingroomsInviteManager.invitations.length).toEqual(1);
    MeetingroomsInviteManager.sendInvitation('jluke', 'Jean Luke', {raw: 'some.valid.token'});
    expect(MeetingroomsInviteManager.invitations.length).toEqual(2);
    MeetingroomsInviteManager.onResponse(response("Accept", "James Bond", 1));
    expect(MeetingroomsInviteManager.invitations.length).toEqual(1);
    MeetingroomsInviteManager.onResponse(response("Reject", "Jean Luke", 2));
    expect(MeetingroomsInviteManager.invitations.length).toEqual(0);
  });
    
  it('propagate an event on invitation accepted', () => {
    spyOn($rootScope, "$broadcast");
    MeetingroomsInviteManager.sendInvitation('jbond', 'James Bond', {raw: 'some.valid.token'});
    expect($rootScope.$broadcast).toHaveBeenCalledWith(MeetingroomsInviteManager.NEW_INVITATION_COUNT_EVENT, 1);
    MeetingroomsInviteManager.onResponse(response("Accept", "James Bond", 1));
    expect($rootScope.$broadcast).toHaveBeenCalledWith(MeetingroomsInviteManager.NEW_INVITATION_COUNT_EVENT, 0);
    expect($rootScope.$broadcast).toHaveBeenCalledWith(
      MeetingroomsInviteManager.DISPLAY_INVITATION_TOAST_EVENT,
      MeetingroomsInviteManager.TOAST_ACCEPTED_MESSAGE,
      "James Bond");
  });
    
  it('propagate an event on invitation refused', () => {
    spyOn($rootScope, "$broadcast");
    MeetingroomsInviteManager.sendInvitation('jbond', 'James Bond', {raw: 'some.valid.token'});
    expect($rootScope.$broadcast).toHaveBeenCalledWith(MeetingroomsInviteManager.NEW_INVITATION_COUNT_EVENT, 1);
    MeetingroomsInviteManager.onResponse(response("Reject", "James Bond", 1));
    expect($rootScope.$broadcast).toHaveBeenCalledWith(MeetingroomsInviteManager.NEW_INVITATION_COUNT_EVENT, 0);
    expect($rootScope.$broadcast).toHaveBeenCalledWith(
      MeetingroomsInviteManager.DISPLAY_INVITATION_TOAST_EVENT,
      MeetingroomsInviteManager.TOAST_REJECTED_MESSAGE,
      "James Bond");
  });
    
  it('propagate an event on invitation nack', () => {
    spyOn($rootScope, "$broadcast");
    MeetingroomsInviteManager.sendInvitation('jbond', 'James Bond', {raw: 'some.valid.token'});
    expect(MeetingroomsInviteManager.invitations[0]).toEqual(jasmine.objectContaining({requestId: 1, displayName: "James Bond"}));
    MeetingroomsInviteManager.onAck(ack("NACK", "James Bond", 1));
    expect(MeetingroomsInviteManager.invitations[0]).toEqual(undefined);
    expect($rootScope.$broadcast).toHaveBeenCalledWith(
      MeetingroomsInviteManager.DISPLAY_INVITATION_TOAST_EVENT,
      MeetingroomsInviteManager.TOAST_UNAVAILABLE_MESSAGE,
      "James Bond"
    );
  });
    
});
