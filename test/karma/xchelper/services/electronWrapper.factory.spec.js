'use strict';

describe('Service: Electron wrapper', function () {

  var electronWrapper;
  var $window;
  var locationValue = {
    origin: 'test',
    protocol: 'http:',
    href: 'http://myxucmgt/',
    replace: function() {}
  };
  beforeEach(angular.mock.module('xcCti', 'xcHelper'));

  var windowProvider = {
    $get: function() {
      return {
        setElectronConfig: function() {},
        location: locationValue,
        navigator: {languages: 'en_EN'}
      };
    }
  };

  beforeEach(function() {
    angular.mock.module(function($provide) {
      $provide.provider('$window', windowProvider);
    });
  });

  beforeEach(angular.mock.inject(function(_$window_) {
    $window = _$window_;
    spyOn($window, 'setElectronConfig');
  }));

  it('should know if web app is running without electron context', function() {
    angular.mock.inject(function(_electronWrapper_) {
      electronWrapper = _electronWrapper_;
    });
    delete $window.setElectronConfig;
    expect(electronWrapper.isElectron()).toBe(false);
  });

  it('should know if web app is running with electron context', function() {
    angular.mock.inject(function(_electronWrapper_) {
      electronWrapper = _electronWrapper_;
    });

    expect(electronWrapper.isElectron()).toBe(true);
  });


  it('should set electron config', function() {

    angular.mock.inject(function(_electronWrapper_) {
      electronWrapper = _electronWrapper_;
    });

    electronWrapper.setElectronConfig({title:'myTitle'});
    expect($window.setElectronConfig).toHaveBeenCalledWith({title: 'myTitle'});

    electronWrapper.setElectronConfig({width:100, height:200});
    expect($window.setElectronConfig).toHaveBeenCalledWith({size: {w: 100, h: 200, mini: false}});

    electronWrapper.setElectronConfig({width:100, height:200, title:'myTitle', minimalist: true});
    expect($window.setElectronConfig).toHaveBeenCalledWith({size: {w: 100, h: 200, mini: true}, title: 'myTitle'});

    electronWrapper.setElectronConfig({width:null, height:200, title:null});
    expect($window.setElectronConfig).toHaveBeenCalledWith({});

    electronWrapper.setElectronConfig({confirmQuit: {data: {}}});
    expect($window.setElectronConfig).toHaveBeenCalledWith({confirmQuit: {data: {}}});
  });

  it('should focus electron window', function() {

    angular.mock.inject(function(_electronWrapper_) {
      electronWrapper = _electronWrapper_;
    });

    electronWrapper.setFocus();
    expect($window.setElectronConfig).toHaveBeenCalledWith({focus: true});
  });

  it('should send runExecutable command to electron', function() {
    angular.mock.inject(function(_electronWrapper_) {
      electronWrapper = _electronWrapper_;
    });

    var path = "executable";
    var args = ["titi", "tutu"];
    electronWrapper.runExecutable(path, args);
    expect($window.setElectronConfig).toHaveBeenCalledWith({runExecutable: path, executableArgs: args});
  });

  it('should toggle window', function() {
    angular.mock.inject(function(_electronWrapper_) {
      electronWrapper = _electronWrapper_;
    });

    let electronToggledSize = {width: 10, height: 20, minimalist: true};
    let electronDefaultSize = { width: 100, height: 200 };

    expect(electronWrapper.isWindowToggled()).toBe(false);
    electronWrapper.toggleWindow(electronToggledSize, electronDefaultSize);
    expect($window.setElectronConfig).toHaveBeenCalledWith({size: { w: 10, h: 20, mini: true }});
    expect(electronWrapper.isWindowToggled()).toBe(true);
    electronWrapper.toggleWindow(electronToggledSize, electronDefaultSize);
    expect($window.setElectronConfig).toHaveBeenCalledWith({size: { w: 100, h: 200, mini: false }});
    expect(electronWrapper.isWindowToggled()).toBe(false);
  });

  it('sets electron to fullscreen', function(){
    angular.mock.inject(function(_electronWrapper_) {
      electronWrapper = _electronWrapper_;
    });

    electronWrapper.setElectronFullscreen({setFullScreen: true});
    expect($window.setElectronConfig).toHaveBeenCalledWith({setFullScreen: true});
  });

});
