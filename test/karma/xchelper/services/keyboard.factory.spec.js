describe('Keyboard on demand', function() {
  var $uibModal;

  beforeEach(angular.mock.module('html-templates'));
  beforeEach(angular.mock.module('karma-backend'));
  beforeEach(angular.mock.module('xcHelper'));
  beforeEach(angular.mock.module('xcCti'));

  beforeEach(angular.mock.inject(function(_$uibModal_) {
    $uibModal = _$uibModal_;
  }));

  it('Open popup when asking for DTMF keyboard', angular.mock.inject(function(keyboard) {
    spyOn($uibModal, 'open').and.returnValue({result: { then: function(){}}});
    keyboard.show();
    expect($uibModal.open).toHaveBeenCalled();
  }));

});
