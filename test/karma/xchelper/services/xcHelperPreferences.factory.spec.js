describe('xcHelper preferences factory', () => {

  var xcHelperPreferences;
  var localStorageService;
  
  beforeEach(angular.mock.module('karma-backend'));
  beforeEach(angular.mock.module('html-templates'));
  beforeEach(angular.mock.module('xcHelper'));
  beforeEach(angular.mock.module('xcCti'));

  beforeEach(angular.mock.inject(function (_xcHelperPreferences_, _localStorageService_) {
    xcHelperPreferences = _xcHelperPreferences_;
    localStorageService = _localStorageService_;
  }));

  it('save ringing device id in localStorage', () => {
    let selectedDeviceId = '05841a6c85ff6dec33856edc2402a2649acde8e14e305283d4a67cd73b187ad9';
    spyOn(localStorageService, 'set');
    xcHelperPreferences.setRingingDeviceId('05841a6c85ff6dec33856edc2402a2649acde8e14e305283d4a67cd73b187ad9');

    expect(localStorageService.set).toHaveBeenCalledWith('ringingDeviceId', selectedDeviceId);
  });

  it('get ringing device id from localStorage', () => {
    let selectedDeviceId = '05841a6c85ff6dec33856edc2402a2649acde8e14e305283d4a67cd73b187ad9';
    spyOn(localStorageService, 'get').and.returnValue(selectedDeviceId);
    let val = xcHelperPreferences.getRingingDeviceId();

    expect(localStorageService.get).toHaveBeenCalledWith('ringingDeviceId');
    expect(val).toBe(selectedDeviceId);
  });

  it ('return default if no ringing device id is set', () => {
    let expectedDeviceId = 'default';
    spyOn(localStorageService, 'get').and.returnValue(null);
    let val = xcHelperPreferences.getRingingDeviceId();

    expect(localStorageService.get).toHaveBeenCalledWith('ringingDeviceId');
    expect(val).toBe(expectedDeviceId);
  });
});