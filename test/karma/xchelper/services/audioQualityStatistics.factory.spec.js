describe('audioQualityStatistics', function() {
  
  var $rootScope;
  var audioQualityStatistics;
  var xcWebrtcMock;
  var oldXcWebrtc;
  var $interval;
  var xcWebrtcMethods;
  var $log;

  var BadRTCOutboundRTPAudioStreamMock = { id: 'RTCOutboundRTPAudioStream', kind: 'audio', packetsSent: 100 };
  var BadRTCRemoteInboundRtpAudioStreamMock = { id: 'RTCRemoteInboundRtpAudioStream', kind: 'audio', packetsLost: 40, jitter: 0.136, roundTripTime: 0.319 };
  var BadRTCInboundRtpAudioStreamMock = { id: 'RTCInboundRTPAudioStream', kind: 'audio', packetsLost: 40, packetsReceived: 60, jitter: 0.191};

  var MediumRTCOutboundRTPAudioStreamMock = { id: 'RTCOutboundRTPAudioStream', kind: 'audio', packetsSent: 100 };
  var MediumRTCRemoteInboundRtpAudioStreamMock = { id: 'RTCRemoteInboundRtpAudioStream', kind: 'audio', packetsLost: 15, jitter: 0.065, roundTripTime: 0.175 };
  var MediumRTCInboundRtpAudioStreamMock = { id: 'RTCInboundRTPAudioStream', kind: 'audio', packetsLost: 15, packetsReceived: 85, jitter: 0.058};

  var GoodRTCOutboundRTPAudioStreamMock = { id: 'RTCOutboundRTPAudioStream', kind: 'audio', packetsSent: 100 };
  var GoodRTCRemoteInboundRtpAudioStreamMock = { id: 'RTCRemoteInboundRtpAudioStream', kind: 'audio', packetsLost: 0, jitter: 0.013, roundTripTime: 0.036 };
  var GoodRTCInboundRtpAudioStreamMock = { id: 'RTCInboundRTPAudioStream', kind: 'audio', packetsLost: 0, packetsReceived: 100, jitter: 0.016};

  var createXcWebrtcMock = (quality) => {
    xcWebrtcMethods = {
      getCurrentRTCPeerConnection: (() => {
        return {
          getStats: () => { return new Promise((res) => {
            if (quality == 'bad') res([BadRTCOutboundRTPAudioStreamMock, BadRTCRemoteInboundRtpAudioStreamMock, BadRTCInboundRtpAudioStreamMock]);
            else if (quality == 'medium') res([MediumRTCOutboundRTPAudioStreamMock, MediumRTCRemoteInboundRtpAudioStreamMock, MediumRTCInboundRtpAudioStreamMock]);
            else if (quality == 'good') res([GoodRTCOutboundRTPAudioStreamMock, GoodRTCRemoteInboundRtpAudioStreamMock, GoodRTCInboundRtpAudioStreamMock]);
          });}
        };
      })()
    };

    xcWebrtcMock = jasmine.createSpyObj('xc_webrtc', xcWebrtcMethods);
    xcWebrtcMock.MessageType = xc_webrtc.MessageType;
    oldXcWebrtc = xc_webrtc;
    xc_webrtc = xcWebrtcMock;
  };

  beforeEach(angular.mock.module('xcHelper'));
  beforeEach(angular.mock.module('xcCti'));
  beforeEach(angular.mock.module('html-templates'));
  beforeEach(angular.mock.module('karma-backend'));

  beforeEach(angular.mock.inject(function(_audioQualityStatistics_, _$rootScope_, _$interval_, _$log_) {
    audioQualityStatistics = _audioQualityStatistics_;
    $rootScope = _$rootScope_;
    $interval = _$interval_;
    $log = _$log_;
  }));

  afterEach(function() {
    xc_webrtc = oldXcWebrtc;
  });
  
  it('should send an event if the audio quality is bad', function(done){
    createXcWebrtcMock('bad');

    $rootScope.$on(audioQualityStatistics.AUDIO_QUALITY_EVENT,(event, data)=>{
      expect(data).toEqual({
        quality: audioQualityStatistics.AUDIO_QUALITY_BAD,
        statistics: {
          uploadStatistics: jasmine.objectContaining({ jitter: 136, packetsLostPercentage: 40, roundTripTime: 319, packetTotal: '40/60', 
            packetsTotalAccumulator: 100, packetsLostAccumulator: 40 }),
          downloadStatistics: jasmine.objectContaining({ jitter: 191, packetsLostPercentage: 40, packetTotal: '40/60', 
            packetsTotalAccumulator: 100, packetsLostAccumulator: 40 }) 
        }
      });
      done();
    });

    audioQualityStatistics.startAudioQualityMonitoring();
    $interval.flush(audioQualityStatistics.AUDIO_QUALITY_MONITORING_MS + 1);
    $rootScope.$digest();
  });
  
  it('should send an event if the audio quality is medium', function(done){
    createXcWebrtcMock('medium');

    $rootScope.$on(audioQualityStatistics.AUDIO_QUALITY_EVENT,(event, data)=>{
      expect(data).toEqual({
        quality: audioQualityStatistics.AUDIO_QUALITY_MEDIUM,
        statistics: {
          uploadStatistics: jasmine.objectContaining({ jitter: 65, packetsLostPercentage: 15, roundTripTime: 175, packetTotal: '15/85', 
            packetsTotalAccumulator: 100, packetsLostAccumulator: 15 }),
          downloadStatistics: jasmine.objectContaining({ jitter: 58, packetsLostPercentage: 15, packetTotal: '15/85', 
            packetsTotalAccumulator: 100, packetsLostAccumulator: 15 }) 
        }
      });
      done();
    });

    audioQualityStatistics.startAudioQualityMonitoring();
    $interval.flush(audioQualityStatistics.AUDIO_QUALITY_MONITORING_MS + 1);
    $rootScope.$digest();
  });

  it('should send an event if the audio quality is good', function(done){
    createXcWebrtcMock('good');

    $rootScope.$on(audioQualityStatistics.AUDIO_QUALITY_EVENT,(event, data)=>{
      expect(data).toEqual({
        quality: audioQualityStatistics.AUDIO_QUALITY_GOOD, 
        statistics: {
          uploadStatistics: jasmine.objectContaining({ jitter: 13, packetsLostPercentage: 0, roundTripTime: 36, packetTotal: '0/100',
            packetsTotalAccumulator: 100, packetsLostAccumulator: 0 }),
          downloadStatistics: jasmine.objectContaining({ jitter: 16, packetsLostPercentage: 0, packetTotal: '0/100', 
            packetsTotalAccumulator: 100, packetsLostAccumulator: 0 }) 
        }
      });
      done();
    });
    
    audioQualityStatistics.startAudioQualityMonitoring();
    $interval.flush(audioQualityStatistics.AUDIO_QUALITY_MONITORING_MS + 1);
    $rootScope.$digest();
  });

  it('should propagate a call quality to xuc after the monitoring stops', function(){
    createXcWebrtcMock('good');
    spyOn($log, 'info');
    audioQualityStatistics.startAudioQualityMonitoring('test-sip-call-id');
    $interval.flush(audioQualityStatistics.AUDIO_QUALITY_MONITORING_MS * 2);
    audioQualityStatistics.stopAudioQualityMonitoring('test-sip-call-id');
    $rootScope.$digest();
    expect($log.info).toHaveBeenCalledWith('test-sip-call-id Call quality report -> Highest RTT 0ms - Highest Jitter upstream 0ms / downstream 0ms - Highest Packet loss upstream 0% / downstream 0% ', undefined, true );
  });

});
