import { Token, TokenContent } from '../../../../app/assets/javascripts/xchelper/services/jitsiProxy.service';

describe('Service: jitsi proxy', function () {

  var jitsiProxy;
  var XucLink;
  var meetingroomsInviteManager;
  var $window, $rootScope, $httpBackend, $uibModal, $q, XucVideoEventManager, electronWrapper, XucUser;

  const fakeValidator = () => {
    return {} ;
  };

  const fakeToken = new Token('some.to-ken', new TokenContent(
    'xivo',
    'myRoom',
    '*',
    'xivo',
    'some-uuid',
    'My meeting room',
    '025',
    false)
  );

  const jwt = 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpc3MiOiJ4aXZvIiwicm9vbSI6InNlY3JldHJvb20iLCJzdWIiOiIqIiwiYXVkIjoieGl2byIsInJvb211dWlkIjoiYWVlMDQ2MjQtYjhkMC00OTlmLWE0N2UtZDVkZjJlZmZhZDU5Iiwicm9vbWRpc3BsYXluYW1lIjoiU3VwM3IuIHPDqcOncsOodF9yMDBtOyBeXiIsInJvb21udW1iZXIiOiIwMDciLCJyZXF1aXJlcGluIjp0cnVlfQ.vGv1vBLNFrvcaWC7j4h-dGJ2XX98lcNbDM-S-m75O3g';
  const jwtContent = new TokenContent(
    'xivo',
    'secretroom',
    '*',
    'xivo',
    'aee04624-b8d0-499f-a47e-d5df2effad59',
    'Sup3r. séçrèt_r00m; ^^',
    '007',
    true
  );
  const token = new Token(jwt, jwtContent);

  beforeEach(angular.mock.module('html-templates'));
  beforeEach(angular.mock.module('karma-backend'));
  beforeEach(angular.mock.module('xcHelper'));
  beforeEach(angular.mock.module('xcCti'));

  beforeEach(angular.mock.inject(function (_JitsiProxy_, _$window_, _XucLink_, _$rootScope_, _$httpBackend_, _$uibModal_,  _$q_, _MeetingroomsInviteManager_, _XucVideoEventManager_, _electronWrapper_, _XucUser_) {
    jitsiProxy = _JitsiProxy_;
    $window = _$window_;
    XucLink = _XucLink_;
    $rootScope = _$rootScope_;
    $httpBackend = _$httpBackend_;
    $uibModal = _$uibModal_;
    meetingroomsInviteManager = _MeetingroomsInviteManager_;
    XucVideoEventManager = _XucVideoEventManager_;
    electronWrapper = _electronWrapper_;
    XucUser = _XucUser_;
    $q =  _$q_;

    var token = "aaaa-bbbb-cccc-dddd-1234";
    var user = { username: "jbond", phoneNumber: "1001", token: token, fullName: "John Boo" };
    $window.externalConfig = { host: '192.168.0.3' };

    class jitsiConstructor {
      constructor(domain, options) {
        this.domain = domain;
        this.options = options,
        this.dispose = () => { delete this; },
        this.addListener = () => { };
        this.isAudioMuted = () => {return Promise.resolve(false);};
        this.executeCommand = jasmine.createSpy();
      }
    }

    window.JitsiMeetExternalAPI = jitsiConstructor;

    spyOn(XucLink, 'whenLogged').and.callFake(() => {
      return Promise.resolve(user);
    });

    spyOn(XucUser, 'getUserAsync').and.callFake(() => {
      return Promise.resolve(user);
    });

    spyOn(XucLink, 'getServerUrl').and.returnValue("http://localhost");
  }));

  afterEach(() => {
    jitsiProxy.dismissVideo();
    $httpBackend.verifyNoOutstandingExpectation();
    $httpBackend.verifyNoOutstandingRequest();
  });

  it('can instantiate service', () => {
    expect(jitsiProxy).not.toBeUndefined();
  });

  it('should initialize jitsi API object', (done) => {
    spyOn(jitsiProxy, 'getMeetingRoomToken').and.returnValue(Promise.resolve(fakeToken));
    spyOn(jitsiProxy, 'tradeTokenForCookie').and.returnValue(Promise.resolve(fakeValidator));
    spyOn(jitsiProxy, 'checkPinCode').and.returnValue(Promise.resolve(fakeToken));
    var promise = jitsiProxy.startVideo(
      42,
      'static'
    );
    $rootScope.$apply();

    promise.then((api) => {
      expect(api).toBeDefined();
      done();
    });

  });

  it('should create a confRoom from the right route, name and parent node', (done) => {
    
    spyOn(jitsiProxy, 'getMeetingRoomToken').and.returnValue(Promise.resolve(fakeToken));
    spyOn(jitsiProxy, 'tradeTokenForCookie').and.returnValue(Promise.resolve(fakeValidator));
    spyOn(jitsiProxy, 'checkPinCode').and.returnValue(Promise.resolve(fakeToken));
    spyOn(Cti, "videoEvent");
    
    const promise = jitsiProxy.startVideo(
      42,
      'personal'
    );
    $rootScope.$apply();

    promise.then(function (api) {
      expect(api.domain).toBe('192.168.0.3/video');
      expect(api.options.roomName).toBe('some-uuid');
      expect(api.options.jwt).toBe(fakeToken.raw);
      expect(api.options.configOverwrite.subject).toBe('My meeting room (**025)');
      expect(Cti.videoEvent).toHaveBeenCalledWith(XucVideoEventManager.EVENT_VIDEO_START);
      done();
    });

  });


  it('should create a confRoom from electron context', (done) => {
    
    spyOn(jitsiProxy, 'getMeetingRoomToken').and.returnValue(Promise.resolve(fakeToken));
    spyOn(jitsiProxy, 'tradeTokenForCookie').and.returnValue(Promise.resolve(fakeValidator));
    spyOn(jitsiProxy, 'checkPinCode').and.returnValue(Promise.resolve(fakeToken));
    spyOn(electronWrapper, 'isElectron').and.returnValue(true);
    spyOn(electronWrapper, 'startJitsi');
    spyOn(Cti, "videoEvent");
    jitsiProxy.init();
    const promise = jitsiProxy.startVideo(
      42,
      'personal'
    );
    $rootScope.$apply();

    promise.then(function () {
      expect(electronWrapper.startJitsi).toHaveBeenCalledWith('some-uuid', 'My meeting room (**025)', fakeToken.raw, 'John Boo');
      done();
    });

  });

  it("should prevent creating a new confRoom if there is already one started locally", (done) => {
    spyOn(jitsiProxy, 'getMeetingRoomToken').and.returnValue(Promise.resolve(fakeToken));
    spyOn(jitsiProxy, 'tradeTokenForCookie').and.returnValue(Promise.resolve(fakeValidator));
    spyOn(jitsiProxy, 'checkPinCode').and.returnValue(Promise.resolve(fakeToken));
    spyOn($rootScope, "$emit");

    jitsiProxy
      .startVideo(42, "static")
      .then(function () {
        jitsiProxy
          .startVideo(43, "personal")
          .then(
            function () {
              fail("The second meeting room shouln't be instantiated");
            },
            function (err) {
              expect(err).toBe(jitsiProxy.ERR_VIDEO_ALREADY_STARTED);
              expect($rootScope.$emit).toHaveBeenCalledWith(jitsiProxy.VIDEO_START_ERROR_EVENT, jitsiProxy.ERR_VIDEO_ALREADY_STARTED);       
              done();
            }
          );
      });
  });

  it('should prevent creating a new confRoom if there is already one externally (eg electron)', (done) => {
    spyOn(jitsiProxy, 'getMeetingRoomToken').and.returnValue(Promise.resolve(fakeToken));
    spyOn(jitsiProxy, 'tradeTokenForCookie').and.returnValue(Promise.resolve(fakeValidator));
    spyOn(jitsiProxy, 'checkPinCode').and.returnValue(Promise.resolve(fakeToken));
    jitsiProxy.electronJitsiRunning = true;
    spyOn($rootScope, "$emit");

    jitsiProxy.startVideo(
      43,
      'personal'
    ).then(function () {
      fail("The second meeting room shouln't be instatiated");
    }, function (err) {
      expect(err).toBe(jitsiProxy.ERR_VIDEO_ALREADY_STARTED);
      expect($rootScope.$emit).toHaveBeenCalledWith(jitsiProxy.VIDEO_START_ERROR_EVENT, jitsiProxy.ERR_VIDEO_ALREADY_STARTED);   
      done();
    });
  });

  it('should get the token for the route for personal room', () => {
    const roomId = 42;
    const roomType = 'personal';
    $httpBackend.expectGET(`http://localhost/xuc/api/2.0/config/meetingrooms/${roomType}/token/${roomId}`)
      .respond('fake-token');
    jitsiProxy.getToken(roomId, { token: 'fake-token' }, roomType);
    $httpBackend.flush();
  });

  it('should get the token for the route for static room', () => {
    const roomId = 42;
    const roomType = 'static';
    $httpBackend.expectGET(`http://localhost/xuc/api/2.0/config/meetingrooms/${roomType}/token/${roomId}`)
      .respond('fake-token');
    jitsiProxy.getToken(roomId, { token: 'fake-token' }, roomType);
    $httpBackend.flush();
  });

  it('should decode jwt token and extract meetingRoom info', () => {
    const result = jitsiProxy.parseToken(jwt);

    expect(result).toEqual(token);
  });

  it('validate the token to have cookies set', () => {
    const jwt = 'abc.def.xyz';

    jitsiProxy.validateToken(jwt, { token: 'uio.hkl.sdf' });

    $httpBackend.expectGET(`http://localhost/meetingrooms/token/validate/${jwt}`)
      .respond(200, "");

    $httpBackend.flush();
  });

  it('validate the token with pin code', () => {
    const jwt = 'abc.def.xyz';
    const pin = '01234';

    jitsiProxy.validateToken(jwt, { token: 'uio.hkl.sdf' }, pin);

    $httpBackend.expectGET(`http://localhost/meetingrooms/token/validate/${jwt}?pin=${pin}`)
      .respond(200, "");

    $httpBackend.flush();
  });

  it('Force validation with empty query parameter for empty string pin', () => {
    const jwt = 'abc.def.xyz';

    jitsiProxy.validateToken(jwt, { token: 'uio.hkl.sdf' }, '');

    $httpBackend.expectGET(`http://localhost/meetingrooms/token/validate/${jwt}?pin=`)
      .respond(200, "");

    $httpBackend.flush();
  });

  it('shows modal when pin code is required', function(done) {

    spyOn(jitsiProxy, 'getMeetingRoomToken').and.returnValue(Promise.resolve(token));
    spyOn(jitsiProxy, 'tradeTokenForCookie').and.returnValue(Promise.resolve(fakeValidator));

    spyOn($uibModal, 'open').and.returnValue({result : $q.resolve(jwt)});
    var promise = jitsiProxy.startVideo(
      'myMeetingRoom',
      'static'
    );
    $rootScope.$apply();
    promise.then(function () {
      expect($uibModal.open).toHaveBeenCalled();
      done();
    });
  });

  it('do not show modal when pin code is required but you are invited to a meeting room', function(done) {

    spyOn(jitsiProxy, 'getMeetingRoomToken').and.returnValue(Promise.resolve(token));
    spyOn(jitsiProxy, 'tradeTokenForCookie').and.returnValue(Promise.resolve(fakeValidator));

    spyOn($uibModal, 'open').and.returnValue({result : $q.resolve(jwt)});
    var promise = jitsiProxy.joinOngoingVideo(fakeToken);
    $rootScope.$apply();
    promise.then(function () {
      expect($uibModal.open).not.toHaveBeenCalled();
      done();
    });
  });


  it('should allow you to know if a video is currently ongoing locally', (done) => {
    spyOn(jitsiProxy, 'getMeetingRoomToken').and.returnValue(Promise.resolve(fakeToken));
    spyOn(jitsiProxy, 'tradeTokenForCookie').and.returnValue(Promise.resolve(fakeValidator));
    spyOn(jitsiProxy, 'checkPinCode').and.returnValue(Promise.resolve(fakeToken));
    expect(jitsiProxy.videoIsOngoing()).toBe(false);
    var promise = jitsiProxy.startVideo(
      'myMeetingRoom',
      'static'
    );
    $rootScope.$apply();
    promise.then(function () {
      expect(jitsiProxy.videoIsOngoing()).toBe(true);
      jitsiProxy.uninit();
      expect(jitsiProxy.videoIsOngoing()).toBe(false);
      done();
    });
  });

  it('should allow you to know if a video is currently ongoing externally (eg electron)', () => {
    expect(jitsiProxy.videoIsOngoing()).toBe(false);
    jitsiProxy.electronJitsiRunning = true;

    expect(jitsiProxy.videoIsOngoing()).toBe(true);
  });


  it('should stop the video if the user cti link breaks while a video is ongoing', function (done) {
    spyOn(jitsiProxy, 'getMeetingRoomToken').and.returnValue(Promise.resolve(fakeToken));
    spyOn(jitsiProxy, 'tradeTokenForCookie').and.returnValue(Promise.resolve(fakeValidator));
    spyOn(jitsiProxy, 'checkPinCode').and.returnValue(Promise.resolve(fakeToken));
    spyOn($rootScope, '$emit');
    spyOn(Cti, 'videoEvent');

    var promise = jitsiProxy.startVideo(
      '42',
      'personal'
    );
    $rootScope.$apply();
    promise.then(function () {
      expect(jitsiProxy.videoIsOngoing()).toBe(true);
      $rootScope.$broadcast("linkDisConnected");
      expect(Cti.videoEvent).toHaveBeenCalledWith(XucVideoEventManager.EVENT_VIDEO_END);
      done();
    });
  });

  it('should invite another user to the meeting room', function (done) {
    spyOn(jitsiProxy, 'getMeetingRoomToken').and.returnValue(Promise.resolve(fakeToken));
    spyOn(jitsiProxy, 'tradeTokenForCookie').and.returnValue(Promise.resolve(fakeValidator));
    spyOn(jitsiProxy, 'checkPinCode').and.returnValue(Promise.resolve(fakeToken));
    spyOn(meetingroomsInviteManager, 'sendInvitation');

    const username = "cjohnson";
    const displayName = "Johnson";
    var promise = jitsiProxy.startVideo(
      '42',
      'personal'
    );
    $rootScope.$apply();
    promise.then(function () {
      jitsiProxy.inviteToMeetingRoom(username, displayName);
      expect(meetingroomsInviteManager.sendInvitation).toHaveBeenCalledWith(username, displayName, fakeToken.raw);
      done();
    });
    
  });

  it('should accept invitation to a meeting room', function () {
    spyOn(meetingroomsInviteManager, 'acceptInvitation');
    spyOn(jitsiProxy, 'joinOngoingVideo');
    spyOn(jitsiProxy, 'initJitsi').and.callFake(() => {});

    const username = "cjohnson";
    const requestId = 1;

    jitsiProxy.acceptInvitation(requestId, username, jwt);
    expect(meetingroomsInviteManager.acceptInvitation).toHaveBeenCalledWith(requestId, username, jwt);
    expect(jitsiProxy.joinOngoingVideo).toHaveBeenCalledWith(token);
  });

  it('should reject invitation to a meeting room', function () {
    
    spyOn(meetingroomsInviteManager, 'rejectInvitation');
  
    const username = "cjohnson";
    const requestId = 1;
  
    jitsiProxy.rejectInvitation(requestId, username);
    expect(meetingroomsInviteManager.rejectInvitation).toHaveBeenCalledWith(requestId, username);
  });

  it('should check on start if we can reach jitsi APIs', function() {
    expect(jitsiProxy.jitsiAvailable).toBeFalsy();

    spyOn(jitsiProxy, 'isApiReachable').and.callFake( () => {
      jitsiProxy.jitsiAvailable = true;
    });
    jitsiProxy.init();

    expect(jitsiProxy.jitsiAvailable).toBeTruthy();
  });

  it('should display a kicked toast and close the video if the current user is kicked', () => {
    spyOn(jitsiProxy, 'dismissVideo').and.returnValue(undefined);
    spyOn(jitsiProxy, 'toast').and.returnValue(undefined);
    jitsiProxy.onKickedOut({kicked: {id: "someId", local: true}, kicker: {id: "someOtherId"}});
    expect(jitsiProxy.toast).toHaveBeenCalledTimes(1);
    expect(jitsiProxy.dismissVideo).toHaveBeenCalledTimes(1);
  });

  it('should decode advanced json kick events for the video', () => {
    let encodedEvent = JSON.stringify({
      event: jitsiProxy.VIDEO_KICKED_EVENT,
      eventFromJitsi: {
        kicker: {id: "someId"},
        kicked: {id: "someId", local: true}
      }
    });
    spyOn(jitsiProxy, 'onKickedOut').and.returnValue(undefined);
    jitsiProxy.electronAdvancedVideoEventDecoder(encodedEvent);
    expect(jitsiProxy.onKickedOut).toHaveBeenCalledWith({
      kicker: {id: "someId"},
      kicked: {id: "someId", local: true}
    });
  });

  it('allows you to mute the jitsi microphone', (done) => {
    spyOn(jitsiProxy, 'getMeetingRoomToken').and.returnValue(Promise.resolve(fakeToken));
    spyOn(jitsiProxy, 'tradeTokenForCookie').and.returnValue(Promise.resolve(fakeValidator));
    spyOn(jitsiProxy, 'checkPinCode').and.returnValue(Promise.resolve(fakeToken));
    jitsiProxy.init();
    var promise = jitsiProxy.startVideo(
      '42',
      'personal'
    );
    $rootScope.$apply();
    promise.then(function () {
      jitsiProxy.muteMicrophoneInJitsi().then(() => {
        $rootScope.$apply();
        expect(jitsiProxy.jitsiApiObj.executeCommand).toHaveBeenCalledWith('toggleAudio');
        done();
      });
    });
  });


  it('allows you to mute the jitsi microphone in electron context', (done) => {
    spyOn(electronWrapper, 'muteJitsi');
    jitsiProxy.init();
    jitsiProxy.electronJitsiRunning = true;

    $rootScope.$apply();
    jitsiProxy.muteMicrophoneInJitsi().then(() => {
      $rootScope.$apply();
      expect(jitsiProxy.jitsiApiObj).toBeUndefined();
      expect(electronWrapper.muteJitsi).toHaveBeenCalled();
      done();
    });
  });

  it('doesnt do anything if trying to mute while already muted in jitsi', (done) => {
    spyOn(jitsiProxy, 'getMeetingRoomToken').and.returnValue(Promise.resolve(fakeToken));
    spyOn(jitsiProxy, 'tradeTokenForCookie').and.returnValue(Promise.resolve(fakeValidator));
    spyOn(jitsiProxy, 'checkPinCode').and.returnValue(Promise.resolve(fakeToken));
    jitsiProxy.init();
    var promise = jitsiProxy.startVideo(
      '42',
      'personal'
    );
    $rootScope.$apply();
    promise.then(function () {
      jitsiProxy.jitsiApiObj.isAudioMuted = () => {return Promise.resolve(true);};
      jitsiProxy.muteMicrophoneInJitsi().then(() => {
        $rootScope.$apply();
        expect(jitsiProxy.jitsiApiObj.executeCommand).not.toHaveBeenCalled();
        done();
      });
    });
  });
});
