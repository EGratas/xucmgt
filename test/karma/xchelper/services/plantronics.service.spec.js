describe('Service : plantronics', () => {
  'use strict';

  let plantronics;
  let $httpBackend;
  let $interval;
  let $rootScope;

  beforeEach(angular.mock.module('xcCti'));
  beforeEach(angular.mock.module('html-templates'));
  beforeEach(angular.mock.module('karma-backend'));
  beforeEach(angular.mock.module('ucAssistant'));
  beforeEach(angular.mock.module('xcHelper'));

  let initializeService = () => {
    expect(plantronics.getState()).toBe("Uninitialized");    
    // Initialize XucLink
    Cti.Topic(Cti.MessageType.LOGGEDON).publish("");
  
    $httpBackend.expectJSONP("http://127.0.0.1:32017/Spokes/DeviceServices/Info?callback=JSON_CALLBACK").respond(200, { Result : { Uid : 13579 } , isError : false});
    $httpBackend.flush();
    expect(plantronics.getState()).toBe("Paused");
    expect(plantronics.getUid()).toEqual(13579);
  };

  beforeEach(angular.mock.inject((_plantronics_, _$httpBackend_, _$interval_, _$rootScope_) => {
    plantronics = _plantronics_;
    $httpBackend = _$httpBackend_;
    $interval = _$interval_;
    $rootScope = _$rootScope_;
    spyOn(xc_webrtc, 'disableICE').and.callFake(() => {});
  }));

  let startPollingAndCleanupEvent = () => {
    expect(plantronics.getState()).toBe("Paused");
    $httpBackend.expectJSONP("http://127.0.0.1:32017/Spokes/DeviceServices/Attach?uid=13579&callback=JSON_CALLBACK").respond(200, {Result: 89911012});
    $httpBackend.expectJSONP("http://127.0.0.1:32017/Spokes/DeviceServices/Events?sess=89911012&queue=0&callback=JSON_CALLBACK").respond(200);
    plantronics.startPolling();
    $httpBackend.flush();
    expect(plantronics.getState()).toBe("Polling");
    expect(plantronics.getSessId()).toBe(89911012);
    expect(plantronics.getPollingInterval()).not.toBeUndefined();
  };

  const unsetService = () => {
    Cti.Topic(Cti.MessageType.LINKSTATUSUPDATE).publish({status: 'closed'});
    if(plantronics.getState() == "Polling"){
      $httpBackend.expectJSONP("http://127.0.0.1:32017/Spokes/DeviceServices/Release?sess=89911012&callback=JSON_CALLBACK").respond(200);
      $httpBackend.flush();
    }
    $httpBackend.verifyNoOutstandingExpectation();
    $httpBackend.verifyNoOutstandingRequest();
    $rootScope.$digest();
  };

  afterEach( () => {
    unsetService();
  });

  it('should be instanciated', () => {
    expect(plantronics).not.toBeUndefined();
  });

  it('can start polling', () => {
    initializeService();
    startPollingAndCleanupEvent();
    $httpBackend.expectJSONP("http://127.0.0.1:32017/Spokes/DeviceServices/Events?sess=89911012&queue=0&callback=JSON_CALLBACK").respond(200);
    $interval.flush(300);
    $httpBackend.flush();
    $httpBackend.expectJSONP("http://127.0.0.1:32017/Spokes/DeviceServices/Events?sess=89911012&queue=0&callback=JSON_CALLBACK").respond(200);
    $interval.flush(300);
    $httpBackend.flush();
  });

  it('can stop polling', () => {
    initializeService();
    startPollingAndCleanupEvent();
    $httpBackend.expectJSONP("http://127.0.0.1:32017/Spokes/DeviceServices/Events?sess=89911012&queue=0&callback=JSON_CALLBACK").respond(200);
    $interval.flush(300);
    $httpBackend.expectJSONP("http://127.0.0.1:32017/Spokes/DeviceServices/Release?sess=89911012&callback=JSON_CALLBACK").respond(200);
    plantronics.stopPolling();
    $httpBackend.flush();
    expect(plantronics.getState()).toBe("Paused");
    expect(plantronics.getPollingInterval()).toBeUndefined;
    expect(plantronics.getSessId()).toBeUndefined;
  });

  it('should fire main headphone event when talk is pressed', () => {
    initializeService();
    let handler = {
      callback: () => {}
    };

    spyOn(handler, 'callback');
    let spyee = handler.callback;

    plantronics.addHeadphoneEventHandler(handler.callback);

    startPollingAndCleanupEvent();
    $httpBackend.expectJSONP("http://127.0.0.1:32017/Spokes/DeviceServices/Events?sess=89911012&queue=0&callback=JSON_CALLBACK").respond(200, {isError : false, Result : [ {Event_Name : "Talk"}, {Event_Name : "DON"}, {Event_Name : "Unknown"} ] });
    $interval.flush(300);
    $httpBackend.flush();
    expect(spyee).toHaveBeenCalledWith('Main');
  });

  it('should notify when receiving any event (when register by addHeadphoneEventHandler)', () => {
    let eventTypes = {
      'HeadphoneEvent' : 'Main',
    };

    let handler = {
      callback: () => {}
    };

    let spyee = spyOn(handler, 'callback');

    plantronics.addHeadphoneEventHandler(handler.callback);

    angular.forEach(eventTypes, (eventSubType, eventType) => {
      $rootScope.$emit(eventType, eventSubType);
      expect(spyee).toHaveBeenCalledWith(eventSubType);
    });
  });

  it('should uninit when Xuclink is closed ', () => {
    initializeService();
    startPollingAndCleanupEvent();
    expect(plantronics.getState()).toBe("Polling");
        
    //close XucLink
    Cti.Topic(Cti.MessageType.LINKSTATUSUPDATE).publish({status: 'closed'});
    $httpBackend.expectJSONP("http://127.0.0.1:32017/Spokes/DeviceServices/Release?sess=89911012&callback=JSON_CALLBACK").respond(200);
    $httpBackend.flush();
    $rootScope.$digest();
    expect(plantronics.getState()).toBe("Uninitialized");
    expect(plantronics.getPollingInterval()).toBeUndefined();
    expect(plantronics.getUid()).toBeUndefined;
    expect(plantronics.getSessId()).toBeUndefined;
  });

  it('should be able to restart when XucLink is open', () => {

    //close XucLink
    Cti.Topic(Cti.MessageType.LINKSTATUSUPDATE).publish({status: 'closed'});
    $rootScope.$digest();
    expect(plantronics.getState()).toBe("Uninitialized");
    expect(plantronics.getPollingInterval()).toBeUndefined();
    expect(plantronics.getUid()).toBeUndefined;
    expect(plantronics.getSessId()).toBeUndefined;

    //Service can be restarted with XucLink
    initializeService();
  });

  it('should retry the service initialization with more time after each try with a maximum treshold', () => {
    jasmine.clock().install();
    let xucLinkLogoutHandler = spyOn(plantronics, "registerXucLinkLogoutHandler").and.callThrough();
    let plantronicsBaseTimeoutSeconds = 2;

    xucLinkLogoutHandler.calls.reset();
    expect(plantronics.getState()).toBe("Uninitialized");
    Cti.Topic(Cti.MessageType.LOGGEDON).publish("");
    spyOn(plantronics, "stopPolling").and.returnValue(Promise.resolve());
    expect(plantronics.retryTimeoutSeconds).toEqual(plantronicsBaseTimeoutSeconds);

    while(plantronics.retryTimeoutSeconds <= plantronics.retryTimeoutMax) {
      $httpBackend.expectJSONP("http://127.0.0.1:32017/Spokes/DeviceServices/Info?callback=JSON_CALLBACK").respond(404);
      jasmine.clock().tick(plantronics.retryTimeoutSeconds * 1001);
      plantronicsBaseTimeoutSeconds *= 2;
      $httpBackend.flush();
      $rootScope.$digest();
      expect(plantronics.getState()).toBe("Uninitialized");
      expect(plantronics.retryTimeoutSeconds).toEqual(plantronicsBaseTimeoutSeconds);
    }

    // Checks if the timeout seconds stops after it reached max time
    jasmine.clock().tick(plantronics.retryTimeoutSeconds * 1001);
    $httpBackend.verifyNoOutstandingExpectation();
    $httpBackend.verifyNoOutstandingRequest();
    expect(plantronics.getState()).toBe("Uninitialized");
    jasmine.clock().uninstall();
  });

  it('should only call init once on various logout and login events', () => {
    let initSpy = spyOn(plantronics, "init").and.callThrough();
    let uninitSpy = spyOn(plantronics, "uninit").and.callThrough();
    expect(plantronics.getState()).toBe("Uninitialized");
    initSpy.calls.reset();
    uninitSpy.calls.reset();

    $httpBackend.expectJSONP("http://127.0.0.1:32017/Spokes/DeviceServices/Info?callback=JSON_CALLBACK").respond(200, { Result : { Uid : 13579 } , isError : false});
    
    Cti.Topic(Cti.MessageType.LOGGEDON).publish("");
    $httpBackend.flush();
    $rootScope.$digest();
    expect(plantronics.getState()).toBe("Paused");
    expect(initSpy).toHaveBeenCalledTimes(1);
    
    unsetService();
    expect(uninitSpy).toHaveBeenCalledTimes(1);

    initSpy.calls.reset();
    uninitSpy.calls.reset();

    $httpBackend.expectJSONP("http://127.0.0.1:32017/Spokes/DeviceServices/Info?callback=JSON_CALLBACK").respond(200, { Result : { Uid : 13579 } , isError : false});

    Cti.Topic(Cti.MessageType.LOGGEDON).publish("");
    $httpBackend.flush();
    $rootScope.$digest();
    expect(plantronics.getState()).toBe("Paused");
    expect(initSpy).toHaveBeenCalledTimes(1);

    Cti.Topic(Cti.MessageType.LINKSTATUSUPDATE).publish({status: 'closed'});
    $rootScope.$digest();
    expect(uninitSpy).toHaveBeenCalledTimes(1);
  });
});