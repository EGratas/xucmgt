'use strict';

describe('Service: drag and drop event handling', function () {

  var dragDrop, fakeData, fakeEvent;
  beforeEach(angular.mock.module('html-templates'));
  beforeEach(angular.mock.module('karma-backend'));
  beforeEach(angular.mock.module('xcHelper'));
  beforeEach(angular.mock.module('xcCti'));

  beforeEach(angular.mock.inject(function(_dragDrop_) {
    dragDrop = _dragDrop_;

    fakeData = {aId: 1, bId:'test', cId: true};

    fakeEvent = {
      dataTransfer: new MockDataTransfer(),
      preventDefault: () => {},
      stopPropagation: () => {}
    };
  }));

  it('should un/serialize dataTransfer DOM event in a cross-compatible browser way', function() {
    dragDrop.setDataTransfer(fakeEvent, fakeData);
    expect(dragDrop.getDataTransfer(fakeEvent)).toEqual(fakeData);
  });

  it('should handle dragStart event', function() {
    dragDrop.onDragStartEvent(fakeEvent);
    expect(fakeEvent.dataTransfer.effectAllowed).toBe('move');
  });

  it('should handle drop event', function() {
    spyOn(fakeEvent, 'preventDefault');
    spyOn(fakeEvent, 'stopPropagation');
    dragDrop.onDropEvent(fakeEvent);
    expect(fakeEvent.preventDefault).toHaveBeenCalled();
    expect(fakeEvent.stopPropagation).toHaveBeenCalled();
  });

});
