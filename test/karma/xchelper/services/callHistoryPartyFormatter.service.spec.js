describe('Service: Reshape JSON for better display', function () {

  var callHistoryPartyFormatter;
  beforeEach(angular.mock.module('xcHelper'));
  beforeEach(angular.mock.module('xcCti'));

  beforeEach(angular.mock.inject(function (_callHistoryPartyFormatter_) {
    callHistoryPartyFormatter = _callHistoryPartyFormatter_;
  }));

  let createCall = function (firstname1, lastname1, phone1, firstname2, lastname2, phone2, start, duration, status) {
    let call = {
      "dstFirstName": firstname1,
      "dstLastName": lastname1,
      "dstNum": phone1,
      "duration": duration,
      "firstName": firstname1,
      "lastName": lastname1,
      "number": phone1,
      "srcFirstName": firstname2,
      "srcLastName": lastname2,
      "srcNum": phone2,
      "start": start,
      "status": status
    };
    return call;
  };

  let calls = [
    createCall('USER A', 'A', '1020', 'YOU', 'YOU', '1000', '12:04:55', '00:32', 'emitted'),
    createCall('USER A', 'A', '1020', 'YOU', 'YOU', '1000', '12:06:30', '00:11', 'emitted'),
    createCall('YOU', 'YOU', '1000', 'USER B', 'B', '1030', '12:20:22', '00:39', 'answered'),
    createCall('YOU', 'YOU', '1000', 'USER B', 'B', '1030', '12:36:01', '01:29', 'missed')
  ];

  let days = [
    {
      day: "2019-07-30T22:00:00.000Z",
      details: calls
    }
  ];


  let createExtractedCall = function (targetFirstName, targetLastName, targetNumber, start, duration, status, display) {
    let extractedCall = {
      "targetFirstName": targetFirstName,
      "targetLastName": targetLastName,
      "targetNumber": targetNumber,
      "duration": duration,
      "start": start,
      "status": status,
      "display": display
    };
    return extractedCall;
  };

  let newCalls = [
    createExtractedCall('USER A', 'A', '1020', '12:04:55', '00:32', 'emitted', false),
    createExtractedCall('USER A', 'A', '1020', '12:06:30', '00:11', 'emitted', false),
    createExtractedCall('USER B', 'B', '1030', '12:20:22', '00:39', 'answered', false),
    createExtractedCall('USER B', 'B', '1030', '12:36:01', '01:29', 'missed', false)
  ];

  let createPartyCall = function (targetFirstName, targetLastName, targetNumber, childCount, callDetailList) {
    let partyCall = {
      "targetFirstName": targetFirstName,
      "targetLastName": targetLastName,
      "targetNumber": targetNumber,
      "childCount": childCount,
      "callDetailList": callDetailList
    };
    return partyCall;
  };

  let createPartyCallDetailList = function (start1, duration1, status1, start2, duration2, status2) {
    let partyCallDetails = [
      {
        "duration": duration1,
        "start": start1,
        "status": status1
      },
      {
        "duration": duration2,
        "start": start2,
        "status": status2
      }
    ];
    return partyCallDetails;
  };

  let partyCallDetailsA = createPartyCallDetailList('12:04:55', '00:32', 'emitted', '12:06:30', '00:11', 'emitted');
  let partyCallDetailsB = createPartyCallDetailList('12:20:22', '00:39', 'answered', '12:36:01', '01:29', 'missed');
  let partyCalls = [
    createPartyCall('USER A', 'A', '1020', 2, partyCallDetailsA),
    createPartyCall('USER B', 'B', '1030', 2, partyCallDetailsB)
  ];

  let partyCallsPerDay = [
    {
      day: "2019-07-30T22:00:00.000Z",
      details: partyCalls
    }
  ];


  let newCallsResult = [{
    "targetFirstName": "USER A",
    "targetLastName": "A",
    "targetNumber": "1020",
    "childCount": 2,
    "callDetailList": [
      {
        "duration": "00:32",
        "start": "12:04:55",
        "status": "emitted"
      },
      {
        "duration": "00:11",
        "start": "12:06:30",
        "status": "emitted"
      }
    ]
  },
  {
    "targetFirstName": "USER B",
    "targetLastName": "B",
    "targetNumber": "1030",
    "childCount": 2,
    "callDetailList": [
      {
        "duration": "00:39",
        "start": "12:20:22",
        "status": "answered"
      },
      {
        "duration": "01:29",
        "start": "12:36:01",
        "status": "missed"
      }
    ]
  }
  ];

  let newChildrensParentsJson = function (targetFirstName, targetLastName, targetNumber, start, duration, status) {
    let myNewChildrensParentsJson = {
      "targetFirstName": targetFirstName,
      "targetLastName": targetLastName,
      "targetNumber": targetNumber,
      "callsList": [
        {
          "duration": duration,
          "start": start,
          "status": status
        }
      ]
    };
    return myNewChildrensParentsJson;
  };

  let newChildrensParentsCallArray = [];
  newChildrensParentsCallArray.push(newChildrensParentsJson('USER A', 'A', '1020', '12:04:55', '00:32', 'emitted'));
  newChildrensParentsCallArray.push(newChildrensParentsJson('USER A', 'A', '1020', '12:06:30', '00:11', 'emitted'));
  newChildrensParentsCallArray.push(newChildrensParentsJson('USER B', 'B', '1030', '12:20:22', '00:39', 'answered'));
  newChildrensParentsCallArray.push(newChildrensParentsJson('USER B', 'B', '1030', '12:36:01', '01:29', 'emitted'));

  it('should refactor initial json to an easier json', function () {
    let myOriginalCallArray = calls;
    let myNewCalls = newCalls;

    let extractedCalls = callHistoryPartyFormatter.extractCalls(myOriginalCallArray, '1000');
    expect(extractedCalls).toEqual(myNewCalls);
  });

  it('groups calls per target number', function () {
    let result = callHistoryPartyFormatter.groupCallsByParty(newCalls);
    expect(result).toEqual(newCallsResult);
  });

  it('should return an empty array if the parameter is empty', function () {
    let emptyCalls = [];
    let result = callHistoryPartyFormatter.groupCallsByParty(emptyCalls);
    expect(result).toEqual(emptyCalls);
  });

  it('should convert calls per day into party calls per day', function () {
    let callsPerDay = days;
    let result = callHistoryPartyFormatter.toPartyCallsPerDay(callsPerDay);
    expect(result).toEqual(partyCallsPerDay);
  });

});