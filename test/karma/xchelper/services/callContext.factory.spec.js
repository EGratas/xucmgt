describe('callContext', function() {
  var XucPhoneState;
  var XucUtils;
  var CtiProxy;
  var callContext;
  var vars = {a: 21};

  beforeEach(angular.mock.module('xcHelper'));
  beforeEach(angular.mock.module('xcCti'));

  beforeEach(angular.mock.inject(function(_callContext_, _XucPhoneState_, _XucUtils_, _CtiProxy_) {
    callContext = _callContext_;
    XucPhoneState = _XucPhoneState_;
    XucUtils = _XucUtils_;
    CtiProxy = _CtiProxy_;
    spyOn(XucPhoneState, 'getCalls').and.returnValue([]);
    spyOn(CtiProxy, 'dial');
  }));

  it('propagates parameters on dial request', function(){
    callContext.dialOrAttTrans('123', vars);
    expect(CtiProxy.dial).toHaveBeenCalledWith('123', vars);
  });

  it('propagates parameters on dial request with fallback on {} for vars', function(){
    callContext.dialOrAttTrans('123');
    expect(CtiProxy.dial).toHaveBeenCalledWith('123', {});
  });

  it('propagates parameters on normalized dial request', function(){
    spyOn(XucUtils, 'normalizePhoneNb').and.returnValue('456');
    callContext.normalizeDialOrAttTrans('123', vars);
    expect(CtiProxy.dial).toHaveBeenCalledWith('456', vars);
  });

  it('propagates parameters on normalized dial request with fallback on {} for vars', function(){
    spyOn(XucUtils, 'normalizePhoneNb').and.returnValue('456');
    callContext.normalizeDialOrAttTrans('123');
    expect(CtiProxy.dial).toHaveBeenCalledWith('456', {});
  });

  it('returns true if one of the contact is a meeting room', () => {
    const contact1 = {
      "entry": ["Annabelle Beaumont","568778","456","video:dev daily",false,""],
      "source": "xivo_meetingroom",
      "personal": false,
      "url": "/invitation/video?uuid=44f3c883-7db8-404a-a5aa-215724ee417f&token=eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpc3MiOiJ4aXZvIiwicm9vbSI6InRlc3QgY29uZiByb29tIiwic3ViIjoiKiIsImF1ZCI6Inhpdm8iLCJuYW1lIjoidGVzdCBjb25mIHJvb20iLCJ1dWlkIjoiNDRmM2M4ODMtN2RiOC00MDRhLWE1YWEtMjE1NzI0ZWU0MTdmIn0.0G9OTqi_deksQuinry-83ommkSLLtyV6-KmXxDwZAAE"
    };
    
    expect(callContext.isMeetingRoom(contact1)).toBe(true);

    const contact2 = {
      "entry": ["Marion Dubois","568778","456","",false,"4587"],
      "source": "internal",
      "personal": false,
      "url": ""
    };

    expect(callContext.isMeetingRoom(contact2)).toBe(false);    
  });
});
