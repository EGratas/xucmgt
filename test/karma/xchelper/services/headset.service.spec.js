describe('Service : headset', () => {
  'use strict';

  let headset;
  let $httpBackend;
  let $rootScope;
  let plantronics;
  let CtiProxy;
  let XucPhoneState;

  beforeEach(angular.mock.module('xcCti'));
  beforeEach(angular.mock.module('html-templates'));
  beforeEach(angular.mock.module('karma-backend'));
  beforeEach(angular.mock.module('ucAssistant'));
  beforeEach(angular.mock.module('xcHelper'));

  beforeEach(angular.mock.inject((_headset_, _$httpBackend_, _XucPhoneState_, _$rootScope_, _plantronics_, _CtiProxy_) => {
    headset = _headset_;
    $httpBackend = _$httpBackend_;
    XucPhoneState = _XucPhoneState_;
    $rootScope = _$rootScope_;
    plantronics = _plantronics_;
    CtiProxy = _CtiProxy_;
    
    spyOn(plantronics, 'addHeadphoneEventHandler');
    spyOn(XucPhoneState, 'addStateHandler');
    spyOn(xc_webrtc, 'disableICE').and.callFake(() => {});
    // Initialize XucLink
    Cti.Topic(Cti.MessageType.LOGGEDON).publish("");
    $httpBackend.expectJSONP("http://127.0.0.1:32017/Spokes/DeviceServices/Info?callback=JSON_CALLBACK").respond(200, { Result : { Uid : 13579 } , isError : false});
    $httpBackend.flush();
    expect(plantronics.getState()).toBe("Paused");
    $rootScope.$digest();
    expect(plantronics.addHeadphoneEventHandler).toHaveBeenCalledWith(headset.onKeyPress);
    expect(XucPhoneState.addStateHandler).toHaveBeenCalledWith(headset.onPhoneState);
    spyOn(plantronics, 'startPolling');
    spyOn(plantronics, 'stopPolling');
  }));

  it('can be instanciated', () => {
    expect(headset).not.toBeUndefined();
  });

  it('can answer', () => {
    headset.onPhoneState('Ringing');
    expect(plantronics.startPolling).toHaveBeenCalled();
    spyOn(CtiProxy, 'answer');
    headset.onKeyPress('Main');
    expect(CtiProxy.answer).toHaveBeenCalled();
  });

  it('can hangup while dialing', () => {
    headset.onPhoneState('Dialing');
    expect(plantronics.startPolling).toHaveBeenCalled();
    spyOn(CtiProxy, 'hangup');
    headset.onKeyPress('Main');
    expect(CtiProxy.hangup).toHaveBeenCalled();
  });

  it('can hangup while established', () => {
    headset.onPhoneState('Established');
    expect(plantronics.startPolling).toHaveBeenCalled();
    spyOn(CtiProxy, 'hangup');
    headset.onKeyPress('Main');
    expect(CtiProxy.hangup).toHaveBeenCalled();
  });

  it('can retrieve calls on hold', () => {
    headset.onPhoneState('OnHold');
    expect(plantronics.startPolling).toHaveBeenCalled();
    spyOn(CtiProxy, 'hold');
    headset.onKeyPress('Main');
    expect(CtiProxy.hold).toHaveBeenCalled();
  });

  it('can start and stop polling', () => {
    headset.onPhoneState('Ringing');
    expect(plantronics.startPolling).toHaveBeenCalled();
    headset.onPhoneState('Available');
    expect(plantronics.stopPolling).toHaveBeenCalled();
  });

});