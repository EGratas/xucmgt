describe('mobileAppCompatibility', function() {
  var $rootScope;
  var CtiProxy;
  var $uibModal;
  var $q;
  var mockModalInstance;
  var mobileAppCompatibility;

  beforeEach(angular.mock.module('html-templates'));
  beforeEach(angular.mock.module('karma-backend'));
  beforeEach(angular.mock.module('xcHelper'));
  beforeEach(angular.mock.module('xcCti'));

  beforeEach(angular.mock.inject(function(_$rootScope_, _CtiProxy_, _$uibModal_, _$q_, _MobileAppCompatibilityService_) {
    CtiProxy = _CtiProxy_;
    $rootScope = _$rootScope_;
    $uibModal = _$uibModal_;
    mobileAppCompatibility = _MobileAppCompatibilityService_;
    $q = _$q_;

    mockModalInstance = {
      result: $q.resolve({}),
      opened: $q.resolve({}),
      closed: $q.resolve({})
    };

  }));


  it('Open modal when mobile application is not compatible with user ua line config', angular.mock.inject(function(){
    spyOn($uibModal, 'open').and.returnValue(mockModalInstance);
    mobileAppCompatibility.init();
    spyOn(CtiProxy, "isUsingMobileApp").and.returnValue(true);
    spyOn(CtiProxy, "isUsingUa").and.returnValue(true);
    spyOn(CtiProxy, "isUsingCti").and.returnValue(false);
    spyOn(CtiProxy, "unregisterMobileApp");
    $rootScope.$broadcast(CtiProxy.LINECONFIG_PROCESSED);
    $rootScope.$digest();

    expect($uibModal.open).toHaveBeenCalled();
    expect(CtiProxy.unregisterMobileApp).toHaveBeenCalled();
  }));

  it('Open modal when mobile application is not compatible with user phone line config', angular.mock.inject(function(){
    spyOn($uibModal, 'open').and.returnValue(mockModalInstance);
    mobileAppCompatibility.init();
    spyOn(CtiProxy, "isUsingMobileApp").and.returnValue(true);
    spyOn(CtiProxy, "isUsingUa").and.returnValue(false);
    spyOn(CtiProxy, "isUsingCti").and.returnValue(true);
    spyOn(CtiProxy, "unregisterMobileApp");
    $rootScope.$broadcast(CtiProxy.LINECONFIG_PROCESSED);
    $rootScope.$digest();
    expect($uibModal.open).toHaveBeenCalled();
    expect(CtiProxy.unregisterMobileApp).toHaveBeenCalled();
  }));

  it("doesn't show modal when mobile application is compatible with user line config", angular.mock.inject(function(){
    spyOn($uibModal, 'open').and.returnValue(mockModalInstance);
    mobileAppCompatibility.init();
    spyOn(CtiProxy, "isUsingMobileApp").and.returnValue(true);
    spyOn(CtiProxy, "isUsingUa").and.returnValue(false);
    spyOn(CtiProxy, "isUsingCti").and.returnValue(false);
    spyOn(CtiProxy, "unregisterMobileApp");
    $rootScope.$broadcast(CtiProxy.LINECONFIG_PROCESSED);
    $rootScope.$digest();
    expect($uibModal.open).not.toHaveBeenCalled();
    expect(CtiProxy.unregisterMobileApp).not.toHaveBeenCalled();
  }));

  it("doesn't show modal when there is no mobile application in user line config", angular.mock.inject(function(){
    spyOn($uibModal, 'open').and.returnValue(mockModalInstance);
    mobileAppCompatibility.init();
    spyOn(CtiProxy, "isUsingMobileApp").and.returnValue(false);
    spyOn(CtiProxy, "isUsingUa").and.returnValue(false);
    spyOn(CtiProxy, "isUsingCti").and.returnValue(true);
    spyOn(CtiProxy, "unregisterMobileApp");
    $rootScope.$broadcast(CtiProxy.LINECONFIG_PROCESSED);
    $rootScope.$digest();
    expect($uibModal.open).not.toHaveBeenCalled();
    expect(CtiProxy.unregisterMobileApp).not.toHaveBeenCalled();
  }));

});
