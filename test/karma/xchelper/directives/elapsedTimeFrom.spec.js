import angular from 'angular';
import moment from 'moment';
import 'xchelper/helper.module';



describe('elapsedTimeFrom directive', () => {
  var $compile;
  var $rootScope;

  beforeEach(angular.mock.module('html-templates'));
  beforeEach(angular.mock.module('karma-backend'));
  beforeEach(angular.mock.module('xcHelper'));
  beforeEach(angular.mock.module('xcCti'));

  beforeEach(angular.mock.inject((_$compile_, _$rootScope_) =>{
    $compile = _$compile_;
    $rootScope = _$rootScope_;
  }));
  
  it('should render elapsed time from a timestamp', () => {
    var scope = $rootScope.$new();
    scope.myTime = moment().subtract(90, 'minutes').toDate();
    var element = $compile('<span elapsed-time-from="myTime"></a-great-eye>')(scope);
    
    $rootScope.$digest();
    expect(element.html()).toContain("1:30:00");
  });

  it('should render "-:--:--" if start time is not defined', () => {
    var scope = $rootScope.$new();
    var element = $compile('<span elapsed-time-from="myTime"></a-great-eye>')(scope);
    $rootScope.$digest();
    expect(element.html()).toContain("-:--:--");
  });
});
