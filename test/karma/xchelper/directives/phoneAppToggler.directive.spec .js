describe('phone app toggler video directive', function() {
  
  var $rootScope, $compile;

  beforeEach(angular.mock.module('xcCti', 'xcHelper', 'karma-backend', 'html-templates', 'Agent' ));

  beforeEach(function () {
    jasmine.addMatchers({
      toEqualData : customMatchers.toEqualData
    });
    
    angular.mock.inject(function(_$rootScope_, _$compile_) {
      $rootScope = _$rootScope_;
      $compile = _$compile_;
    });
  });

  function jitsiVideoDirectiveBind() {
    let newScope = $rootScope.$new();
    let elementStr = angular.element('<phone-app-toggler></phone-app-toggler>');
    let element = $compile(elementStr)(newScope);
    newScope.$digest();

    let innerScope = element.children().scope();
    return {element, innerScope};

  }
  
  it('can instantiate directive', function() {
    let {element} = jitsiVideoDirectiveBind();
    $rootScope.$digest();
    expect(element).not.toBeUndefined();
  });

  it('should show the button to extend the video when starting a video call, and hide it when finishing the call', function() {
    let {innerScope} = jitsiVideoDirectiveBind();
    innerScope.showExtendVideoButton = false;
    
    $rootScope.$broadcast('EventStartVideo');    
    expect(innerScope.showExtendVideoButton).toEqual(true);

    $rootScope.$broadcast('EventCloseVideo');
    expect(innerScope.showExtendVideoButton).toEqual(false);    
  });
});
