describe('meetingrooms invite displayer directive', function() {
  
  var $rootScope, $compile;
  
  beforeEach(angular.mock.module('xcCti', 'xcHelper', 'karma-backend', 'html-templates', 'Agent' ));
  
  beforeEach(function () {
    jasmine.addMatchers({
      toEqualData : customMatchers.toEqualData
    });
      
    angular.mock.inject(function(_$rootScope_, _$compile_) {
      $rootScope = _$rootScope_;
      $compile = _$compile_;
    });
  });
  
  function displayerDirectiveBind() {
    let newScope = $rootScope.$new();
    let elementStr = angular.element('<jitsi-video></jitsi-video>');
    let element = $compile(elementStr)(newScope);
    newScope.$digest();
  
    let innerScope = element.children().scope();
    return {element, innerScope};
  
  }
    
  it('can instantiate directive', function() {
    let {element} = displayerDirectiveBind();
    $rootScope.$digest();
    expect(element).not.toBeUndefined();
  });
});
  