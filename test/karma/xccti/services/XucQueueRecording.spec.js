import 'xccti/cti-webpack';

describe('Factory: Xuc Queue Recording', function () {
  var $q;
  var $rootScope;
  var $httpBackend;
  var XucQueueRecording;
  var XucLink;
  var XucQueue;
  var token = "aaaa-bbbb-cccc-dddd-1234";
  var user = {username: "jbond", phoneNumber: "1001", token: token};
  var profile = {rights: ['recording']};
  var checkAuthHeader;

  beforeEach(angular.mock.module('html-templates'));
  beforeEach(angular.mock.module('karma-backend'));
  beforeEach(angular.mock.module('xcCti'));

  beforeEach(function() {
    jasmine.addMatchers({
      toEqualData : customMatchers.toEqualData
    });
  });

  beforeEach(angular.mock.inject(function(_XucQueueRecording_, _XucLink_, _XucQueue_, _$q_, _$rootScope_, _$httpBackend_) {
    XucQueueRecording = _XucQueueRecording_;
    XucLink = _XucLink_;
    XucQueue = _XucQueue_;
    $q = _$q_;
    $rootScope = _$rootScope_;
    $httpBackend = _$httpBackend_;

    spyOn(XucLink, 'whenLogged').and.returnValue($q.resolve(user));
    spyOn(XucLink, 'getProfileRightAsync').and.returnValue($q.resolve(profile));
    spyOn(XucLink, 'getServerUrl').and.returnValue("http://localhost");

    checkAuthHeader = (headers) => {
      return headers['Authorization'] === 'Bearer ' + token;
    };
  }));

  afterEach(function() {
    $httpBackend.verifyNoOutstandingExpectation();
    $httpBackend.verifyNoOutstandingRequest();
  });

  it('should get recording status when all queues are recorded and activated', function() {
    var queues = $q.defer();
    queues.resolve([{
      'id' : 1,
      'number': '3000',
      'displayName': 'My queue',
      'recording_activated': 1,
      'recording_mode': 'recorded'
    }]);

    spyOn(XucQueue, "getQueuesAsync").and.callFake(() => {
      return queues.promise;
    });

    let promise = XucQueueRecording.getState();
    $rootScope.$digest();

    promise.then((result) => {
      expect(result).toEqual(XucQueueRecording.ALL_RECORDED);
    });
  });

  it('should get recording status when all queues not recorded and activated', function() {
    var queues = $q.defer();
    queues.resolve([{
      'id' : 1,
      'number': '3000',
      'displayName': 'My queue',
      'recording_activated': 0,
      'recording_mode': 'recorded'
    }]);

    spyOn(XucQueue, "getQueuesAsync").and.callFake(() => {
      return queues.promise;
    });

    let promise = XucQueueRecording.getState();
    $rootScope.$digest();

    promise.then((result) => {
      expect(result).toEqual(XucQueueRecording.NONE_RECORDED);
    });
  });

  it('should get recording status when mixed queues not/recorded and activated', function() {
    var queues = $q.defer();
    queues.resolve([{
      'id' : 1,
      'number': '3000',
      'displayName': 'My queue',
      'recording_activated': 0,
      'recording_mode': 'recorded'
    },
    {
      'id' : 2,
      'number': '3001',
      'displayName': 'My other queue',
      'recording_activated': 1,
      'recording_mode': 'recorded'
    }
    ]);
    spyOn(XucQueue, "getQueuesAsync").and.callFake(() => {
      return queues.promise;
    });

    let promise = XucQueueRecording.getState();
    $rootScope.$digest();

    promise.then((result) => {
      expect(result).toEqual(XucQueueRecording.SOME_RECORDED);
    });
  });


  it('should get failure if no recording queue', function() {
    var queues = $q.defer();
    queues.resolve([{
      'id' : 1,
      'number': '3000',
      'displayName': 'My queue',
      'recording_activated': 0,
      'recording_mode': 'notrecorded'
    },
    {
      'id' : 2,
      'number': '3001',
      'displayName': 'My other queue',
      'recording_activated': 1,
      'recording_mode': 'notrecorded'
    }
    ]);
    spyOn(XucQueue, "getQueuesAsync").and.callFake(() => {
      return queues.promise;
    });

    let promise = XucQueueRecording.getState();
    promise.catch(error => { expect(error).toEqual("NO_RECORDING_QUEUE"); });
  });

  it('should update recording status with success', function() {
    const param = "on";
    const state = {'all': true};

    $httpBackend.expectPOST('http://localhost/xuc/api/2.0/config/recording/status/'+param, {}, checkAuthHeader).respond(() => {
      return [200, state];
    });
    let promise = XucQueueRecording.setState(param);
    $httpBackend.flush();

    promise.then(() => {
      expect(XucQueueRecording.hasConfigManagerError()).toBe(false);
    });
  });

  it('should update recording status having failure', function() {
    const param = "off";
    const state = {'all': false};

    $httpBackend.expectPOST('http://localhost/xuc/api/2.0/config/recording/status/'+param, {}, checkAuthHeader).respond(() => {
      return [500, state];
    });
    let promise = XucQueueRecording.setState(param);
    $httpBackend.flush();

    promise.then(() => {
      expect(XucQueueRecording.hasConfigManagerError()).toBe(true);
    });
  });

  it('should return value "all" if recording_mode set and activated in QueueConfig event', function() {
    var queues = $q.defer();
    queues.resolve([]);
    spyOn(XucQueue, "getQueuesAsync").and.callFake(() => {
      return queues.promise;
    });

    var queueConfigUpdate = {'id':1, 'name' : 'Hello', 'recording_activated': 1, 'recording_mode': 'recorded'};
    spyOn($rootScope, '$broadcast');

    XucQueueRecording.processQueueConfig(queueConfigUpdate);
    $rootScope.$digest();
    expect($rootScope.$broadcast).toHaveBeenCalledWith('RecordingStateUpdated', "all");
  });

  it('should return value "all" if recording_mode and set and activated in QueueConfig event with existing queues', function() {
    var queuesData = [{'id':1, 'name' : 'Bonjour', 'recording_activated': 1, 'recording_mode': 'recorded'}];
    var queues = $q.defer([]);
    queues.resolve(queuesData);
    spyOn(XucQueue, "getQueuesAsync").and.callFake(() => {
      return queues.promise;
    });

    var queueConfigUpdate = {'id':2, 'name' : 'Hello', 'recording_activated': 1, 'recording_mode': 'recorded'};
    spyOn($rootScope, '$broadcast');

    XucQueueRecording.processQueueConfig(queueConfigUpdate);
    $rootScope.$digest();
    expect($rootScope.$broadcast).toHaveBeenCalledWith('RecordingStateUpdated', "all");
  });

  it('should return value "none" if recording_mode set but not activated in QueueConfig event', function() {
    var queues = $q.defer();
    queues.resolve([]);
    spyOn(XucQueue, "getQueuesAsync").and.callFake(() => {
      return queues.promise;
    });

    var queueConfigUpdate = {'id':1, 'name' : 'Hello', 'recording_activated': 0, 'recording_mode': 'recorded'};
    spyOn($rootScope, '$broadcast');

    XucQueueRecording.processQueueConfig(queueConfigUpdate);
    $rootScope.$digest();
    expect($rootScope.$broadcast).toHaveBeenCalledWith('RecordingStateUpdated', "none");
  });

  it('should return value "none" if recording_mode set but not activated in QueueConfig event with existing queues', function() {
    var queuesData = [{'id':1, 'name' : 'Bonjour', 'recording_activated': 0, 'recording_mode': 'recorded'}];
    var queues = $q.defer();
    queues.resolve(queuesData);
    spyOn(XucQueue, "getQueuesAsync").and.callFake(() => {
      return queues.promise;
    });

    var queueConfigUpdate = {'id':2, 'name' : 'Hello', 'recording_activated': 0, 'recording_mode': 'recorded'};
    spyOn($rootScope, '$broadcast');

    XucQueueRecording.processQueueConfig(queueConfigUpdate);
    $rootScope.$digest();
    expect($rootScope.$broadcast).toHaveBeenCalledWith('RecordingStateUpdated', "none");
  });

  it('should return value "all" if queue is notrecorded and not activated in QueueConfig event', function() {
    var queuesData = [{'id':1, 'name' : 'Bonjour', 'recording_activated': 1, 'recording_mode': 'recorded'},
      {'id':2, 'name' : 'Bonjour', 'recording_activated': 1, 'recording_mode': 'recorded'}];
    var queues = $q.defer();
    queues.resolve(queuesData);
    spyOn(XucQueue, "getQueuesAsync").and.callFake(() => {
      return queues.promise;
    });

    var queueConfigUpdate = {'id':3, 'name' : 'Hello', 'recording_activated': 0, 'recording_mode': 'notrecorded'};
    spyOn($rootScope, '$broadcast');

    XucQueueRecording.processQueueConfig(queueConfigUpdate);
    $rootScope.$digest();
    expect($rootScope.$broadcast).toHaveBeenCalledWith('RecordingStateUpdated',"all");
  });

  it('should return value "some" if queue is recorded but not activated in QueueConfig event with existing queues', function() {
    var queuesData = [{'id':1, 'name' : 'Bonjour', 'recording_activated': 1, 'recording_mode': 'recorded'},
      {'id':2, 'name' : 'Bonjour', 'recording_activated': 1, 'recording_mode': 'recorded'},
      {'id':3, 'name' : 'Hello', 'recording_activated': 0, 'recording_mode': 'recorded'}];
    var queues = $q.defer();
    queues.resolve(queuesData);
    spyOn(XucQueue, "getQueuesAsync").and.callFake(() => {
      return queues.promise;
    });

    var queueConfigUpdate = {'id':3, 'name' : 'Hello', 'recording_activated': 0, 'recording_mode': 'recorded'};
    spyOn($rootScope, '$broadcast');

    XucQueueRecording.processQueueConfig(queueConfigUpdate);
    $rootScope.$digest();
    expect($rootScope.$broadcast).toHaveBeenCalledWith('RecordingStateUpdated', "some");
  });

  it('should return value "all" if queue is recorded and activated in QueueConfig event with existing queues', function() {
    var queuesData = [{'id':1, 'name' : 'Bonjour', 'recording_activated': 1, 'recording_mode': 'recorded'},
      {'id':2, 'name' : 'Bonjour', 'recording_activated': 1, 'recording_mode': 'recorded'},
      {'id':3, 'name' : 'Hello', 'recording_activated': 0, 'recording_mode': 'not_recorded'}];
    var queues = $q.defer();
    queues.resolve(queuesData);
    spyOn(XucQueue, "getQueuesAsync").and.callFake(() => {
      return queues.promise;
    });

    var queueConfigUpdate = {'id':3, 'name' : 'Hello', 'recording_activated': 1, 'recording_mode': 'recorded'};
    spyOn($rootScope, '$broadcast');

    XucQueueRecording.processQueueConfig(queueConfigUpdate);
    $rootScope.$digest();
    expect($rootScope.$broadcast).toHaveBeenCalledWith('RecordingStateUpdated', "all");
  });

  it('should return value "some" true if one existing queue is recorded but not activated', function() {
    var queuesData = [{'id':1, 'name' : 'Bonjour', 'recording_activated': 1, 'recording_mode': 'recorded'},
      {'id':2, 'name' : 'Bonjour', 'recording_activated': 0, 'recording_mode': 'recorded'}];
    var queues = $q.defer();
    queues.resolve(queuesData);
    spyOn(XucQueue, "getQueuesAsync").and.callFake(() => {
      return queues.promise;
    });

    var queueConfigUpdate = {'id':3, 'name' : 'Hello', 'recording_activated': 1, 'recording_mode': 'recorded'};
    spyOn($rootScope, '$broadcast');

    XucQueueRecording.processQueueConfig(queueConfigUpdate);
    $rootScope.$digest();
    expect($rootScope.$broadcast).toHaveBeenCalledWith('RecordingStateUpdated', "some");
  });

  it('should return recording queue information', function() {
    var queue = {'id':1, 'name' : 'myQueue', 'recording_activated': 1, 'recording_mode': 'recorded'};
    spyOn(XucQueue, 'getQueue').and.returnValue(queue);

    expect(XucQueueRecording.getRecordingQueueInfo(queue.id)).toEqual({'activated': 1, 'mode': 'recorded'});
  });

  it('should handle failure if not a known error', function() {
    var knownError = "NO_RECORDING_RIGHT";
    var unknownError = "DUMMY_ERROR";

    XucQueueRecording.handleError(knownError);
    expect(XucQueueRecording.hasConfigManagerError()).toBe(false);

    XucQueueRecording.handleError(unknownError);
    expect(XucQueueRecording.hasConfigManagerError()).toBe(true);
  });
});
