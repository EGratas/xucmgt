import { Message, MessageHistory, Conversation } from "../../../../app/assets/javascripts/xccti/services/XucChat.service";
import _ from 'lodash';


describe('Xuc chat service', () => {
  var xucChat;
  var $rootScope;

  beforeEach(() => {
    Cti.Topic(Cti.MessageType.FLASHTEXTEVENT).clear();
    spyOn(Cti,'setHandler').and.callThrough();
    spyOn(Cti, 'markFlashTextAsRead');
    spyOn(Cti, 'sendFlashTextMessage');
  });

  beforeEach(angular.mock.module('xcCti'));
  beforeEach(angular.mock.module('xcChat'));
  beforeEach(angular.mock.module('karma-backend'));

  beforeEach(function() {
    jasmine.addMatchers({
      toEqualData : customMatchers.toEqualData,
      statEqual: customMatchers.statEqual
    });
  });

  beforeEach(angular.mock.inject((_XucChat_,_$rootScope_) => {
    $rootScope = _$rootScope_;
    xucChat = _XucChat_;

    spyOn($rootScope, '$broadcast').and.callThrough();
  }));

  function buildMessage(msg, user, seq) {
    return {
      event: "FlashTextUserMessage",
      message: msg,
      date: 'fake date',
      from: {
        username: user, displayName: "Fake User", phoneNumber: "1000"
      },
      sequence: seq
    };
  }

  function buildFlashtextAck() {
    return {
      event: "FlashTextSendMessageAck",
      sequence: 1,
      offline: false,
      date: "2020-04-17T09:40:00.000Z",
    };
  }

  function buildFlashtextOfflineAck() {
    return {
      event: "FlashTextSendMessageAckOffline",
      sequence: 1,
      offline: true,
      date: "2020-04-17T09:40:00.000Z",
    };
  }

  function buildFlashtextNack() {
    return {
      event: "FlashTextSendMessageNack",
      sequence: 1,
    };
  }

  function buildFlashtextHistory() {
    return {
      event: "FlashTextUserMessageHistory",
      users: [{ username: "myself", displayName: "Fake User 1"}, { username: "remoteParty", displayName: "Fake User 2"}],
      messages: [
        { from: {username: "remoteParty"}, to: {username: "myself"}, message: "hello 1", date: "2020-04-17T09:40:00.000Z", sequence: 1},
        { from: {username: "myself"}, to: {username: "remoteParty"}, message: "hello 2", date: "2020-04-17T09:50:00.000Z", sequence: 1}
      ],
      sequence: 1
    };
  }

  function buildFlashtextUnreadMessages() {
    return {
      "from": {
        "username": "bruce",
        "displayName": "Bruce Willis"
      },
      "sequence": 145,
      "messages": [{
        from: {
          username: "alice"
        },
        to: {
          username: "bruce"
        },
        message: "unread 1",
        date: '2019-05-28T14:55:08.628+02:00',
        sequence: 0
      },
      {
        from: {
          username: "fakeone"
        },
        to: {
          username: "bruce"
        },
        message: "unread 2",
        date: '2019-05-28T14:56:08.628+02:00',
        sequence: 0
      }
      ],
      "event": "FlashTextUnreadMessages"
    };
  }

  it('sends new message', () => {
    xucChat.sendMessage('jbond', 'my message');
    Cti.Topic(Cti.MessageType.FLASHTEXTEVENT).publish(buildFlashtextAck('jbond', 1));
    $rootScope.$digest();

    expect(Cti.sendFlashTextMessage).toHaveBeenCalledWith('jbond', 1, 'my message');
  });

  it('broadcasts message with date after ack received', function() {
    xucChat.sendMessage('jbond', 'message text');

    var expectedMessage = new Message(1, 'message text', '2020-04-17T09:40:00.000Z', 'outgoing');

    var callbackWasCalled = false;
    var callbackFunction = function(event, message) {
      expect(message).toEqualData(expectedMessage);
      callbackWasCalled = true;
    };

    $rootScope.$on(xucChat.CHAT_SEND_MESSAGE_ACK, callbackFunction);

    Cti.Topic(Cti.MessageType.FLASHTEXTEVENT).publish(buildFlashtextAck());
    $rootScope.$apply();
    expect(callbackWasCalled).toBeTruthy();
  });

  it('broadcasts message with date after offline ack received', function() {
    xucChat.sendMessage('jbond', 'message text');

    var expectedMessage = new Message(1, 'message text', '2020-04-17T09:40:00.000Z', 'outgoing');

    var callbackWasCalled = false;
    var callbackFunction = function(event, message) {
      expect(message).toEqualData(expectedMessage);
      callbackWasCalled = true;
    };

    $rootScope.$on(xucChat.CHAT_SEND_MESSAGE_ACK_OFFLINE, callbackFunction);

    Cti.Topic(Cti.MessageType.FLASHTEXTEVENT).publish(buildFlashtextOfflineAck());
    $rootScope.$apply();
    expect(callbackWasCalled).toBeTruthy();
  });

  it('broadcast a message after receiving it from the xuc', function() {
    var publishedMessage = buildMessage('my first message from jbond', 'jbond', 1);
    var expectedMessage = new Message (publishedMessage.sequence, publishedMessage.message, publishedMessage.date, 'incoming');

    var callbackWasCalled = false;
    var callbackFunction = function(event, message, remoteParty) {
      expect(message).toEqualData(expectedMessage);
      expect(remoteParty).toEqualData('jbond');
      callbackWasCalled = true;
    };
    $rootScope.$on(xucChat.CHAT_RECEIVED_MESSAGE, callbackFunction);
    Cti.Topic(Cti.MessageType.FLASHTEXTEVENT).publish(publishedMessage);
    $rootScope.$apply();
    expect(callbackWasCalled).toBeTruthy();
  });

  it('throw event if send message fails with NACK event', function() {
    xucChat.sendMessage('jbond', 'error sending message');

    var callbackWasCalled = false;
    var callbackFunction = function(event, data) {
      expect(data.sequence).toEqual(1);
      callbackWasCalled = true;
    };
    $rootScope.$on(xucChat.CHAT_SEND_MESSAGE_NACK, callbackFunction);
    Cti.Topic(Cti.MessageType.FLASHTEXTEVENT).publish(buildFlashtextNack());
    $rootScope.$apply();
    expect(callbackWasCalled).toBeTruthy();
  });

  it('requests conversation', function () {
    spyOn(Cti, 'getFlashTextPartyHistory');

    var publishedResponse = buildFlashtextHistory();
    var promise = xucChat.getRemotePartyConversation('remoteParty');

    var resolvedValue;

    promise.then((response) => {
      resolvedValue = response;
    });

    expect(Cti.getFlashTextPartyHistory).toHaveBeenCalledWith('remoteParty', 1);
    Cti.Topic(Cti.MessageType.FLASHTEXTEVENT).publish(publishedResponse);
    $rootScope.$apply();

    var messages = _.map(publishedResponse.messages, function(message) {
      var direction = message.from.username == 'myself' ? 'outgoing' : 'incoming';
      return new Message(message.sequence, message.message, message.date, direction);
    });

    expect(resolvedValue).toEqualData(new Conversation('remoteParty', messages));
  });

  it('marks a conversation as read when conversation is displayed', function () {
    xucChat.conversationDisplayed('jbond');
    expect(Cti.markFlashTextAsRead).toHaveBeenCalledWith('jbond', 1);
  });

  it('increment sequence when sending multiple cti requests', function () {
    xucChat.conversationDisplayed('jbond');
    expect(Cti.markFlashTextAsRead).toHaveBeenCalledWith('jbond', 1);

    xucChat.conversationDisplayed('jdoe');
    expect(Cti.markFlashTextAsRead).toHaveBeenCalledWith('jdoe', 2);

    xucChat.sendMessage('jdoe', 'a message');
    expect(Cti.sendFlashTextMessage).toHaveBeenCalledWith('jdoe', 3, 'a message');
  });


  it('builds conversation history cache on message received with unread counter set', function() {
    var publishedMessage = buildMessage('my message from jbond', 'jbond', 1);
    Cti.Topic(Cti.MessageType.FLASHTEXTEVENT).publish(publishedMessage);
    $rootScope.$apply();
    expect(xucChat.getConversationHistory().get('jbond')).toEqualData(new MessageHistory('my message from jbond', 'fake date', 1));

    publishedMessage = buildMessage('my last message from jbond', 'jbond', 2);
    Cti.Topic(Cti.MessageType.FLASHTEXTEVENT).publish(publishedMessage);
    $rootScope.$apply();
    expect(xucChat.getConversationHistory().get('jbond')).toEqualData(new MessageHistory('my last message from jbond', 'fake date', 2));
  });

  it('add entry to conversation history cache on message sent ACK received', function() {
    xucChat.sendMessage('jbond', 'message text');

    Cti.Topic(Cti.MessageType.FLASHTEXTEVENT).publish(buildFlashtextAck());
    $rootScope.$apply();
    expect(xucChat.getConversationHistory().get('jbond')).toEqualData(new MessageHistory('message text', '2020-04-17T09:40:00.000Z', 0));
  });

  it('add entry to conversation history cache on message sent ACK offline received', function() {
    xucChat.sendMessage('jbond', 'message text');

    Cti.Topic(Cti.MessageType.FLASHTEXTEVENT).publish(buildFlashtextOfflineAck());
    $rootScope.$apply();
    expect(xucChat.getConversationHistory().get('jbond')).toEqualData(new MessageHistory('message text', '2020-04-17T09:40:00.000Z', 0));
  });

  it('does not add entry to conversation history cache on message sent NACK received', function() {
    xucChat.sendMessage('jbond', 'message text');

    Cti.Topic(Cti.MessageType.FLASHTEXTEVENT).publish(buildFlashtextNack());
    $rootScope.$apply();
    expect(xucChat.getConversationHistory().get('jbond')).toBeUndefined();
  });

  it('does not update conversation history cache when conversation is active', function() {
    var publishedMessage = buildMessage('my message from jbond', 'jbond', 1);
    xucChat.conversationDisplayed('jbond');
    Cti.Topic(Cti.MessageType.FLASHTEXTEVENT).publish(publishedMessage);
    $rootScope.$apply();
    expect(xucChat.getConversationHistory().get('jbond')).toEqualData(new MessageHistory('my message from jbond', 'fake date', 0));

    xucChat.conversationEnded();
    Cti.Topic(Cti.MessageType.FLASHTEXTEVENT).publish(publishedMessage);
    $rootScope.$apply();
    expect(xucChat.getConversationHistory().get('jbond')).toEqualData(new MessageHistory('my message from jbond', 'fake date', 1));
  });

  it('builds conversation history cache from flashext unread messages', function() {
    var publishedUnreadMessage = buildFlashtextUnreadMessages();
    Cti.Topic(Cti.MessageType.FLASHTEXTEVENT).publish(publishedUnreadMessage);
    $rootScope.$apply();
    expect(xucChat.getConversationHistory().get('alice')).toEqualData(new MessageHistory('unread 1', '2019-05-28T14:55:08.628+02:00', 1));
    expect(xucChat.getConversationHistory().get('fakeone')).toEqualData(new MessageHistory('unread 2', '2019-05-28T14:56:08.628+02:00', 1));
  });

  it('gets the unread message count when receiving messages', function() {
    var publishedUnreadMessage = buildFlashtextUnreadMessages();
    expect(xucChat.getUnreadMessagesCount()).toBe(0);
    Cti.Topic(Cti.MessageType.FLASHTEXTEVENT).publish(publishedUnreadMessage);
    $rootScope.$apply();
    expect(xucChat.getUnreadMessagesCount()).toBe(2);
    xucChat.conversationDisplayed('jbond');
    expect(xucChat.getUnreadMessagesCount()).toBe(2);
    xucChat.conversationDisplayed('alice');
    expect(xucChat.getUnreadMessagesCount()).toBe(1);
    xucChat.conversationDisplayed('fakeone');
    expect(xucChat.getUnreadMessagesCount()).toBe(0);


  });

});
