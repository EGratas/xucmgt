'use strict';

describe('Xuc service link', function() {
  var xucLink;
  var rootScope;
  var locationValue = {
    origin: 'test',
    protocol: 'http:',
    href: 'http://myxucmgt/',
    replace: function() {}
  };
  var origLocalStorage;
  var windowProvider = {
    $get: function() {
      return {
        location: locationValue,
        navigator: {languages: 'en_EN'}};      
    }
  };
  var $timeout;
  var $httpBackend;

  beforeEach(function() {
    jasmine.addMatchers({
      toEqualData : customMatchers.toEqualData
    });
    angular.mock.module(function($provide) {
      $provide.provider('$window', windowProvider);
    });
    angular.mock.module('xcCti', function ($translateProvider) {
      $translateProvider.preferredLanguage('en');
    });
    Cti.Topic(Cti.MessageType.LOGGEDON).clear();
    Cti.Topic(Cti.MessageType.LINKSTATUSUPDATE).clear();
    Cti.Topic(Cti.MessageType.RIGHTPROFILE).clear();

    var localStorageMock = (function() {
      return {
        getItem: function() { return 'isSpiedAfterWithASpecificReturnValue';},
        setItem: jasmine.createSpy(),
        removeItem: jasmine.createSpy(),
      };
    })();
    origLocalStorage = window.localStorage;
    Object.defineProperty(window, 'localStorage', { value: localStorageMock,configurable:true,enumerable:true,writable:true });
  });

  beforeEach(angular.mock.inject(function(_XucLink_, _$rootScope_, _$timeout_, _$httpBackend_) {
    xucLink = _XucLink_;
    rootScope = _$rootScope_;
    $timeout = _$timeout_;
    $httpBackend = _$httpBackend_;
    spyOn(rootScope, '$broadcast');
  }));

  afterEach(function(){
    Object.defineProperty(window, 'localStorage', {value: origLocalStorage});
  });

  it('should be able to start cti using http', function() {
    spyOn(Cti.WebSocket,'init');
    locationValue.protocol = 'http:';

    xucLink.setHostAndPort('host:8090');

    xucLink.initCti("john","doe",44500);

    var wsurl = "ws://host:8090/xuc/ctichannel?username=john&amp;agentNumber=44500&amp;password=doe";

    expect(Cti.WebSocket.init).toHaveBeenCalledWith(wsurl, "john", 44500);
  });

  it('should be able to start cti using https', function() {
    spyOn(Cti.WebSocket,'init');
    locationValue.protocol = 'https:';

    xucLink.setHostAndPort('host:8090');

    xucLink.initCti("john","doe",44500);

    var wsurl = "wss://host/xuc/ctichannel?username=john&amp;agentNumber=44500&amp;password=doe";

    expect(Cti.WebSocket.init).toHaveBeenCalledWith(wsurl, "john", 44500);
  });


  it('should broadcast ctiLoggedOn and set logged to true on user login', function() {
    var whenLogged = jasmine.createSpy('whenLogged');
    xucLink.whenLogged().then(whenLogged);
    Cti.Topic(Cti.MessageType.LOGGEDON).publish("");

    expect(xucLink.isLogged()).toEqual(true);
    expect(rootScope.$broadcast).toHaveBeenCalledWith('ctiLoggedOn', {});
    rootScope.$digest();
    expect(whenLogged).toHaveBeenCalled();
  });

  it('should resolve whenLogged promise after failed and then successfull login', function() {
    var whenLogged = jasmine.createSpy('whenLogged');
    xucLink.whenLogged().then(whenLogged);
    Cti.Topic(Cti.MessageType.LINKSTATUSUPDATE).publish({status: 'closed'});
    Cti.Topic(Cti.MessageType.LOGGEDON).publish("");

    expect(xucLink.isLogged()).toEqual(true);
    expect(rootScope.$broadcast).toHaveBeenCalledWith('ctiLoggedOn', {});
    rootScope.$digest();
    expect(whenLogged).toHaveBeenCalled();
  });

  it('should login using new api',function() {
    spyOn(Cti.WebSocket, "init");
    var token = "aaaa-bbbb-cccc-dddd-1234";
    locationValue.protocol = "http:";
    xucLink.setHostAndPort("xuc:9000");
    xucLink.login("jbond", "1234", "1001");
    $httpBackend.expectPOST('http://xuc:9000/xuc/api/2.0/auth/login', {"login": "jbond", "password": "1234", "softwareType":"webBrowser"}).respond(201, {login: "jbond", token: token});
    $httpBackend.flush();
    expect(localStorage.setItem).toHaveBeenCalled();
    expect(Cti.WebSocket.init).toHaveBeenCalledWith('ws://xuc:9000/xuc/api/2.0/cti?token=' + token, "jbond", "1001");
  });

  it('should use default timeout to login', angular.mock.inject(function($httpBackend) {
    spyOn(Cti.WebSocket, "init");
    var token = "aaaa-bbbb-cccc-dddd-1234";
    locationValue.protocol = "http:";
    xucLink.setHostAndPort("xuc:9000");
    xucLink.login("jbond", "1234", "1001", false);
    $httpBackend.expectPOST('http://xuc:9000/xuc/api/2.0/auth/login', {"login": "jbond", "password": "1234", "softwareType":"webBrowser"}).respond(201, {login: "jbond", token: token}, {timeout: 5000});
    $httpBackend.flush();
  }));

  it('should use defined timeout to login', angular.mock.inject(function($httpBackend) {
    spyOn(Cti.WebSocket, "init");
    var token = "aaaa-bbbb-cccc-dddd-1234";
    locationValue.protocol = "http:";
    xucLink.setHostAndPort("xuc:9000");
    xucLink.setLoginTimeoutMs(7000);
    xucLink.login("jbond", "1234", "1001", false);
    $httpBackend.expectPOST('http://xuc:9000/xuc/api/2.0/auth/login', {"login": "jbond", "password": "1234", "softwareType":"webBrowser"}).respond(201, {login: "jbond", token: token}, {timeout: 7000});
    $httpBackend.flush();
  }));

  it('should get user when logged', function() {
    spyOn(Cti.WebSocket, "init");
    var whenLoggedCB = jasmine.createSpy('whenLoggedCB');
    var token = "aaaa-bbbb-cccc-dddd-1234";
    locationValue.protocol = "http:";
    xucLink.setHostAndPort("xuc:9000");
    xucLink.login("jbond", "1234", "1001", false);
    $httpBackend.expectPOST('http://xuc:9000/xuc/api/2.0/auth/login', {"login": "jbond", "password": "1234", "softwareType":"webBrowser"}).respond(201, {login: "jbond", token: token});
    $httpBackend.flush();
    expect(Cti.WebSocket.init).toHaveBeenCalledWith('ws://xuc:9000/xuc/api/2.0/cti?token=' + token, "jbond", "1001");
    // mock login event
    Cti.Topic(Cti.MessageType.LOGGEDON).publish("");
    xucLink.whenLogged().then(whenLoggedCB);
    rootScope.$digest();
    expect(whenLoggedCB).toHaveBeenCalledWith({username: "jbond", phoneNumber: "1001", token: token});
  });

  it('should fails if server respond 502 with html', function() {

    var errorCallback = jasmine.createSpy('errorCallback');

    locationValue.protocol = "http:";
    xucLink.setHostAndPort("xuc:9000");
    xucLink.login("jbond", "1234", "1001", false).catch(errorCallback);
    $httpBackend.expectPOST('http://xuc:9000/xuc/api/2.0/auth/login', {"login": "jbond", "password": "1234", "softwareType":"webBrowser"}).respond(502, '<html><head><title>ERROR</title></head><body>Some message</body></html>');
    $httpBackend.flush();

    expect(errorCallback).toHaveBeenCalledWith({error: 'NoResponse', message: 'No response from server' });
    
  });

  it('should fail if server responds user not found', function() {
    spyOn(Cti.WebSocket, "init");
    var errorCallback = jasmine.createSpy('errorCallback');
    locationValue.protocol = "http:";
    locationValue.host = 'xuc.org';
    locationValue.pathname = '';
    locationValue.hash = '';
    xucLink.setHostAndPort("xuc:9000");
    xucLink.login("jbond", "1234", "1001", false).catch(errorCallback);
    $httpBackend.expectPOST('http://xuc:9000/xuc/api/2.0/auth/login', {"login": "jbond", "password": "1234", "softwareType":"webBrowser"}).respond(403, '{"error":"UserNotFound","message":"User not found"}');
    $httpBackend.flush();

    expect(errorCallback).toHaveBeenCalledWith({error: 'UserNotFound', message: 'User not found' });
  });

  it('should fail if server responds invalid password', function() {
    spyOn(Cti.WebSocket, "init");
    var errorCallback = jasmine.createSpy('errorCallback');
    locationValue.protocol = "http:";
    locationValue.host = 'xuc.org';
    locationValue.pathname = '';
    locationValue.hash = '';
    xucLink.setHostAndPort("xuc:9000");
    xucLink.login("jbond", "1234", "1001", false).catch(errorCallback);
    $httpBackend.expectPOST('http://xuc:9000/xuc/api/2.0/auth/login', {"login": "jbond", "password": "1234", "softwareType":"webBrowser"}).respond(403, '{"error":"InvalidPassword","message":"Invalid password"}');
    $httpBackend.flush();
    expect(errorCallback).toHaveBeenCalledWith({error: 'InvalidPassword', message: 'Invalid password' });
  });

  it('should login using wss when using https', function() {
    spyOn(Cti.WebSocket, "init");
    var token = "aaaa-bbbb-cccc-dddd-1234";
    locationValue.protocol = "https:";
    locationValue.host = 'xuc.org';
    locationValue.pathname = '';
    locationValue.hash = '';
    xucLink.setHostAndPort("xuc:9000");
    xucLink.login("jbond", "1234", "1001");
    $httpBackend.expectPOST('https://xuc/xuc/api/2.0/auth/login', {"login": "jbond", "password": "1234", "softwareType":"webBrowser"}).respond(201, {login: "jbond", token: token});
    $httpBackend.flush();
    expect(localStorage.setItem).toHaveBeenCalled();
    expect(Cti.WebSocket.init).toHaveBeenCalledWith('wss://xuc/xuc/api/2.0/cti?token=' + token, "jbond", "1001");
  });

  it('should persist credentials when asked to', function() {
    spyOn(Cti.WebSocket, "init");
    var token = "aaaa-bbbb-cccc-dddd-1234";
    locationValue.protocol = "http:";
    locationValue.host = 'xuc.org';
    locationValue.pathname = '';
    locationValue.hash = '';
    xucLink.setHostAndPort("xuc:9000");
    xucLink.login("jbond", "1234", "1001", true);
    $httpBackend.expectPOST('http://xuc:9000/xuc/api/2.0/auth/login', {"login": "jbond", "password": "1234", "softwareType":"webBrowser"}).respond(201, {login: "jbond", token: token});
    $httpBackend.flush();
    var expected = JSON.stringify({login: "jbond", token: token, phoneNumber: "1001"});
    expect(localStorage.setItem).toHaveBeenCalledWith("credentials", expected);
  });

  it('when login with credentials, check token validity and renew using new api', function() {
    var credentials = {login: "jbond", token: "aaaa-bbbb-cccc-dddd-1234", phoneNumber: "1001"};
    var newToken = "eeee-ffff-gggg-hhhh-5678";
    spyOn(Cti.WebSocket, "init");
    locationValue.protocol = "http:";
    locationValue.host = 'xuc.org';
    locationValue.pathname = '';
    locationValue.hash = '';
    xucLink.setHostAndPort("xuc:9000");
    xucLink.checkAndloginWithCredentials(credentials);
    $httpBackend.expectGET('http://xuc:9000/xuc/api/2.0/auth/check', undefined, function(headers) {
      return headers['Authorization'] === ('Bearer ' + credentials.token) ;
    }).respond(201, {login: "jbond", token: newToken});
    $httpBackend.flush();
    expect(Cti.WebSocket.init).toHaveBeenCalledWith('ws://xuc:9000/xuc/api/2.0/cti?token=' + newToken, "jbond", "1001");
  });

  it('when login, should schedule a token renew', function() {
    var TTL = 3600;
    var newToken = "eeee-ffff-gggg-hhhh-5678";
    var credentials = {login: "jbond", token: "aaaa-bbbb-cccc-dddd-1234", phoneNumber: "1001", TTL};
    spyOn(Cti.WebSocket, "init");
    locationValue.protocol = "http:";
    locationValue.host = 'xuc.org';
    locationValue.pathname = '';
    locationValue.hash = '';
    xucLink.setHostAndPort("xuc:9000");
    xucLink.checkAndloginWithCredentials(credentials);
    $httpBackend.expectGET('http://xuc:9000/xuc/api/2.0/auth/check', undefined, function(headers) {
      return headers['Authorization'] === ('Bearer ' + credentials.token) ;
    }).respond(201, {login: "jbond", token: newToken, TTL});
    $httpBackend.flush();

    $httpBackend.expectGET('http://xuc:9000/xuc/api/2.0/auth/check', undefined, function(headers) {
      return headers['Authorization'] === ('Bearer ' + credentials.token) ;
    }).respond(201, {login: "jbond", token: newToken, TTL});

    $timeout.flush((TTL * 1000) / 2);
    $httpBackend.flush();
    expect(xucLink.getXucToken()).toBe(newToken);
  });

  it('should schedule a token renew when login', function() {
    var TTL = 3600;
    spyOn(Cti.WebSocket, "init");
    var token = "aaaa-bbbb-cccc-dddd-1234";
    var newToken = "eeee-ffff-gggg-hhhh-5678";

    locationValue.protocol = "http:";
    locationValue.host = 'xuc.org';
    locationValue.pathname = '';
    locationValue.hash = '';
    xucLink.setHostAndPort("xuc:9000");
    xucLink.login("jbond", "1234", "1001");


    $httpBackend.expectPOST('http://xuc:9000/xuc/api/2.0/auth/login', {"login": "jbond", "password": "1234", "softwareType":"webBrowser"}).respond(201, {login: "jbond", token: token, TTL});
    $httpBackend.flush();
    
    $httpBackend.expectGET('http://xuc:9000/xuc/api/2.0/auth/check', undefined, function(headers) {
      return headers['Authorization'] === ('Bearer ' + token) ;
    }).respond(201, {login: "jbond", token: newToken, TTL});
    
    $timeout.flush((TTL * 1000) / 2);
    expect(localStorage.setItem).toHaveBeenCalled();
    $httpBackend.flush();

    expect(localStorage.setItem).toHaveBeenCalledWith('credentials', JSON.stringify({login: "jbond", token: newToken, TTL, phoneNumber:"1001"}));
  });

  it('should cancel token renewal if the user logs out', function(done) {
    var TTL = 3600;
    spyOn(Cti.WebSocket, "init");
    spyOn(Cti, "close");
    var token = "aaaa-bbbb-cccc-dddd-1234";

    locationValue.protocol = "http:";
    locationValue.host = 'xuc.org';
    locationValue.pathname = '';
    locationValue.hash = '';
    xucLink.setHostAndPort("xuc:9000");
    let promise = xucLink.login("jbond", "1234", "1001");
    $httpBackend.expectPOST('http://xuc:9000/xuc/api/2.0/auth/login', {"login": "jbond", "password": "1234", "softwareType":"webBrowser"}).respond(201, {login: "jbond", token: token, TTL});
    $httpBackend.flush();

    expect(localStorage.setItem).toHaveBeenCalled();
    expect(Cti.WebSocket.init).toHaveBeenCalledWith('ws://xuc:9000/xuc/api/2.0/cti?token=' + token, "jbond", "1001");

    xucLink.logout();

    promise.then(() => {
      xucLink.getTokenRenewPromise().then(
        () =>  {
          done.fail('Should be canceled when logged out');
        }, () => {
          done();
        }
      ).catch(() => {
        done.fail('Should be resolved');
      });
    });
    rootScope.$digest();
    $timeout.flush();
  });

  it('should clear credentials upon connection error', function() {
    var credentials = {login: "jbond", token: "aaaa-bbbb-cccc-dddd-1234", phoneNumber: "1001"};
    spyOn(Cti.WebSocket, "init");
    spyOn(localStorage, "getItem").and.returnValue(JSON.stringify(credentials));
    locationValue.protocol = "http:";
    locationValue.host = 'xuc.org';
    locationValue.pathname = '';
    locationValue.hash = '';
    xucLink.setHostAndPort("xuc:9000");
    xucLink.checkAndloginWithCredentials(credentials).catch(function(){});
    $httpBackend.expectGET('http://xuc:9000/xuc/api/2.0/auth/check', undefined, function(headers) {
      return headers['Authorization'] === ('Bearer ' + credentials.token) ;
    }).respond(403, {error: "TokenExpired", message: "Token expired"});
    $httpBackend.flush();
    expect(localStorage.removeItem).toHaveBeenCalledWith("credentials");
    expect(Cti.WebSocket.init).not.toHaveBeenCalled();
  });

  it('(CAS Auth) should redirect to cas server when no ticket is found and if cas is up', function() {
    let serviceUrl = 'https://cas.server.org/cas/login?service=' + escape('http://xuc.org?phoneNumber=007');
    locationValue.search = '';
    locationValue.host = 'xuc.org';
    locationValue.pathname = '';
    locationValue.hash = '';
    locationValue.protocol = 'http:';
    locationValue.href = 'http://xuc.org';
    xucLink.getCasToken('https://cas.server.org/cas', "007");
    expect(locationValue.href).toEqual(serviceUrl);
  });

  it('(CAS Auth) should validate ticket against xuc', function() {
    var service = escape('http://xuc.org?phoneNumber=1000');
    var ticket = 'ST-27-abcd-efgh';
    locationValue.search = '';
    locationValue.protocol = 'http:';
    locationValue.host = 'xuc.org';
    locationValue.pathname = '';
    locationValue.hash = '';
    locationValue.href = 'http://xuc.org';
    xucLink.setHostAndPort("xuc:9000");
    $httpBackend.expectGET('http://xuc:9000/xuc/api/2.0/auth/cas?service=' + service + '&ticket=' + ticket + "&softwareType=webBrowser").respond(201, {login: "jbond", token: "abcd-efgh"});
    xucLink.tradeCasToken('https://cas.server.org/cas', ticket, "1000").catch(function() {});
    $httpBackend.flush();
  });

  it('(oidc Auth) should redirect to oidc server if no token', function() {
    locationValue.search = '';
    locationValue.protocol = 'http:';
    locationValue.host = 'xuc.org';
    locationValue.hash = '';
    locationValue.pathname = '';
    locationValue.href = 'http://xuc.org';
    let redirectUrl = 'http://xuc.org?phoneNumber=007';
    let openIdServerUrl = "http://oidc.example.com";
    let openIdClientId = "clientid";
    let loginUrl = openIdServerUrl + '/protocol/openid-connect/auth?' +
                'client_id=' + encodeURIComponent(openIdClientId) +
                '&redirect_uri=' + encodeURIComponent(redirectUrl) +
                '&response_type=token' +
                '&scope=openid'+
                '&nonce=nonce';
    $httpBackend.expectGET(openIdServerUrl).respond(200, "I'm alive");
    xucLink.getOpenidToken(openIdServerUrl, openIdClientId, "007");
    $httpBackend.flush();
    rootScope.$apply();
    expect(locationValue.href).toEqual(loginUrl);
  });

  it('(oidc Auth) should extract token from url', function() {
    var token = 'ST-27-abcd-efgh';
    locationValue.search = '?access_token=' + token;
    locationValue.protocol = 'http:';
    locationValue.host = 'xuc.org';
    locationValue.pathname = '';
    locationValue.hash = '';
    locationValue.href = 'http://xuc.org?access_token=' + token;
    let response = xucLink.getOpenidToken("http://oidc.example.com", "clientid", undefined);
    rootScope.$apply();
    response.then((data) => {
      expect(data).toEqual({login: "jbond", token: "abcd-efgh"});
    });
  });

  it('(oidc Auth) should validate token against xuc', function() {
    var token = 'ST-27-abcd-efgh';
    locationValue.protocol = 'http:';
    xucLink.setHostAndPort("xuc:9000");
    $httpBackend.expectGET('http://xuc:9000/xuc/api/2.0/auth/oidc?token=' + token + "&softwareType=webBrowser").respond(201, {login: "jbond", token: "abcd-efgh"});
    let response = xucLink.tradeOpenidToken(token);
    $httpBackend.flush();
    rootScope.$apply();
    response.then((data) => {
      expect(data).toEqual({login: "jbond", token: "abcd-efgh"});
    });
  });
  /*
  it('(oidc Auth) should request oidc server for a logout on manual logout from an oidc authenticated user if the key is set', function() {
    locationValue.href = 'http://xuc.org/#!/ucassistant';
    locationValue.protocol = 'http:';
    locationValue.host = 'xuc.org';
    locationValue.pathname = '';
    locationValue.hash = '';
    xucLink.setHostAndPort("xuc:9000");
    const openidUrl = "https://someUrl.oidc/some-realm";
    $httpBackend.expectGET('https://someUrl.oidc/some-realm/protocol/openid-connect/logout?redirect_uri=http%3A%2F%2Fxuc.org', undefined, ()=>{}).respond(200);

    xucLink.logoutFromOpenid(openidUrl);
    $httpBackend.flush();
    $httpBackend.verifyNoOutstandingRequest();
  });
*/
  it('should parse URL params correctly', function() {
    let params = xucLink.parseUrlParameters('http://xuc.org/ccagent#!/login?lastError=Logout&ranparam=param1&chocolate=milk');
    expect(params).toEqual({lastError: 'Logout', ranparam: 'param1', chocolate: 'milk'});
  });

  it('can resolve rightProfile promise when receiving RIGHTPROFILE event', function(done) {
    var rightProfile = {profile: 'Supervisor', rights: 'recording'};
    spyOn(xucLink, 'getProfileRightAsync').and.callThrough();

    var p = xucLink.getProfileRightAsync();

    p.then((value) => {
      expect(value).toEqual(rightProfile);
      done();
    });

    Cti.Topic(Cti.MessageType.RIGHTPROFILE).publish(rightProfile);
    rootScope.$digest();
  });

  it('should answer if right is allowed in RightProfile', function() {
    var rightProfile = {
      profile: "myProfile",
      rights: 'recording'
    };
    expect(xucLink.isRightAllowed(rightProfile, 'recording')).toEqual(true);
    expect(xucLink.isRightAllowed(rightProfile, 'other')).toEqual(false);
  });

});

describe('Xuc service link when wrapped in Electron', function() {
  var xucLink;
  var rootScope;
  var locationValue = {
    origin: 'test',
    protocol: 'http:',
    href: 'http://myxucmgt/',
    replace: function() {}
  };
  var origLocalStorage;
  var windowProvider = {
    $get: function() {
      return {
        location: locationValue,
        navigator: {languages: 'en_EN'},
        setElectronConfig: () => {}};
    }
  };
  var $httpBackend;

  beforeEach(function() {
    jasmine.addMatchers({
      toEqualData : customMatchers.toEqualData
    });
    angular.mock.module(function($provide) {
      $provide.provider('$window', windowProvider);
    });
    angular.mock.module('xcCti', function ($translateProvider) {
      $translateProvider.preferredLanguage('en');
    });
    Cti.Topic(Cti.MessageType.LOGGEDON).clear();
    Cti.Topic(Cti.MessageType.LINKSTATUSUPDATE).clear();
    Cti.Topic(Cti.MessageType.RIGHTPROFILE).clear();

    var localStorageMock = (function() {
      return {
        getItem: function() { return 'isSpiedAfterWithASpecificReturnValue';},
        setItem: jasmine.createSpy(),
        removeItem: jasmine.createSpy(),
      };
    })();
    origLocalStorage = window.localStorage;
    Object.defineProperty(window, 'localStorage', { value: localStorageMock,configurable:true,enumerable:true,writable:true });
  });

  beforeEach(angular.mock.inject(function(_XucLink_, _$rootScope_, _$httpBackend_) {
    xucLink = _XucLink_;
    rootScope = _$rootScope_;
    $httpBackend = _$httpBackend_;
    spyOn(rootScope, '$broadcast');
  }));

  afterEach(function(){
    Object.defineProperty(window, 'localStorage', {value: origLocalStorage});
  });

  it('should update softwareType in header if electron',function() {
    spyOn(Cti.WebSocket, "init");

    var token = "aaaa-bbbb-cccc-dddd-1234";
    locationValue.protocol = "http:";
    xucLink.setHostAndPort("xuc:9000");
    xucLink.login("jbond", "1234", "1001");
    $httpBackend.expectPOST('http://xuc:9000/xuc/api/2.0/auth/login', {"login": "jbond", "password": "1234", "softwareType":"electron"}).respond(201, {login: "jbond", token: token});
    $httpBackend.flush();
    expect(localStorage.setItem).toHaveBeenCalled();
    expect(Cti.WebSocket.init).toHaveBeenCalledWith('ws://xuc:9000/xuc/api/2.0/cti?token=' + token, "jbond", "1001");
  });
});

