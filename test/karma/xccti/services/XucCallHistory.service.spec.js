describe('XucCallHistory', function() {

  var $intervalSpy;
  var $rootScope;
  var XucPhoneEventListener;
  var registeredCallback = null;
  var remoteConfiguration;
  var $q;
  var deferred;

  beforeEach(angular.mock.module('html-templates'));
  beforeEach(angular.mock.module('karma-backend'));

  beforeEach(function() {
    angular.mock.module('xcCti');
    angular.mock.module('xcHelper');

    $intervalSpy = jasmine.createSpy('$interval');
    angular.mock.module(function($provide) {
      $provide.value('$interval', $intervalSpy);
    });
    jasmine.addMatchers({
      toEqualData : customMatchers.toEqualData
    });
    
    spyOn(Cti,'getUserCallHistoryByDays');
    spyOn(Cti,'getAgentCallHistory');
    Cti.Topic(Cti.MessageType.CALLHISTORY).clear();
    Cti.Topic(Cti.MessageType.PHONESTATUSUPDATE).clear();
  });

  beforeEach(angular.mock.inject(function(_$rootScope_, _XucPhoneEventListener_, _remoteConfiguration_, _$q_) {
    $rootScope = _$rootScope_;
    remoteConfiguration = _remoteConfiguration_;
    $q = _$q_;
    deferred = $q.defer();
    spyOn($rootScope, '$broadcast').and.callThrough();
    XucPhoneEventListener = _XucPhoneEventListener_;
    spyOn(XucPhoneEventListener, 'addReleasedHandler').and.callFake(function(scope, callback) {
      registeredCallback = callback;
    });
  }));

  it('can instanciate service', angular.mock.inject(function(XucCallHistory) {
    expect(XucCallHistory).not.toBeUndefined();
  }));

  it("request call history on PHONESTATUSUPDATE AVAILABLE for user if you subscribed to it", function() {
    spyOn(remoteConfiguration, 'getInt').and.callFake(() => {
      return deferred.promise;
    });
    angular.mock.inject(function(XucCallHistory) {
      jasmine.clock().install();

      expect(XucCallHistory).not.toBeUndefined();
      expect(XucPhoneEventListener.addReleasedHandler).toHaveBeenCalled();
      expect(registeredCallback).not.toBeUndefined();
      XucCallHistory.subscribeToUserCallHistory($rootScope, () => { });
      registeredCallback();

      jasmine.clock().tick(3010);
      deferred.resolve(7);
      $rootScope.$apply();
      expect(Cti.getAgentCallHistory).not.toHaveBeenCalled();
      expect(Cti.getUserCallHistoryByDays).toHaveBeenCalledWith(7);
      jasmine.clock().uninstall();
    });
  });

  it("do not request for call history on PHONESTATUSUPDATE AVAILABLE for user call history if you did not subscribed to it", function() {
    spyOn(remoteConfiguration, 'getInt').and.callFake(() => {
      return deferred.promise;
    });
    angular.mock.inject(function(XucCallHistory) {
      jasmine.clock().install();

      expect(XucCallHistory).not.toBeUndefined();
      expect(XucPhoneEventListener.addReleasedHandler).toHaveBeenCalled();
      expect(registeredCallback).not.toBeUndefined();
      registeredCallback();

      jasmine.clock().tick(3010);
      deferred.resolve(7);
      $rootScope.$apply();
      expect(Cti.getAgentCallHistory).not.toHaveBeenCalled();
      expect(Cti.getUserCallHistoryByDays).not.toHaveBeenCalledWith(7);
      jasmine.clock().uninstall();
    });
  });

  it('do not request call history on PHONESTATUSUPDATE AVAILABLE for agent if you did not subscribed to it', function() {
    angular.mock.inject(function (XucCallHistory) {
      jasmine.clock().install();
      expect(XucCallHistory).not.toBeUndefined();
      remoteConfiguration.setIsAgent(true);
      expect(XucPhoneEventListener.addReleasedHandler).toHaveBeenCalled();
      expect(registeredCallback).not.toBeUndefined();
      registeredCallback();

      jasmine.clock().tick(3010);
      expect(Cti.getAgentCallHistory).not.toHaveBeenCalledWith(20);
      expect(Cti.getUserCallHistoryByDays).not.toHaveBeenCalled();
      jasmine.clock().uninstall();
    });
  });

  it('request call history on PHONESTATUSUPDATE AVAILABLE for agent if you subscribed to it', function() {
    angular.mock.inject(function (XucCallHistory) {
      jasmine.clock().install();
      expect(XucCallHistory).not.toBeUndefined();
      remoteConfiguration.setIsAgent(true);
      XucCallHistory.subscribeToAgentCallHistory($rootScope, () => {});
      expect(XucPhoneEventListener.addReleasedHandler).toHaveBeenCalled();
      expect(registeredCallback).not.toBeUndefined();
      registeredCallback();

      jasmine.clock().tick(3010);
      expect(Cti.getAgentCallHistory).toHaveBeenCalledWith(20);
      expect(Cti.getUserCallHistoryByDays).not.toHaveBeenCalled();
      jasmine.clock().uninstall();
    });
  });

  it('on CALLHISTORY with empty history process new user call history and publish empty result', angular.mock.inject(function (XucCallHistory, $rootScope) {
    remoteConfiguration.setIsAgent(false);
    Cti.Topic(Cti.MessageType.CALLHISTORY).publish([]);
    expect($rootScope.$broadcast).toHaveBeenCalledWith('CallHistoryUpdated', []);
  }));

  it('on CALLHISTORY with empty history process new agent call history and publish empty result', angular.mock.inject(function(XucCallHistory, $rootScope) {
    remoteConfiguration.setIsAgent(true);
    XucCallHistory.subscribeToAgentCallHistory($rootScope, () => { });
    Cti.Topic(Cti.MessageType.CALLHISTORY).publish([]);
    expect($rootScope.$broadcast).toHaveBeenCalledWith('CallHistoryUpdated', []);
  }));

  it('on CALLHISTORY with non-empty history process new call history and publish empty result', angular.mock.inject(function(XucCallHistory, $rootScope) {
    var history = ' \
            [ \
                { \
                    "start": "2017-03-10 11:21:25", \
                    "duration": "00:00:02", \
                    "srcNum": "1002", \
                    "dstNum": "1000", \
                    "status": "missed", \
                    "srcFirstName": "Ann", \
                    "srcLastName": "Young", \
                    "dstFirstName": "Peter", \
                    "dstLastName": "Black" \
                }, \
                { \
                    "start": "2017-03-10 16:24:27", \
                    "duration": "00:00:02", \
                    "srcNum": "1002", \
                    "dstNum": "1000", \
                    "status": "missed", \
                    "srcFirstName": "Bob", \
                    "srcLastName": "Smith", \
                    "dstFirstName": "Peter", \
                    "dstLastName": "Black" \
                }, \
                { \
                    "start": "2017-05-09 10:10:24", \
                    "duration": "00:00:01", \
                    "srcNum": "1090", \
                    "dstNum": "1000", \
                    "status": "missed", \
                    "srcFirstName": "Ann", \
                    "srcLastName": "Young", \
                    "dstFirstName": "Peter", \
                    "dstLastName": "Black" \
                } \
            ] \
        ';

    Cti.Topic(Cti.MessageType.CALLHISTORY).publish(JSON.parse(history));

    var expected = ' \
            [ \
              { \
                "day": "2017-03-09T23:00:00.000Z", \
                "details": [ \
                  { \
                    "start": "2017-03-10 11:21:25", \
                    "duration": "00:00:02", \
                    "srcNum": "1002", \
                    "dstNum": "1000", \
                    "status": "missed", \
                    "srcFirstName": "Ann", \
                    "srcLastName": "Young", \
                    "dstFirstName": "Peter", \
                    "dstLastName": "Black", \
                    "number": "1002", \
                    "firstName": "Ann", \
                    "lastName": "Young" \
                  }, \
                  { \
                    "start": "2017-03-10 16:24:27", \
                    "duration": "00:00:02", \
                    "srcNum": "1002", \
                    "dstNum": "1000", \
                    "status": "missed", \
                    "srcFirstName": "Bob", \
                    "srcLastName": "Smith", \
                    "dstFirstName": "Peter", \
                    "dstLastName": "Black", \
                    "number": "1002", \
                    "firstName": "Bob", \
                    "lastName": "Smith" \
                  } \
                ] \
              }, \
              { \
                "day": "2017-05-08T22:00:00.000Z", \
                "details": [ \
                  { \
                    "start": "2017-05-09 10:10:24", \
                    "duration": "00:00:01", \
                    "srcNum": "1090", \
                    "dstNum": "1000", \
                    "status": "missed", \
                    "srcFirstName": "Ann", \
                    "srcLastName": "Young", \
                    "dstFirstName": "Peter", \
                    "dstLastName": "Black", \
                    "number": "1090", \
                    "firstName": "Ann", \
                    "lastName": "Young" \
                  } \
                ] \
              } \
            ] \
        ';

    expect($rootScope.$broadcast).toHaveBeenCalledWith('CallHistoryUpdated', JSON.parse(expected));
  }));
  
  it('On CALLHISTORY , should not display failed emitted call', angular.mock.inject(function(XucCallHistory, $rootScope) {
    var history = ' \
            [ \
                { \
                    "start": "2017-03-10 11:21:25", \
                    "duration": "00:00:00", \
                    "srcNum": "1002", \
                    "dstNum": null, \
                    "status": "emitted", \
                    "srcFirstName": "Ann", \
                    "srcLastName": "Young", \
                    "dstFirstName": null, \
                    "dstLastName": null \
                }, \
                { \
                    "start": "2017-03-10 16:24:27", \
                    "duration": "00:00:02", \
                    "srcNum": "1002", \
                    "dstNum": "1000", \
                    "status": "emitted", \
                    "srcFirstName": "Bob", \
                    "srcLastName": "Smith", \
                    "dstFirstName": "Peter", \
                    "dstLastName": "Black" \
                } \
            ] \
        ';

    Cti.Topic(Cti.MessageType.CALLHISTORY).publish(JSON.parse(history));

    var expected = ' \
            [ \
              { \
                "day": "2017-03-09T23:00:00.000Z", \
                "details": [ \
                  { \
                    "start": "2017-03-10 16:24:27", \
                    "duration": "00:00:02", \
                    "srcNum": "1002", \
                    "dstNum": "1000", \
                    "status": "emitted", \
                    "srcFirstName": "Bob", \
                    "srcLastName": "Smith", \
                    "dstFirstName": "Peter", \
                    "dstLastName": "Black", \
                    "number": "1000", \
                    "firstName": "Peter", \
                    "lastName": "Black" \
                  } \
                ] \
              } \
            ] \
        ';

    expect($rootScope.$broadcast).toHaveBeenCalledWith('CallHistoryUpdated', JSON.parse(expected));
  })); 
  
  it('on CALLHISTORY with empty history, should not diplay failed emitted call so user call history and publish empty result', angular.mock.inject(function (XucCallHistory, $rootScope) {
    var history = ' \
            [ \
                { \
                    "start": "2017-03-10 11:21:25", \
                    "duration": "00:00:00", \
                    "srcNum": "1002", \
                    "dstNum": null, \
                    "status": "emitted", \
                    "srcFirstName": "Ann", \
                    "srcLastName": "Young", \
                    "dstFirstName": "Peter", \
                    "dstLastName": "Black" \
                } \
            ] \
        ';
    Cti.Topic(Cti.MessageType.CALLHISTORY).publish(JSON.parse(history));
    expect($rootScope.$broadcast).toHaveBeenCalledWith('CallHistoryUpdated', []);
  }));

  it('should find customer call history and return it asynchronously', angular.mock.inject(function(XucCallHistory, $rootScope) {
    Cti.Topic(Cti.MessageType.LOGGEDON).publish("");
    $rootScope.$digest();
    var rqId = 1;
    var query = { filters: [{field:"src_num", operator:"=", value:"1000"}], limit: 10 };
    spyOn(Cti, 'findCustomerCallHistory');

    XucCallHistory.findCustomerCallHistoryAsync(query);
    $rootScope.$digest();
    expect(Cti.findCustomerCallHistory).toHaveBeenCalledWith(rqId, query.filters, query.limit);
  }));
});
