'use strict';
describe('Xuc queue group service', function() {
  var xucQueueGroup;
  var $rootScope;
  var xucAgent;
  var xucGroup;
  var $q;
  var cb;

  beforeEach(angular.mock.module('xcCti'));

  beforeEach(angular.mock.inject(function(_XucQueueGroup_,_XucAgent_,_XucGroup_,_$rootScope_,_$q_) {
    xucQueueGroup = _XucQueueGroup_;
    $rootScope = _$rootScope_;
    xucAgent = _XucAgent_;
    xucGroup = _XucGroup_;
    $q = _$q_;
  
    cb = {getCb: function() {}};
    spyOn(cb, 'getCb');
  
  }));

  it('return one empty list of groups for penalty 0 if no agents for a queue ', function(){
    var queueId = 895;

    spyOn(xucAgent,'getAgentsInQueue').and.returnValue([]);

    var expectedGroups = [{"penalty":0,"groups":[]}];

    xucQueueGroup.getGroupsForAQueue(queueId).then(cb.getCb);

    $rootScope.$digest();

    expect(cb.getCb).toHaveBeenCalledWith(expectedGroups);
  });
  
  it('do not process agents not in a group ', function(){
    var queueId = 895;
    var agentNotInGroup = new MockAgentBuilder(74,'Kim','Gunard').inQueue(queueId,0).build();
    spyOn(xucAgent,'getAgentsInQueue').and.returnValue([agentNotInGroup]);

    var expectedGroups = [{"penalty":0,"groups":[]}];

    xucQueueGroup.getGroupsForAQueue(queueId).then(cb.getCb);
    
    $rootScope.$digest();
    
    expect(cb.getCb).toHaveBeenCalledWith(expectedGroups);
  });

  it('return groups for penalty 0 and an empty group for penalty 1', function(){
    var queueId = 3;
    var groupId = 78;
    var agent1 = new MockAgentBuilder(74,'Kim','Gunard').inQueue(queueId,0).inGroup(groupId).build();
    var agents = [agent1];
    var group = new MockGroupBuilder(groupId,'blue').build();
    
    spyOn(xucAgent,'getAgentsInQueue').and.returnValue(agents);
    spyOn(xucGroup, 'getGroupAsync').and.returnValue($q.resolve(group));
    
    var vgroup = new MockGroupBuilder(groupId,'blue').build();
    
    vgroup.nbOfAgents=1;
    vgroup.agents=agents;
    
    var expectedGroups = [
      {"penalty":0,"groups":[vgroup]},
      {"penalty":1,"groups":[]}
    ];
    
    xucQueueGroup.getGroupsForAQueue(queueId).then(cb.getCb);
    $rootScope.$digest();    
    expect(cb.getCb).toHaveBeenCalledWith(expectedGroups);
    expect(group.nbOfAgents).toBeUndefined();
  });
  
  it('return empty group if agent not in queue', function(){
    var queueId = 3;
    var groupId = 9;
    var agent1 = new MockAgentBuilder(74,'Kim','Gunard').inGroup(groupId).build();
    var agents = [agent1];
    var group = new MockGroupBuilder(groupId,'blue');
    
    spyOn(xucAgent,'getAgentsInQueue').and.returnValue(agents);
    spyOn(xucGroup, 'getGroupAsync').and.returnValue($q.resolve(group));
    
    var expectedGroups = [{"penalty":0,"groups":[]}];
    
    xucQueueGroup.getGroupsForAQueue(queueId).then(cb.getCb);
    $rootScope.$digest();    
    
    expect(cb.getCb).toHaveBeenCalledWith(expectedGroups);
  });
  
  it('return only one queue group when agents are in the same group with same penalty', function(){
    var queueId = 7;
    var groupId = 54;
    var agent1 = new MockAgentBuilder(86,'Louis','Hantou').inQueue(queueId,0).inGroup(groupId).build();
    var agent2 = new MockAgentBuilder(91,'Mike','Birch').inQueue(queueId,0).inGroup(groupId).build();
    var agents = [agent1, agent2];
    var group = new MockGroupBuilder(groupId,'blue').build();
    
    spyOn(xucAgent,'getAgentsInQueue').and.returnValue(agents);
    spyOn(xucGroup, 'getGroupAsync').and.returnValue($q.resolve(group));
    
    var vGroup = angular.copy(group);
    vGroup.nbOfAgents = 2;
    vGroup.agents = agents;
    
    var expectedGroups = [{"penalty":0,"groups":[vGroup]},{"penalty":1,"groups":[]}];
    
    xucQueueGroup.getGroupsForAQueue(queueId).then(cb.getCb);
    $rootScope.$digest();    
    
    expect(cb.getCb).toHaveBeenCalledWith(expectedGroups);
  });
  
  it('should return empty queue groups when no agents with this penalty', function() {
    var queueId = 1;
    var group1Id = 67;
    var group2Id = 71;
    var group1 = new MockGroupBuilder(group1Id,'red').build();
    var group2 = new MockGroupBuilder(group2Id,'yellow').build();
    var agent1 = new MockAgentBuilder(102,'Noe','Carlos').inQueue(queueId,0).inGroup(group1Id).build();
    var agent2 = new MockAgentBuilder(41,'Oscar','Dante').inQueue(queueId,3).inGroup(group2Id).build();
    var agents = [agent1, agent2];
    
    spyOn(xucAgent,'getAgentsInQueue').and.returnValue(agents);
    spyOn(xucGroup, 'getGroupAsync').and.returnValues($q.resolve(group1),$q.resolve(group2));
    
    
    var vGroup1 = angular.copy(group1);
    vGroup1.nbOfAgents = 1;
    vGroup1.agents = [agent1];
    var vGroup2 = angular.copy(group2);
    vGroup2.nbOfAgents = 1;
    vGroup2.agents = [agent2];
    var expectedGroupsArrayForQueue = [
      {"penalty":0,"groups":[vGroup1]},
      {"penalty":1,"groups":[]},
      {"penalty":2,"groups":[]},
      {"penalty":3,"groups":[vGroup2]},
      {"penalty":4,"groups":[]}
    ];
    
    xucQueueGroup.getGroupsForAQueue(queueId).then(cb.getCb);
    $rootScope.$digest();    
    
    expect(cb.getCb).toHaveBeenCalledWith(expectedGroupsArrayForQueue);
  });
  
  it ('max penalty is 20 no more group returned if max agent penalty is 20', function(){
    var queueId = 1;
    var groupId = 54;
    var agent1 = new MockAgentBuilder(66,'Joe','Foxtrot').inQueue(queueId,20).inGroup(groupId).build();
    var agents = [agent1];
    var group = new MockGroupBuilder(groupId,'blue');
    
    var cb = {getCb: (groups) => {
      expect(groups[21]).toBeUndefined();      
    }};

    spyOn(xucAgent,'getAgentsInQueue').and.returnValue(agents);
    spyOn(xucGroup, 'getGroupAsync').and.returnValues($q.resolve(group));
    

    xucQueueGroup.getGroupsForAQueue(queueId).then(cb.getCb);
    $rootScope.$digest();    
    
  });

});

