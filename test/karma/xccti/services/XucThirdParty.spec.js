'use strict';

describe('XucThirdParty', function() {
  var $rootScope;
  var XucThirdParty;
  var $scope;
  var $httpBackend;
  var XucUser;
  var XucQueue;
  var XucAgent;
  var XucLink;
  var $q;
  var wsPayload;
  var wsPayloadDirect;
  var wsUrl;
  var wsUserData;

  beforeEach(angular.mock.module('xcCti'));

  beforeEach(angular.mock.inject(function (_$rootScope_, _XucThirdPartyService_, _$httpBackend_, _XucUser_, _XucQueue_, _$q_, _XucAgent_, _XucLink_) {
    $rootScope = _$rootScope_;
    $scope = $rootScope.$new();

    XucThirdParty = _XucThirdPartyService_;
    $httpBackend = _$httpBackend_;
    XucUser = _XucUser_;
    XucLink = _XucLink_;
    XucQueue = _XucQueue_;
    XucAgent = _XucAgent_;

    $q = _$q_;

    spyOn(Cti, 'sendCallback');

    Cti.Topic(Cti.MessageType.PHONEEVENT).clear();

    // Initialize XucLink
    Cti.Topic(Cti.MessageType.LOGGEDON).publish("");

    // Sample integration
    wsUrl = "http://dummy.com/ws";
    var user = {
      userId: 1,
      agentId: 1001,
      firstName: "James",
      lastName: "Bond",
      token: "token"
    };

    var queue = {
      id: 3001,
      name: 'trucks',
      displayName: 'Trucks'
    };

    wsUserData = {
      "XIVO_CONTEXT":"default",
      "XIVO_USERID":"2",
      "XIVO_SRCNUM":"1001",
      "XIVO_DSTNUM":"3001"
    };

    wsPayload = {
      user: user,
      callee: "1000",
      caller: "1001",
      queue: queue,
      userData: wsUserData,
      callDataCallId: "1471858488.233"
    };

    wsPayloadDirect = {
      user: user,
      callee: "1000",
      caller: "1001",
      userData: wsUserData,
      callDataCallId: "1471858488.233"
    };


    $httpBackend.whenPOST(wsUrl, wsPayload).respond({});
    spyOn(XucUser, 'getUserAsync').and.callFake(function() { return successfullPromise(user); });
    spyOn(XucLink, 'whenLogged').and.callFake(function() { return successfullPromise(user); });
    spyOn(XucQueue, 'getQueueByNameAsync').and.callFake(function(qname) {
      expect(qname).toEqual("trucks");
      return successfullPromise(queue);
    });

    XucThirdParty.setThirdPartyWs(wsUrl);
    
    $rootScope.$digest();
  }));

  afterEach(function () {
    $httpBackend.resetExpectations();
    $scope.$destroy();
  });

  function getEvent(eventType) {
    return {
      "eventType": eventType,
      "DN":"1000",
      "otherDN":"1001",
      "linkedId":"1471858488.233",
      "uniqueId":"1471858490.236",
      "queueName":"trucks",
      "userData": wsUserData
    };
  }

  function getDirectEvent(eventType) {
    return {
      "eventType": eventType,
      "DN":"1000",
      "otherDN":"1001",
      "linkedId":"1471858488.233",
      "uniqueId":"1471858490.236",
      "userData": wsUserData
    };
  }

  function successfullPromise(data) {
    var defer = $q.defer();
    defer.resolve(data);
    return defer.promise;
  }

  it('should call third party ws when call rings', function(){

    $httpBackend.expectPOST(wsUrl, wsPayload);
    Cti.Topic(Cti.MessageType.PHONEEVENT).publish(getEvent('EventRinging'));
    $httpBackend.flush();
    $httpBackend.verifyNoOutstandingExpectation();

  });

  it('should not call third party ws if not configured', function(){

    XucThirdParty.setThirdPartyWs(null);
    Cti.Topic(Cti.MessageType.PHONEEVENT).publish(getEvent('EventRinging'));
    $httpBackend.verifyNoOutstandingRequest(false);

  });

  it('should set current action when call is ringing', function(){
    var wsResponse = {
      action: "open",
      url: "http://dummy.com/open",
      event: "EventEstablished",
      autopause: false
    };

    $httpBackend.expectPOST(wsUrl, wsPayload).respond(wsResponse);
    Cti.Topic(Cti.MessageType.PHONEEVENT).publish(getEvent('EventRinging'));
    $httpBackend.flush();
    $rootScope.$digest();
    expect(XucThirdParty.getCurrentAction()).toEqual(wsResponse);

    $httpBackend.verifyNoOutstandingExpectation(false);

  });

  it('should clear action when asked', function(){
    var wsResponse = {
      action: "open",
      url: "http://dummy.com/open",
      event: "EventRinging",
      autopause: false
    };

    $httpBackend.expectPOST(wsUrl, wsPayload).respond(wsResponse);
    Cti.Topic(Cti.MessageType.PHONEEVENT).publish(getEvent('EventRinging'));
    $httpBackend.flush();
    $rootScope.$digest();
    XucThirdParty.clearAction();
    expect(XucThirdParty.getCurrentAction()).toEqual(undefined);

    $httpBackend.verifyNoOutstandingExpectation(false);

  });

  it('should notify subscriber of action when call is ringing', function(){
    var wsResponse = {
      action: "open",
      url: "http://dummy.com/open",
      event: "EventRinging",
      autopause: false
    };

    var handler = {
      onAction : function() {}
    };

    spyOn(handler, "onAction");

    $httpBackend.expectPOST(wsUrl, wsPayload).respond(wsResponse);
    XucThirdParty.addActionHandler($scope, handler.onAction);
    Cti.Topic(Cti.MessageType.PHONEEVENT).publish(getEvent('EventRinging'));
    $httpBackend.flush();
    $rootScope.$digest();
    expect(handler.onAction).toHaveBeenCalledWith(wsResponse);

    handler.onAction.calls.reset();
    Cti.Topic(Cti.MessageType.PHONEEVENT).publish(getEvent('EventEstablished'));
    $rootScope.$digest();
    expect(handler.onAction).not.toHaveBeenCalled();

    handler.onAction.calls.reset();
    Cti.Topic(Cti.MessageType.PHONEEVENT).publish(getEvent('EventReleased'));
    $rootScope.$digest();
    expect(handler.onAction).not.toHaveBeenCalled();
    $httpBackend.verifyNoOutstandingExpectation(false);
  });

  it('should notify subscriber of action when direct call is ringing (without queue)', function(){
    var wsResponse = {
      action: "open",
      url: "http://dummy.com/open",
      event: "EventRinging",
      autopause: false
    };

    var handler = {
      onAction : function() {}
    };

    spyOn(handler, "onAction");

    $httpBackend.expectPOST(wsUrl, wsPayloadDirect).respond(wsResponse);
    XucThirdParty.addActionHandler($scope, handler.onAction);
    Cti.Topic(Cti.MessageType.PHONEEVENT).publish(getDirectEvent('EventRinging'));
    $httpBackend.flush();
    $rootScope.$digest();
    expect(handler.onAction).toHaveBeenCalledWith(wsResponse);


  });

  it('should notify subscriber of action when call is established', function(){
    var wsResponse = {
      action: "open",
      url: "http://dummy.com/open",
      event: "EventEstablished",
      autopause: false
    };

    var handler = {
      onAction : function() {}
    };

    spyOn(handler, "onAction");

    $httpBackend.expectPOST(wsUrl, wsPayload).respond(wsResponse);
    XucThirdParty.addActionHandler($scope, handler.onAction);
    Cti.Topic(Cti.MessageType.PHONEEVENT).publish(getEvent('EventRinging'));
    $httpBackend.flush();
    $rootScope.$digest();
    expect(handler.onAction).not.toHaveBeenCalled();

    Cti.Topic(Cti.MessageType.PHONEEVENT).publish(getEvent('EventEstablished'));
    $rootScope.$digest();
    expect(handler.onAction).toHaveBeenCalledWith(wsResponse);

    handler.onAction.calls.reset();
    Cti.Topic(Cti.MessageType.PHONEEVENT).publish(getEvent('EventReleased'));
    $rootScope.$digest();
    expect(handler.onAction).not.toHaveBeenCalled();
    $httpBackend.verifyNoOutstandingExpectation();

  });

  it('should notify subscriber of action only once when call is established', function(){
    var wsResponse = {
      action: "open",
      url: "http://dummy.com/open",
      event: "EventEstablished",
      autopause: false
    };

    var handler = {
      onAction : function() {}
    };

    spyOn(handler, "onAction");

    $httpBackend.expectPOST(wsUrl, wsPayload).respond(wsResponse);
    XucThirdParty.addActionHandler($scope, handler.onAction);
    Cti.Topic(Cti.MessageType.PHONEEVENT).publish(getEvent('EventRinging'));
    $httpBackend.flush();
    $rootScope.$digest();
    expect(handler.onAction).not.toHaveBeenCalled();

    Cti.Topic(Cti.MessageType.PHONEEVENT).publish(getEvent('EventEstablished'));
    $rootScope.$digest();
    expect(handler.onAction).toHaveBeenCalledWith(wsResponse);

    handler.onAction.calls.reset();
    Cti.Topic(Cti.MessageType.PHONEEVENT).publish(getEvent('EventEstablished'));
    $rootScope.$digest();
    expect(handler.onAction).not.toHaveBeenCalled();
    $httpBackend.verifyNoOutstandingExpectation();

  });

  it('should notify subscriber of action when call is released', function(){
    var wsResponse = {
      action: "open",
      url: "http://dummy.com/open",
      event: "EventReleased",
      autopause: false
    };

    var handler = {
      onAction : function() {}
    };

    spyOn(handler, "onAction");

    $httpBackend.expectPOST(wsUrl, wsPayload).respond(wsResponse);
    XucThirdParty.addActionHandler($scope, handler.onAction);
    Cti.Topic(Cti.MessageType.PHONEEVENT).publish(getEvent('EventRinging'));
    $httpBackend.flush();
    $rootScope.$digest();
    expect(handler.onAction).not.toHaveBeenCalled();

    Cti.Topic(Cti.MessageType.PHONEEVENT).publish(getEvent('EventEstablished'));
    $rootScope.$digest();
    expect(handler.onAction).not.toHaveBeenCalled();

    Cti.Topic(Cti.MessageType.PHONEEVENT).publish(getEvent('EventReleased'));
    $rootScope.$digest();
    expect(handler.onAction).toHaveBeenCalledWith(wsResponse);
    $httpBackend.verifyNoOutstandingExpectation();

  });

  it('should notify subscriber of action when call is released but not if not established', function(){
    var wsResponse = {
      action: "open",
      url: "http://dummy.com/open",
      event: "EventReleased",
      autopause: false
    };

    var handler = {
      onAction : function() {}
    };

    spyOn(handler, "onAction");

    $httpBackend.expectPOST(wsUrl, wsPayload).respond(wsResponse);
    XucThirdParty.addActionHandler($scope, handler.onAction);
    Cti.Topic(Cti.MessageType.PHONEEVENT).publish(getEvent('EventRinging'));
    $httpBackend.flush();
    $rootScope.$digest();
    expect(handler.onAction).not.toHaveBeenCalled();

    Cti.Topic(Cti.MessageType.PHONEEVENT).publish(getEvent('EventReleased'));
    $rootScope.$digest();
    expect(handler.onAction).not.toHaveBeenCalled();
    $httpBackend.verifyNoOutstandingExpectation();

  });

  it('should pause agent if required', function(){
    var wsResponse = {
      action: "open",
      url: "http://dummy.com/open",
      event: "EventEstablished",
      autopause: true
    };

    spyOn(XucAgent, "pause");

    $httpBackend.expectPOST(wsUrl, wsPayload).respond(wsResponse);
    Cti.Topic(Cti.MessageType.PHONEEVENT).publish(getEvent('EventRinging'));
    $httpBackend.flush();
    $rootScope.$digest();

    Cti.Topic(Cti.MessageType.PHONEEVENT).publish(getEvent('EventEstablished'));
    $rootScope.$digest();
    expect(XucAgent.pause).toHaveBeenCalled();
    $httpBackend.verifyNoOutstandingExpectation();

  });

  it('should pause agent with specific reason if required', function(){
    var wsResponse = {
      action: "open",
      url: "http://dummy.com/open",
      event: "EventEstablished",
      autopause: true,
      autopauseReason: "testReason"
    };

    spyOn(XucAgent, "pauseWithReason");

    $httpBackend.expectPOST(wsUrl, wsPayload).respond(wsResponse);
    Cti.Topic(Cti.MessageType.PHONEEVENT).publish(getEvent('EventRinging'));
    $httpBackend.flush();
    $rootScope.$digest();

    Cti.Topic(Cti.MessageType.PHONEEVENT).publish(getEvent('EventEstablished'));
    $rootScope.$digest();
    expect(XucAgent.pauseWithReason).toHaveBeenCalledWith('testReason');
    $httpBackend.verifyNoOutstandingExpectation();
  });

  it('should unpause agent when clearing action', function(){
    var wsResponse = {
      action: "open",
      url: "http://dummy.com/open",
      event: "EventEstablished",
      autopause: true
    };

    spyOn(XucAgent, "unpause");

    $httpBackend.expectPOST(wsUrl, wsPayload).respond(wsResponse);
    Cti.Topic(Cti.MessageType.PHONEEVENT).publish(getEvent('EventRinging'));
    $httpBackend.flush();
    $rootScope.$digest();

    Cti.Topic(Cti.MessageType.PHONEEVENT).publish(getEvent('EventEstablished'));
    $rootScope.$digest();
    XucThirdParty.clearAction();
    expect(XucAgent.unpause).toHaveBeenCalled();
    $httpBackend.verifyNoOutstandingExpectation();

  });
});
