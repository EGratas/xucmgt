describe('Xuc service agent', function() {
  var xucAgent;
  var xucQueue;
  var $rootScope;

  beforeEach(function() {
    jasmine.addMatchers({
      toEqualData : customMatchers.toEqualData
    });
  });

  beforeEach(angular.mock.module('xcCti'));

  beforeEach(angular.mock.inject(function(_$rootScope_,_XucQueue_,_XucAgent_) {
    xucAgent = _XucAgent_;
    xucQueue = _XucQueue_;
    $rootScope = _$rootScope_;
    spyOn($rootScope, '$broadcast');
    spyOn(Cti,'subscribeToAgentStats');
    spyOn(xucAgent, 'buildMoment').and.callFake(function() {return {'momentStart': 32, 'timeInState' : 1234};});

  }));

  it('should have empty agents at init', function() {

    expect(xucAgent.getAgents()).toEqualData([]);
  });

  it('should init agent on agent config received', function() {

    var expectedAgent = {
      'id':3,
      'firstName' : '',
      'lastName' : '',
      'queueMembers' : [],
      'defaultMembership': [],
      'state' : 'AgentLoggedOut',
      'status' : '-',
      'stats' : {}
    };

    var agentConfig = {'id':3};
    xucAgent.onAgentConfig(agentConfig);

    expect(xucAgent.getAgents()[0]).toEqualData(expectedAgent);

  });

  it('should init agent on agent config list received and broadcast loaded event', function() {
    var agentList = [{'id':8, 'firstName' : 'Jack'},{'id':17, 'firstName' : 'Bill'}];
    xucAgent.onAgentList(agentList);

    expect(xucAgent.getAgent(8).firstName).toBe('Jack');
    expect(xucAgent.getAgents().length).toBe(2);
    expect($rootScope.$broadcast).toHaveBeenCalledWith('AgentsLoaded');
  });
  it('should get agents asynchronously', function() {
    var agentList = [{'id':8, 'firstName' : 'Jack'},{'id':17, 'firstName' : 'Bill'}];
    var queueMembers = [{'agentId' : 8, 'queueId' : 11, 'penalty' : 9 },{'agentId' : 8, 'queueId' : 4, 'penalty' : 2 }];

    var result = {};
    var cb = {getAgentCb: function(data) {result = data;}};
    spyOn(cb, "getAgentCb").and.callThrough();
    xucAgent.getAgentAsync(8).then(cb.getAgentCb);
    xucAgent.onAgentList(agentList);
    xucAgent.onQueueMemberList(queueMembers);
    $rootScope.$digest();
    expect(cb.getAgentCb).toHaveBeenCalled();
    expect(result.id).toBe(8);
    expect(result.firstName).toBe('Jack');
  });
  it('should remove agents not in config list received when set only by agent state event', function() {
    var agentState = {
      'name':'AgentOnCall',
      'acd' : true,
      'agentId':33,
      'phoneNb':null,
      'since':8749,
      'queues':[2,13],
      'cause':'outToLunch'};
    xucAgent.onAgentState(agentState);

    var agentList = [{'id':8, 'firstName' : 'Jack'}];

    xucAgent.onAgentList(agentList);

    expect(xucAgent.getAgent(33)).toBeNull();

  });
  
  it('should update agent on new config received', function(){
    var agentConfig = {'id':7, 'firstName' : 'Bob'};
    var updateAgentConfig = {'id':7, 'firstName' : 'Jack'};

    xucAgent.onAgentConfig(agentConfig);
    xucAgent.onAgentConfig(updateAgentConfig);

    expect(xucAgent.getAgent(7).firstName).toBe('Jack');
    expect(xucAgent.getAgents().length).toBe(1);

  });

  it('should update agent on new config received with previously received agent state', function(){
    var agentState = {
      'name':'AgentOnCall',
      'acd' : false,
      'agentId':128,
      'phoneNb':null,
      'since':8749,
      'queues':[2,13],
      'cause':'outToLunch'};

    xucAgent.onAgentState(agentState);
    var updateAgentConfig = {'id':128, 'firstName' : 'Jack', 'lastName':'Daniel'};

    xucAgent.onAgentConfig(updateAgentConfig);

    expect(xucAgent.getAgent(128).state).toBe('AgentOnCall');

  });

  it('should update agent on state event received', function(){
    var userStatuses = [
      {"name":"outToLunch","color":"#7D1707","longName":"Out To Lunch","actions":[{"name":"queuepause_all","parameters":"true"}]},
      {"name":"disconnected","color":"#202020","longName":"Déconnecté","actions":[{"name":"agentlogoff","parameters":""}]}
    ];
    xucAgent.onUserStatuses(userStatuses);

    var agentState = {
      'name':'AgentReady',
      'agentId':33,
      'phoneNb':'1000',
      'since':8749,
      'queues':[2,13],
      'cause':'outToLunch'};

    new MockAgentBuilder(33,'Jack','Henry').load(xucAgent);

    xucAgent.onAgentState(agentState);

    expect(xucAgent.getAgent(33).state).toBe('AgentReady');
    expect(xucAgent.getAgent(33).stateName).toBe('AgentReady');
    expect(xucAgent.getAgent(33).phoneNb).toBe('1000');
    expect(xucAgent.getAgent(33).queues).toEqualData([2,13]);
    expect(xucAgent.getAgent(33).status).toBe('Out To Lunch');
    expect(xucAgent.getAgent(33).momentStart).toBe(32);
    expect(xucAgent.getAgent(33).timeInState).toBe(1234);

    expect(xucAgent.buildMoment).toHaveBeenCalledWith(8749);

  });

  it('should update state name with status on AgentOnPause', function(){
    var userStatuses = [
      {"name":"outToLunch","color":"#7D1707","longName":"Out To Lunch","actions":[{"name":"queuepause_all","parameters":"true"}]},
      {"name":"disconnected","color":"#202020","longName":"Déconnecté","actions":[{"name":"agentlogoff","parameters":""}]}
    ];
    xucAgent.onUserStatuses(userStatuses);

    var agentState = {
      'name':'AgentOnPause',
      'agentId':33,
      'phoneNb':'1000',
      'since':8749,
      'queues':[2,13],
      'cause':'outToLunch'};

    var agent = new MockAgentBuilder(33,'Jack','Henry').load(xucAgent);

    xucAgent.onAgentState(agentState);

    expect(xucAgent.getAgent(33).stateName).toBe(agent.status);

  });
  it('should keep state name on AgentOnPause if status is empty', function(){
    var agentState = {
      'name':'AgentOnPause',
      'agentId':33,
      'phoneNb':'1000',
      'since':8749,
      'queues':[2,13],
      'cause':'outToLunch'};

    new MockAgentBuilder(33,'Jack','Henry').load(xucAgent);

    xucAgent.onAgentState(agentState);

    expect(xucAgent.getAgent(33).stateName).toBe('AgentOnPause');
  });
  it('should keep state name on AgentOnPause if status is not a not ready status', function(){
    var userStatuses = [
      {"name":"outToLunch","color":"#7D1707","longName":"Out To Lunch","actions":[{"name":"queuepause_all","parameters":"true"}]},
      {"name":"disconnected","color":"#202020","longName":"Déconnecté","actions":[{"name":"agentlogoff","parameters":""}]}
    ];
    xucAgent.onUserStatuses(userStatuses);

    var agentState = {
      'name':'AgentOnPause',
      'agentId':33,
      'phoneNb':'1000',
      'since':8749,
      'queues':[2,13],
      'cause':'disconnected'};

    new MockAgentBuilder(33,'Jack','Henry').load(xucAgent);

    xucAgent.onAgentState(agentState);

    expect(xucAgent.getAgent(33).stateName).toBe('AgentOnPause');
  });
  it('should update list of not ready statuses', function() {
    var userStatuses = [
      {"name":"outToLunch","color":"#7D1707","longName":"Out To Lunch","actions":[{"name":"queuepause_all","parameters":"true"}]},
      {"name":"invalidinput","color":"#7D1707","longName":"invalid input","actions":[{"name":"queuepause_all","parameters":"true"}]},
      {"name":"backFromLunch","color":"#7D1707","longName":"Back From Lunch","actions":[{"name":"queuepause_all","parameters":""}]},
      {"name":"disconnected","color":"#202020","longName":"Déconnecté","actions":[{"name":"agentlogoff","parameters":""}]}
    ];
    var notReadyStatuses = [
      {"name":"outToLunch","color":"#7D1707","longName":"Out To Lunch","actions":[{"name":"queuepause_all","parameters":"true"}]},
      {"name":"invalidinput","color":"#7D1707","longName":"invalid input","actions":[{"name":"queuepause_all","parameters":"true"}]},
      {"name":"backFromLunch","color":"#7D1707","longName":"Back From Lunch","actions":[{"name":"queuepause_all","parameters":""}]},
    ];

    xucAgent.onUserStatuses(userStatuses);

    expect(xucAgent.getNotReadyStatuses()).toEqualData(notReadyStatuses);
  });

  it('should update list of ready statuses', function() {
    var userStatuses = [
      {"name":"available","color":"#7D1707","longName":"Available","actions":[{"name":"queueunpause_all","parameters":"true"}]},
      {"name":"invalidinput","color":"#7D1707","longName":"invalid input","actions":[{"name":"queuepause_all","parameters":"true"}]},
      {"name":"available","color":"#7D1707","longName":"Available","actions":[{"name":"queueunpause_all","parameters":"true"}]}
    ];
    var readyStatuses = [
      {"name":"available","color":"#7D1707","longName":"Available","actions":[{"name":"queueunpause_all","parameters":"true"}]},
      {"name":"available","color":"#7D1707","longName":"Available","actions":[{"name":"queueunpause_all","parameters":"true"}]}
    ];
    xucAgent.onUserStatuses(userStatuses);

    expect(xucAgent.getReadyStatuses()).toEqualData(readyStatuses);
  });

  it('should default phone nb if null', function() {
    new MockAgentBuilder(33,'Jack','Henry').load(xucAgent);
    var agentState = {
      'name':'AgentReady',
      'agentId':33,
      'phoneNb':'null',
      'since':8749,
      'queues':[2,13],
      'cause':'outToLunch'};
    xucAgent.onAgentState(agentState);
    expect(xucAgent.getAgent(33).phoneNb).toBe('');

  });
  it('should default phone nb if undefined', function() {
    new MockAgentBuilder(33,'Jack','Henry').load(xucAgent);
    var agentState = {
      'name':'AgentReady',
      'agentId':33,
      'since':8749,
      'queues':[2,13],
      'cause':'outToLunch'};
    xucAgent.onAgentState(agentState);
    expect(xucAgent.getAgent(33).phoneNb).toBe('');

  });
  it('should default status if undefined or null', function() {
    new MockAgentBuilder(33,'Jack','Henry').load(xucAgent);
    var agentState = {
      'name':'AgentReady',
      'agentId':33,
      'since':8749,
      'queues':[2,13]};
    xucAgent.onAgentState(agentState);
    expect(xucAgent.getAgent(33).status).toBe('');

  });

  it('should state on acd call if agent on acd', function() {
    new MockAgentBuilder(33,'Jack','Henry').load(xucAgent);
    var agentState = {
      'name':'AgentOnCall',
      'acd' : true,
      'direction': 'Incoming',
      'agentId':33,
      'phoneNb':null,
      'since':8749,
      'queues':[2,13],
      'cause':'outToLunch'};
    xucAgent.onAgentState(agentState);
    expect(xucAgent.getAgent(33).state).toBe('AgentOnAcdCall');

  });
  it('should state on incoming call ', function() {
    new MockAgentBuilder(34,'Karl','Iann').load(xucAgent);
    var agentState = {
      'name':'AgentOnCall',
      'acd' : false,
      'direction': 'Incoming',
      'agentId':34,
      'phoneNb':null,
      'since':8749,
      'queues':[2,13],
      'cause':'outToLunch'};
    xucAgent.onAgentState(agentState);
    expect(xucAgent.getAgent(34).state).toBe('AgentOnIncomingCall');

  });
  //Outgoing
  it('should state on outgoing call ', function() {
    new MockAgentBuilder(35,'Louis','Jaune').load(xucAgent);
    var agentState = {
      'name':'AgentOnCall',
      'acd' : false,
      'direction': 'Outgoing',
      'agentId':35,
      'phoneNb':null,
      'since':8749,
      'queues':[2,13],
      'cause':'outToLunch'};
    xucAgent.onAgentState(agentState);
    expect(xucAgent.getAgent(35).state).toBe('AgentOnOutgoingCall');

  });
  it('should state on outgoing call even on acd', function() {
    new MockAgentBuilder(35,'Louis','Jaune').load(xucAgent);
    var agentState = {
      'name':'AgentOnCall',
      'acd' : true,
      'direction': 'Outgoing',
      'agentId':35,
      'phoneNb':null,
      'since':8749,
      'queues':[2,13],
      'cause':'outToLunch'};
    xucAgent.onAgentState(agentState);
    expect(xucAgent.getAgent(35).state).toBe('AgentOnOutgoingCall');

  });
  it('should update put on state event received on hold when agent not in store', function(){
    var agentState = {
      'name':'AgentOnCall',
      'acd' : true,
      'agentId':33,
      'phoneNb':null,
      'since':8749,
      'queues':[2,13],
      'cause':'outToLunch'};
    xucAgent.onAgentState(agentState);
    expect(xucAgent.getAndCopyAgentStatesWaitingForConfig(33).state).toBe('AgentOnAcdCall');
  });

  it('should broadcast AgentStateUpdated event on agent state received', function(){
    var agentState = {
      'name':'AgentOnCall',
      'acd' : true,
      'agentId':33,
      'phoneNb':null,
      'since':8749,
      'queues':[2,13],
      'cause':'outToLunch'};

    xucAgent.onAgentState(agentState);
    expect($rootScope.$broadcast).toHaveBeenCalledWith('AgentStateUpdated',33);

  });
  it('should update queue member and broadcast QueueMemberUpdated', function(){
    new MockAgentBuilder(24,'Bill','Mountain').load(xucAgent);
    var queueMember = {
      'agentId' : 24,
      'queueId' : 7,
      'penalty' : 12
    };
    xucAgent.onQueueMember(queueMember);

    expect(xucAgent.getAgent(24).queueMembers[7]).toBe(12);

    expect($rootScope.$broadcast).toHaveBeenCalledWith('QueueMemberUpdated',7);
  });
  it('should update queue member on queue member list received and broadcast QueueMemberUpdated for each queue', function(){
    new MockAgentBuilder(32,'Jack','Highway').load(xucAgent);
    var queueMembers = [{'agentId' : 32, 'queueId' : 11, 'penalty' : 9 },{'agentId' : 32, 'queueId' : 4, 'penalty' : 2 }];

    xucAgent.onQueueMemberList(queueMembers);

    expect(xucAgent.getAgent(32).queueMembers[11]).toBe(9);

    expect($rootScope.$broadcast).toHaveBeenCalledWith('QueueMemberUpdated',11);
    expect($rootScope.$broadcast).toHaveBeenCalledWith('QueueMemberUpdated',4);
  });

  it('should update default membership on queue member list received and broadcast DefaultMembershipUpdated for each queue', function(){
    var agent = new MockAgentBuilder(32,'Jack','Highway').load(xucAgent);
    var membership = {memberships: [{'userId' : agent.userId, membership: [{'queueId' : 11, 'penalty' : 9 },{'queueId' : 4, 'penalty' : 2 }]}]};

    xucAgent.onDefaultMemberList(membership);

    expect(xucAgent.getAgent(agent.id).defaultMembership[11]).toBe(9);
    expect(xucAgent.getAgent(agent.id).defaultMembership[4]).toBe(2);

    expect(xucAgent.getUser(agent.userId).defaultMembership[11]).toBe(9);
    expect(xucAgent.getUser(agent.userId).defaultMembership[4]).toBe(2);

    expect($rootScope.$broadcast).toHaveBeenCalledWith('DefaultMembershipUpdated',11);
    expect($rootScope.$broadcast).toHaveBeenCalledWith('DefaultMembershipUpdated',4);
  });

  it('should update default membership for agent and broadcast DefaultMembershipUpdated for each queue', function(){
    var agent = new MockAgentBuilder(32,'Jack','Highway').load(xucAgent);
    var membership1 = {'userId' : agent.userId, membership: [{'queueId' : 11, 'penalty' : 9 },{'queueId' : 4, 'penalty' : 2 }]};
    var membership2 = {'userId' : agent.userId, membership: [{'queueId' : 12, 'penalty' : 1 },{'queueId' : 4, 'penalty' : 3 }]};

    xucAgent.onDefaultMember(membership1);

    expect(xucAgent.getAgent(agent.id).defaultMembership[11]).toBe(9);
    expect(xucAgent.getAgent(agent.id).defaultMembership[4]).toBe(2);

    expect(xucAgent.getUser(agent.userId).defaultMembership[11]).toBe(9);
    expect(xucAgent.getUser(agent.userId).defaultMembership[4]).toBe(2);

    expect($rootScope.$broadcast).toHaveBeenCalledWith('DefaultMembershipUpdated',11);
    expect($rootScope.$broadcast).toHaveBeenCalledWith('DefaultMembershipUpdated',4);

    xucAgent.onDefaultMember(membership2);
    expect(xucAgent.getAgent(agent.id).defaultMembership[11]).toBeUndefined();
    expect(xucAgent.getAgent(agent.id).defaultMembership[12]).toBe(1);
    expect(xucAgent.getAgent(agent.id).defaultMembership[4]).toBe(3);

    expect(xucAgent.getUser(agent.userId).defaultMembership[11]).toBeUndefined();
    expect(xucAgent.getUser(agent.userId).defaultMembership[12]).toBe(1);
    expect(xucAgent.getUser(agent.userId).defaultMembership[4]).toBe(3);

    expect($rootScope.$broadcast).toHaveBeenCalledWith('DefaultMembershipUpdated',11);
    expect($rootScope.$broadcast).toHaveBeenCalledWith('DefaultMembershipUpdated',12);
    expect($rootScope.$broadcast).toHaveBeenCalledWith('DefaultMembershipUpdated',4);
  });

  it('should update DefaultMembershipIndicator correctly', function(){
    var agent = new MockAgentBuilder(32,'Jack','Highway').load(xucAgent);
    var defaultMembership = {'userId' : agent.userId, membership: [{'queueId' : 11, 'penalty' : 9 },{'queueId' : 4, 'penalty' : 2 }]};
    var queueMembers = [{'agentId' : agent.id, 'queueId' : 11, 'penalty' : 9 },{'agentId' : agent.id, 'queueId' : 4, 'penalty' : 2 }];
    
    xucAgent.onDefaultMember(defaultMembership);

    expect(xucAgent.getAgent(agent.id).defaultMembership[11]).toBe(9);
    expect(xucAgent.getAgent(agent.id).defaultMembership[4]).toBe(2);
    expect(xucAgent.getAgent(agent.id).matchDefaultMembership).toBe("DOES_NOT_MATCH_DEFAULT_MEMBERSHIP");
    
    xucAgent.onQueueMemberList(queueMembers);
    expect(xucAgent.getAgent(agent.id).queueMembers[11]).toBe(9);
    expect(xucAgent.getAgent(agent.id).queueMembers[4]).toBe(2);
    expect(xucAgent.getAgent(agent.id).matchDefaultMembership).toBe("MATCH_DEFAULT_MEMBERSHIP");
  });

  it('should update DefaultMembershipIndicator when queue removed from active configuration', function(){
    var agent = new MockAgentBuilder(32,'Jack','Highway').load(xucAgent);
    var defaultMembership = {'userId' : agent.userId, membership: [{'queueId' : 11, 'penalty' : 9 },{'queueId' : 4, 'penalty' : 2 }]};
    var queueMembers = [{'agentId' : agent.id, 'queueId' : 11, 'penalty' : 9 },{'agentId' : agent.id, 'queueId' : 4, 'penalty' : 2 }];
    
    xucAgent.onDefaultMember(defaultMembership);

    expect(xucAgent.getAgent(agent.id).defaultMembership[11]).toBe(9);
    expect(xucAgent.getAgent(agent.id).defaultMembership[4]).toBe(2);
    expect(xucAgent.getAgent(agent.id).matchDefaultMembership).toBe("DOES_NOT_MATCH_DEFAULT_MEMBERSHIP");
    
    xucAgent.onQueueMemberList(queueMembers);
    expect(xucAgent.getAgent(agent.id).queueMembers[11]).toBe(9);
    expect(xucAgent.getAgent(agent.id).queueMembers[4]).toBe(2);
    expect(xucAgent.getAgent(agent.id).matchDefaultMembership).toBe("MATCH_DEFAULT_MEMBERSHIP");

    xucAgent.onQueueMember({'agentId' : agent.id, 'queueId' : 20, 'penalty' : 0 });
    expect(xucAgent.getAgent(agent.id).matchDefaultMembership).toBe("DOES_NOT_MATCH_DEFAULT_MEMBERSHIP");

    xucAgent.onQueueMember({'agentId' : agent.id, 'queueId' : 20, 'penalty' : -1 });
    expect(xucAgent.getAgent(agent.id).matchDefaultMembership).toBe("MATCH_DEFAULT_MEMBERSHIP");
  });

  it ('should trigger queue member updated only once by queue id', function(){
    var queueMembers = [{'agentId' : 32, 'queueId' : 11, 'penalty' : 9 },{'agentId' : 4, 'queueId' : 11, 'penalty' : 2 }];

    xucAgent.onQueueMemberList(queueMembers);

    expect($rootScope.$broadcast).toHaveBeenCalledWith('QueueMemberUpdated',11);
    expect($rootScope.$broadcast.calls.count()).toBe(1);
  });
  
  it('should update waiting for config on queue member even if agent not in store', function(){
    var queueMember = {
      'agentId' : 56,
      'queueId' : 4,
      'penalty' : 7
    };
    xucAgent.onQueueMember(queueMember);

    expect(xucAgent.getAndCopyAgentStatesWaitingForConfig(56).queueMembers[4]).toBe(7);
  });

  it('agent store should be eventually consistent even if message received in wrong order', function(){
    var queueMember = {
      'agentId' : 32,
      'queueId' : 4,
      'penalty' : 7
    };
    var mock = new MockAgentBuilder(32,'Jack','Highway');
    var membership = {memberships: [{'userId' : mock.agent.userId, membership: [{'queueId' : 11, 'penalty' : 9 },{'queueId' : 4, 'penalty' : 2 }]}]};

    xucAgent.onDefaultMemberList(membership);
    xucAgent.onQueueMember(queueMember);
    mock.load(xucAgent);
    expect(xucAgent.getAgent(32).queueMembers[4]).toBe(7);
    expect(xucAgent.getAgent(32).defaultMembership[11]).toBe(9);
    expect(xucAgent.getAgent(32).defaultMembership[4]).toBe(2);
  });

  it('agent store should be eventually consistent and not mixup agents', function(){
    var mock1 = new MockAgentBuilder(32,'Jack','Highway');
    var mock2 = new MockAgentBuilder(17,'James','Bond');
    var queueMember1 = {
      'agentId' : mock1.agent.id,
      'queueId' : 4,
      'penalty' : 7
    };
    var queueMember2 = {
      'agentId' : mock2.agent.id,
      'queueId' : 5,
      'penalty' : 2
    };
    var membership1 = {memberships: [{'userId' : mock1.agent.userId, membership: [{'queueId' : 11, 'penalty' : 9 },{'queueId' : 4, 'penalty' : 2 }]}]};
    var membership2 = {memberships: [{'userId' : mock2.agent.userId, membership: [{'queueId' : 10, 'penalty' : 5 },{'queueId' : 4, 'penalty' : 0 }]}]};

    xucAgent.onDefaultMemberList(membership1);
    xucAgent.onDefaultMemberList(membership2);
    xucAgent.onQueueMember(queueMember1);
    xucAgent.onQueueMember(queueMember2);
    var agent1 = mock1.load(xucAgent);
    var agent2 = mock2.load(xucAgent);
    expect(xucAgent.getAgent(agent1.id).queueMembers[4]).toBe(7);
    expect(xucAgent.getAgent(agent1.id).defaultMembership[11]).toBe(9);
    expect(xucAgent.getAgent(agent1.id).defaultMembership[4]).toBe(2);

    expect(xucAgent.getAgent(agent2.id).queueMembers[5]).toBe(2);
    expect(xucAgent.getAgent(agent2.id).defaultMembership[10]).toBe(5);
    expect(xucAgent.getAgent(agent2.id).defaultMembership[4]).toBe(0);
  });

  it('should remove queue member on negative penalty', function(){
    new MockAgentBuilder(24,'Bill','Mountain')
      .load(xucAgent);
    var queueMember = {
      'agentId' : 24,
      'queueId' : 7,
      'penalty' : -1
    };
    xucAgent.onQueueMember(queueMember);

    expect(xucAgent.getAgent(24).queueMembers[7]).toBeUndefined();
  });
  it('should be able to return a list of agents not in a queue', function() {
    new MockAgentBuilder(24,'Bill','Mountain')
      .inQueue(7,5)
      .load(xucAgent);
    new MockAgentBuilder(32,'Jacl','Lam')
      .inQueue(6,2)
      .load(xucAgent);

    var agents = xucAgent.getAgentsNotInQueue(6);

    expect(agents[0]).toEqualData(xucAgent.getAgent(24));

  });
  it('should be able to return a list of agents in a queue', function() {
    new MockAgentBuilder(16,'Edile','Assoir')
      .inQueue(20,15)
      .load(xucAgent);
    new MockAgentBuilder(19,'Marie','Wast')
      .inQueue(45,3)
      .load(xucAgent);

    var agents = xucAgent.getAgentsInQueue(45);

    expect(agents[0]).toEqualData(xucAgent.getAgent(19));

  });

  it('should be able to return a list of agents in a group', function() {
    new MockAgentBuilder(24,'Bill','Mountain')
      .inGroup(2)
      .load(xucAgent);
    new MockAgentBuilder(32,'Jacl','Lam')
      .inGroup(2)
      .load(xucAgent);
    new MockAgentBuilder(2,'Out','Of Group')
      .inGroup(3)
      .load(xucAgent);

    var agents = xucAgent.getAgentsInGroup(2);

    expect(agents[0]).toEqualData(xucAgent.getAgent(24));
    expect(agents.length).toEqual(2);

  });

  it('should report an agent logged out to be able to log in', function(){

    new MockAgentBuilder(16,'Oscar','Martel')
      .onState('AgentLoggedOut')
      .load(xucAgent);

    expect(xucAgent.canLogIn(16)).toBe(true);

  });

  it('can log in an agent', function(){
    new MockAgentBuilder(22,'Pascal','Hotel')
      .onState('AgentLoggedOut')
      .onPhone('5600')
      .load(xucAgent);

    spyOn(Cti,'loginAgent');
    xucAgent.login(22);

    expect(Cti.loginAgent).toHaveBeenCalledWith('5600',22);
  });

  it('should report an agent ready to be able to log out', function(){
    new MockAgentBuilder(90,'Louis','Dor')
      .onState('AgentReady')
      .load(xucAgent);

    expect(xucAgent.canLogOut(90)).toBe(true);
  });
  it('should report an agent not ready to be able to log out', function(){
    new MockAgentBuilder(120,'Alfred','Wright')
      .onState('AgentOnPause')
      .load(xucAgent);

    expect(xucAgent.canLogOut(120)).toBe(true);
  });

  it('can log out a agent', function(){
    new MockAgentBuilder(54,'Remy','Isouar')
      .onState('AgentReady')
      .load(xucAgent);

    spyOn(Cti,'logoutAgent');

    xucAgent.logout(54);

    expect(Cti.logoutAgent).toHaveBeenCalledWith(54);

  });
  it('shoud report an agent ready to be able to pause', function(){
    new MockAgentBuilder(62,'Sophie','Juadim')
      .onState('AgentReady')
      .load(xucAgent);
    expect(xucAgent.canPause(62)).toBe(true);
  });
  it('shoud report an agent on wrapup to be able to pause', function(){
    new MockAgentBuilder(62,'Sophie','Juadim')
      .onState('AgentOnWrapup')
      .load(xucAgent);
    expect(xucAgent.canPause(62)).toBe(true);
  });

  it ('can pause an agent', function(){
    spyOn(Cti,'pauseAgent');

    xucAgent.pause(529);

    expect(Cti.pauseAgent).toHaveBeenCalledWith(529);
  });
  it ('can pause an agent with reason', function(){
    spyOn(Cti,'pauseAgent');

    xucAgent.pauseWithReason('testReason', 529);

    expect(Cti.pauseAgent).toHaveBeenCalledWith(529, 'testReason');
  });


  it('should report an agent not ready to be able to un pause', function(){
    new MockAgentBuilder(425,'Veronique','LeRouge')
      .onState('AgentOnPause')
      .load(xucAgent);
    expect(xucAgent.canUnPause(425)).toBe(true);
  });
  it('should report an agent on wrapup to be able to un pause', function(){
    new MockAgentBuilder(325,'Capitaine','Fracasse')
      .onState('AgentOnWrapup')
      .load(xucAgent);
    expect(xucAgent.canUnPause(325)).toBe(true);
  });

  it ('can un pause an agent', function(){
    spyOn(Cti,'unpauseAgent');

    xucAgent.unpause(819);

    expect(Cti.unpauseAgent).toHaveBeenCalledWith(819);
  });
  it('should report an agent on acd call to be able to be listened', function() {
    new MockAgentBuilder(521,'Walid','Moutil')
      .onState('AgentOnAcdCall')
      .load(xucAgent);
    expect(xucAgent.canListen(521)).toBe(true);
  });
  it('should report an agent on call to be able to be listened', function() {
    new MockAgentBuilder(521,'Walid','Moutil')
      .onState('AgentOnCall')
      .load(xucAgent);
    expect(xucAgent.canListen(521)).toBe(true);
  });
  it('should report an agent on incoming call to be able to be listened', function() {
    new MockAgentBuilder(521,'Walid','Moutil')
      .onState('AgentOnIncomingCall')
      .load(xucAgent);
    expect(xucAgent.canListen(521)).toBe(true);
  });
  it('should report an agent on outgoing call to be able to be listened', function() {
    new MockAgentBuilder(521,'Walid','Moutil')
      .onState('AgentOnOutgoingCall')
      .load(xucAgent);
    expect(xucAgent.canListen(521)).toBe(true);
  });
  it('should report an agent on dialing to be able to be listened', function() {
    new MockAgentBuilder(521,'Walid','Moutil')
      .onState('AgentDialing')
      .load(xucAgent);
    expect(xucAgent.canListen(521)).toBe(true);
  });
  it('should report an agent on ringing to be able to be listened', function() {
    new MockAgentBuilder(521,'Walid','Moutil')
      .onState('AgentRinging')
      .load(xucAgent);
    expect(xucAgent.canListen(521)).toBe(true);
  });
  it('should report a ready or paused agent to be able to be called', function() {
    new MockAgentBuilder(521,'Walid','Moutil')
      .onPhone('1001')      
      .onState('AgentReady')
      .load(xucAgent);
    new MockAgentBuilder(522,'James','Bond')
      .onPhone('1001')      
      .onState('AgentOnPause')
      .load(xucAgent);
    expect(xucAgent.canBeCalled(521)).toBe(true);
    expect(xucAgent.canBeCalled(522)).toBe(true);
  });
  it('should report a non ready agent to be not able to be called', function() {
    var testStates = ['AgentRinging', 'AgentDialing', 'AgentOnCall', 'AgentOnAcdCall', 'AgentOnIncomingCall','AgentOnOutgoingCall', 'AgentLoggedOut'];
    for(var i=0; i<testStates.length; i++) {
      new MockAgentBuilder(500 + i,'Walid','Moutil')
        .onPhone('100' + i)      
        .onState(testStates[i])
        .load(xucAgent);
      expect(xucAgent.canBeCalled(500 + i)).toBe(false);
    }
  });
  it('can listen to an agent', function(){
    spyOn(Cti,'listenAgent');

    xucAgent.listen(63);

    expect(Cti.listenAgent).toHaveBeenCalledWith(63);
  });
  it('can call an agent', function(){
    spyOn(Cti,'dial');
    new MockAgentBuilder(521,'Walid','Moutil')
      .onPhone('1001')      
      .onState('AgentReady')
      .load(xucAgent);

    xucAgent.callAgent(521);

    expect(Cti.dial).toHaveBeenCalledWith('1001');
  });
  it ('can remove agent from queues when updating queues', function(){
    spyOn(Cti,'removeAgentFromQueue');

    var updatedQueues = [{'id':7,'penalty':1}];
    var agentId = 324;
    var queueId = 5;
    new MockAgentBuilder(agentId,'Noemie','Ange')
      .inQueue(queueId,2)
      .inQueue(7,1)
      .load(xucAgent);
    xucAgent.updateQueues(agentId, updatedQueues);
    expect(Cti.removeAgentFromQueue).toHaveBeenCalledWith(agentId,queueId);
    expect(Cti.removeAgentFromQueue.calls.count()).toEqual(1);
  });
  it('can update penalty when updating queues', function(){
    spyOn(Cti,'setAgentQueue');
    var queueId = 7;
    var agentId = 43;
    var penalty = 9;
    var updatedQueues = [{'id':queueId,'penalty':penalty}];
    new MockAgentBuilder(agentId,'Noemie','Ange')
      .inQueue(queueId,2)
      .load(xucAgent);

    xucAgent.updateQueues(agentId, updatedQueues);

    expect(Cti.setAgentQueue).toHaveBeenCalledWith(agentId,queueId,penalty);

  });
  it('should not change anything if penalty did not change when updating queues', function(){
    spyOn(Cti,'setAgentQueue');
    var queueId = 234;
    var agentId = 43;
    var penalty = 2;
    var updatedQueues = [{'id':queueId,'penalty':penalty}];
    new MockAgentBuilder(agentId,'Noemie','Ange')
      .inQueue(queueId,penalty)
      .load(xucAgent);

    xucAgent.updateQueues(agentId, updatedQueues);

    expect(Cti.setAgentQueue.calls.count()).toEqual(0);
  });
  it('should add an agent in queue when updating queues', function(){
    spyOn(Cti,'setAgentQueue');
    var queueId = 745;
    var agentId = 439;
    var penalty = 12;
    var updatedQueues = [{'id':queueId,'penalty':penalty}];
    new MockAgentBuilder(agentId,'Noemie','Ange')
      .load(xucAgent);

    xucAgent.updateQueues(agentId, updatedQueues);

    expect(Cti.setAgentQueue).toHaveBeenCalledWith(agentId,queueId,penalty);
  });
  it('should return a list a agent in queue by name filter', function(){
    var qsales = QueueBuilder('sales','sales channel').build();
    var qsalesexpert = QueueBuilder('salesexpert','sales expert').build();
    var qsupport = QueueBuilder('support','support channel').build();

    var noemie = new MockAgentBuilder(100,'Noemie','Ange')
      .inQueue(qsales.id,0)
      .inQueue(qsupport.id,0)
      .load(xucAgent);
    var john = new MockAgentBuilder(200,'John','Degg')
      .inQueue(qsupport.id,0)
      .load(xucAgent);

    var jack = new MockAgentBuilder(300,'John','Degg')
      .inQueue(qsales.id,1)
      .inQueue(qsalesexpert.id,5)
      .load(xucAgent);

    spyOn(xucQueue,'getQueues').and.returnValue([qsales,qsupport,qsalesexpert]);

    var agents = xucAgent.getAgentsInQueues('sales');

    expect(agents).toContain(noemie);
    expect(agents).toContain(jack);
    expect(agents).not.toContain(john);
    expect(agents.length).toBe(2);

  });
  it('should return a list a agent in queue by list of ids', function(){
    var qsales = QueueBuilder('sales','sales channel').build();
    var qsalesexpert = QueueBuilder('salesexpert','sales expert').build();
    var qsupport = QueueBuilder('support','support channel').build();

    var noemie = new MockAgentBuilder(100,'Noemie','Ange')
      .inQueue(qsales.id,0)
      .inQueue(qsupport.id,0)
      .load(xucAgent);
    var john = new MockAgentBuilder(200,'John','Degg')
      .inQueue(qsales.id,0)
      .inQueue(qsupport.id,0)
      .load(xucAgent);

    var jack = new MockAgentBuilder(300,'John','Degg')
      .inQueue(qsalesexpert.id,5)
      .load(xucAgent);

    spyOn(xucQueue,'getQueueByIds').and.returnValue([qsales,qsupport]);

    var ids = [qsales.id,qsupport.id];
    var agents = xucAgent.getAgentsInQueueByIds(ids);

    expect(agents).toContain(noemie);
    expect(agents).toContain(john);
    expect(agents).not.toContain(jack);
    expect(agents.length).toBe(2);

  });
  it('should update agent statistics and broadcast event', function(){
    var noemie = new MockAgentBuilder(58,'Noemie','Ange')
      .load(xucAgent);

    var statistics = {'id':58, 'statistics':[{'name':'AgentReadyTotalTime', 'value' : 4389}]};

    xucAgent.onAgentStatistics(statistics);

    expect(noemie.stats.AgentReadyTotalTime).toBe(4389);
    expect($rootScope.$broadcast).toHaveBeenCalledWith('AgentStatisticsUpdated');

  });

  it('should update agent statistics and broadcast event even if there is a non existing agent', function(){
    var noemie = new MockAgentBuilder(58,'Noemie','Ange')
      .load(xucAgent);

    var statistics = {'id':55, 'statistics':[{'name':'AgentReadyTotalTime', 'value' : 4389}]};

    xucAgent.onAgentStatistics(statistics);
    statistics.id = 58;
    xucAgent.onAgentStatistics(statistics);

    expect(noemie.stats.AgentReadyTotalTime).toBe(4389);
    expect($rootScope.$broadcast).toHaveBeenCalledWith('AgentStatisticsUpdated');

  });

  it('should add agents to queues', function(){
    spyOn(Cti,'setAgentQueue');

    var marie = new MockAgentBuilder(77,'Marie','Belle').build();

    var q1 = QueueBuilder('sales','sales channel').build();

    xucAgent.addAgentsToQueues([marie], [{queue:q1, penalty:12}]);

    expect(Cti.setAgentQueue).toHaveBeenCalledWith(marie.id,q1.id, 12);
  });
  
  it('should remove agents from queues', function(){
    spyOn(Cti,'removeAgentFromQueue');

    var noe = new MockAgentBuilder(87,'Noe','Champs').build();

    var q1 = QueueBuilder('turn','turn channel').build();

    xucAgent.removeAgentsFromQueues([noe], [q1]);

    expect(Cti.removeAgentFromQueue).toHaveBeenCalledWith(noe.id,q1.id);
  });
  
});
