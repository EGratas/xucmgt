'use strict';

describe('Xuc sheet service', function() {
  let $window;
  let $rootScope;
  let xucSheet;
  let xucLink;
  let remoteConfiguration;
  let electronWrapper = {
    runExecutable: function(){}
  };
  let user = {
    username: "jbond",
    token: "aaaa-bbbb-cccc-dddd",
    phoneNumber: "1001"
  };
 

  beforeEach(angular.mock.module('xcCti'));
  
  beforeEach(angular.mock.module(function($provide) {
    let XucLinkProvider = {
      $get: function($q) {
        let loggedOutDefer = $q.defer();
        return {
          loggedOutDefer: loggedOutDefer,
          whenLogged: () => $q.resolve(user),
          whenLoggedOut: () => loggedOutDefer.promise
        };
      }
    };
    let electronWrapperProvider = {
      $get: function() {
        return electronWrapper;
      }
    };
    $provide.provider('XucLink', XucLinkProvider);
    $provide.provider('electronWrapper', electronWrapperProvider);
  }));

  beforeEach(angular.mock.inject(function(_$window_, _XucSheet_, _$rootScope_, _XucLink_, _remoteConfiguration_) {
    $window = _$window_;
    $rootScope = _$rootScope_;
    xucSheet = _XucSheet_;
    xucLink = _XucLink_;
    remoteConfiguration = _remoteConfiguration_;

    // Initialize XucLink by resolving promise
    $rootScope.$digest();
  }));

  it('should decode payload from sheet event ', function() {
    let payload = {'profile': {'user': {'fake': 'fake'}}};
    expect(xucSheet.decodePayload(payload)).toEqual({});

    payload = {'profile': {'user': {'sheetInfo': [{'name':'myName', 'value': 'myValue'}]}}};
    expect(xucSheet.decodePayload(payload)).toEqual({'myName': 'myValue'});
  });


  it('should popup window when url and mandatory fields are set ', function() {
    spyOn($window, 'open');
    spyOn(remoteConfiguration, 'isAgent').and.returnValue(true);

    let fields = {};
    xucSheet.openPopup(fields);
    expect($window.open).not.toHaveBeenCalled();

    fields["popupUrl"] = "http://localhost/";
    xucSheet.openPopup(fields);
    expect($window.open).not.toHaveBeenCalled();
    fields["folderNumber"] = "23678";

    xucSheet.openPopup(fields);
    expect($window.open).toHaveBeenCalledWith("http://localhost/23678", 'popupUrl');
  });

  it('should popup window in a different tab/window each time when multiTab parameter is set ', function() {
    let lastUrl, lastWindowName;
    spyOn($window, 'open').and.callFake((url, windowName) => {
      lastUrl = url;
      lastWindowName = windowName;
    });
    spyOn(remoteConfiguration, 'isAgent').and.returnValue(true);

    let fields = {};

    fields["popupUrl"] = "http://localhost/";
    fields["folderNumber"] = "987654";
    fields["multiTab"] = "true";

    xucSheet.openPopup(fields);
    let firstWindowName = lastWindowName;
    expect($window.open).toHaveBeenCalled();
    expect(lastUrl).toEqual('http://localhost/987654');

    xucSheet.openPopup(fields);
    let secondWindowName = lastWindowName;
    expect($window.open).toHaveBeenCalled();
    expect(lastUrl).toEqual('http://localhost/987654');
    expect(firstWindowName).not.toEqual(secondWindowName);
  });

  it('should not popup when folder number is empty or has default value', function() {
    spyOn($window, 'open');

    let fields = {};
    fields["folderNumber"] = "";
    fields["popupUrl"] = "http://localhost/";

    xucSheet.openPopup(fields);
    expect($window.open).not.toHaveBeenCalled();

    fields["folderNumber"] = "-";
    xucSheet.openPopup(fields);
    expect($window.open).not.toHaveBeenCalled();
  });

  it('should replace xuc-token and xuc-username variables in url ', function() {
    spyOn($window, 'open');
    spyOn(remoteConfiguration, 'isAgent').and.returnValue(true);
    let fields = {};    
    
    xucSheet.openPopup(fields);
    expect($window.open).not.toHaveBeenCalled();

    fields["popupUrl"] = "http://localhost/token={xuc-token}&username={xuc-username}&folder=";
    fields["folderNumber"] = "123";

    xucSheet.openPopup(fields);
    expect($window.open).toHaveBeenCalledWith("http://localhost/token=" + escape(user.token) + "&username=" + escape(user.username) + "&folder=123", 'popupUrl');
  });

  it('should update current fields when receiving sheet', function () {
    spyOn($rootScope, '$broadcast');

    let fields = {'myName1': 'myValue1', 'myName2': 'myValue2'};
    let payload = {'profile': {'user': {'sheetInfo': [{'name':'myName1', 'value': 'myValue1'}, {'name':'myName2', 'value': 'myValue2'}]}}};
    let sheet = {'payload': payload};
    Cti.Topic(Cti.MessageType.SHEET).publish(sheet);
    $rootScope.$digest();

    expect($rootScope.$broadcast).toHaveBeenCalledWith('SheetFieldUpdated', fields);
    expect(xucSheet.getAttachedData()).toEqual(fields);
  });

  it('should notify when receiving sheet', function () {
    var handler = {
      callback: function () {}
    };
    spyOn(handler, 'callback');

    let fields = {'myName': 'myValue'};
    let payload = {'profile': {'user': {'sheetInfo': [{'name':'myName', 'value': 'myValue'}]}}};
    let sheet = {'payload': payload};
    xucSheet.subscribeToAttachedData(handler.callback);
    Cti.Topic(Cti.MessageType.SHEET).publish(sheet);
    $rootScope.$digest();

    expect(handler.callback).toHaveBeenCalledWith(fields);
  });

  it('should reset current fields when receiving phone event released', function () {
    let fields = {'myName': 'myValue'};
    let payload = {'profile': {'user': {'sheetInfo': [{'name':'myName', 'value': 'myValue'}]}}};
    let sheet = {'payload': payload};
    Cti.Topic(Cti.MessageType.SHEET).publish(sheet);
    $rootScope.$digest();

    expect(xucSheet.getAttachedData()).toEqual(fields);

    Cti.Topic(Cti.MessageType.PHONEEVENT).publish({"eventType": "EventReleased"});
    $rootScope.$digest();

    expect(xucSheet.getAttachedData()).toEqual([]);
  });

  it('should reset current fields when logging out', angular.mock.inject(function ($q) {
    let fields = {'myName': 'myValue'};
    let payload = {'profile': {'user': {'sheetInfo': [{'name':'myName', 'value': 'myValue'}]}}};
    let sheet = {'payload': payload};
    Cti.Topic(Cti.MessageType.SHEET).publish(sheet);
    $rootScope.$digest();

    expect(xucSheet.getAttachedData()).toEqual(fields);
    spyOn(xucLink, 'whenLogged').and.returnValue($q.defer().promise);
    xucLink.loggedOutDefer.resolve(true);
    $rootScope.$digest();

    expect(xucSheet.getAttachedData()).toEqual([]);
  }));

  it('should run executable when asked to ', function() {
    spyOn(electronWrapper, 'runExecutable');
    spyOn(remoteConfiguration, 'isAgent').and.returnValue(true);
    let fields = {};
    fields["popupUrl"] = "executable";
    fields["runAsExecutable"] = "true";
    fields["executableArgs"] = "tutu";
    
    xucSheet.openPopup(fields);
    expect(electronWrapper.runExecutable).toHaveBeenCalledWith(fields["popupUrl"], [fields["executableArgs"]]);
  });

  it('should run executable with no argument ', function() {
    spyOn(electronWrapper, 'runExecutable');
    spyOn(remoteConfiguration, 'isAgent').and.returnValue(true);
    let fields = {};
    fields["popupUrl"] = "executable";
    fields["runAsExecutable"] = "true";
    
    xucSheet.openPopup(fields);
    expect(electronWrapper.runExecutable).toHaveBeenCalledWith(fields["popupUrl"], []);
  });

  it('opens the popup by default on CCagent', function() {
    spyOn(remoteConfiguration, 'isAgent').and.returnValue(true);
    spyOn($window, 'open');
    let fields = {};
    fields["popupUrl"] = "http://localhost/";
    fields["folderNumber"] = "987654";

    xucSheet.openPopup(fields);
    expect($window.open).toHaveBeenCalled();
  });

  it('opens the popup on CCagent when the variable is set to something else than "false"', function() {
    spyOn(remoteConfiguration, 'isAgent').and.returnValue(true);
    let fields = {};
    fields["popupAgentActivated"] = "true";
    fields["popupAgentActivated"] = "undefined";

    expect(xucSheet.showPopupAgent(fields)).toBeTruthy();
  });

  it('should not open the popup on CCagent when the variable is set to "false"', function() {
    spyOn(remoteConfiguration, 'isAgent').and.returnValue(true);
    let fields = {};
    fields["popupAgentActivated"] = "false";

    expect(xucSheet.showPopupAgent(fields)).toBeFalsy();
  });

  it('opens the popup on UCassistant when the variable is set to "true"', function() {
    let fields = {};
    fields["popupUCActivated"] = "true";
  
    expect(xucSheet.showPopupAssistant(fields)).toBeTruthy();
  });

  it('should not open the popup on UCassistant when the variable is set to something else than "true"', function() {
    let fields = {};
    fields["popupUCActivated"] = "undefined";

    expect(xucSheet.showPopupAssistant(fields)).toBeFalsy();
  });


});
