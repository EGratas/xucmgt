import moment from 'moment';
import _ from 'lodash';

'use strict';
describe('Xuc phoneState', function() {
  var phoneState;
  var phoneEventListener;
  var $rootScope;

  function createEvent(eventType, DN, otherDN, linkedId, otherDName, callDirection, callType) {
    return {
      "eventType": eventType,
      "DN": DN,
      "otherDN": otherDN,
      "otherDName": otherDName,
      "linkedId": linkedId,
      "uniqueId": linkedId,
      "callDirection": callDirection,
      "callType": callType || xc_webrtc.mediaType.AUDIO
    };
  }

  function createConfEvent(eventType, uniqueId, confNum, confName, participants, since) {
    if(typeof(participants) === "undefined") {
      participants = [];
    }
    if(typeof(since) === "undefined") {
      since = 0;
    }

    return  {
      "eventType": eventType,
      "uniqueId": uniqueId,
      "conferenceNumber": confNum,
      "conferenceName": confName,
      "participants": participants,
      "since": since
    };
  }

  function createConfParticipantEvent(eventType, uniqueId, confNum, participant) {
    return  {
      "eventType": eventType,
      "uniqueId": uniqueId,
      "conferenceNumber": confNum,
      "index": participant.index,
      "callerIdName": participant.callerIdName,
      "callerIdNum": participant.callerIdNum,
      "since": participant.since,
      "isTalking": participant.isTalking,
      "isMuted": participant.isMuted,
      "role": participant.role,
      "isMe": participant.isMe
    };
  }

  function createConfParticipant(index, number, name, since, isTalking = false, role="User", isMuted = false, isMe = false) {
    return {
      "index": index,
      "callerIdName": name,
      "callerIdNum": number,
      "since": since,
      "isTalking": isTalking,
      "isMuted": isMuted,
      "role": role,
      "isMe": isMe
    };
  }

  function addQueueToEvent(event, queue) {
    event.queueName = queue;
    return event;
  }

  function publishEvent(event) {
    Cti.Topic(Cti.MessageType.PHONEEVENT).publish(event);
    $rootScope.$digest();
  }

  function publishConfEvent(event) {
    Cti.Topic(Cti.MessageType.CONFERENCEEVENT).publish(event);
  }

  function publishConfParticipantEvent(event) {
    Cti.Topic(Cti.MessageType.CONFERENCEPARTICIPANTEVENT).publish(event);
  }

  beforeEach(function() {
    jasmine.addMatchers({
      toEqualData : customMatchers.toEqualData
    });
  });

  beforeEach(angular.mock.module('xcCti'));

  beforeEach(angular.mock.inject(function(_XucPhoneState_,_XucPhoneEventListener_,_$rootScope_) {
    phoneState = _XucPhoneState_;
    phoneEventListener = _XucPhoneEventListener_;
    $rootScope = _$rootScope_;
    spyOn(Cti, 'sendCallback');
    spyOn(phoneEventListener, 'requestPhoneEventsForCurrentCalls');
    Cti.Topic(Cti.MessageType.PHONEEVENT).clear();
    Cti.Topic(Cti.MessageType.CONFERENCEEVENT).clear();
    Cti.Topic(Cti.MessageType.CONFERENCEPARTICIPANTEVENT).clear();

    // Initialize XucLink
    Cti.Topic(Cti.MessageType.LOGGEDON).publish("");
    $rootScope.$digest();

    spyOn($rootScope, '$broadcast');
  }));

  it('should ask for last phone events on init', function() {
    expect(phoneEventListener.requestPhoneEventsForCurrentCalls).toHaveBeenCalled();
  });

  it('function isPhoneAvailable should return true if called with phone state "Available"', function() {
    expect(phoneState.isPhoneAvailable('Available')).toEqualData(true);
    expect(phoneState.isPhoneAvailable('Ringing')).toEqualData(false);
  });

  it('function isPhoneOffHook should return true if called with one of phone states off hook', function() {
    expect(phoneState.isPhoneOffHook('Available')).toEqualData(false);
    expect(phoneState.isPhoneOffHook('OnHold')).toEqualData(true);
    expect(phoneState.isPhoneOffHook('Dialing')).toEqualData(true);
    expect(phoneState.isPhoneOffHook('Established')).toEqualData(true);
    expect(phoneState.isPhoneOffHook('QWERTY')).toEqualData(false);
  });

  it('should record line state when call is ringing', function() {
    var event = createEvent('EventRinging', "1000", "1002", "1471858490.236", "Jane Doe", "Incoming", xc_webrtc.mediaType.AUDIO);
    publishEvent(event);

    var calls = phoneState.getCalls();
    expect(calls.length).toEqual(1);
    expect(calls[0].uniqueId).toEqual(event.uniqueId);
    expect(calls[0].linkedId).toEqual(event.linkedId);
    expect(calls[0].state).toEqual("Ringing");
    expect(calls[0].DN).toEqual(event.DN);
    expect(calls[0].otherDN).toEqual(event.otherDN);
    expect(calls[0].otherDName).toEqual(event.otherDName);
    expect(calls[0].direction).toEqual(event.callDirection);
    expect(calls[0].mediaType).toEqual(event.callType);
  });

  it('should record otherDN even if some event do not contains them', function() {
    var event = createEvent('EventRinging', "1000", "1002", "1471858490.236", "Peter Smith", "Incoming");
    var eventNoDN = createEvent('EventRinging', "1000", "", "1471858490.236", "", "Incoming");
    publishEvent(event);
    publishEvent(eventNoDN);

    var calls = phoneState.getCalls();
    expect(calls[0].otherDN).toEqual(event.otherDN);
    expect(calls[0].otherDName).toEqual(event.otherDName);
  });

  it('should record line state when call is in any state except Available', function() {
    var eventTypes = ['EventRinging', 'EventEstablished', 'EventDialing', 'EventOnHold'];

    _.forEach(eventTypes, function(eventType) {
      var event = createEvent(eventType, "1000", "1002", "1471858490.236", "Dee Dee Bridgewater", "DirectionUnknown");
      publishEvent(event);

      var calls = phoneState.getCalls();
      expect(calls.length).toEqual(1);
      expect(calls[0].uniqueId).toEqual(event.uniqueId);
      expect(calls[0].linkedId).toEqual(event.linkedId);
      expect(calls[0].state).toEqual(eventType.substring(5));
      expect(calls[0].DN).toEqual(event.DN);
      expect(calls[0].otherDN).toEqual(event.otherDN);
      expect(calls[0].otherDName).toEqual(event.otherDName);
      expect(calls[0].direction).toEqual(event.callDirection);
    });

  });

  it('should discard line state when call is released', function() {
    var ringingEvent = createEvent('EventRinging', "1000", "1002", "1471858490.236", "John Black", "Incoming");
    var releaseEvent = createEvent('EventReleased', ringingEvent.DN, ringingEvent.otherDN, ringingEvent.linkedId,
      ringingEvent.otherDName, ringingEvent.direction);
    var calls;

    publishEvent(ringingEvent);
    calls = phoneState.getCalls();
    expect(calls.length).toEqual(1);

    publishEvent(releaseEvent);
    calls = phoneState.getCalls();
    expect(calls.length).toEqual(0);

  });

  it('should track two calls established', function() {
    var line1Event = createEvent('EventEstablished', "1000", "1001", "1471858490.236", "Ann", "Incoming");
    var line2Event = createEvent('EventEstablished', "1000", "1002", "1471859840.127", "Bob", "Outgoing");
    var calls;

    publishEvent(line1Event);
    calls = phoneState.getCalls();
    expect(calls.length).toEqual(1);

    publishEvent(line2Event);
    calls = phoneState.getCalls();
    expect(calls.length).toEqual(2);
  });


  it('should remove one of two identical calls established', function() {
    var line1Event = createEvent('EventEstablished', "1000", "1001", "1471858490.236", "Ann", "Incoming");
    var line2Event = createEvent('EventEstablished', "1000", "1001", "1471858490.236", "Ann", "Outgoing");
    var calls;

    publishEvent(line1Event);
    publishEvent(line2Event);
    calls = phoneState.getCalls();
    expect(calls.length).toEqual(1);
  });


  it('should untrack correct call when one of two calls is hung up', function() {
    var line1Event = createEvent('EventEstablished', "1000", "1001", "1471858490.236", "Ann", "Incoming");
    var line2Event = createEvent('EventEstablished', "1000", "1002", "1471859840.127", "Bob", "Incoming");
    var hangup1Event = createEvent('EventReleased', "1000", "1001", "1471858490.236", "Ann", "Incoming");
    var calls;

    publishEvent(line1Event);
    publishEvent(line2Event);

    calls = phoneState.getCalls();
    expect(calls.length).toEqual(2);

    publishEvent(hangup1Event);
    calls = phoneState.getCalls();
    expect(calls.length).toEqual(1);
    expect(calls[0].linkedId).toEqual(line2Event.linkedId);
  });

  it('should clear call state when disconnected', function() {
    var line1Event = createEvent('EventEstablished', "1000", "1001", "1471858490.236", "Ann", "Incoming");
    var calls;

    publishEvent(line1Event);
    calls = phoneState.getCalls();
    expect(calls.length).toEqual(1);

    Cti.Topic(Cti.MessageType.LINKSTATUSUPDATE).publish({status: 'closed'});
    $rootScope.$apply();
    calls = phoneState.getCalls();
    expect(calls.length).toEqual(0);
  });

  it('should Notify of ringing even when reconnected', function() {

    Cti.Topic(Cti.MessageType.LINKSTATUSUPDATE).publish({status: 'closed'});
    Cti.Topic(Cti.MessageType.PHONEEVENT).clear();

    // Re-Initialize XucLink
    Cti.Topic(Cti.MessageType.LOGGEDON).publish("");
    $rootScope.$digest();


    var event = createEvent('EventRinging', "1000", "1002", "1471858490.236", "Ann", "Incoming");
    publishEvent(event);

    var calls = phoneState.getCalls();
    expect(calls.length).toEqual(1);
    expect(calls[0].uniqueId).toEqual(event.uniqueId);
    expect(calls[0].linkedId).toEqual(event.linkedId);
    expect(calls[0].state).toEqual("Ringing");
    expect(calls[0].DN).toEqual(event.DN);
    expect(calls[0].otherDN).toEqual(event.otherDN);
    expect(calls[0].otherDName).toEqual(event.otherDName);
  });

  it('should track call start time only when call is established', function() {
    var event = createEvent('EventDialing', "1000", "1002", "1471858490.236", "Jane Doe", "Outgoing");
    publishEvent(event);

    var calls = phoneState.getCalls();
    expect(calls.length).toEqual(1);
    expect(calls[0].startTime).toBeUndefined();

    event = createEvent('EventEstablished', "1000", "1002", "1471858490.236", "Jane Doe", "Outgoing");
    publishEvent(event);
    calls = phoneState.getCalls();
    
    expect(calls.length).toEqual(1);
    expect(calls[0].startTime).not.toBeUndefined();
    var delay = Date.now() - calls[0].startTime;
    expect(delay).toBeLessThan(100);
  });

  it('should say if already in one call', function() {
    var event = createEvent('EventDialing', "1000", "1002", "1471858490.236", "Jane Doe", "Outgoing");
    publishEvent(event);
    expect(phoneState.isInCall()).toBe(true);
    publishEvent(createEvent('EventReleased', "1000", "1002", "1471858490.236", "Jane Doe", "Outgoing"));
    expect(phoneState.isInCall()).toBe(false);
  });

  it('should init from currentCallsPhoneEvents', function() {
    var currentCallsPhoneEvents = { events: [
      createEvent('EventEstablished', "1000", "1002", "1471858490.236", "Jane Doe", "Incoming"),
      createEvent('EventRinging', "1000", "1003", "1471858491.888", "Tom", "Outgoing")
    ]};
    Cti.Topic(Cti.MessageType.CURRENTCALLSPHONEEVENTS).publish(currentCallsPhoneEvents);
    $rootScope.$digest();
    expect(phoneState.isInCall()).toBe(true);

    var calls = phoneState.getCalls();
    expect(calls.length).toEqual(2);
    expect(calls[0].direction).toEqual("Incoming");
    expect(calls[1].direction).toEqual("Outgoing");
  });

  it('should set ACD to true if queue is available in PhoneEvent, to false otherwise', function() {
    var event = createEvent('EventRinging', "1000", "1002", "1471858490.236", "Jane Doe", "Incoming");
    publishEvent(event);
    expect(phoneState.getCalls()[0].acd).toEqual(false);

    publishEvent(addQueueToEvent(event, 'green'));
    expect(phoneState.getCalls()[0].acd).toEqual(true);
  });

  it('should add conference information to call when joining a conference', function() {
    var callEvent = createEvent('EventEstablished', "1000", "4000", "1471858490.236", "My Conf Room", "Outgoing");
    publishEvent(callEvent);
    var p1 = createConfParticipant(1, "1001", "Some other dude", 10);
    var p2 = createConfParticipant(2, "1000", "Hey that's me !", 0, true);
    var confEvent = createConfEvent("Join", "1471858490.236", "4000", "My Conf Room", [p1, p2], 10);

    publishConfEvent(confEvent);
    
    var confCall = phoneState.getCalls()[0].conference;
    expect(confCall.conferenceName).toEqual("My Conf Room");
    expect(confCall.conferenceNumber).toEqual("4000");
    expect(confCall.participants).toEqual([p1, p2]);
  });

  it('should remove conference information from call when leaving a conference', function() {
    var callEvent = createEvent('EventEstablished', "1000", "4000", "1471858490.236", "My Conf Room", "Outgoing");
    publishEvent(callEvent);
    var p1 = createConfParticipant(1, "1001", "Some other dude", 10);
    var p2 = createConfParticipant(2, "1000", "Hey that's me !", 0);
    var joinConfEvent = createConfEvent("Join", "1471858490.236", "4000", "My Conf Room", [p1, p2], 10);
    publishConfEvent(joinConfEvent);

    var leaveConfEvent = createConfEvent("Leave", "1471858490.236", "4000", "My Conf Room", [p1, p2], 10);
    publishConfEvent(leaveConfEvent);
    
    expect(phoneState.getCalls()[0].conference).toBeUndefined();
  });

  it('should update conference participants when someone join the conference', function() {
    var callEvent = createEvent('EventEstablished', "1000", "4000", "1471858490.236", "My Conf Room", "Outgoing");
    publishEvent(callEvent);
    var p1 = createConfParticipant(1, "1000", "Hey that's me !", 0);
    var joinConfEvent = createConfEvent("Join", "1471858490.236", "4000", "My Conf Room", [p1], 10);
    publishConfEvent(joinConfEvent);

    var p2 = createConfParticipant(2, "1001", "Some other dude", 0, true);
    var p2JoinEvent = createConfParticipantEvent("Join", "1471858490.236", "4000", p2);
    publishConfParticipantEvent(p2JoinEvent);

    var confCall = phoneState.getCalls()[0].conference;
    expect(confCall.conferenceName).toEqual("My Conf Room");
    expect(confCall.conferenceNumber).toEqual("4000");
    expect(confCall.participants.length).toEqual(2);
    expect(confCall.participants[0].callerIdName).toEqual(p1.callerIdName);
    expect(confCall.participants[0].callerIdNum).toEqual(p1.callerIdNum);
    expect(moment().diff(confCall.participants[0].startTime, "seconds")).toEqual(0);

    expect(confCall.participants[1].callerIdName).toEqual(p2.callerIdName);
    expect(confCall.participants[1].callerIdNum).toEqual(p2.callerIdNum);
    expect(moment().diff(confCall.participants[1].startTime, "seconds")).toEqual(0);
    expect(confCall.participants[1].isTalking).toEqual(p2.isTalking);
    
  });


  it('should update conference participants when someone leave the conference', function() {
    var callEvent = createEvent('EventEstablished', "1000", "4000", "1471858490.236", "My Conf Room", "Outgoing");
    publishEvent(callEvent);
    var p1 = createConfParticipant(1, "1001", "Some other dude", 10);
    var p2 = createConfParticipant(2, "1000", "Hey that's me !", 0);
    var joinConfEvent = createConfEvent("Join", "1471858490.236", "4000", "My Conf Room", [p1, p2], 10);
    publishConfEvent(joinConfEvent);

    var p2LeaveEvent = createConfParticipantEvent("Leave", "1471858490.236", "4000", p2);
    publishConfParticipantEvent(p2LeaveEvent);

    var confCall = phoneState.getCalls()[0].conference;
    expect(confCall.conferenceName).toEqual("My Conf Room");
    expect(confCall.conferenceNumber).toEqual("4000");
    expect(confCall.participants.length).toEqual(1);
    expect(confCall.participants[0].callerIdName).toEqual(p1.callerIdName);
    expect(confCall.participants[0].callerIdNum).toEqual(p1.callerIdNum);
    expect(moment().diff(confCall.participants[0].startTime, "seconds")).toEqual(10);
  });

  it('should not crash when when leaving a conference and receiving a conference participant event', function() {
    var callEvent = createEvent('EventEstablished', "1000", "4000", "1471858490.236", "My Conf Room", "Outgoing");
    publishEvent(callEvent);
    var p1 = createConfParticipant(1, "1001", "Some other dude", 10);
    var p2 = createConfParticipant(2, "1000", "Hey that's me !", 0);
    var joinConfEvent = createConfEvent("Join", "1471858490.236", "4000", "My Conf Room", [p1, p2], 10);
    publishConfEvent(joinConfEvent);

    var leaveConfEvent = createConfEvent("Leave", "1471858490.236", "4000", "My Conf Room", [p1, p2], 10);
    publishConfEvent(leaveConfEvent);

    var p2LeaveEvent = createConfParticipantEvent("Leave", "1471858490.236", "4000", p2);
    publishConfParticipantEvent(p2LeaveEvent);
    
    expect(phoneState.getCalls()[0].conference).toBeUndefined();
  });

  it('should update the conference participant when someone begin to speak', function () {
    var callEvent = createEvent('EventEstablished', "1000", "4000", "1471858490.236", "My Conf Room", "Outgoing");
    publishEvent(callEvent);
    var p1 = createConfParticipant(1, "1001", "Some other dude", 0);
    var p2 = createConfParticipant(2, "1000", "Hey that's me !", 0);
    var joinConfEvent = createConfEvent("Join", "1471858490.236", "4000", "My Conf Room", [p1, p2], 10);
    publishConfEvent(joinConfEvent);

    var p1Talking = createConfParticipant(1, "1001", "Some other dude", 0, true);

    var updateConfParticipantEvent = createConfParticipantEvent("Update", "1471858490.236", "4000", p1Talking);
    publishConfParticipantEvent(updateConfParticipantEvent);

    var confCall = phoneState.getCalls()[0].conference;
    expect(confCall.conferenceName).toEqual("My Conf Room");
    expect(confCall.conferenceNumber).toEqual("4000");
    expect(confCall.participants.length).toEqual(2);
    expect(confCall.participants[0].callerIdName).toEqual(p1.callerIdName);
    expect(confCall.participants[0].callerIdNum).toEqual(p1.callerIdNum);
    expect(moment().diff(confCall.participants[0].startTime, "seconds")).toEqual(0);
    expect(confCall.participants[0].isTalking).toEqual(true);

    expect(confCall.participants[1].callerIdName).toEqual(p2.callerIdName);
    expect(confCall.participants[1].callerIdNum).toEqual(p2.callerIdNum);
    expect(moment().diff(confCall.participants[1].startTime, "seconds")).toEqual(0);
    expect(confCall.participants[1].isTalking).toEqual(false);
  });

  it('should update the conference participant when someone stop to speak', function () {
    var callEvent = createEvent('EventEstablished', "1000", "4000", "1471858490.236", "My Conf Room", "Outgoing");
    publishEvent(callEvent);
    var p1 = createConfParticipant(1, "1001", "Some other dude", 0, true);
    var p2 = createConfParticipant(2, "1000", "Hey that's me !", 0);
    var joinConfEvent = createConfEvent("Join", "1471858490.236", "4000", "My Conf Room", [p1, p2], 10);
    publishConfEvent(joinConfEvent);

    var p1StopTalking = createConfParticipant(1, "1001", "Some other dude", 0);

    var updateConfParticipantEvent = createConfParticipantEvent("Update", "1471858490.236", "4000", p1StopTalking);
    publishConfParticipantEvent(updateConfParticipantEvent);

    var confCall = phoneState.getCalls()[0].conference;
    expect(confCall.conferenceName).toEqual("My Conf Room");
    expect(confCall.conferenceNumber).toEqual("4000");
    expect(confCall.participants.length).toEqual(2);

    expect(confCall.participants[0].callerIdName).toEqual(p1.callerIdName);
    expect(confCall.participants[0].callerIdNum).toEqual(p1.callerIdNum);
    expect(moment().diff(confCall.participants[0].startTime, "seconds")).toEqual(0);
    expect(confCall.participants[0].isTalking).toEqual(false);

    expect(confCall.participants[1].callerIdName).toEqual(p2.callerIdName);
    expect(confCall.participants[1].callerIdNum).toEqual(p2.callerIdNum);
    expect(moment().diff(confCall.participants[1].startTime, "seconds")).toEqual(0);
    expect(confCall.participants[1].isTalking).toEqual(false);
  });

  it('should store the current user role in the conference', function() {
    var callEvent = createEvent('EventEstablished', "1000", "4000", "1471858490.236", "My Conf Room", "Outgoing");
    publishEvent(callEvent);
    var p1 = createConfParticipant(1, "1001", "Some other dude", 0, false, "User", false, true);
    var joinConfEvent = createConfEvent("Join", "1471858490.236", "4000", "My Conf Room", [p1], 10);
    publishConfEvent(joinConfEvent);

    var confCall = phoneState.getCalls()[0].conference;
    expect(confCall.participants[0].role).toEqual("User");
    expect(confCall.currentUserRole).toEqual("User");

    var p1IsOrganizer = createConfParticipant(1, "1001", "Some other dude", 0, false, "Organizer", false, true);
    var updateConfParticipantEvent = createConfParticipantEvent("Update", "1471858490.236", "4000", p1IsOrganizer);
    publishConfParticipantEvent(updateConfParticipantEvent);

    confCall = phoneState.getCalls()[0].conference;
    expect(confCall.participants[0].role).toEqual("Organizer");
    expect(confCall.currentUserRole).toEqual("Organizer");
  });
  
  it('should update the conference participant when someone becomes muted', function () {
    var callEvent = createEvent('EventEstablished', "1000", "4000", "1471858490.236", "My Conf Room", "Outgoing");
    publishEvent(callEvent);
    var p1 = createConfParticipant(1, "1001", "Some other dude", 0, false, "User");
    var joinConfEvent = createConfEvent("Join", "1471858490.236", "4000", "My Conf Room", [p1], 10);
    publishConfEvent(joinConfEvent);

    var p1Updated = createConfParticipant(1, "1001", "Some other dude", 0, false, "User", true);

    var updateConfParticipantEvent = createConfParticipantEvent("Update", "1471858490.236", "4000", p1Updated);
    publishConfParticipantEvent(updateConfParticipantEvent);

    var confCall = phoneState.getCalls()[0].conference;
    expect(confCall.conferenceName).toEqual("My Conf Room");
    expect(confCall.conferenceNumber).toEqual("4000");
    expect(confCall.participants.length).toEqual(1);

    expect(confCall.participants[0].isMuted).toEqual(true);
  });

  it ('should retrieve conference participants even if functions are called in disorder', () => {
    var callEvent = createEvent('EventEstablished', "1000", "4000", "1471858490.236", "My Conf Room", "Outgoing");

    var p1 = createConfParticipant(1, "1001", "Some other dude", 0, false, "User");
    var joinConfEvent = createConfEvent("Join", "1471858490.236", "4000", "My Conf Room", [p1], 10);
    publishConfEvent(joinConfEvent);
    publishEvent(callEvent);

    var confCall = phoneState.getCalls()[0].conference;
    expect(confCall.conferenceName).toEqual("My Conf Room");
    expect(confCall.conferenceNumber).toEqual("4000");
    expect(confCall.participants.length).toEqual(1);
  });

  it('should say if calls are on hold', function() {
    var line1Event = createEvent('EventEstablished', "1000", "1001", "1471858490.236", "Ann", "Incoming");
    var line2Event = createEvent('EventOnHold', "1000", "1002", "1471859840.127", "Bob", "Outgoing");
    var calls;

    publishEvent(line1Event);
    calls = phoneState.getCallsOnHold();
    expect(calls.length).toEqual(0);

    publishEvent(line2Event);
    calls = phoneState.getCallsOnHold();
    expect(calls.length).toEqual(1);
  });

  it('should say if calls are Not on hold', function() {
    var line1Event = createEvent('EventOnHold', "1000", "1001", "1471858490.236", "Ann", "Incoming");
    var line2Event = createEvent('EventEstablished', "1000", "1002", "1471859840.127", "Bob", "Incoming");
    var calls;

    publishEvent(line1Event);
    calls = phoneState.getCallsNotOnHold();
    expect(calls.length).toEqual(0);

    publishEvent(line2Event);
    calls = phoneState.getCallsNotOnHold();
    expect(calls.length).toEqual(1);
  });
  
  it ('should get Established phone state if one call is ongoing', () => {
    const event = createEvent('EventEstablished', "1000", "1002", "1471858490.236", "Jane Doe", "Outgoing");
    publishEvent(event);
    expect(phoneState.getState()).toBe("Established");
  });

  it ('should get Available phone state if one call is dialing and then released', () => {
    const event = createEvent('EventDialing', "1000", "1002", "1471858490.236", "Jane Doe", "Outgoing");
    publishEvent(event);
    expect(phoneState.getState()).toBe("Dialing");
    publishEvent(createEvent('EventReleased', "1000", "1002", "1471858490.236", "Jane Doe", "Outgoing"));
    expect(phoneState.getState()).toBe("Available");
  });

  it ('should get Established phone state if two calls are ongoing and then one released', () => {
    const event1 = createEvent('EventEstablished', "1000", "1002", "1471858490.236", "Jane Doe", "Outgoing");
    publishEvent(event1);
    const event2 = createEvent('EventRinging', "1003", "1000", "1471858490.237", "Mike Dwarf", "Incoming");
    publishEvent(event2);
    expect(phoneState.getState()).toBe("Ringing");
    publishEvent(createEvent('EventReleased', "1003", "1000", "1471858490.237", "Mike Dwarf", "Incoming"));
    expect(phoneState.getState()).toBe("Established");
  });

  it ('should get Ringing phone state if one call is ringing and other one released', () => {
    const event1 = createEvent('EventEstablished', "1000", "1002", "1471858490.236", "Jane Doe", "Outgoing");
    publishEvent(event1);
    const event2 = createEvent('EventRinging', "1003", "1000", "1471858490.237", "Mike Dwarf", "Incoming");
    publishEvent(event2);
    expect(phoneState.getState()).toBe("Ringing");
    publishEvent(createEvent('EventReleased', "1000", "1002", "1471858490.236", "Jane Doe", "Outgoing"));
    expect(phoneState.getState()).toBe("Ringing");
  });

  it ('should get Dialing phone state if one call is dialing and other one released', () => {
    const event1 = createEvent('EventEstablished', "1000", "1002", "1471858490.236", "Jane Doe", "Outgoing");
    publishEvent(event1);
    const event2 = createEvent('EventDialing', "1003", "1000", "1471858490.237", "Mike Dwarf", "Incoming");
    publishEvent(event2);
    expect(phoneState.getState()).toBe("Dialing");
    publishEvent(createEvent('EventReleased', "1000", "1002", "1471858490.236", "Jane Doe", "Outgoing"));
    expect(phoneState.getState()).toBe("Dialing");
  });

  it ('should get Hold phone state if one call is on hold and other one released', () => {
    const event1 = createEvent('EventOnHold', "1000", "1002", "1471858490.236", "Jane Doe", "Outgoing");
    publishEvent(event1);
    const event2 = createEvent('EventDialing', "1003", "1000", "1471858490.237", "Mike Dwarf", "Incoming");
    publishEvent(event2);
    expect(phoneState.getState()).toBe("Dialing");
    publishEvent(createEvent('EventReleased', "1003", "1000", "1471858490.237", "Mike Dwarf", "Incoming"));
    expect(phoneState.getState()).toBe("OnHold");
  });

  it ('should trigger the handler function when the state is changed', () => {
    let handler = {
      callback: () => {}
    };
    spyOn(handler, 'callback');
    let spyee = handler.callback;
    phoneState.addStateHandler(handler.callback);
    const event1 = createEvent('EventOnHold', "1000", "1002", "1471858490.236", "Jane Doe", "Outgoing");
    publishEvent(event1);
    $rootScope.$digest();   
    expect(spyee).toHaveBeenCalled();
    expect(spyee).toHaveBeenCalledWith('OnHold');
    const event2 = createEvent('EventDialing', "1003", "1000", "1471858490.237", "Mike Dwarf", "Incoming");
    publishEvent(event2);
    $rootScope.$digest();
    expect(spyee).toHaveBeenCalledWith('Dialing');
  });

  it ('should get the conference', () => {
    var callEvent = createEvent('EventEstablished', "1000", "4000", "1471858490.236", "My Conf Room", "Outgoing");
    publishEvent(callEvent);
    var p1 = createConfParticipant(1, "1001", "Some other dude", 10);
    var p2 = createConfParticipant(2, "1000", "Hey that's me !", 0, true);
    var confEvent = createConfEvent("Join", "1471858490.236", "4000", "My Conf Room", [p1, p2], 10);

    publishConfEvent(confEvent);
    var conference = phoneState.getConference();
    expect(conference.conferenceName).toEqual("My Conf Room");
    expect(conference.conferenceNumber).toEqual("4000");
  });

  it ('should check if I am the conference organizer', () => {
    
    var callEvent = createEvent('EventEstablished', "1000", "4000", "1471858490.236", "My Conf Room", "Outgoing");
    publishEvent(callEvent);

    var p1 = createConfParticipant(1, "1001", "Some other dude", 10);
    var p2 = createConfParticipant(2, "1000", "Hey that's me !", 0, true);
    var confEvent = createConfEvent("Join", "1471858490.236", "4000", "My Conf Room", [p1, p2], 10);

    publishConfEvent(confEvent);
    var p1IsOrganizer = createConfParticipant(1, "1001", "Some other dude", 0, false, "Organizer", false, true);
    var updateConfParticipantEvent = createConfParticipantEvent("Update", "1471858490.236", "4000", p1IsOrganizer);
    publishConfParticipantEvent(updateConfParticipantEvent);
    
    var conference = phoneState.getConference();
    var isOrganizer = phoneState.getConferenceOrganizer();
    expect(isOrganizer).toBeTruthy();
    expect(conference.currentUserRole).toEqual("Organizer");
  });

  it('should not track call if phone event is in failure state', function() {
    var lineEvent = createEvent('EventFailure', "1000", "1001", null, "Ann", "Unknown");
    var calls;

    publishEvent(lineEvent);
    calls = phoneState.getCalls();
    expect(calls.length).toEqual(0);
  });

});
