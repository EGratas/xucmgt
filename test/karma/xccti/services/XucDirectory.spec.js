'use strict';

describe('Xuc directory', function() {
  var xucDirectory;
  var $rootScope;
  var token = "aaaa-bbbb-cccc-dddd-1234";
  var user = {username: "jbond", phoneNumber: "1001", token: token};

  var favEntries = [{"status":4,"entry":["Fav2","568778","","",true,""],"contact_id":"24","source":"internal","favorite":true},
    {"status":4,"entry":["Fav1","45654","","",true,""],"contact_id":"25","source":"internal","favorite":true}];
  var favResult = {"headers":[], "entries": favEntries};

  var searchEntries = [{"status":4,"entry":["EXTERNE2","568778","","",false,""],"contact_id":"24","source":"internal","favorite":false},
    {"status":4,"entry":["EXTERNE1","45654","","",false,""],"contact_id":"25","source":"internal","favorite":false}];
  var searchResult = {"headers": [], "entries": searchEntries};


  beforeEach(function() {
    Cti.Topic(Cti.MessageType.USERDISPLAYNAME).clear();
    jasmine.addMatchers({
      toEqualData : customMatchers.toEqualData
    });
  });

  beforeEach(angular.mock.module('xcCti'));
  beforeEach(angular.mock.module('xcHelper'));
  beforeEach(angular.mock.module('html-templates'));
  beforeEach(angular.mock.module('karma-backend'));

  beforeEach(angular.mock.module(function($provide) {
    let XucLinkProvider = {
      $get: function($q) {
        let loggedOutDefer = $q.defer();
        return {
          loggedOutDefer: loggedOutDefer,
          whenLogged: () => $q.resolve(user),
          whenLoggedOut: () => loggedOutDefer.promise
        };
      }
    };
    $provide.provider('XucLink', XucLinkProvider);
  }));

  beforeEach(angular.mock.inject(function(_XucDirectory_, _$rootScope_) {
    xucDirectory = _XucDirectory_;
    $rootScope = _$rootScope_;
    spyOn($rootScope, '$broadcast').and.callThrough();

    // Initialize XucLink by resolving promise
    $rootScope.$digest();
  }));

  it('should remove Favoris and Personal from header list', function() {
    var result = {
      "headers": ["Nom","Numéro","Mobile","Autre numéro","Favoris", "Perso", "Email"],
      "entries":[]
    };
    xucDirectory.onSearchResult(result);
    expect(xucDirectory.getHeaders()).toEqualData(["Nom","Numéro","Mobile","Autre numéro","Email"]);
  });
  it('should not modify header list if Favoris is not in the list', function() {
    var result = {
      "headers": ["Nom","Numéro","Mobile","Autre numéro","Email"],
      "entries":[]
    };
    xucDirectory.onSearchResult(result);
    expect(xucDirectory.getHeaders()).toEqualData(["Nom","Numéro","Mobile","Autre numéro","Email"]);
  });

  it('should broadcast notification on search result', function() {
    xucDirectory.onSearchResult(searchResult);
    expect(xucDirectory.getSearchResult()).toEqualData(searchEntries);
    expect($rootScope.$broadcast).toHaveBeenCalledWith('searchResultUpdated');
  });
  it('should broadcast notification on favorites result', function() {
    xucDirectory.onFavorites(favResult);
    expect(xucDirectory.getFavorites()).toEqualData(favEntries);
    expect($rootScope.$broadcast).toHaveBeenCalledWith('favoritesUpdated');
  });
  it('should update favorites and broadcast notification on favorites add', function() {
    var entries = [
      {"status":4,"entry":["EXTERNE2","568778","","",false,""],"contact_id":"13","source":"internal","favorite":false},
      {"status":4,"entry":["autre","23423","","",false,""],"contact_id":"41","source":"internal","favorite":false}
    ];
    var result = {"headers":[], "entries": entries};
    var successfulAdd = {"action":"Added","contact_id":"13","source":"internal"};
    xucDirectory.onSearchResult(result);
    expect(xucDirectory.getFavorites()).toEqualData([]);
    xucDirectory.onFavoriteUpdated(successfulAdd);
    entries[0].favorite = true;
    expect(xucDirectory.getFavorites()).toEqualData([entries[0]]);
    expect($rootScope.$broadcast).toHaveBeenCalledWith('favoritesUpdated');
  });
  it('should update favorites and  broadcast notification on favorites remove', function() {
    var entries = [
      {"status":4,"entry":["fav2","568778","","",true,""],"contact_id":"13","source":"internal","favorite":true},
      {"status":4,"entry":["fav1","4564","","",true,""],"contact_id":"32","source":"internal","favorite":true}
    ];
    var result = {"headers":[], "entries": entries};
    var successfulAdd = {"action":"Removed","contact_id":"13","source":"internal"};
    xucDirectory.onFavorites(result);
    expect(xucDirectory.getFavorites()).toEqualData(entries);
    xucDirectory.onFavoriteUpdated(successfulAdd);
    expect(xucDirectory.getFavorites()).toEqualData([entries[1]]);
    expect($rootScope.$broadcast).toHaveBeenCalledWith('favoritesUpdated');
  });
  it('should not empty favorites on searchResult', function() {
    xucDirectory.onFavorites(favResult);
    xucDirectory.onSearchResult(searchResult);
    expect(xucDirectory.getFavorites()).toEqualData(favEntries);
  });

  it('should lookup for search term and store it', function() {
    spyOn(Cti, 'directoryLookUp');
    xucDirectory.directoryLookup(null);
    expect(xucDirectory.getSearchTerm()).toEqual('');
    expect(Cti.directoryLookUp).toHaveBeenCalledWith('');

    xucDirectory.directoryLookup('user');
    expect(xucDirectory.getSearchTerm()).toEqual('user');
    expect(Cti.directoryLookUp).toHaveBeenCalledWith('user');
  });

  it('should show that a directory lookup is ongoing', function() {
    spyOn(Cti, 'directoryLookUp');
    expect(xucDirectory.isSearching()).toEqual(false);
    xucDirectory.directoryLookup('user');
    expect(xucDirectory.isSearching()).toEqual(true);
  });

  it('should show that lookup is finished', function() {
    spyOn(Cti, 'directoryLookUp');
    xucDirectory.directoryLookup('user');
    expect(xucDirectory.isSearching()).toEqual(true);

    xucDirectory.onSearchResult(searchResult);
    expect(xucDirectory.isSearching()).toEqual(false);
  });

  it('should broadcast notification on clear result', function() {
    xucDirectory.clearResults();
    expect(xucDirectory.getSearchResult()).toEqualData([]);
    expect(xucDirectory.isSearching()).toEqual(false);
    expect(xucDirectory.getSearchTerm()).toEqual(null);
    expect($rootScope.$broadcast).toHaveBeenCalledWith('searchResultUpdated');
  });

  it('should get display name from remote party username', function() {
    spyOn(Cti, 'displayNameLookup');
    var promise = xucDirectory.getUserDisplayName('jbond');

    var resolvedValue;

    promise.then(function(displayName) { resolvedValue = displayName; });

    Cti.Topic(Cti.MessageType.USERDISPLAYNAME).publish({userName: 'jbond', displayName: 'James Bond'});
    $rootScope.$apply();
    expect(resolvedValue).toEqual('James Bond');
  });

  it('should handle if result has unexpected username', function() {
    spyOn(Cti, 'displayNameLookup');
    var promise = xucDirectory.getUserDisplayName('jbond');

    var resolvedValue;

    promise.then(function(displayName) { resolvedValue = displayName; });

    Cti.Topic(Cti.MessageType.USERDISPLAYNAME).publish({userName: 'jdoe', displayName: 'Jane Doe'});
    $rootScope.$apply();
    expect(resolvedValue).toEqual(undefined);
  });

  it('should get correct display name for concurrent requests', function() {
    spyOn(Cti, 'displayNameLookup');
    var promise1 = xucDirectory.getUserDisplayName('jbond');
    var promise2 = xucDirectory.getUserDisplayName('bwayne');

    var resolvedValue1;
    var resolvedValue2;

    promise1.then(function(displayName) { resolvedValue1 = displayName; });
    promise2.then(function(displayName) { resolvedValue2 = displayName; });

    Cti.Topic(Cti.MessageType.USERDISPLAYNAME).publish({userName: 'bwayne', displayName: 'Bruce Wayne'});
    Cti.Topic(Cti.MessageType.USERDISPLAYNAME).publish({userName: 'jbond', displayName: 'James Bond'});
    $rootScope.$apply();
    expect(resolvedValue1).toEqual('James Bond');
    expect(resolvedValue2).toEqual('Bruce Wayne');
  });

  it('should retrieve display name from cache', function() {
    spyOn(Cti, 'displayNameLookup');

    var promiseNotCached = xucDirectory.getUserDisplayName('jbond');
    var resolvedValueNotCached;

    promiseNotCached.then(function(displayName) { resolvedValueNotCached = displayName; });

    Cti.Topic(Cti.MessageType.USERDISPLAYNAME).publish({userName: 'jbond', displayName: 'James Bond'});
    $rootScope.$apply();

    expect(resolvedValueNotCached).toEqual('James Bond');

    var promiseCached = xucDirectory.getUserDisplayName('jbond');
    var resolvedValueCached;

    promiseCached.then(function(displayName) { resolvedValueCached = displayName; });
    $rootScope.$apply();

    expect(resolvedValueCached).toEqual('James Bond');
    expect(Cti.displayNameLookup).toHaveBeenCalledTimes(1);
  });

});
