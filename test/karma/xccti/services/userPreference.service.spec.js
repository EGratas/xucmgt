import {
  UserPreferenceBoolean,
  UserPreferenceKey,
  UserPreferencePreferredDevice,
  UserPreferenceValueType
} from "xchelper/models/userPreference.model";
import UserPreferenceService from "xchelper/services/userPreference.service";

describe('UserPreference Test', () => {
  var $rootScope;
  var $scope;
  var remoteConfiguration;
  var XucLink;
  var $log;

  beforeEach(angular.mock.module('xcCti'));

  beforeEach(angular.mock.inject(function (_$rootScope_, _remoteConfiguration_, _XucLink_, _$log_) {
    $rootScope = _$rootScope_;
    $scope = $rootScope.$new();
    remoteConfiguration = _remoteConfiguration_;
    XucLink = _XucLink_;
    $log = _$log_;

    Cti.Topic(Cti.MessageType.USERPREFERENCE).clear();
    Cti.Topic(Cti.MessageType.FLASHTEXTEVENT).clear();
    spyOn(Cti, 'setHandler').and.callThrough();
    spyOn(Cti, 'sendCallback');
    spyOn(remoteConfiguration, 'isAgent');
    spyOn(remoteConfiguration, 'hasSwitchBoard');
    spyOn($rootScope, '$broadcast');
    $rootScope.$digest();
  }));

  afterEach(function () {
    $scope.$destroy();
    $rootScope.$digest();
  });



  it('Send UserPreference Request', function () {
    Cti.setUserPreference(UserPreferenceKey.PreferredDevice, UserPreferencePreferredDevice.WebApp, UserPreferenceValueType.string);

    expect(Cti.sendCallback).toHaveBeenCalledWith({
      claz: 'web',
      command: 'setUserPreference',
      key: UserPreferenceKey.PreferredDevice,
      value: UserPreferencePreferredDevice.WebApp,
      value_type: UserPreferenceValueType.string
    });

  });

  it('Send PreferredDevice from Service', function () {

    let service = new UserPreferenceService(
      {
        externalConfig: null,
        JitsiMeetExternalAPI: null,
        Cti: Cti
      },
      null,
      null,
      remoteConfiguration,
      XucLink,
      $rootScope
    );

    service.onChangePreferredDevice(UserPreferencePreferredDevice.WebApp);
    expect(Cti.sendCallback).toHaveBeenCalledWith({
      claz: 'web',
      command: 'setUserPreference',
      key: UserPreferenceKey.PreferredDevice,
      value: UserPreferencePreferredDevice.WebApp,
      value_type: UserPreferenceValueType.string
    });

  });

  it('Send MobileAppInfo from Service', function () {

    let service = new UserPreferenceService(
      {
        externalConfig: null,
        JitsiMeetExternalAPI: null,
        Cti: Cti
      },
      null,
      null,
      remoteConfiguration,
      XucLink,
      $rootScope
    );

    service.onChangeMobileAppInfo(UserPreferenceBoolean.True);
    expect(Cti.sendCallback).toHaveBeenCalledWith({
      claz: 'web',
      command: 'setUserPreference',
      key: UserPreferenceKey.MobileAppInfo,
      value: UserPreferenceBoolean.True,
      value_type: UserPreferenceValueType.boolean
    });

  });

  it('Send userPreference from Service', function () {

    let service = new UserPreferenceService(
      {
        externalConfig: null,
        JitsiMeetExternalAPI: null,
        Cti: Cti
      },
      null,
      null,
      remoteConfiguration,
      XucLink,
      $rootScope
    );

    service.onChangeUserPreference(UserPreferenceKey.MobileAppInfo, UserPreferenceBoolean.True, UserPreferenceValueType.boolean);
    expect(Cti.sendCallback).toHaveBeenCalledWith({
      claz: 'web',
      command: 'setUserPreference',
      key: UserPreferenceKey.MobileAppInfo,
      value: UserPreferenceBoolean.True,
      value_type: UserPreferenceValueType.boolean
    });

  });

  it('Test UserService Listener', function () {

    let service = new UserPreferenceService(
      {
        externalConfig: null,
        JitsiMeetExternalAPI: null,
        Cti: Cti
      },
      null,
      $log,
      remoteConfiguration,
      XucLink,
      $rootScope
    );
    service.init();

    service.onChangeUserPreference(UserPreferenceKey.MobileAppInfo, UserPreferenceBoolean.True, UserPreferenceValueType.boolean);

    const rawEvent = {
      [UserPreferenceKey.MobileAppInfo]: { value: UserPreferenceBoolean.False, value_type: UserPreferenceValueType.boolean },
      [UserPreferenceKey.PreferredDevice]: { value: UserPreferencePreferredDevice.WebApp, value_type: UserPreferenceValueType.string }
    };

    Cti.Topic(Cti.MessageType.USERPREFERENCE).publish(rawEvent);
    $rootScope.$digest();
    expect(service.userPreferences).toEqual({
      [UserPreferenceKey.MobileAppInfo]: { value: UserPreferenceBoolean.False, value_type: UserPreferenceValueType.boolean },
      [UserPreferenceKey.PreferredDevice]: { value: UserPreferencePreferredDevice.WebApp, value_type: UserPreferenceValueType.string }
    });

    rawEvent[UserPreferenceKey.PreferredDevice].value = UserPreferencePreferredDevice.MobileApp;
    Cti.Topic(Cti.MessageType.USERPREFERENCE).publish(rawEvent);
    $rootScope.$digest();

  });

  it('Retrieve the number of missed call or 0', () => {
    let service = new UserPreferenceService(
      {
        externalConfig: null,
        JitsiMeetExternalAPI: null,
        Cti: Cti
      },
      null,
      $log,
      remoteConfiguration,
      XucLink,
      $rootScope
    );
    service.init();

    expect(service.getMissedCalls()).toEqual(0);

    const rawEvent = {
      [UserPreferenceKey.MobileAppInfo]: { value: UserPreferenceBoolean.False, value_type: UserPreferenceValueType.boolean },
      [UserPreferenceKey.PreferredDevice]: { value: UserPreferencePreferredDevice.WebApp, value_type: UserPreferenceValueType.string },
      [UserPreferenceKey.NbMissedCall]: { value: "42", value_type: UserPreferenceValueType.string }
    };
    Cti.Topic(Cti.MessageType.USERPREFERENCE).publish(rawEvent);
    $rootScope.$digest();

    expect(service.getMissedCalls()).toEqual(42);
  });

  it('Does send a newNbMissedCall message in number', () => {
    let service = new UserPreferenceService(
      {
        externalConfig: null,
        JitsiMeetExternalAPI: null,
        Cti: Cti
      },
      null,
      $log,
      remoteConfiguration,
      XucLink,
      $rootScope
    );
    service.init();

    const rawEvent = {
      [UserPreferenceKey.MobileAppInfo]: { value: UserPreferenceBoolean.False, value_type: UserPreferenceValueType.boolean },
      [UserPreferenceKey.PreferredDevice]: { value: UserPreferencePreferredDevice.WebApp, value_type: UserPreferenceValueType.string },
      [UserPreferenceKey.NbMissedCall]: { value: "42", value_type: UserPreferenceValueType.string }
    };
    Cti.Topic(Cti.MessageType.USERPREFERENCE).publish(rawEvent);
    $rootScope.$digest();

    expect($rootScope.$broadcast).toHaveBeenCalledWith('newNbMissedCalls', 42);
  });

  it('Does not send a newNbMissedCall message if the value is not a number', () => {
    let service = new UserPreferenceService(
      {
        externalConfig: null,
        JitsiMeetExternalAPI: null,
        Cti: Cti
      },
      null,
      $log,
      remoteConfiguration,
      XucLink,
      $rootScope
    );
    service.init();

    const rawEvent = {
      [UserPreferenceKey.MobileAppInfo]: { value: UserPreferenceBoolean.False, value_type: UserPreferenceValueType.boolean },
      [UserPreferenceKey.PreferredDevice]: { value: UserPreferencePreferredDevice.WebApp, value_type: UserPreferenceValueType.string },
      [UserPreferenceKey.NbMissedCall]: { value: "You should not parse", value_type: UserPreferenceValueType.string }
    };
    Cti.Topic(Cti.MessageType.USERPREFERENCE).publish(rawEvent);
    $rootScope.$digest();

    expect($rootScope.$broadcast).not.toHaveBeenCalled();
  });


  it('Sets the number of missed calls to 0', () => {
    let service = new UserPreferenceService(
      {
        externalConfig: null,
        JitsiMeetExternalAPI: null,
        Cti: Cti
      },
      null,
      $log,
      remoteConfiguration,
      XucLink,
      $rootScope
    );
    service.init();
    expect(service.getMissedCalls()).toEqual(0);

    const rawEvent = {
      [UserPreferenceKey.MobileAppInfo]: { value: UserPreferenceBoolean.False, value_type: UserPreferenceValueType.boolean },
      [UserPreferenceKey.PreferredDevice]: { value: UserPreferencePreferredDevice.WebApp, value_type: UserPreferenceValueType.string },
      [UserPreferenceKey.NbMissedCall]: { value: "42", value_type: UserPreferenceValueType.string }
    };
    Cti.Topic(Cti.MessageType.USERPREFERENCE).publish(rawEvent);
    $rootScope.$digest();

    expect(service.getMissedCalls()).toEqual(42);


    service.resetMissedCalls();
    expect(Cti.sendCallback).toHaveBeenCalledWith({
      claz: 'web',
      command: 'setUserPreference',
      key: 'NB_MISSED_CALL',
      value: '0',
      value_type: 'String'
    });
  });
});
