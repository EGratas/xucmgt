import 'ccagent/app';

describe("Login Controller", function() {

  var $controller;
  var $scope;
  var electronWrapper;
  var remoteConfiguration;
  var ctrl;
  var $window;

  beforeEach(angular.mock.module('karma-backend'));
  beforeEach(angular.mock.module('Agent'));

  beforeEach(angular.mock.inject(function (_$controller_, _electronWrapper_, _remoteConfiguration_, _$window_){
    $controller = _$controller_;
    electronWrapper = _electronWrapper_;
    remoteConfiguration = _remoteConfiguration_;
    $window = _$window_;

    $window.externalConfig = { switchboard: false };
    $window.electronRelativeDownloadPath = "xivo-desktop-assistant/1.0/1.0.5";
    $window.appVersionNoExtension = "1.0.5";
    
    ctrl = $controller('LoginController', {
      '$scope' : $scope,
      'ctrl'  : ctrl,
      '$controller' : $controller,  
      'electronWrapper' : electronWrapper,
      'remoteConfiguration' : remoteConfiguration
    });

  }));

  it("can be instantiated", function() {
    expect(ctrl).not.toBeUndefined();
  });

  it('returns if electron', function() {
    spyOn(electronWrapper, 'isElectron');

    ctrl.isElectron();
    expect(electronWrapper.isElectron).toHaveBeenCalled();
  });

  it('display download buttons when conditions are met', function() {
    spyOn(electronWrapper, 'isElectron').and.returnValue(false);
    ctrl.displayDesktopDownload = true;

    let display = ctrl.displayDownloadDesktop();
    expect(display).toBeTruthy();
    
  });

  it('doesnt display download buttons when conditions are not met', function() {
    spyOn(electronWrapper, 'isElectron').and.returnValue(false);
    ctrl.displayDesktopDownload = false;

    let display = ctrl.displayDownloadDesktop();
    expect(display).toBeFalsy();
    
  });
});
