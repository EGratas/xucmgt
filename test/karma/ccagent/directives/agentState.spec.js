describe('agentState directive', () => {
  var $compile;
  var $rootScope;
  var XucLink;
  var XucAgentUser;
  var externalEvent;
  var $window;

  beforeEach(angular.mock.module('karma-backend'));
  beforeEach(angular.mock.module('html-templates'));
  beforeEach(angular.mock.module('xcCti'));
  beforeEach(angular.mock.module('Agent'));

  beforeEach(angular.mock.inject((_$compile_, _$rootScope_, _XucLink_, _XucAgentUser_, _ExternalEvent_, _$window_) =>{
    $compile = _$compile_;
    $rootScope = _$rootScope_;
    XucLink = _XucLink_;
    XucAgentUser = _XucAgentUser_;
    externalEvent = _ExternalEvent_;
    $window = _$window_;
    $window.externalConfig = {
      useSso: true
    };
  }));


  it('should logout and clear credentials when AgentLoggedOut state is received and sso is used', () => {
    $window.externalConfig.useSso = true;
    spyOn(XucLink, 'clearCredentials');
    spyOn(XucLink, 'logout');

    var scope = $rootScope.$new();
    $compile('<agent-state></agent-state>')(scope);
    $rootScope.$digest();

    scope.checkLogout({name: 'AgentLoggedOut'});
    expect(scope.loggedIn).toBe(false);
    expect(XucLink.clearCredentials).not.toHaveBeenCalled();
    expect(XucLink.logout).not.toHaveBeenCalled();

    scope.checkLogout({name: 'AgentLogin'});
    expect(scope.loggedIn).toBe(true);
    expect(XucLink.clearCredentials).not.toHaveBeenCalled();
    expect(XucLink.logout).not.toHaveBeenCalled();

    scope.checkLogout({name: 'AgentLoggedOut'});
    expect(scope.loggedIn).toBe(false);
    expect(XucLink.clearCredentials).toHaveBeenCalled();
    expect(XucLink.logout).toHaveBeenCalled();
  });

  it('should not clear credentials when AgentLoggedOut state is received and sso is turned OFF', () => {
    $window.externalConfig.useSso = false;
    spyOn(XucLink, 'clearCredentials');
    spyOn(XucLink, 'logout');

    var scope = $rootScope.$new();
    $compile('<agent-state></agent-state>')(scope);
    $rootScope.$digest();

    scope.checkLogout({name: 'AgentLoggedOut'});
    expect(scope.loggedIn).toBe(false);
    expect(XucLink.clearCredentials).not.toHaveBeenCalled();
    expect(XucLink.logout).not.toHaveBeenCalled();

    scope.checkLogout({name: 'AgentLogin'});
    expect(scope.loggedIn).toBe(true);
    expect(XucLink.clearCredentials).not.toHaveBeenCalled();
    expect(XucLink.logout).not.toHaveBeenCalled();

    scope.checkLogout({name: 'AgentLoggedOut'});
    expect(scope.loggedIn).toBe(false);
    expect(XucLink.clearCredentials).not.toHaveBeenCalled();
    expect(XucLink.logout).toHaveBeenCalled();
  });

  it('should check if needs to logout when agent switching state', () => {
    spyOn(XucAgentUser,'switchAgentState');

    var scope = $rootScope.$new();
    $compile('<agent-state></agent-state>')(scope);
    $rootScope.$digest();

    spyOn(scope, 'checkLogout');
    scope.switchState({name: 'AgentLoggedOut'});
    expect(scope.checkLogout).not.toHaveBeenCalled();
  });

  it('should return a countdown object time in state', () => {
    var elem = angular.element('<agent-state></agent-state>');
    var scope = $rootScope.$new();
    $compile(elem)(scope);
    $rootScope.$digest();

    scope.refreshState();
    expect(scope.agentState.timeInState.toString()).toBeDefined();
  });

  it('register electron callback for when agent will be logged off', () => {
    spyOn(externalEvent, 'registerConfirmQuitCallback');

    var elem = angular.element('<agent-state></agent-state>');
    var scope = $rootScope.$new();
    $compile(elem)(scope);

    $rootScope.$digest();
    expect(externalEvent.registerConfirmQuitCallback).toHaveBeenCalled();
  });
});
