import _ from 'lodash';

describe('statsPanel directive', () => {
  var $compile;
  var $rootScope;
  var XucAgent;
  var $q;

  beforeEach(angular.mock.module('karma-backend'));
  beforeEach(angular.mock.module('html-templates'));
  beforeEach(angular.mock.module('xcCti'));
  beforeEach(angular.mock.module('Agent'));

  beforeEach(angular.mock.inject((_$compile_, _$rootScope_, _XucAgent_, _$q_) =>{
    $compile = _$compile_;
    $rootScope = _$rootScope_;
    XucAgent = _XucAgent_;
    $q = _$q_;
  }));

  it('should display statistics content', () => {
    var scope = $rootScope.$new();
    scope.user = { agentId: 123 };
    var element = $compile('<stats-panel agent-id="{{user.agentId}}" display="horizontal"></stats-panel>')(scope);
    $rootScope.$digest();

    var agent = {
      stats: {
        PausedTime: 0, 
        InbAcdCallTime: 0,
        InbAverAcdCallTime: 0,
        OutCallTime: 0,
        WrapupTime: 0,
        InbAcdCalls: 0,
        InbAnsAcdCalls: 0,
        InbUnansAcdCalls: 0,
        OutCalls: 0,
        InbPercUnansAcdCalls: 0

      }
    };

    spyOn(XucAgent, 'getAgentAsync').and.returnValue($q.resolve(agent));
    $rootScope.$broadcast('AgentStatisticsUpdated');
    scope.$digest();
    expect(XucAgent.getAgentAsync).toHaveBeenCalled();
    _.forEach(agent.stats, (value, key) => {
      var span = element.find('.' + key);
      expect(span.length).toBeGreaterThan(0, key + " item not found");
    });    
  });

  it('should display statistics content even if agent is received after stats', () => {
    var scope = $rootScope.$new();
    scope.user = { };
    var element = $compile('<stats-panel agent-id="{{user.agentId}}" display="horizontal"></stats-panel>')(scope);
    $rootScope.$digest();

    var agent = {
      stats: {
        PausedTime: 0, 
        InbAcdCallTime: 0,
        InbAverAcdCallTime: 0,
        OutCallTime: 0,
        WrapupTime: 0,
        InbAcdCalls: 0,
        InbAnsAcdCalls: 0,
        InbUnansAcdCalls: 0,
        OutCalls: 0,
        InbPercUnansAcdCalls: 0
      }
    };

    spyOn(XucAgent, 'getAgentAsync').and.returnValue($q.resolve(agent));
    $rootScope.$broadcast('AgentStatisticsUpdated');
    scope.$digest();
    expect(XucAgent.getAgentAsync).not.toHaveBeenCalled();
    scope.$apply(() => { scope.user.agentId = 123;});
    $rootScope.$broadcast('AgentStatisticsUpdated');
    scope.$digest();
    expect(XucAgent.getAgentAsync).toHaveBeenCalled();

    _.forEach(agent.stats, (value, key) => {
      var span = element.find('.' + key);
      expect(span.length).toBeGreaterThan(0, key + " item not found");
    });    
  });

  it('should prepare stats for chart based on the clicked item id', () => {
    var scope = $rootScope.$new();
    scope.user = { agentId: 123 };
    var element = angular.element('<stats-panel agent-id="{{user.agentId}}" display="horizontal"></stats-panel>');
    $compile(element)(scope);
    $rootScope.$digest();
    var isolatedScope = element.isolateScope();

    isolatedScope.stats = [ {id: 0}, {id: 1}, {id: 2}, {id: 3}, {id: 4}, {id: 5}, {id: 6}, {id: 7}, {id: 8}, {id: 9}, {id: 10} ];
    isolatedScope.toggleChart("PausedTime");
    expect(isolatedScope.chart).toEqual([{id: 10}, {id: 0}, {id: 1}, {id: 3}, {id: 4}]);
  });  

  it('should not display chart when click item id that has no charts', () => {
    var scope = $rootScope.$new();
    scope.user = { agentId: 123 };
    var element = angular.element('<stats-panel agent-id="{{user.agentId}}" display="horizontal"></stats-panel>');
    $compile(element)(scope);
    $rootScope.$digest();
    var isolatedScope = element.isolateScope();

    isolatedScope.toggleChart("DummyItem");
    expect(isolatedScope.displayChartDonut).toBe(false);
    expect(isolatedScope.displayChartBar).toBe(false);
  });  

  it('should toggle chart when click same item id if chart is displayed', () => {
    var scope = $rootScope.$new();
    scope.user = { agentId: 123 };
    var element = angular.element('<stats-panel agent-id="{{user.agentId}}" display="horizontal"></stats-panel>');
    $compile(element)(scope);
    $rootScope.$digest();
    var isolatedScope = element.isolateScope();

    isolatedScope.toggleChart('PausedTime');
    expect (isolatedScope.displayChartBar).toBe(true);
    isolatedScope.toggleChart('PausedTime');
    expect (isolatedScope.displayChartBar).toBe(false);
  });

  it('should not toggle chart when click other item id if chart is displayed', () => {
    var scope = $rootScope.$new();
    scope.user = { agentId: 123 };
    var element = angular.element('<stats-panel agent-id="{{user.agentId}}" display="horizontal"></stats-panel>');
    $compile(element)(scope);
    $rootScope.$digest();
    var isolatedScope = element.isolateScope();

    isolatedScope.toggleChart('PausedTime');
    expect (isolatedScope.displayChartBar).toBe(true);
    isolatedScope.toggleChart('InbAcdCallTime');
    expect (isolatedScope.displayChartBar).toBe(true);
  });


  it('should hide chart when click on container and not item', () => {
    var scope = $rootScope.$new();
    scope.user = { agentId: 123 };
    var element = angular.element('<stats-panel agent-id="{{user.agentId}}" display="horizontal"></stats-panel>');
    $compile(element)(scope);
    $rootScope.$digest();
    var isolatedScope = element.isolateScope();

    isolatedScope.toggleChart();
    expect (isolatedScope.displayChartBar).toBe(false);
    expect (isolatedScope.displayChartDonut).toBe(false);
  });
    
});