import angular from 'angular';

describe('callConference directive', () => {
  var $compile;
  var $rootScope;
  var $scope;
  var audioQualityStatistics;

  beforeEach(angular.mock.module('karma-backend'));
  beforeEach(angular.mock.module('html-templates'));
  beforeEach(angular.mock.module('xcCti'));
  beforeEach(angular.mock.module('Agent'));

  beforeEach(angular.mock.inject((_$compile_, _$rootScope_, _audioQualityStatistics_) =>{
    $compile = _$compile_;
    $rootScope = _$rootScope_;
    $scope = $rootScope.$new();
    audioQualityStatistics = _audioQualityStatistics_;
  }));


  it('should display the phone number only once when there is no user name', () => {
    spyOn(audioQualityStatistics, 'startAudioQualityMonitoring').and.callFake(()=> {});
    
    var element = $compile('<call-conference></call-conference>')($scope);
    
    $rootScope.$digest();
    var isolatedScope = element.isolateScope();

    var call = {
      otherDN: '123', 
      otherDName: '123'
    };

    let displayPhoneNumber = isolatedScope.hasNameAndNumber(call);
    expect(displayPhoneNumber).toBe(false);
  });
});
