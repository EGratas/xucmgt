import angular from 'angular';
import 'ccagent/app';


describe('callControl directive', () => {

  var $compile;
  var $rootScope;
  var scope;
  var remoteConfiguration;
  var CtiProxy;
  var audioQualityStatistics;
  var $timeout;

  beforeEach(angular.mock.module('karma-backend'));
  beforeEach(angular.mock.module('html-templates'));
  beforeEach(angular.mock.module('xcCti'));
  beforeEach(angular.mock.module('Agent'));

  beforeEach(angular.mock.inject((_$compile_, _$rootScope_, _remoteConfiguration_, _$q_, _CtiProxy_, _audioQualityStatistics_, _$timeout_) => {
    $compile = _$compile_;
    $rootScope = _$rootScope_;
    remoteConfiguration = _remoteConfiguration_;
    CtiProxy = _CtiProxy_;
    audioQualityStatistics = _audioQualityStatistics_;
    $timeout = _$timeout_;

    spyOn(remoteConfiguration, 'getBoolean').and.returnValue(_$q_.resolve(true));
  }));

  it('should hangup if action is triggered with hangup parameter', function () {
    scope = $rootScope.$new();
    $compile('<call-control></call-control>')(scope);
    $rootScope.$digest();
    spyOn(CtiProxy, 'hangup');
    spyOn(CtiProxy, 'answer');
    spyOn(scope, 'isRinging').and.returnValue(true);
    scope.triggerAction('answer');
    expect(CtiProxy.hangup).not.toHaveBeenCalled();
    expect(CtiProxy.answer).toHaveBeenCalled();
  });

  it('should answer if action is triggered with answer parameter', function () {
    scope = $rootScope.$new();
    $compile('<call-control></call-control>')(scope);
    $rootScope.$digest();
    spyOn(CtiProxy, 'hangup');
    spyOn(CtiProxy, 'answer');
    spyOn(scope, 'isEstablished').and.returnValue(true);
    scope.triggerAction('hangup');
    expect(CtiProxy.hangup).toHaveBeenCalled();
    expect(CtiProxy.answer).not.toHaveBeenCalled();
  });

  it('starts a quality error removal timeout when the quality was bad but is returning to good', function() {
    spyOn(CtiProxy, 'isUsingWebRtc').and.returnValue(true);
    scope = $rootScope.$new();
    $compile('<call-control></call-control>')(scope);
    $rootScope.$digest();    
    $rootScope.$broadcast(audioQualityStatistics.AUDIO_QUALITY_EVENT, {quality: audioQualityStatistics.AUDIO_QUALITY_BAD, statistics: {}});
    expect(scope.displayAudioQuality).toEqual(true);
    $rootScope.$broadcast(audioQualityStatistics.AUDIO_QUALITY_EVENT, {quality: audioQualityStatistics.AUDIO_QUALITY_GOOD, statistics: {}});
    expect(scope.displayAudioQuality).toEqual(true);
    expect(scope.audioQualityTimeout).not.toEqual(undefined);
    $timeout.flush(scope.audioQualityTimeoutMS);
    expect(scope.displayAudioQuality).toEqual(false);
    expect(scope.audioQualityTimeout).toEqual(undefined);
  });

  it('cancels the quality error removal timeout when the quality was bad,went back to good, then went back to bad', function() {
    spyOn(CtiProxy, 'isUsingWebRtc').and.returnValue(true);
    scope = $rootScope.$new();
    $compile('<call-control></call-control>')(scope);
    $rootScope.$digest();    
    $rootScope.$broadcast(audioQualityStatistics.AUDIO_QUALITY_EVENT, {quality: audioQualityStatistics.AUDIO_QUALITY_BAD, statistics: {}});
    expect(scope.displayAudioQuality).toEqual(true);
    $rootScope.$broadcast(audioQualityStatistics.AUDIO_QUALITY_EVENT, {quality: audioQualityStatistics.AUDIO_QUALITY_GOOD, statistics: {}});
    expect(scope.displayAudioQuality).toEqual(true);
    expect(scope.audioQualityTimeout).not.toEqual(undefined);
    $timeout.flush(scope.audioQualityTimeoutMS / 2);
    $rootScope.$broadcast(audioQualityStatistics.AUDIO_QUALITY_EVENT, {quality: audioQualityStatistics.AUDIO_QUALITY_BAD, statistics: {}});
    $timeout.flush(scope.audioQualityTimeoutMS / 2);
    expect(scope.displayAudioQuality).toEqual(true);
  });

  it('stops any ongoing quality timeout and hide the quality error when there is no more calls', function() {
    spyOn(CtiProxy, 'isUsingWebRtc').and.returnValue(true);
    scope = $rootScope.$new();
    $compile('<call-control></call-control>')(scope);
    $rootScope.$digest();
    scope.calls = [{callID: 1}];
    $rootScope.$broadcast(audioQualityStatistics.AUDIO_QUALITY_EVENT, {quality: audioQualityStatistics.AUDIO_QUALITY_BAD, statistics: {}});
    expect(scope.displayAudioQuality).toEqual(true);
    $rootScope.$broadcast(audioQualityStatistics.AUDIO_QUALITY_EVENT, {quality: audioQualityStatistics.AUDIO_QUALITY_GOOD, statistics: {}});
    expect(scope.audioQualityTimeout).not.toEqual(undefined);
    $timeout.flush(scope.audioQualityTimeoutMS / 2);
    scope.calls = [];
    $rootScope.$digest();
    expect(scope.displayAudioQuality).toEqual(false);
    expect(scope.audioQualityTimeout).toEqual(undefined);
  });

  it('doesnt start the webrtc audio monitoring if its not a webrtc user', function() {
    spyOn(CtiProxy, 'isUsingWebRtc').and.returnValue(false);
    scope = $rootScope.$new();
    $compile('<call-control></call-control>')(scope);
    $rootScope.$digest();
    scope.calls = [{callID: 1}];
    $rootScope.$broadcast(audioQualityStatistics.AUDIO_QUALITY_EVENT, {quality: audioQualityStatistics.AUDIO_QUALITY_BAD, statistics: {}});
    expect(scope.displayAudioQuality).toEqual(false);
    $rootScope.$broadcast(audioQualityStatistics.AUDIO_QUALITY_EVENT, {quality: audioQualityStatistics.AUDIO_QUALITY_GOOD, statistics: {}});
    expect(scope.audioQualityTimeout).toEqual(undefined);
  });

  it('display the call placeholder and start the timeout', function() {
    scope = $rootScope.$new();
    $compile('<call-control></call-control>')(scope);
    $rootScope.$digest();
    scope.calls = [];
    expect(scope.callPlaceholderDisplayed).toEqual(false);
    $rootScope.$broadcast("EventShowCallPlaceHolder");
    expect(scope.callPlaceholderDisplayed).toEqual(true);
    expect(scope.placeholderTimeout).not.toEqual(undefined);
  });

  it('then removes the call placeholder and stop the timeout when the call appears', function() {
    scope = $rootScope.$new();
    $compile('<call-control></call-control>')(scope);
    $rootScope.$digest();
    scope.calls = [];
    expect(scope.callPlaceholderDisplayed).toEqual(false);
    $rootScope.$broadcast("EventShowCallPlaceHolder");
    expect(scope.callPlaceholderDisplayed).toEqual(true);
    expect(scope.placeholderTimeout).not.toEqual(undefined);
    scope.calls = [{'fake': 'call'}];
    $rootScope.$digest();
    expect(scope.placeholderTimeout).toEqual(undefined);
  });

  it('end up removing the call placeholder if the call takes too much time appearing (aka forever lost in the endless void of asterisk)', function() {
    scope = $rootScope.$new();
    $compile('<call-control></call-control>')(scope);
    $rootScope.$digest();
    scope.calls = [];
    expect(scope.callPlaceholderDisplayed).toEqual(false);
    $rootScope.$broadcast("EventShowCallPlaceHolder");
    expect(scope.callPlaceholderDisplayed).toEqual(true);
    expect(scope.placeholderTimeout).not.toEqual(undefined);
    $timeout.flush(5001);
    expect(scope.callPlaceholderDisplayed).toEqual(false);
    expect(scope.placeholderTimeout).toEqual(undefined);
  });

  it('retrieve the latest dialed number', function() {
    scope = $rootScope.$new();
    $compile('<call-control></call-control>')(scope);
    $rootScope.$digest();
    spyOn(scope, 'closeCallInitError');
    scope.calls = [];
    $rootScope.$broadcast("dialingNumber", 3553);
    expect(scope.closeCallInitError).toHaveBeenCalled();
    expect(scope.dynamicTranslation.phoneNb).toEqual(3553);
  });

});