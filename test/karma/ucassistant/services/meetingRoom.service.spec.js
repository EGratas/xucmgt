describe('Service: Meeting Rooms', function () {
  var $q;
  var $httpBackend;
  var meetingRoom;
  var token = "aaaa-bbbb-cccc-dddd-1234";
  var user = { username: "jbond", phoneNumber: "1001", token: token };
  var checkAuthHeader;

  beforeEach(angular.mock.module('html-templates'));
  beforeEach(angular.mock.module('karma-backend'));
  beforeEach(angular.mock.module('ucAssistant'));


  beforeEach(angular.mock.inject(function (_MeetingRoomService_, _XucLink_, _$q_, _$httpBackend_) {
    meetingRoom = _MeetingRoomService_;
    $q = _$q_;
    $httpBackend = _$httpBackend_;

    spyOn(_XucLink_, 'whenLogged').and.returnValue($q.resolve(user));
    spyOn(_XucLink_, 'getServerUrl').and.returnValue("http://localhost");

    checkAuthHeader = (headers) => {
      return headers['Authorization'] === 'Bearer ' + token;
    };
  }));

  afterEach(function () {
    $httpBackend.verifyNoOutstandingExpectation();
    $httpBackend.verifyNoOutstandingRequest();
  });


  it('should add personal room', function () {
    const pm = { 'name': 'myroom', 'displayName': 'My Room' };

    $httpBackend.expectPOST('http://localhost/xuc/api/2.0/config/meetingrooms/personal', pm, checkAuthHeader).respond(() => {
      return [200, {}];
    });
    let promise = meetingRoom.add(pm);
    $httpBackend.flush();

    promise.then((result) => {
      expect(result.data).toEqual({});
    });
  });

  it('should get personal room', function () {
    const pm = { 'name': 'myroom', 'displayName': 'My Room' };

    $httpBackend.expectGET('http://localhost/xuc/api/2.0/config/meetingrooms/personal/1', checkAuthHeader).respond(() => {
      return [200, pm];
    });
    let promise = meetingRoom.get('1');
    $httpBackend.flush();

    promise.then((result) => {
      expect(result.data).toEqual(pm);
    });
  });

  it('should update personal room', function () {
    const pm = { 'name': 'myroom', 'displayName': 'My Room', 'userPin': 1234 };

    $httpBackend.expectPUT('http://localhost/xuc/api/2.0/config/meetingrooms/personal', pm, checkAuthHeader).respond(() => {
      return [200, pm];
    });
    let promise = meetingRoom.update(pm, '1');
    $httpBackend.flush();

    promise.then((result) => {
      expect(result.data).toEqual(pm);
    });
  });

  it('should remove personal room', function () {
    $httpBackend.expectDELETE('http://localhost/xuc/api/2.0/config/meetingrooms/personal/1', checkAuthHeader).respond(() => {
      return [204];
    });
    let promise = meetingRoom.remove('1');
    $httpBackend.flush();

    promise.then((result) => {
      expect(result.status).toEqual(204);
    });
  });

});
