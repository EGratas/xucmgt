const ContactService = require("ucassistant/services/contact.service");

describe('Service: Contact Service', function () {
  var ContactService;

  beforeEach(angular.mock.module('ucAssistant'));

  beforeEach(angular.mock.inject(function (_ContactService_) {
    ContactService = _ContactService_;
  }));

  it('should hasUsername', function () {
    expect(ContactService.hasUsername({})).toBe(false);
    expect(ContactService.hasUsername({username: 'toto'})).toBe(true);
  });

  it('should getPhoneStateBackColor', function() {
    expect(ContactService.getPhoneStateBackColor({})).toBe("user-statusundefined");
    expect(ContactService.getPhoneStateBackColor({videoStatus: 'Busy'})).toBe("user-videostatus-Busy");
  });

  it('should getPhoneStateLabel', function() {
    expect(ContactService.getPhoneStateLabel({})).toBe("USER_STATUS_[object Object]");
    expect(ContactService.getPhoneStateLabel({videoStatus: 'Busy'})).toBe("USER_STATUS_[object Object]");
  });

  it('should canShowChat', function() {
    expect(ContactService.canShowChat({})).toBe(false);
    expect(ContactService.canShowChat({username: 'toto'})).toBe(true);
  });

  it('should canShowVideo', function() {
    expect(ContactService.canShowVideo({})).toBe(false);
    expect(ContactService.canShowVideo({username: 'toto'})).toBe(true);
  });

  it('should userWithInviteToConferenceIcon', function() {
    expect(ContactService.userWithInviteToConferenceIcon({})).toBe(false);
  });

  it('should userWithInviteToConferenceIcon', function() {
    expect(ContactService.meetingRoomWithInviteToConferenceIcon({})).toBe(false);
  });

  it('should hasDetail', function() {
    expect(ContactService.hasDetail({phoneNumbers:''})).toBe(false);
    expect(ContactService.hasDetail({phoneNumbers: '0'})).toBe(true);
  });

  it('should userWithCallIcon', function() {
    expect(ContactService.userWithCallIcon({phoneNumbers: '0', entry: ['0','0','0','0','0','0']})).toBe(true);
  });

  it('should meetingRoomWithCallIcon', function() {
    expect(ContactService.meetingRoomWithCallIcon({})).toBe(false);
  });

  it('should hasJustEmail', function() {
    expect(ContactService.hasJustEmail({phoneNumbers: ''})).toBe(false);
  });

  it('should _isPersonal', function() {
    expect(ContactService._isPersonal({})).toBe('static');
  });

});