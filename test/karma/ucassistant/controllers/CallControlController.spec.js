import moment from 'moment';

describe('CallControl controller', function() {
  var $rootScope;
  var $scope;
  var ctrl;
  var CtiProxy;
  var lastIndex = 10;
  var XucPhoneState;
  var $httpBackend;
  var audioQualityStatistics;
  var $timeout;
  var $controller;
  var applicationConfiguration;

  beforeEach(angular.mock.module('html-templates'));
  beforeEach(angular.mock.module('karma-backend'));
  beforeEach(angular.mock.module('xcCti'));
  beforeEach(angular.mock.module('xcHelper'));
  beforeEach(angular.mock.module('ucAssistant'));

  function nextIndex() {
    return ++lastIndex;
  }

  function createParticipant(number, isTalking = false, role = 'User', isMuted = false, isMe = false, index = nextIndex()) {
    return {
      index: index,
      callerIdName: 'Number ' + number,
      callerIdNum: number,
      isTalking: isTalking,
      role: role,
      isMuted: isMuted,
      isMe: isMe
    };
  }

  function createConference(participants, currentUserRole = 'User', number = "4000") {
    return {
      conferenceName: "Conference " + number,
      conferenceNumber: number,
      participants: participants,
      since: 0,
      currentUserRole: currentUserRole,
      startTime: moment().toDate()
    };
  }

  beforeEach(function() {
    jasmine.addMatchers({
      toEqualData : customMatchers.toEqualData
    });
  });

  beforeEach(angular.mock.inject(function(_$rootScope_, _$controller_, _CtiProxy_, _XucPhoneState_, _$httpBackend_, _audioQualityStatistics_, _$timeout_, _applicationConfiguration_) {
    $rootScope =_$rootScope_;
    $scope = $rootScope.$new();
    CtiProxy = _CtiProxy_;
    XucPhoneState = _XucPhoneState_;
    $httpBackend = _$httpBackend_;
    audioQualityStatistics = _audioQualityStatistics_;
    $timeout = _$timeout_;
    applicationConfiguration = _applicationConfiguration_;
    $controller = _$controller_;
    
    spyOn(XucPhoneState, 'getCalls');
    $httpBackend.whenGET('/config/notifyOnHold').respond(false);
    ctrl = createController();
  }));

  var createController = () => {
    return $controller('CallControlController', {
      '$scope' :                  $scope,
      '$timeout':                 $timeout,
      'XucPhoneState':           XucPhoneState,
      'applicationConfiguration': applicationConfiguration,
      '$rootScope' :              $rootScope,
      'CtiProxy': CtiProxy,
    });
  };

  it('can instantiate controller', function(){
    expect(ctrl).not.toBeUndefined();
  });

  it('forwards toggleMicrophone action to ctiProxy', function() {
    spyOn(CtiProxy, 'toggleMicrophone');

    var call = {
      uniqueId: '1625046515.153',
      userData: {
        SIPCALLID: '123',
      }
    };

    ctrl.toggleMicrophone(call);
    expect(CtiProxy.toggleMicrophone).toHaveBeenCalledWith(call.uniqueId);
  });

  it('changes the display of the volume meter for server side conference muting', function() {
    spyOn(ctrl, 'conferenceMuteMe').and.callThrough();
    ctrl.isMuted = false;
    ctrl.toggleMicrophone({conference: true});

    expect(ctrl.conferenceMuteMe).toHaveBeenCalled();
    expect(ctrl.isMuted).toEqual(true);
  });

  it('forward conference muteme action to CtiProxy', function() {
    spyOn(CtiProxy, 'conferenceMuteMe');
    ctrl.conferenceMuteMe('4000');
    expect(CtiProxy.conferenceMuteMe).toHaveBeenCalledWith('4000');
  });

  it('forward conference unmuteme action to CtiProxy', function() {
    spyOn(CtiProxy, 'conferenceUnmuteMe');
    ctrl.conferenceUnmuteMe('4000');
    expect(CtiProxy.conferenceUnmuteMe).toHaveBeenCalledWith('4000');
  });

  it('forward conference mute all action to CtiProxy', function() {
    spyOn(CtiProxy, 'conferenceMuteAll');
    ctrl.conferenceMuteAll('4000');
    expect(CtiProxy.conferenceMuteAll).toHaveBeenCalledWith('4000');
  });

  it('forward conference unmute all action to CtiProxy', function() {
    spyOn(CtiProxy, 'conferenceUnmuteAll');
    ctrl.conferenceUnmuteAll('4000');
    expect(CtiProxy.conferenceUnmuteAll).toHaveBeenCalledWith('4000');
  });

  it('forward conference mute action to CtiProxy', function() {
    spyOn(CtiProxy, 'conferenceMute');
    ctrl.conferenceMute('4000', 1);
    expect(CtiProxy.conferenceMute).toHaveBeenCalledWith('4000', 1);
  });

  it('forward conference unmute action to CtiProxy', function() {
    spyOn(CtiProxy, 'conferenceUnmute');
    ctrl.conferenceUnmute('4000', 1);
    expect(CtiProxy.conferenceUnmute).toHaveBeenCalledWith('4000', 1);
  });

  it('should allow to mute all when someone in the conference (other from the organizer) is not muted', function() {
    var participants = [
      createParticipant('1000', false, 'Organizer', false, true),
      createParticipant('1001'),
      createParticipant('1002')
    ];
    var conference = createConference(participants, 'Organizer');
    
    expect(ctrl.canMuteAll(conference)).toEqual(true);
  });

  it('should not allow to mute all when not an organizer', function() {
    var participants = [
      createParticipant('1000', false, 'User', false, true),
      createParticipant('1001'),
      createParticipant('1002')
    ];
    var conference = createConference(participants, 'User');
    
    expect(ctrl.canMuteAll(conference)).toEqual(false);
  });

  it('should allow to unmute all when everyone in the conference (other than self) is muted', function() {
    var participants = [
      createParticipant('1000', false, 'Organizer', false, true),
      createParticipant('1001', false, 'User', true),
      createParticipant('1002', false, 'User', true)
    ];
    var conference = createConference(participants, 'Organizer');
    
    expect(ctrl.canUnmuteAll(conference)).toEqual(true);
  });

  it('should not allow to unmute all when not an organizer', function() {
    var participants = [
      createParticipant('1000', false, 'User', false, true),
      createParticipant('1001'),
      createParticipant('1002')
    ];
    var conference = createConference(participants, 'User');
    
    expect(ctrl.canUnmuteAll(conference)).toEqual(false);
  });

  it('should not allow to unmute all when someone (other than me) is not muted', function() {
    var participants = [
      createParticipant('1000', false, 'Organizer', false, true),
      createParticipant('1001', false, 'User', true),
      createParticipant('1002', false, 'User', false)
    ];    
    var conference = createConference(participants, 'Organizer');

    expect(ctrl.canUnmuteAll(conference)).toEqual(false);
  });

  it('forward conference kick action to CtiProxy', function() {
    var participant = createParticipant('1001', false, 'User', false, false, 1);
    spyOn(CtiProxy, 'conferenceKick');
    ctrl.conferenceKick('4000', participant);
    expect(CtiProxy.conferenceKick).toHaveBeenCalledWith('4000', 1);
  });

  it('set kicked property to true when kicking out a participant', function() {
    var participant = createParticipant('1001', false, 'User', false, false, 1);
    spyOn(CtiProxy, 'conferenceKick');
    ctrl.conferenceKick('4000', participant);
    expect(participant.kicked).toBeTruthy();
  });

  it('should display participant action for myself if not organizer', function() {
    var participant = createParticipant('1001', false, 'User', false, true, 1);
    var conference = createConference([participant], 'User');
    
    var result = ctrl.displayParticipantActions(conference, participant);
    expect(result).toBeTruthy();
  });

  it('should not display participant action for other if not organizer', function() {
    var participant = createParticipant('1001', false, 'User', false, false, 1);    
    var conference = createConference([participant], 'User');
    
    var result = ctrl.displayParticipantActions(conference, participant);
    expect(result).toBeFalsy();
  });

  it('should display participant action for other if organizer', function() {
    var participant = createParticipant('1001', false, 'User', false, false, 1);    
    var conference = createConference([participant], 'Organizer');
    
    var result = ctrl.displayParticipantActions(conference, participant);
    expect(result).toBeTruthy();
  });

  it('should not display participant action for kicked participant', function() {
    var participant = createParticipant('1001', false, 'User', false, false, 1);
    participant.kicked = true;
    var conference = createConference([participant], 'Organizer');
    
    var result = ctrl.displayParticipantActions(conference, participant);
    expect(result).toBeFalsy();
  });

  it('should display chat icon if the state is established', function() {
    var call = {
      state: "Established",
      username: "Alain"
    };
    var result = ctrl.canChat(call, call.username);
    expect(result).toBeTruthy(); 
  });

  it('should display chat icon if the state is OnHold', function() {
    var call = {
      state: "OnHold"
    };
    var participant = {
      username: "Alain"
    };
    var result = ctrl.canChat(call, participant.username);
    expect(result).toBeTruthy(); 
  });

  it('should not display chat icon if there is no username', function() {
    var call = {
      state: "OnHold"
    };
    var participant = {
    };
    var result = ctrl.canChat(call, participant.username);
    expect(result).toBeFalsy(); 
  });

  it('should not display chat icon if call is non established', function() {
    var call = {
      state: "EventReleased"
    };
    var participant = {
      username: "Alain"
    };
    var result = ctrl.canChat(call, participant.username);
    expect(result).toBeFalsy(); 
  }); 
  
  it('displays the webrtc mute button button only for webrtc users', function() {
    spyOn(CtiProxy, 'isUsingWebRtc');
    
    ctrl.muteWebRtcUser();
    expect(CtiProxy.isUsingWebRtc).toHaveBeenCalled();
  }); 

  it('displays the webrtc mute button button only after the event is established', function() {
    spyOn(CtiProxy, 'isUsingWebRtc').and.returnValue(true);
    var call = {
      state: 'Established',
    };
    var result =  ctrl.muteWebRtcUser(call);
    expect(result).toBeTruthy();
  }); 

  it('does not display the webrtc mute button if it is a conference', function() {
    spyOn(CtiProxy, 'isUsingWebRtc').and.returnValue(true);
    var call = {
      conference: ['123'],
    };
    var result =  ctrl.muteWebRtcUser(call);
    expect(result).toBeFalsy();
  });

  it('user can  stay muted as long as the call is ongoing', function() {
    $rootScope.$digest();
    ctrl.calls = [{call: "call 1"}];
    ctrl.isMuted = true;
    $rootScope.$digest();
    
    expect(ctrl.isMuted).toBeTruthy();
  });
  
  it('toggles the mute button back to false when user hangs up', function() {
    $rootScope.$digest();
    ctrl.calls = [];
    ctrl.isMuted = true;
    $rootScope.$digest();
    expect(ctrl.isMuted).toBeFalsy();
  });

  it('starts a quality error removal timeout when the quality was bad but is returning to good', function() {
    spyOn(CtiProxy, 'isUsingWebRtc').and.returnValue(true);
    ctrl = createController();
    $rootScope.$broadcast(audioQualityStatistics.AUDIO_QUALITY_EVENT, {quality: audioQualityStatistics.AUDIO_QUALITY_BAD, statistics: {}});
    expect(ctrl.displayAudioQuality).toEqual(true);
    $rootScope.$broadcast(audioQualityStatistics.AUDIO_QUALITY_EVENT, {quality: audioQualityStatistics.AUDIO_QUALITY_GOOD, statistics: {}});
    expect(ctrl.displayAudioQuality).toEqual(true);
    expect(ctrl.audioQualityTimeout).not.toEqual(undefined);
    $timeout.flush(ctrl.audioQualityTimeoutMS);
    expect(ctrl.displayAudioQuality).toEqual(false);
    expect(ctrl.audioQualityTimeout).toEqual(undefined);
  });

  it('cancels the quality error removal timeout when the quality was bad,went back to good, then went back to bad', function() {
    spyOn(CtiProxy, 'isUsingWebRtc').and.returnValue(true);
    ctrl = createController();
    $rootScope.$broadcast(audioQualityStatistics.AUDIO_QUALITY_EVENT, {quality: audioQualityStatistics.AUDIO_QUALITY_BAD, statistics: {}});
    expect(ctrl.displayAudioQuality).toEqual(true);
    $rootScope.$broadcast(audioQualityStatistics.AUDIO_QUALITY_EVENT, {quality: audioQualityStatistics.AUDIO_QUALITY_GOOD, statistics: {}});
    expect(ctrl.displayAudioQuality).toEqual(true);
    expect(ctrl.audioQualityTimeout).not.toEqual(undefined);
    $timeout.flush(ctrl.audioQualityTimeoutMS / 2);
    $rootScope.$broadcast(audioQualityStatistics.AUDIO_QUALITY_EVENT, {quality: audioQualityStatistics.AUDIO_QUALITY_BAD, statistics: {}});
    $timeout.flush(ctrl.audioQualityTimeoutMS / 2);
    expect(ctrl.displayAudioQuality).toEqual(true);
  });

  it('stops any ongoing quality timeout and hide the quality error when there is no more calls', function() {
    spyOn(CtiProxy, 'isUsingWebRtc').and.returnValue(true);
    ctrl = createController();
    ctrl.calls = [{callID: 1}];
    $rootScope.$broadcast(audioQualityStatistics.AUDIO_QUALITY_EVENT, {quality: audioQualityStatistics.AUDIO_QUALITY_BAD, statistics: {}});
    expect(ctrl.displayAudioQuality).toEqual(true);
    $rootScope.$broadcast(audioQualityStatistics.AUDIO_QUALITY_EVENT, {quality: audioQualityStatistics.AUDIO_QUALITY_GOOD, statistics: {}});
    expect(ctrl.audioQualityTimeout).not.toEqual(undefined);
    $timeout.flush(ctrl.audioQualityTimeoutMS / 2);
    ctrl.calls = [];
    $rootScope.$digest();
    expect(ctrl.displayAudioQuality).toEqual(false);
    expect(ctrl.audioQualityTimeout).toEqual(undefined);
  });

  it('doesnt start the webrtc audio monitoring if its not a webrtc user', function() {
    spyOn(CtiProxy, 'isUsingWebRtc').and.returnValue(false);
    ctrl = createController();
    ctrl.calls = [{callID: 1}];
    $rootScope.$broadcast(audioQualityStatistics.AUDIO_QUALITY_EVENT, {quality: audioQualityStatistics.AUDIO_QUALITY_BAD, statistics: {}});
    expect(ctrl.displayAudioQuality).toEqual(false);
    $rootScope.$broadcast(audioQualityStatistics.AUDIO_QUALITY_EVENT, {quality: audioQualityStatistics.AUDIO_QUALITY_GOOD, statistics: {}});
    expect(ctrl.audioQualityTimeout).toEqual(undefined);
  });

  it('display the call placeholder and start the timeout', function() {
    ctrl = createController();
    ctrl.calls = [];
    expect(ctrl.callPlaceholderDisplayed).toEqual(false);
    $rootScope.$broadcast("EventShowCallPlaceHolder");
    expect(ctrl.callPlaceholderDisplayed).toEqual(true);
    expect(ctrl.placeholderTimeout).not.toEqual(undefined);
  });

  it('then removes the call placeholder and stop the timeout when the call appears', function() {
    ctrl = createController();
    ctrl.calls = [];
    expect(ctrl.callPlaceholderDisplayed).toEqual(false);
    $rootScope.$broadcast("EventShowCallPlaceHolder");
    expect(ctrl.callPlaceholderDisplayed).toEqual(true);
    expect(ctrl.placeholderTimeout).not.toEqual(undefined);
    ctrl.calls = [{'fake': 'call'}];
    $rootScope.$digest();
    expect(ctrl.placeholderTimeout).toEqual(undefined);
  });

  it('end up removing the call placeholder if the call takes too much time appearing (aka forever lost in the endless void of asterisk)', function() {
    ctrl = createController();
    ctrl.calls = [];
    expect(ctrl.callPlaceholderDisplayed).toEqual(false);
    $rootScope.$broadcast("EventShowCallPlaceHolder");
    expect(ctrl.callPlaceholderDisplayed).toEqual(true);
    expect(ctrl.placeholderTimeout).not.toEqual(undefined);
    $timeout.flush(5001);
    expect(ctrl.callPlaceholderDisplayed).toEqual(false);
    expect(ctrl.placeholderTimeout).toEqual(undefined);
  });

  it('retrieve the latest dialed number and remove any previously set dial error', function() {
    ctrl = createController();
    ctrl.calls = [];
    spyOn(ctrl, 'closeCallInitError');
    $rootScope.$broadcast("dialingNumber", 3553);
    expect(ctrl.closeCallInitError).toHaveBeenCalled();
    expect(ctrl.dynamicTranslation.phoneNb).toEqual(3553);
  });

  it('should reset the mute state to false when putting on hold since the mute status is not persisted', function() {
    ctrl = createController();
    spyOn(ctrl.CtiProxy, 'hold').and.callFake(() => {});
    ctrl.calls = [{callID: 1}];
    ctrl.isMuted = true;
    expect(ctrl.isMuted).toEqual(true);
    ctrl.hold(1);
    expect(ctrl.isMuted).toEqual(false);
  });
});
