import { CallHistoryModel } from "ucassistant/models/CallHistory.model";

describe('CallHistory controller', function() {
  var $rootScope;
  var $state;
  var $scope;
  var ctrl;
  var xucUtils;
  var xucPhoneState;
  var ctiProxy;

  beforeEach(angular.mock.module('html-templates'));
  beforeEach(angular.mock.module('karma-backend'));
  beforeEach(angular.mock.module('xcHelper'));
  beforeEach(angular.mock.module('ucAssistant'));
  beforeEach(angular.mock.module('xcCti'));

  beforeEach(function() {
    jasmine.addMatchers({
      toEqualData : customMatchers.toEqualData
    });
  });

  beforeEach(angular.mock.inject(function(_$rootScope_, $controller, _$translate_ , _$state_, _XucUtils_, _XucPhoneState_, _CtiProxy_) {
    $rootScope =_$rootScope_;
    $scope = $rootScope.$new();
    $state = _$state_;
    xucUtils = _XucUtils_;
    xucPhoneState = _XucPhoneState_;
    ctiProxy = _CtiProxy_;

    ctrl = $controller('CallHistoryController', {
      '$scope' :      $scope,
      '$translate':  _$translate_,
      '$state' : $state
    });
  }));

  it('can instanciate controller', function(){
    expect(ctrl).not.toBeUndefined();
  });

  it('dial when no call in progress', function(){
    spyOn(xucUtils,'normalizePhoneNb').and.returnValue('5646546');
    spyOn(xucPhoneState, 'getCalls').and.returnValue([]);
    spyOn(ctiProxy,'dial');

    ctrl.stopEventAndDial({stopPropagation: function() {}}, '5646546');

    expect(ctiProxy.dial).toHaveBeenCalled();

  });

  it('attendedTransfer when call in progress', function(){
    spyOn(xucUtils,'normalizePhoneNb').and.returnValue('5646546');
    spyOn(xucPhoneState, 'getCalls').and.returnValue(['one']);
    spyOn(ctiProxy,'attendedTransfer');

    ctrl.stopEventAndDial({stopPropagation: function() {}}, '5646546');

    expect(ctiProxy.attendedTransfer).toHaveBeenCalled();

  });

  it('redirect to new personal contact creation page with values set', function() {
    spyOn($state,'go');

    ctrl.stopEventAndCreateContact({stopPropagation: function() {}}, '1234', 'John', 'Doe');

    expect($state.go).toHaveBeenCalledWith('interface.personalContact',
      {"contact": {"number": "1234", "firstname": "John", "lastname": "Doe"}}
    );
  });


  it('should toggle the item display property when the function is called', function () {
    let item = {
      'display': true
    };

    ctrl.toggleChildrens(item);

    expect(item.display).toBe(false);
  });

  it('should return the correct css class for user-status in call history', function () {
    let callHistory = new CallHistoryModel();
    callHistory.initials = "";

    expect(ctrl.getCallHistoryUserStatusClass(callHistory)).toBe("externalNumber");

    callHistory.initials = "J D";

    expect(ctrl.getCallHistoryUserStatusClass(callHistory)).toBe(undefined);
  });

}); 
