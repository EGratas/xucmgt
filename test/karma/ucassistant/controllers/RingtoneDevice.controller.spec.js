describe('Ringtone device controller', function () {
  var $rootScope;
  var $scope;
  var mediaDevices;
  var ctrl;
  var xcHelperPreferences;
  var webRtcAudio;
  var $controller;
  
  beforeEach(angular.mock.module('ucAssistant'));
  beforeEach(angular.mock.module('xcCti'));

  const newControllerWithDevices = function(devices) {
    mediaDevices = jasmine.createSpy("mediaDevices", {
      "getAudioOutput": () => { return Promise.resolve(devices); }
    });
    ctrl = $controller('RingtoneDeviceController', {
      '$scope': $scope,
      'mediaDevices': mediaDevices,
      'xcHelperPreferences': xcHelperPreferences,
      'webRtcAudio': webRtcAudio
    });
    ctrl.devices = dummyDevices;
  };

  const dummyDevices = [
    { 
      deviceId: "Device A",
      groupId: "1",
      kind: "audiooutput",
      label: "Some label A"
    },
    { 
      deviceId: "Device B",
      groupId: "2",
      kind: "audiooutput",
      label: "Some label B"
    },
    { 
      deviceId: "Device C",
      groupId: "3",
      kind: "audiooutput",
      label: "Some label C"
    }
  ];
  
  beforeEach(angular.mock.inject(function (_mediaDevices_, _xcHelperPreferences_, _webRtcAudio_, _$rootScope_, _$controller_) {
    $rootScope = _$rootScope_;
    $scope = $rootScope.$new();
    mediaDevices = _mediaDevices_;    
    xcHelperPreferences = _xcHelperPreferences_;
    webRtcAudio = _webRtcAudio_;
    $controller = _$controller_;
    
    ctrl = $controller('RingtoneDeviceController', {
      '$scope': $scope,
      'mediaDevices': mediaDevices,
      'xcHelperPreferences': xcHelperPreferences,
      'webRtcAudio': webRtcAudio
    });
  }));
  
  it('can instantiate controller', function () {
    expect(ctrl).not.toBeUndefined();
    expect(ctrl.devices).toEqual([]);
    expect(ctrl.ringingDeviceId).toEqual('default');
  });

  it('can find a device in an array based on its device id', function() {
    const devices = [{deviceId: 'abcd', label: 'A'}, {deviceId: 'efgh', label: 'B'}];

    expect(ctrl.containsDeviceId(devices, 'abcd')).toEqual({deviceId: 'abcd', label: 'A'});
    expect(ctrl.containsDeviceId(devices, 'ijkl')).toEqual(undefined);
  });

  it('can instantiate controller with a preselected localstorage device', function() {
    spyOn(xcHelperPreferences, "getRingingDeviceId").and.returnValue("Device B");
    spyOn(xcHelperPreferences, "setRingingDeviceId").and.callThrough();
    newControllerWithDevices(dummyDevices);
    expect(ctrl.devices).toEqual(dummyDevices);
    expect(ctrl.ringingDeviceId).toEqual("Device B");
  });

  it('falls back to default device if preselected device is not connected anymore', function(done) {
    spyOn(xcHelperPreferences, "getRingingDeviceId").and.returnValue("Disconnected device");
    spyOn(xcHelperPreferences, "setRingingDeviceId").and.callThrough();

    newControllerWithDevices(dummyDevices);
    ctrl.init("Disconnected device").then(() => {
      expect(ctrl.devices).toEqual(dummyDevices);
      expect(ctrl.ringingDeviceId).toEqual("default");
      expect(xcHelperPreferences.setRingingDeviceId).toHaveBeenCalledWith("default");
      done();
    });
  });
  
});
