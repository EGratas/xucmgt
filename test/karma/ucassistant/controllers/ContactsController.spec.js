describe('Contacts controller', function() {
  var $rootScope;
  var $scope;
  var remoteConfiguration;
  var ctrl;
  var XucPhoneState;

  beforeEach(angular.mock.module('html-templates'));
  beforeEach(angular.mock.module('karma-backend'));
  beforeEach(angular.mock.module('xcCti'));
  beforeEach(angular.mock.module('xcHelper'));
  beforeEach(angular.mock.module('ucAssistant'));

  beforeEach(function() {
    jasmine.addMatchers({
      toEqualData : customMatchers.toEqualData
    });
  });

  beforeEach(angular.mock.inject(function(_$rootScope_, $controller, _remoteConfiguration_, _XucPhoneState_) {
    $rootScope =_$rootScope_;
    $scope = $rootScope.$new();
    $rootScope = _$rootScope_;
    remoteConfiguration = _remoteConfiguration_;
    XucPhoneState = _XucPhoneState_;

    ctrl = $controller('ContactsController', {
      '$scope' :                  $scope,
      '$rootScope' :              $rootScope,
      'remoteConfiguration':      remoteConfiguration
    });
  }));

  it('can instantiate controller', function(){
    expect(ctrl).not.toBeUndefined();
  });

  it('returns true if contact is a meeting room', () => {
    const contact = {
      source: 'xivo_meetingroom',
    };
    expect($scope.isMeetingRoom(contact)).toBe(true);
  });

  it('returns false if contact is not a meeting room', () => {
    const contact = {
      source: 'internal',
    };
    expect($scope.isMeetingRoom(contact)).toBe(false);
  });

  it('isCallable returns true if contact contains a number', () => {
    const contact = {
      source: 'internal',
      entry: ["bloop", "1001", "", ""]
    };
    expect($scope.isCallable(contact)).toBe(true);

    const contact2 = {
      source: 'internal',
      entry: ["bloop", "", "1001", ""]
    };
    expect($scope.isCallable(contact2)).toBe(true);

    const contact3 = {
      source: 'internal',
      entry: ["bloop", "", "", "1001"]
    };
    expect($scope.isCallable(contact3)).toBe(true);
  });

  it('isCallable returns true if contact contains an email', () => {
    const contact = {
      source: 'internal',
      entry: ["bloop", "", "", "", false, "bloop@bloop.com"]
    };
    expect($scope.isCallable(contact)).toBe(true);
  });

  it('isCallable returns true if contact is a meeting room', () => {
    const contact = {
      source: 'xivo_meetingroom',
      entry: ["bloop room", "", "", "", false, ""]
    };
    expect($scope.isCallable(contact)).toBe(true);
  });

  it('isCallable returns false if contact contains no number and is not a meeting room', () => {
    const contact = {
      source: 'internal',
      entry: ["bloop", "", "", ""]
    };
    expect($scope.isCallable(contact)).toBe(false);
  });

  it('shows the chevron if the contact has either more than one number, a number + email, and cannot invite to audio conf', () => {
    
    const contact1 = {
      source: 'internal',
      phoneNumbers: ["1000"]
    };

    const contact2 = {
      source: 'internal',
      phoneNumbers: ["1000", "0606060606"]
    };

    const contact3 = {
      source: 'internal',
      phoneNumbers: ["1000"],
      email: "email@email.com"
    };

    const contact4 = {
      source: 'internal',
      phoneNumbers: ["1000", "0606060606"],
      email: "email@email.com"
    };

    const contact5 = {
      source: 'internal',
      phoneNumbers: [],
      email: "email@email.com"
    };

    expect($scope.hasChevron(contact1)).toBe(false);
    expect($scope.hasChevron(contact2)).toBe(true);
    expect($scope.hasChevron(contact3)).toBe(true);
    expect($scope.hasChevron(contact4)).toBe(true);
    expect($scope.hasChevron(contact5)).toBe(false);
  });

  it('hasJustEmail returns true if contact contains email and no number' , () => {
    const contact1 = {
      source: 'internal',
      phoneNumbers: ["1000", "0606060606"]
    };

    const contact2 = {
      source: 'internal',
      phoneNumbers: ["1000"],
      email: "email@email.com"
    };

    const contact3 = {
      source: 'internal',
      phoneNumbers: [],
      email: "email@email.com"
    };

    const contact4 = {
      source: 'internal',
      phoneNumbers: []
    };
    
    expect($scope.hasJustEmail(contact1)).toBe(false);
    expect($scope.hasJustEmail(contact2)).toBe(false);
    expect($scope.hasJustEmail(contact3)).toBe(true);
    expect($scope.hasJustEmail(contact4)).toBe(false);
  });

  it('shows the call icon near the user' , () => {
    spyOn(XucPhoneState, 'getConferenceOrganizer').and.returnValue(false);
    const contact1 = {
      toggleDetails : false,
      source: 'internal',
      phoneNumbers: ["1000"],
      entry: ["bloop room", "1000", "","",false, "bloop@bloop.com"],
      email: "bloop@bloop.com"
    };
    
    const contact2 = {
      toggleDetails : false,
      source: 'internal',
      phoneNumbers: [],
      entry: ["bloop room", "", "", "", false, "bloop@bloop.com"],
      email: "bloop@bloop.com"
    };
    
    const contact3 = {
      toggleDetails : true,
      source: 'internal',
      phoneNumbers: ["1000"],
      entry: ["bloop room", "1000", "","",false, "bloop@bloop.com"],
      email: "bloop@bloop.com"
    }; 

    const contact4 = {
      toggleDetails : false,
      source: 'xivo-meetingroom',
      phoneNumbers: ["1000"],
      entry: ["bloop room", "1000", "","",false, "bloop@bloop.com"],
      email: "bloop@bloop.com"
    }; 

    const contact5 = {
      toggleDetails : false,
      source: 'internal',
      phoneNumbers: [],
      entry: ["bloop room", "", "",""],
    }; 

    $rootScope.$digest();
    expect($scope.userWithCallIcon(contact1)).toBe(true);
    expect($scope.userWithCallIcon(contact2)).toBe(false);
    expect($scope.userWithCallIcon(contact3)).toBe(false);
    expect($scope.userWithCallIcon(contact4)).toBe(true);
    expect($scope.userWithCallIcon(contact5)).toBe(false);
  });    
});
