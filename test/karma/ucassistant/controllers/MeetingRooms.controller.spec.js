import { MeetingRoom } from "ucassistant/services/meetingRoom.service";

describe('MeetingRooms controller', function () {
  var $rootScope;
  var $scope;
  var $state;
  var $q;
  var $uibModalStack;
  var $controller;
  var meetingRoom;
  var XucDirectory;
  var ctrl;

  beforeEach(angular.mock.module('html-templates'));
  beforeEach(angular.mock.module('karma-backend'));
  beforeEach(angular.mock.module('ucAssistant'));

  beforeEach(angular.mock.inject(function (_$rootScope_, _$state_, _$uibModalStack_, _MeetingRoomService_, _$q_, _$controller_, _XucDirectory_) {
    $rootScope = _$rootScope_;
    $scope = $rootScope.$new();
    $state = _$state_;
    $uibModalStack = _$uibModalStack_;
    $q = _$q_;
    $controller = _$controller_;
    meetingRoom = _MeetingRoomService_;
    XucDirectory = _XucDirectory_;
  }));

  const initCtrl = (roomId) => {
    ctrl = $controller('MeetingRoomsController', {
      '$scope': $scope,
      '$state': $state,
      '$uibModalStack': $uibModalStack,
      'meetingRoom': meetingRoom,
      'XucDirectory': XucDirectory,
      'roomId': roomId
    });
  };

  it('can instantiate controller', function () {
    initCtrl();
    expect(ctrl).not.toBeUndefined();
  });

  it('creates a meeting room', function (done) {
    initCtrl();
    let mr = new MeetingRoom(undefined, "Planning Meeting", "1234");

    spyOn(meetingRoom, 'add').and.returnValue(Promise.resolve(mr));
    spyOn($state, 'go');
    spyOn(Cti, 'addFavorite');


    ctrl.save(mr).then(() => {
      expect(meetingRoom.add).toHaveBeenCalledWith(mr);
      expect(Cti.addFavorite).not.toHaveBeenCalled();
      expect($state.go).toHaveBeenCalledWith('interface.favorites');
      expect(ctrl.ajaxRequest.ongoing).toBe(false);
      done();
    });
  });


  it('dismiss modal if opened', function () {
    initCtrl();
    spyOn($state, 'go');
    spyOn($uibModalStack, 'dismissAll');

    ctrl.close('destination');
    expect($state.go).toHaveBeenCalledWith('destination');
    expect($uibModalStack.dismissAll).toHaveBeenCalled();
  });

  it('creates a meeting room and sets it as favorite', function (done) {
    initCtrl();
    let mr = new MeetingRoom(1, "Planning Meeting", "Planning Meeting", "1234");
    spyOn(XucDirectory, 'getFavorites').and.returnValue([{ contact_id: '1' }]);
    spyOn(meetingRoom, 'add').and.returnValue(Promise.resolve({data: mr}));


    spyOn($state, 'go');
    spyOn(Cti, 'addFavorite');
    ctrl.favorite = true;

    ctrl.save(mr).then(() => {
      expect(meetingRoom.add).toHaveBeenCalledWith(mr);
      expect(Cti.addFavorite).toHaveBeenCalledWith('1', 'xivo_meetingroom');
      done();
    });
  });

  it('updates a meeting room', function (done) {
    initCtrl(1);
    let mr = new MeetingRoom(1, "Planning Meeting", "Planning Meeting", "1234");
    spyOn(meetingRoom, 'update').and.returnValue(Promise.resolve(mr));

    spyOn($state, 'go');
    spyOn(Cti, 'addFavorite');

    ctrl.save(mr).then(() => {
      expect(meetingRoom.update).toHaveBeenCalledWith(mr);
      expect(Cti.addFavorite).not.toHaveBeenCalled();
      expect($state.go).toHaveBeenCalledWith('interface.favorites');
      expect(ctrl.ajaxRequest.ongoing).toBe(false);
      done();
    });
  });

  it('deletes a meeting room', function () {
    initCtrl('1');

    var result = $q.defer();
    result.resolve({});
    spyOn(meetingRoom, 'remove').and.callFake(() => {
      return result.promise;
    });
    spyOn($state, 'go');

    ctrl.remove();
    $rootScope.$digest();
    expect(meetingRoom.remove).toHaveBeenCalledWith('1');
    expect($state.go).toHaveBeenCalledWith('interface.favorites');
  });
  
  it('creating meeting room with empty pin code should not insert an empty pincode', function (done) {
    initCtrl();
    let mr = new MeetingRoom(undefined, "Planning Meeting", "");

    spyOn(meetingRoom, 'add').and.returnValue(Promise.resolve(mr));
    spyOn($state, 'go');
    spyOn(Cti, 'addFavorite');

    ctrl.save(mr).then(() => {
      expect(meetingRoom.add).toHaveBeenCalledWith(mr);
      expect(ctrl.save.userPin).toBe(undefined);
      expect(Cti.addFavorite).not.toHaveBeenCalled();
      expect($state.go).toHaveBeenCalledWith('interface.favorites');
      expect(ctrl.ajaxRequest.ongoing).toBe(false);
      done();
    });
  });

});