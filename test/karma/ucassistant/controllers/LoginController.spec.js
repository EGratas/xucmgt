describe('LoginController', function() {

  var $rootScope;
  var $scope;
  var $state;
  var $stateParams;
  var $window;
  var ctrl;
  var XucLink;
  var CtiProxy;
  var WebSocket;
  var electronWrapper;
  var remoteConfiguration;

  var oldWebsocket = null;

  beforeEach(angular.mock.module('karma-backend'));
  beforeEach(angular.mock.module('ucAssistant'));
  beforeEach(angular.mock.module('xcCti'));

  beforeEach(function() {
    $window = { location: { search: { indexOf: jasmine.createSpy().and.returnValue(0) } } };

    angular.mock.module(function($provide) {
      $provide.value('$window', $window);
    });
  });

  beforeEach(function() {
    jasmine.addMatchers({
      toEqualData : customMatchers.toEqualData
    });
  });

  beforeEach(angular.mock.inject(function(_$rootScope_, $controller, _$state_, _$stateParams_, _$window_, _XucLink_, _CtiProxy_, _electronWrapper_, _remoteConfiguration_) {
    $rootScope =_$rootScope_;
    $scope = $rootScope.$new();
    $state = _$state_;
    $stateParams = _$stateParams_;
    $window = _$window_;
    XucLink = _XucLink_;
    CtiProxy = _CtiProxy_;
    electronWrapper =_electronWrapper_;
    remoteConfiguration = _remoteConfiguration_;

    if (typeof WebSocket === 'object') {
      oldWebsocket = WebSocket;
    }
    WebSocket = function() {
      return {
        send: function() {},
        close: function() {}
      };
    };

    spyOn(Cti, 'setHandler');
    spyOn(XucLink, 'initCti');
    $window.externalConfig = { hostAndPort: 'xuc:1234', useSso: false };
    $window.electronRelativeDownloadPath = "xivo-desktop-assistant/1.0/1.0.5";
    $window.appVersionNoExtension = "1.0.5";

    ctrl = $controller('LoginController', {
      '$scope' : $scope,
      '$state' : $state,
      '$stateParams' : $stateParams,
      '$window' : $window,
      'XucLink' : XucLink,
      'CtiProxy' : CtiProxy,
      'electronWrapper' : electronWrapper,
      'remoteConfiguration' : remoteConfiguration
    });
  }));

  afterEach(function() {
    if (typeof oldWebsocket === 'object') {
      WebSocket = oldWebsocket;
    } else {
      WebSocket = null;
    }
    oldWebsocket = null;
  });

  it('can instanciate controller', function() {
    expect(ctrl).not.toBeUndefined();
  });

  it('display download buttons when conditions are met', function() {
    spyOn(electronWrapper, 'isElectron').and.returnValue(false);
    $scope.isSwitchboard = false;
    $scope.displayDesktopDownload = true;

    let display = $scope.displayDownloadDesktop();
    expect(display).toBeTruthy();
  });

  it('doesnt display download buttons when conditions are not met', function() {
    spyOn(electronWrapper, 'isElectron').and.returnValue(false);
    $scope.displayDesktopDownload = false;

    let display = $scope.displayDownloadDesktop();
    expect(display).toBeFalsy();
    
  });

});
