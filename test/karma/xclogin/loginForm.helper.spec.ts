import angular from "angular"
import { LoginHelper, AuthConfiguration } from "../../../app/assets/javascripts/xclogin/directives/loginForm.helper"
import { XucLink } from "../../../app/assets/javascripts/xccti/services/XucLink"
import { IRootScopeService, ILogService, ITimeoutService, IWindowService } from "angular"


describe('LoginFormHelper', () => {

  let loginHelper:        LoginHelper;
  let xucLink:            XucLink;
  let $rootScope:         IRootScopeService;
  let $timeout:           ITimeoutService;
  let $log:               ILogService;
  let $window:            IWindowService;
  let authConfig:         AuthConfiguration;

  let defaultHref = 'http://myxucmgt/'
  let locationValue = {
    origin: 'test',
    protocol: 'http:',
    href: defaultHref,
    replace: function(href: string) {
      this.href = href
    }
  }

  let windowProvider = {
    $get: function() {
      return {
        location: locationValue,
        navigator: {languages: 'en_EN'}}
    }
  }

  beforeEach(() => {
    angular.mock.module('html-templates')
    angular.mock.module('karma-backend')
    angular.mock.module('xcCti')
    angular.mock.module('xcHelper')
    angular.mock.module('xcLogin')
  
    angular.mock.module(($provide: any) => {
      $provide.provider('$window', windowProvider)
    })

    angular.mock.inject((_XucLink_: XucLink, _$rootScope_: IRootScopeService, _$timeout_: ITimeoutService,_$log_: ILogService, _$window_: IWindowService) => {
      xucLink     = _XucLink_;
      $rootScope  = _$rootScope_;
      $timeout    = _$timeout_;
      $log        = _$log_;
      $window     = _$window_;
      
      authConfig  = {
        useSso: false,
        casServerUrl: '',
        casLogoutEnable: false,
        openidServerUrl: '',
        openidClientId: '',
        requirePhoneNumber: false
      }
    })
  })

  it('should attempt autologin with token', async () => {
    spyOn(xucLink, 'getStoredCredentials').and.callFake(() => undefined)

    loginHelper = new LoginHelper(
      authConfig,
      xucLink,
      $timeout,
      $log,
      $window
    )

    let credentials = {
      username: undefined,
      phoneNumber: undefined,
      token: 'aaaa-bbbb-cccc-dddd'
    }

    locationValue.replace(`http://myxucmgt/?token=${credentials.token}`)

    loginHelper.getXucCredentials()
      .then((res) => {
        expect(res).toEqual(credentials)
      })
      .catch((err) => {
        expect(err).toBeUndefined
      })
    
    locationValue.replace(defaultHref)
    $rootScope.$digest()
  })

  it('should recect an error if no credentials and no token is provided', () => {
    loginHelper = new LoginHelper(
      authConfig,
      xucLink,
      $timeout,
      $log,
      $window
    )

    spyOn(xucLink, 'getStoredCredentials').and.callFake(() => undefined);

    loginHelper.getXucCredentials()
      .then()
      .catch((err) => {
        expect(err).toEqual({ error: 'NoStoredCredentials', message: undefined })
      })

    $rootScope.$digest()
  })

  it('should get stored credentials', () => {
    loginHelper = new LoginHelper(
      authConfig,
      xucLink,
      $timeout,
      $log,
      $window
    )

    let storedCredentials = {
      username: 'jbond',
      phoneNumber: '1000',
      token: 'aaaa-bbbb-cccc-dddd'
    }

    jasmine.createSpy('getStoredCredentials')

    xucLink.getStoredCredentials = jasmine.createSpy().and.returnValue(storedCredentials)

    loginHelper.getXucCredentials()
      .then((res) => {
        expect(res).toEqual(storedCredentials)
      })
  })

  
})