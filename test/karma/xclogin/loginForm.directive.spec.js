describe('loginForm directive', function() {
  var XucLink = {
    login: jasmine.createSpy('login'),
    checkAndloginWithCredentials: jasmine.createSpy('checkAndloginWithCredentials'),
    checkXucIsUp: jasmine.createSpy('checkXucIsUp'),
    getStoredCredentials : jasmine.createSpy('getStoredCredentials'),
    getCasToken: jasmine.createSpy('getCasToken'),
    tradeCasToken: jasmine.createSpy('tradeCasToken'),
    getSsoCredentials: jasmine.createSpy('getSsoCredentials'),
    logoutFromCas: jasmine.createSpy('logoutFromCas'),
    setHostAndPort : jasmine.createSpy('setHostAndPort'),
    setRedirectToHomeUrl: jasmine.createSpy('setRedirectToHomeUrl'),
    parseUrlParameters: jasmine.createSpy('setRedirectToHomeUrl'),
    getOpenidToken: jasmine.createSpy('getOpenidToken'),
    tradeOpenidToken: jasmine.createSpy('tradeOpenidToken'),
    setLoginTimeoutMs: jasmine.createSpy('setloginTimeoutMs'),
    getXucUser: jasmine.createSpy('getXucUser').and.returnValue({}),
    logoutFromOpenid: jasmine.createSpy('logoutFromOpenid')
  };

  var locationValue = {
    origin: 'test',
    protocol: 'http:',
    href: 'http://myxucmgt/',
    search: ''
  };
  var windowProvider = {
    $get: function() {
      return {
        location: locationValue,
        navigator: {languages: 'en_EN'}};
    }
  };

  var timeoutParameterDefer;
  var remoteConfiguration;

  beforeEach(angular.mock.module('karma-backend'));

  beforeEach(function() {
    jasmine.addMatchers({
      toEqualData : customMatchers.toEqualData
    });

    angular.mock.module(function($provide, $qProvider) {
      $provide.provider('$window', windowProvider);
      $qProvider.errorOnUnhandledRejections(false);
    });
   
    angular.mock.module('xcLogin', function ($translateProvider) {
      $translateProvider.preferredLanguage('en');
    });

    angular.mock.module('xcCti', function ($provide, _$compileProvider_) {
      $provide.decorator("XucLink", function() {return XucLink;});
      _$compileProvider_.debugInfoEnabled(true);
    });
  });

  beforeEach(angular.mock.inject(function($q, _remoteConfiguration_) {
    remoteConfiguration = _remoteConfiguration_;
    timeoutParameterDefer = $q.defer();
    spyOn(remoteConfiguration, 'getInt').and.returnValue(timeoutParameterDefer.promise);
    XucLink.checkXucIsUp.and.returnValue(Promise.resolve());
    XucLink.getStoredCredentials.and.returnValue(null);
  }));

  it('should set login timeout based on server configuration', angular.mock.inject(function($rootScope, $compile, $httpBackend) {
    var scope = $rootScope.$new();
    scope.hostAndPort = "xuc:9876";
    scope.onLogin = jasmine.createSpy("onLogin");
    var newTimeout = 7652;
    XucLink.parseUrlParameters.and.returnValue({});
    timeoutParameterDefer.resolve(newTimeout);
    
    XucLink.getStoredCredentials.and.returnValue(null);
    
    $compile('<login-form on-login="onLogin()" host-and-port="hostAndPort" />')(scope);
    $httpBackend.expectGET("assets/javascripts/xclogin/directives/loginForm.html").respond("<div>my template</div>");
    
    $rootScope.$digest();
    $httpBackend.flush();

    expect(remoteConfiguration.getInt).toHaveBeenCalledWith(remoteConfiguration.LOGIN_TIMEOUT_MS);
    expect(XucLink.setLoginTimeoutMs).toHaveBeenCalledWith(newTimeout);

  }));
  
  it('should check stored credentials and attempt autologin', function(done) {
    angular.mock.inject(function($rootScope, $compile, $httpBackend) {
      var scope = $rootScope.$new();
      scope.hostAndPort = "xuc:9876";
      scope.onLogin = jasmine.createSpy("onLogin");
      XucLink.parseUrlParameters.and.returnValue({});
    
      var credentials = {login: "jbond", token: "aaaa-bbbb-cccc-dddd-1234", phoneNumber: "1001"};
      XucLink.getStoredCredentials.and.returnValue(credentials);
      XucLink.checkAndloginWithCredentials.and.callFake((c) => {
        expect(loginForm).toBeDefined();
        expect(loginForm.controller).toBeDefined();
        expect(c).toBe(credentials);
        done();
      });
    
      var loginForm = $compile('<login-form on-login="onLogin()" host-and-port="hostAndPort" />')(scope);
      $httpBackend.expectGET("assets/javascripts/xclogin/directives/loginForm.html").respond("<div>my template</div>");
    
      $rootScope.$digest();
      $httpBackend.flush();      
    });});

  it('should attempt autologin with Kerberos when configured to and store credentials', function(done) {
    angular.mock.inject(function($rootScope, $compile, $httpBackend) {
      var scope = $rootScope.$new();
      scope.hostAndPort = "xuc:9876";
      scope.useSso = true;
      scope.onLogin = jasmine.createSpy("onLogin");
      XucLink.getStoredCredentials.and.returnValue(null);
      var credentials = {login: "jbond", token: "aaaa-bbbb-cccc-dddd-1234", phoneNumber: "1001"};
      XucLink.parseUrlParameters.and.returnValue({});
      XucLink.getSsoCredentials.and.returnValue(Promise.resolve(credentials));
      XucLink.checkAndloginWithCredentials.and.callFake((c) => {
        expect(loginForm).toBeDefined();
        expect(loginForm.controller).toBeDefined();
        expect(XucLink.getSsoCredentials).toHaveBeenCalled();
        expect(c).toBe(credentials);
        done();
      });
      
      var loginForm = $compile('<login-form on-login="onLogin()" host-and-port="hostAndPort" use-sso="useSso"/>')(scope);
      $httpBackend.expectGET("assets/javascripts/xclogin/directives/loginForm.html").respond("<div>my template</div>");      
      $httpBackend.flush();
      
      
    });
  });

  it('should attempt autologin with Kerberos and hide password field if required', function(done) {
    angular.mock.inject(function($rootScope, $compile, $httpBackend, $q) {
      var scope = $rootScope.$new();
      scope.hostAndPort = "xuc:9876";
      scope.useSso = true;
      scope.onLogin = jasmine.createSpy("onLogin");
      XucLink.getStoredCredentials.and.returnValue(null);
      var credentials = {login: "jbond", token: "aaaa-bbbb-cccc-dddd-1234", phoneNumber: "1001"};
      XucLink.parseUrlParameters.and.returnValue({});
      XucLink.getSsoCredentials.and.returnValue($q.when(credentials));
      XucLink.checkAndloginWithCredentials.and.callFake(() => {
        expect(loginForm).toBeDefined();
        expect(loginForm.controller).toBeDefined();
        expect(XucLink.getSsoCredentials).toHaveBeenCalled();
        var isolateScope = loginForm.isolateScope();
        expect(isolateScope.ctrl.autoLoginInProgress).toEqual(true);
        expect(isolateScope.ctrl.requireCredentials).toEqual(false);
        done();
      });
    
      var loginForm = $compile('<login-form on-login="onLogin()" host-and-port="hostAndPort" use-sso="useSso" require-phone-number="false"/>')(scope);
      $httpBackend.expectGET("assets/javascripts/xclogin/directives/loginForm.html").respond("<div>my template</div>");
      
      $rootScope.$digest();
      $httpBackend.flush();
    });
  });

  it('should allow autologin with Kerberos when logging out', function(done) {
    angular.mock.inject(function($rootScope, $compile, $httpBackend, $q) {
      var scope = $rootScope.$new();
      scope.hostAndPort = "xuc:9876";
      scope.useSso = true;
      scope.onLogin = jasmine.createSpy("onLogin");
      XucLink.getStoredCredentials.and.returnValue(null);
      var credentials = {login: "jbond", token: "aaaa-bbbb-cccc-dddd-1234", phoneNumber: "1001"};
      XucLink.parseUrlParameters.and.returnValue({});
      XucLink.getSsoCredentials.and.returnValue($q.when(credentials));
      XucLink.checkAndloginWithCredentials.and.callFake(() => {
        expect(loginForm).toBeDefined();
        expect(loginForm.controller).toBeDefined();
        expect(XucLink.getSsoCredentials).toHaveBeenCalled();
        var isolateScope = loginForm.isolateScope();
        expect(isolateScope.ctrl.requireCredentials).toEqual(false);
        done();
      });
      
      var loginForm = $compile('<login-form on-login="onLogin()" host-and-port="hostAndPort" use-sso="useSso" require-phone-number="false"/>')(scope);
      $httpBackend.expectGET("assets/javascripts/xclogin/directives/loginForm.html").respond("<div>my template</div>");
    
      $rootScope.$digest();
      $httpBackend.flush();
    });
  });

  it('should not require credentials with Kerberos when agent is logged on another phone', angular.mock.inject(function($rootScope, $compile, $httpBackend, $q) {
    var scope = $rootScope.$new();
    scope.hostAndPort = "xuc:9876";
    scope.useSso = true;
    scope.error = "LoggedInOnAnotherPhone";
    scope.onLogin = jasmine.createSpy("onLogin");
    XucLink.getStoredCredentials.and.returnValue(null);
    var credentials = {login: "jbond", token: "aaaa-bbbb-cccc-dddd-1234", phoneNumber: "1001"};
    XucLink.parseUrlParameters.and.returnValue({});
    XucLink.getSsoCredentials.and.returnValue($q.when(credentials));
    XucLink.checkAndloginWithCredentials.and.returnValue($q.when({}));
    
    var loginForm = $compile('<login-form on-login="onLogin()" host-and-port="hostAndPort" use-sso="useSso" require-phone-number="false"/>')(scope);
    $httpBackend.expectGET("assets/javascripts/xclogin/directives/loginForm.html").respond("<div>my template</div>");
    
    $rootScope.$digest();
    $httpBackend.flush();
    $rootScope.$digest();
    
    expect(loginForm).toBeDefined();
    expect(loginForm.controller).toBeDefined();
    var isolateScope = loginForm.isolateScope();
    expect(isolateScope.ctrl.requireCredentials).toEqual(false);
  }));

  it('should attempt autologin with Kerberos and fallback to default login', 
    angular.mock.inject(function($rootScope, $compile, $httpBackend, $q) {
      var scope = $rootScope.$new();
      scope.hostAndPort = "xuc:9876";
      scope.useSso = true;
      scope.onLogin = jasmine.createSpy("onLogin");
      XucLink.checkXucIsUp.and.returnValue($q.when());
      XucLink.getStoredCredentials.and.returnValue(null);
      XucLink.parseUrlParameters.and.returnValue({});
      var error = {"error":"SsoAuthenticationFailed","message":"SSO Authentication Failed"};
      XucLink.getSsoCredentials.and.callFake(function() { return $q.reject(error);});        
      var loginForm = $compile('<login-form on-login="onLogin()" host-and-port="hostAndPort" use-sso="useSso"/>')(scope);
      $httpBackend.expectGET("assets/javascripts/xclogin/directives/loginForm.html").respond("<div>my template</div>");
    
      $rootScope.$digest();
      $httpBackend.flush();
    
      expect(loginForm).toBeDefined();
      expect(loginForm.controller).toBeDefined();
      expect(XucLink.getSsoCredentials).toHaveBeenCalled();
      var isolateScope = loginForm.isolateScope();
      expect(isolateScope.autoLogin).toEqual(false);
      expect(isolateScope.ctrl.requireCredentials).toEqual(true);
      expect(isolateScope.ctrl.lastError.error).toEqual(error.error);
    }));

  it('should retry autologin with Kerberos when server did not answer', function(done) {
    angular.mock.inject(function($rootScope, $compile, $httpBackend, $q, $timeout) {
      XucLink.getSsoCredentials.calls.reset();
      XucLink.checkAndloginWithCredentials.calls.reset();

      const expectAutologinWithKerberos = () => {
        expect(XucLink.getSsoCredentials).toHaveBeenCalledTimes(2);
        expect(XucLink.checkAndloginWithCredentials).toHaveBeenCalled();
        expect(loginForm.isolateScope().ctrl.requireCredentials).toEqual(false);
        done();
      };

      XucLink.checkAndloginWithCredentials.and.callFake(expectAutologinWithKerberos);
      const scope = $rootScope.$new();
      const credentials = {login: "jbond", token: "aaaa-bbbb-cccc-dddd-1234", phoneNumber: "1001"};
      scope.hostAndPort = "xuc:9876";
      scope.useSso = true;
      scope.onLogin = jasmine.createSpy("onLogin");

      XucLink.getStoredCredentials.and.returnValue(null);
      XucLink.parseUrlParameters.and.returnValue({});
      XucLink.getSsoCredentials.and.returnValue($q.reject({error: 'NoResponse', message: 'No response from server'}));
      
      const loginForm = $compile('<login-form on-login="onLogin()" host-and-port="hostAndPort" use-sso="useSso"/>')(scope);
      $httpBackend.expectGET("assets/javascripts/xclogin/directives/loginForm.html").respond("<div>my template</div>");
      
      $rootScope.$digest();
      $httpBackend.flush();

      const controller = loginForm.isolateScope().ctrl;
      const setupAutoLoginTimeout = controller.retryAutoLoginLater.bind(controller);
      spyOn(controller, 'retryAutoLoginLater').and.callFake(() => {
        setupAutoLoginTimeout();
        XucLink.getSsoCredentials.and.returnValue($q.resolve(credentials));
        $timeout.flush(6000);
        $rootScope.$digest();
        $timeout.verifyNoPendingTasks();
      });
    });
  });

  it('should retry autologin with Kerberos when link closed', function(done) {
    angular.mock.inject(function($rootScope, $compile, $httpBackend, $q) {
      var scope = $rootScope.$new();
      scope.hostAndPort = "xuc:9876";
      scope.useSso = true;
      scope.onLogin = jasmine.createSpy("onLogin");
      scope.customError = "LinkClosed";
      XucLink.getStoredCredentials.and.returnValue(null);
      var credentials = {login: "jbond", token: "aaaa-bbbb-cccc-dddd-1234", phoneNumber: "1001"};
      XucLink.parseUrlParameters.and.returnValue({});
      XucLink.getSsoCredentials.and.returnValue($q.when(credentials));
      XucLink.checkAndloginWithCredentials.and.callFake(function() {
        expect(loginForm).toBeDefined();
        expect(loginForm.controller).toBeDefined();
        expect(XucLink.getSsoCredentials).toHaveBeenCalled();
        var isolateScope = loginForm.isolateScope();
        expect(isolateScope.ctrl.requireCredentials).toEqual(false);
        done();
        return $q.when({});
      });
      
      var loginForm = $compile('<login-form on-login="onLogin()" host-and-port="hostAndPort" use-sso="useSso" error-code="customError"/>')(scope);
      $httpBackend.expectGET("assets/javascripts/xclogin/directives/loginForm.html").respond("<div>my template</div>");
      
      $rootScope.$digest();
      $httpBackend.flush();
     
    });
  });

  it('should attempt autologin with CAS when configured', function(done) {
    angular.mock.inject(function($rootScope, $compile, $httpBackend, $q) {
      var scope = $rootScope.$new();
      scope.hostAndPort = "xuc:9876";
      scope.casServerUrl = "http://cas.org/cas";
      scope.onLogin = jasmine.createSpy("onLogin");
      XucLink.getStoredCredentials.and.returnValue(null);
      var credentials = {login: "jbond", token: "aaaa-bbbb-cccc-dddd-1234", phoneNumber: "1001"};
      XucLink.parseUrlParameters.and.returnValue({});
      XucLink.getCasToken.and.returnValue($q.when("dsfdscsdcdscdsc"));
      XucLink.tradeCasToken.and.returnValue($q.when(credentials));
      XucLink.checkAndloginWithCredentials.and.callFake(function() {
        expect(loginForm).toBeDefined();
        expect(loginForm.controller).toBeDefined();
        expect(XucLink.getCasToken).toHaveBeenCalled();
        expect(XucLink.tradeCasToken).toHaveBeenCalled();
        done();
        return $q.when({});
      });
      
      var loginForm = $compile('<login-form on-login="onLogin()" host-and-port="hostAndPort" cas-server-url="casServerUrl"/>')(scope);

      $httpBackend.expectGET("assets/javascripts/xclogin/directives/loginForm.html").respond("<div>my template</div>");
      
      $rootScope.$digest();
      $httpBackend.flush();
    });  
  });
  
  it('should logout from CAS server when configured to', angular.mock.inject(function($rootScope, $compile, $httpBackend) {
    var scope = $rootScope.$new();
    scope.hostAndPort = "xuc:9876";
    scope.casServerUrl = "http://cas.org/cas";
    scope.customError = 'Logout';
    scope.casLogoutEnable = "true";
    scope.onLogin = jasmine.createSpy("onLogin");
    XucLink.getStoredCredentials.and.returnValue(null);
    
    XucLink.parseUrlParameters.and.returnValue({lastError: 'Logout'});

    var loginForm = $compile('<login-form on-login="onLogin()" host-and-port="hostAndPort" error-code="customError" cas-server-url="casServerUrl" cas-logout-enable="casLogoutEnable"/>')(scope);

    $httpBackend.expectGET("assets/javascripts/xclogin/directives/loginForm.html").respond("<div>my template</div>");

    $rootScope.$digest();
    $httpBackend.flush();

    expect(loginForm).toBeDefined();
    expect(loginForm.controller).toBeDefined();
    expect(XucLink.logoutFromCas).toHaveBeenCalledWith('http://cas.org/cas');

  }));

  it('should logout from CAS server even after a previous lastError', angular.mock.inject(function($rootScope, $compile, $httpBackend) {
    var scope = $rootScope.$new();
    scope.hostAndPort = "xuc:9876";
    scope.casServerUrl = "http://cas.org/cas";
    scope.customError = 'Logout';
    scope.casLogoutEnable = "true";
    scope.onLogin = jasmine.createSpy("onLogin");
    XucLink.getStoredCredentials.and.returnValue(null);
    XucLink.parseUrlParameters.and.returnValue({lastError: 'LoggedInOnAnotherPhone'});

    var loginForm = $compile('<login-form on-login="onLogin()" host-and-port="hostAndPort" error-code="customError" cas-server-url="casServerUrl" cas-logout-enable="casLogoutEnable"/>')(scope);

    $httpBackend.expectGET("assets/javascripts/xclogin/directives/loginForm.html").respond("<div>my template</div>");

    $rootScope.$digest();
    $httpBackend.flush();

    expect(loginForm).toBeDefined();
    expect(loginForm.controller).toBeDefined();
    expect(XucLink.logoutFromCas).toHaveBeenCalledWith('http://cas.org/cas');

  }));

  it('should fallback to manual logging if CAS failed', angular.mock.inject(function($rootScope, $compile, $httpBackend, $q) {
    var scope = $rootScope.$new();
    scope.hostAndPort = "xuc:9876";
    scope.casServerUrl = "http://cas.org/cas";
    scope.onLogin = jasmine.createSpy("onLogin");
    XucLink.getStoredCredentials.and.returnValue(null);
    var error = {"error":"UserNotFound","message":"User not found"};
    XucLink.checkXucIsUp.and.returnValue($q.when());
    XucLink.parseUrlParameters.and.returnValue({});
    XucLink.getCasToken.and.callFake(function() { return $q.reject(error);});
    
    var loginForm = $compile('<login-form on-login="onLogin()" host-and-port="hostAndPort" cas-server-url="casServerUrl"/>')(scope);

    $httpBackend.expectGET("assets/javascripts/xclogin/directives/loginForm.html").respond("<div>my template</div>");
    
    $rootScope.$digest();
    $httpBackend.flush();
    
    expect(loginForm).toBeDefined();
    expect(loginForm.controller).toBeDefined();
    expect(XucLink.getCasToken).toHaveBeenCalledWith('http://cas.org/cas', undefined);
    $rootScope.$digest();
    var isolateScope = loginForm.isolateScope();
    expect(isolateScope.ctrl.lastError).toEqual(error);
    expect(isolateScope.ctrl.requireCredentials).toEqual(true);
  }));
  
  it('should callback onLogin when logging in', function(done) {
    angular.mock.inject(function($rootScope, $compile, $httpBackend, $q) {
      var scope = $rootScope.$new();
      scope.hostAndPort = "xuc:9876";
      scope.useSso = false;
      scope.onLogin = jasmine.createSpy("onLogin");
      
      var credentials = {login: "jbond", token: "aaaa-bbbb-cccc-dddd-1234", phoneNumber: "1001"};
      XucLink.parseUrlParameters.and.returnValue({});
      XucLink.getStoredCredentials.and.returnValue(credentials);
      XucLink.checkAndloginWithCredentials.and.callFake(function() {
        done();
        return $q.when({});
      });

      $compile('<login-form on-login="onLogin()" host-and-port="hostAndPort"/>')(scope);
      $httpBackend.expectGET("assets/javascripts/xclogin/directives/loginForm.html").respond("<div>my template</div>");    

      $rootScope.$digest();
      $httpBackend.flush();

      $rootScope.$broadcast("ctiLoggedOn");
      
      $rootScope.$digest();      
      expect(scope.onLogin).toHaveBeenCalled();
    });
  });

  it('should attempt to log again when no response from server V2', function(done) {
    angular.mock.inject(function($rootScope, $compile, $httpBackend, $q, $timeout) {
      XucLink.checkAndloginWithCredentials.calls.reset();
      XucLink.getStoredCredentials.calls.reset();

      const expectAutologin = () => {
        expect(XucLink.checkAndloginWithCredentials).toHaveBeenCalled();
        expect(XucLink.getStoredCredentials).toHaveBeenCalledTimes(4);
        done();
      };

      XucLink.checkAndloginWithCredentials.and.callFake(expectAutologin);
      const scope = $rootScope.$new();
      const credentials = {login: "jbond", token: "aaaa-bbbb-cccc-dddd-1234", phoneNumber: "1001"};
      scope.hostAndPort = "xuc:9876";
      scope.useSso = false;
      scope.onLogin = jasmine.createSpy("onLogin");

      XucLink.parseUrlParameters.and.returnValue({});
      XucLink.getStoredCredentials.and.returnValue($q.reject({error: 'NoResponse', message: 'No response from server'}));
      
      const loginForm = $compile('<login-form on-login="onLogin()" host-and-port="hostAndPort"/>')(scope);
      $httpBackend.expectGET("assets/javascripts/xclogin/directives/loginForm.html").respond("<div>my template</div>");
      
      $rootScope.$digest();
      $httpBackend.flush();

      const controller = loginForm.isolateScope().ctrl;
      const setupAutoLoginTimeout = controller.retryAutoLoginLater.bind(controller);
      spyOn(controller, 'retryAutoLoginLater').and.callFake(() => {
        setupAutoLoginTimeout();
        XucLink.getStoredCredentials.and.returnValue($q.resolve(credentials));
        $timeout.flush(6000);
        $rootScope.$digest();
        $timeout.verifyNoPendingTasks();
      });
    });
  });

  it('should attempt to log again when no response from server and when using cas V2', function(done) {
    angular.mock.inject(function($rootScope, $compile, $httpBackend, $q, $timeout) {
      XucLink.checkAndloginWithCredentials.calls.reset();
      XucLink.getCasToken.calls.reset();
      XucLink.tradeCasToken.calls.reset();

      const expectAutologin = () => {
        expect(XucLink.checkAndloginWithCredentials).toHaveBeenCalledTimes(1);
        expect(XucLink.tradeCasToken).toHaveBeenCalledTimes(1);
        expect(XucLink.getCasToken).toHaveBeenCalledTimes(2);
        done();
      };

      XucLink.checkAndloginWithCredentials.and.callFake(expectAutologin);
      const scope = $rootScope.$new();
      const credentials = {login: "jbond", token: "aaaa-bbbb-cccc-dddd-1234", phoneNumber: "1001"};
      scope.hostAndPort = "xuc:9876";
      scope.casServerUrl = "http://cas.org/cas";
      scope.onLogin = jasmine.createSpy("onLogin");

      XucLink.parseUrlParameters.and.returnValue({});
      XucLink.getCasToken.and.returnValue($q.reject({error: 'NoResponse', message: 'No response from server'}));
      
      const loginForm = $compile('<login-form on-login="onLogin()" host-and-port="hostAndPort" cas-server-url="casServerUrl"/>')(scope);
      $httpBackend.expectGET("assets/javascripts/xclogin/directives/loginForm.html").respond("<div>my template</div>");
      
      $rootScope.$digest();
      $httpBackend.flush();

      const controller = loginForm.isolateScope().ctrl;
      const setupAutoLoginTimeout = controller.retryAutoLoginLater.bind(controller);
      spyOn(controller, 'retryAutoLoginLater').and.callFake(() => {
        setupAutoLoginTimeout();
        XucLink.getCasToken.and.returnValue(Promise.resolve(credentials));
        XucLink.tradeCasToken.and.returnValue(Promise.resolve(credentials));
        $timeout.flush(6000);
        $rootScope.$digest();
        $timeout.verifyNoPendingTasks();
      });
    });
  });

  it('should NOT attempt to log again when server answer with invalid session V2', function(done) {
    angular.mock.inject(function($rootScope, $compile, $httpBackend, $q, $timeout) {
      const scope = $rootScope.$new();
      const credentials = {login: "jbond", token: "aaaa-bbbb-cccc-dddd-1234", phoneNumber: "1001"};
      scope.hostAndPort = "xuc:9876";
      scope.onLogin = jasmine.createSpy("onLogin");

      XucLink.parseUrlParameters.and.returnValue({});
      XucLink.getStoredCredentials.and.returnValue(credentials);
      XucLink.checkAndloginWithCredentials.and.returnValue($q.reject({error: 'InvalidSession'}));
      
      const loginForm = $compile('<login-form on-login="onLogin()" host-and-port="hostAndPort" />')(scope);
      $httpBackend.expectGET("assets/javascripts/xclogin/directives/loginForm.html").respond("<div>my template</div>");
      
      $rootScope.$digest();
      $httpBackend.flush();

      const controller = loginForm.isolateScope().ctrl;
      spyOn(controller, 'retryAutoLoginLater');
      $rootScope.$digest();
      $timeout.flush();
      $timeout.verifyNoPendingTasks();
      expect(controller.requireCredentials).toEqual(true);
      expect(controller.retryAutoLoginLater).not.toHaveBeenCalled();
      done();
    });
  });

  it('should fallback to manual logging if openid failed', angular.mock.inject(function($rootScope, $compile, $httpBackend, $q) {
    var scope = $rootScope.$new();
    scope.hostAndPort = "xuc:9876";
    scope.onLogin = jasmine.createSpy("onLogin");
    scope.someopenidurl = 'someopenidurl';
    scope.someopenidclientid = 'xuc-client-id';
    XucLink.getStoredCredentials.and.returnValue(null);
    var error = {"error":"UserNotFound","message":"User not found"};
    XucLink.parseUrlParameters.and.returnValue({access_token: 'mytoken'});
    XucLink.checkXucIsUp.and.returnValue($q.when());
    XucLink.getOpenidToken.and.callFake(function() { return $q.reject(error);});
    
    var loginForm = $compile('<login-form on-login="onLogin()" host-and-port="hostAndPort" openid-server-url="someopenidurl" openid-client-id="someopenidclientid"/>')(scope);

    $httpBackend.expectGET("assets/javascripts/xclogin/directives/loginForm.html").respond("<div>my template</div>");

    $rootScope.$digest();
    $httpBackend.flush();
    
    expect(loginForm).toBeDefined();
    expect(loginForm.controller).toBeDefined();
    expect(XucLink.getOpenidToken).toHaveBeenCalledWith(scope.someopenidurl, scope.someopenidclientid, undefined);
    $rootScope.$digest();
    var isolateScope = loginForm.isolateScope();
    expect(isolateScope.ctrl.lastError).toEqual(error);
    expect(isolateScope.ctrl.requireCredentials).toEqual(true);
  }));

  it('should log in xuc if openid token is retrieved', function(done) {
    angular.mock.inject(function($rootScope, $compile, $httpBackend) {
      var scope = $rootScope.$new();
      scope.hostAndPort = "xuc:9876";
      scope.onLogin = jasmine.createSpy("onLogin");
      scope.someopenidurl = 'someopenidurl';
      scope.someopenidclientid = 'xuc-client-id';
      XucLink.getStoredCredentials.and.returnValue(null);
      XucLink.parseUrlParameters.and.returnValue({access_token: 'mytoken'});
      XucLink.getOpenidToken.and.callFake(function() { return Promise.resolve('mytoken');});
      XucLink.tradeOpenidToken.and.callFake(function() { return Promise.resolve('myxuctoken');});
      XucLink.checkAndloginWithCredentials.and.callFake((c) => {
        expect(loginForm).toBeDefined();
        expect(loginForm.controller).toBeDefined();
        expect(XucLink.getOpenidToken).toHaveBeenCalled();
        expect(XucLink.tradeOpenidToken).toHaveBeenCalledWith('mytoken');

        expect(c).toBe('myxuctoken');
        done();
      });
      
      var loginForm = $compile('<login-form on-login="onLogin()" host-and-port="hostAndPort" openid-server-url="someopenidurl" openid-client-id="someopenidclientid"/>')(scope);

      $httpBackend.expectGET("assets/javascripts/xclogin/directives/loginForm.html").respond("<div>my template</div>");
      
      $rootScope.$digest();
      $httpBackend.flush();
      
    });
  });

});
