describe('FatalError controller', function() {
  var scope;
  var ctrl;
  var modalInstance;

  beforeEach(angular.mock.module('html-templates'));
  beforeEach(angular.mock.module('karma-backend'));
  beforeEach(angular.mock.module('xcCti'));
  beforeEach(angular.mock.module('xcHelper'));
  beforeEach(angular.mock.module('xcLogin'));

  beforeEach(function() {
    jasmine.addMatchers({
      toEqualData : customMatchers.toEqualData
    });
  });

  beforeEach(angular.mock.inject(function($rootScope, $controller) {
    scope = $rootScope.$new();
    modalInstance = {result: { then: jasmine.createSpy('modalInstance.result.then') }};

    ctrl = $controller('FatalErrorController', {
      '$scope': scope,
      'errorMessage': 'testErrorMessage',
      'showSwitchModal': false,
      '$uibModalInstance': modalInstance
    });
  }));

  it('can instantiate controller', function(){
    expect(ctrl).not.toBeUndefined();
  });

});
