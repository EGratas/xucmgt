describe('ccm ccchangeoptioncontroller', function() {
  var $rootScope;
  var $scope;
  var $q;
  var ctrl;
  var preferences;
  var localStorageService;
  var XucQueueRecording;
  var fakeStorage = {};

  beforeEach(angular.mock.module('ccManager'));

  beforeEach(function() {
    jasmine.addMatchers({
      toEqualData : customMatchers.toEqualData
    });
  });

  beforeEach(angular.mock.inject(function(_Preferences_, $controller, _$rootScope_, _localStorageService_, _XucQueueRecording_, _$q_) {
    $rootScope =_$rootScope_;
    $scope = $rootScope.$new();
    $q = _$q_;
    preferences = _Preferences_;
    localStorageService = _localStorageService_;
    XucQueueRecording = _XucQueueRecording_;

    spyOn(localStorageService, "get").and.callFake(function(key) {
      return fakeStorage[key];
    });
    spyOn(localStorageService, "set").and.callFake(function(key, value) {
      fakeStorage[key] = JSON.stringify(value);
    });

    ctrl = $controller('ccChangeOptionController', {
      '$scope' : $scope,
      'Preferences': preferences
    });
  }));

  afterEach(function(){
    localStorageService.clearAll();
    preferences.clearOptions();
    fakeStorage = {};
  });


  it('can instanciate controller', function() {
    expect(ctrl).not.toBeUndefined();
  });


  it('should define view option for display panel and be accessible in scope', function() {
    $scope.option = {
      opt1: preferences.addViewOption($scope,"VIEW_OPT_1", preferences.CheckboxPersistedTypeOption, false)
    };

    var expectedOptions =
      [{
        key: "VIEW_OPT_1",
        type: "checkboxPersisted",
        value: false,
        intermediate: false
      }];
    expect($scope.getViewOptions()).toEqualData(expectedOptions);
  });

  it('should count view option accessible for a scope', function() {
    $scope.option = {
      opt1: preferences.addViewOption($scope,"VIEW_OPT_1", preferences.CheckboxPersistedTypeOption, false),
      opt2: preferences.addViewOption($scope,"VIEW_OPT_2", preferences.CheckboxPersistedTypeOption, false)
    };
    expect($scope.getViewOptions().length).toBe(2);
  });

  it('should retrieve last value in localStorage when adding already defined option', function() {
    $scope.option = {
      opt1: preferences.addViewOption($scope,"VIEW_OPT_1", preferences.CheckboxPersistedTypeOption, false)
    };
    $scope.option.opt1.value = !$scope.option.opt1.value;
    $scope.onSwitchViewOption($scope.option.opt1);

    var expectedOptions =
      [{
        key: "VIEW_OPT_1",
        type: "checkboxPersisted",
        value: true,
        intermediate: false
      }];

    expect($scope.getViewOptions()).toEqualData(expectedOptions);
    expect(preferences.getBoolOption('VIEW_OPT_1')).toBe(true);
  });

  it('should define recording option for display panel and be accessible in scope', function() {
    $scope.option = {
      opt1: preferences.addRecordingOption($scope,"REC_OPT_1", preferences.CheckboxFreeTypeOption, XucQueueRecording.ALL_RECORDED),
      opt2: preferences.addRecordingOption($scope,"REC_OPT_2", preferences.CheckboxFreeTypeOption, XucQueueRecording.SOME_RECORDED),
      opt3: preferences.addRecordingOption($scope,"REC_OPT_3", preferences.CheckboxFreeTypeOption, XucQueueRecording.NONE_RECORDED)
    };

    var expectedOptions =
      [{
        key: "REC_OPT_1",
        type: "checkboxFree",
        value: true,
        intermediate: false
      },
      {
        key: "REC_OPT_2",
        type: "checkboxFree",
        value: false,
        intermediate: true
      },
      {
        key: "REC_OPT_3",
        type: "checkboxFree",
        value: false,
        intermediate: false
      }];
    expect($scope.getRecordingOptions()).toEqualData(expectedOptions);
  });

  it('should count recording option accessible for a scope', function() {
    $scope.option = {
      opt1: preferences.addRecordingOption($scope,"REC_OPT_1", preferences.CheckboxFreeTypeOption, false, false),
      opt2: preferences.addRecordingOption($scope,"REC_OPT_2", preferences.CheckboxFreeTypeOption, false, false)
    };
    expect($scope.getRecordingOptions().length).toBe(2);
  });

  it('should update recording option', function() {
    var result = $q.defer();
    result.resolve({});
    spyOn(XucQueueRecording, 'setState').and.callFake(() => {
      return result.promise;
    });

    $scope.onSwitchRecordingOption({value: false, intermediate: false});
    expect(XucQueueRecording.setState).toHaveBeenCalledWith("off");
    $scope.onSwitchRecordingOption({value: true, intermediate: false});
    expect(XucQueueRecording.setState).toHaveBeenCalledWith("on");
  });

  it('should reset options when switching scope', function() {
    $scope.option = {
      opt1: preferences.addViewOption($scope,"VIEW_OPT_1", preferences.CheckboxPersistedTypeOption, false),
      opt2: preferences.addRecordingOption($scope,"REC_OPT_1", preferences.CheckboxFreeTypeOption, false, false)
    };
    preferences.clearOptions();

    var expectedOptions = [];
    expect($scope.getViewOptions()).toEqualData(expectedOptions);
    expect($scope.getRecordingOptions()).toEqualData(expectedOptions);
  });

});
