describe('agents update modal controller', function() {
  var $scope;
  var $rootScope;
  var ctrl;
  var $uibModalInstance;
  var xucQueue;
  var xucAgent;
  var xucMembership;

  beforeEach(angular.mock.module('ccManager'));

  beforeEach(angular.mock.inject(function($controller, _$rootScope_, _$uibModal_, _XucQueue_, _AuQueueTable_, _XucAgent_, _XucMembership_) {
    $rootScope = _$rootScope_;
    $scope = $rootScope.$new();
    xucQueue = _XucQueue_;
    xucAgent = _XucAgent_;
    xucMembership = _XucMembership_;


    $uibModalInstance = _$uibModal_.open({
      templateUrl: 'agentsUpdateModalContent.html'
    });

    ctrl = $controller('agentsUpdateModalController', {
      '$scope' : $scope,
      '$uibModalInstance' : $uibModalInstance,
      'xucQueue': xucQueue,
      'auQueueTable': _AuQueueTable_,
      'xucAgent': xucAgent,
      'selectedAgents' : []
    });
  }));

  it('should instanciate controller', function() {

    expect(ctrl).not.toBeUndefined();

  });

  it('should init modal', function() {
    var testQueues = [{name:'q1', number:'1001'},{name:'q2', number: '1002'}];
    spyOn(xucQueue, 'getQueues').and.returnValue(testQueues);

    $scope.initModal();

    expect(xucQueue.getQueues).toHaveBeenCalled();
  });

  it('should dismiss modal on cancel', function() {
    $scope.queuesToAdd = [{queue: {name:'q1', number:'1001'}, penalty: 10}];
    $scope.queuesToRemove = [{name:'q2', number:'1002'}];
    spyOn($uibModalInstance, 'dismiss');

    $scope.cancel();

    expect($uibModalInstance.dismiss).toHaveBeenCalledWith('cancel');

    expect($scope.queuesToAdd).toEqual([]);
    expect($scope.queuesToRemove).toEqual([]);
  });

  it('should update queue to be added on add queue', function(){
    var q = QueueBuilder('sales','sales channel').build();

    $scope.newQueue = {queue: q, penalty: 10};

    $scope.addQueue();

    expect($scope.queuesToAdd).toEqual([{queue:q, penalty:10}]);

    expect($scope.newQueue.penalty).toBe(0);
    expect($scope.newQueue.queue).toBe(null);

  });
  it('should remove queue from the list of available queues when queue is added', function(){
    var q1 = QueueBuilder('sales','sales channel').build();
    var q2 = QueueBuilder('insurance','insurance channel').build();
    $scope.auQueues = [q1,q2];

    $scope.newQueue = {queue: q1, penalty: 10};

    $scope.addQueue();

    expect($scope.auQueues).toEqual([q2]);

  });

  it('should remove a queue from a list of queue to be added', function(){
    var q1 = QueueBuilder('sales','sales channel').build();
    var q2 = QueueBuilder('insurance','insurance channel').build();

    $scope.queuesToAdd = [{queue: q1, penalty: 10},{queue: q2, penalty: 5}];

    $scope.removeQueueToAdd({queue: q1, penalty: 10});

    expect($scope.queuesToAdd).toEqual([{queue: q2, penalty: 5}]);
  });

  it('should add the queue to the list of available queues when this queue is not added any more', function(){
    var q1 = QueueBuilder('sales','sales channel').build();
    var q2 = QueueBuilder('insurance','insurance channel').build();
    $scope.auQueues = [q2];

    $scope.queuesToAdd = [{queue: q1, penalty: 10}];

    $scope.removeQueueToAdd({queue: q1, penalty: 10});

    expect($scope.auQueues).toContain(q1);
  });

  it('should add agents to queue to be added on validation', function(){
    var ag1 = new MockAgentBuilder(25,'Franc','Barns');
    var ag2 = new MockAgentBuilder(30,'Allow','Emeline');
    var q1 = QueueBuilder('sales','sales channel').build();
    var q2 = QueueBuilder('insurance','insurance channel').build();

    $scope.selectedAgents = [ag1, ag2];
    $scope.queuesToAdd = [{queue: q1, penalty: 10},{queue: q2, penalty: 5}];


    spyOn($uibModalInstance, 'dismiss');
    spyOn(xucAgent, 'addAgentsToQueues');

    $scope.ok();

    expect(xucAgent.addAgentsToQueues).toHaveBeenCalledWith($scope.selectedAgents, [{queue: q1, penalty: 10},{queue: q2, penalty: 5}]);
    expect($uibModalInstance.dismiss).toHaveBeenCalledWith('ok');
    expect($scope.queuesToAdd).toEqual([]);
  });
  
  it("should apply default configuration for selected agents", function(){
    var ag1 = new MockAgentBuilder(25,'Franc','Barns');
    var ag2 = new MockAgentBuilder(30,'Allow','Emeline');
    $scope.selectedAgents = [ag1, ag2];

    spyOn(xucMembership, 'applyUsersDefaultMembership');
    spyOn($uibModalInstance, 'dismiss');
    
    $scope.applyUsersDefaultMembership();
    
    expect(xucMembership.applyUsersDefaultMembership).toHaveBeenCalledWith([ag1.userId, ag2.userId]);
    expect($uibModalInstance.dismiss).toHaveBeenCalledWith('ok');
  });

  describe('manage queue to be removed', function() {
    it('should add queue to remove', function(){
      var q = QueueBuilder('sales','sales channel').build();

      $scope.newQueue = {queue: q, penalty: 10};

      $scope.addQueueToRemove();

      expect($scope.queuesToRemove).toEqual([q]);

      expect($scope.newQueue.penalty).toBe(0);
      expect($scope.newQueue.queue).toBe(null);

    });
    it('should remove queue from the list of available queues when queue is selected to be removed', function(){
      var q1 = QueueBuilder('sales','sales channel').build();
      var q2 = QueueBuilder('insurance','insurance channel').build();
      $scope.auQueues = [q1,q2];

      $scope.newQueue = {queue: q1, penalty: 10};

      $scope.addQueueToRemove();

      expect($scope.auQueues).toEqual([q2]);

    });

    it('should remove a queue from a list of queue to be removed', function(){
      var q1 = QueueBuilder('sales','sales channel').build();
      var q2 = QueueBuilder('insurance','insurance channel').build();

      $scope.queuesToRemove = [q1,q2];

      $scope.removeQueueToRemove(q2);

      expect($scope.queuesToRemove).toEqual([q1]);
    });

    it('should add the queue to the list of available queues when this queue is not to be removed any more', function(){

      var q1 = QueueBuilder('sales','sales channel').build();
      var q2 = QueueBuilder('insurance','insurance channel').build();
      $scope.auQueues = [q2];

      $scope.queuesToRemove = [q1];

      $scope.removeQueueToRemove(q1);

      expect($scope.auQueues).toContain(q1);

    });

    it('should remove agents from queues on validation', function(){
      var ag1 = new MockAgentBuilder(25,'Franc','Barns');
      var ag2 = new MockAgentBuilder(30,'Allow','Emeline');
      var q1 = QueueBuilder('sales','sales channel').build();
      var q2 = QueueBuilder('insurance','insurance channel').build();

      $scope.selectedAgents = [ag1, ag2];
      $scope.queuesToRemove = [q1,q2];


      spyOn($uibModalInstance, 'dismiss');
      spyOn(xucAgent, 'removeAgentsFromQueues');

      $scope.ok();

      expect(xucAgent.removeAgentsFromQueues).toHaveBeenCalledWith($scope.selectedAgents, [q1,q2]);
      expect($uibModalInstance.dismiss).toHaveBeenCalledWith('ok');
      expect($scope.queuesToRemove).toEqual([]);
    });

  });

});
