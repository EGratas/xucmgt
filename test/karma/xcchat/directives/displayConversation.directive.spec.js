import { Conversation, Message } from "../../../../app/assets/javascripts/xccti/services/XucChat.service";

describe('display conversation directive', () => {

  var scope;
  var directiveScope;
  var XucChat;
  var $compile;
  var $rootScope;
  var element;
  var $q;
  var conversationPromise;

  beforeEach(angular.mock.module('html-templates'));
  beforeEach(angular.mock.module('karma-backend'));
  beforeEach(angular.mock.module('xcChat'));
  beforeEach(angular.mock.module('xcCti'));
  beforeEach(angular.mock.module('xcHelper'));
  beforeEach(angular.mock.module('dbaq.emoji'));

  beforeEach(function () {

    angular.mock.inject(function (_$compile_, _$rootScope_, _XucChat_, _$q_) {
      $rootScope = _$rootScope_;
      scope = $rootScope.$new();
      $compile = _$compile_;
      XucChat = _XucChat_;
      $q =_$q_;

      conversationPromise = $q.defer();
      spyOn(XucChat, "getRemotePartyConversation").and.returnValue(conversationPromise.promise);

      spyOn(XucChat, 'conversationDisplayed');

      var elementStr = angular.element('<display-conversation remote-party="jbond"></display-conversation>');
      element = $compile(elementStr)(scope);
      scope.$digest();
      directiveScope = element.children().scope();
      scope.$digest();
    });
  });

  it('generates html when instantiated', function () {
    expect(element.html()).toContain('div');
  });

  it('displays a conversation', function () {
    let fakeMessage = new Message(1, 'hello world', '2020-04-17T09:40:00.000Z', 'incoming');
    let fakeResponse = new Conversation('jbond', [fakeMessage]);
    conversationPromise.resolve(fakeResponse);
    $rootScope.$apply();
    expect(XucChat.getRemotePartyConversation).toHaveBeenCalledWith('jbond');
    expect(directiveScope.messages).toContain(fakeMessage);
  });

  it('Notify xucChat service when conversation is displayed', function () {
    expect(XucChat.conversationDisplayed).not.toHaveBeenCalled();

    let fakeMessage = new Message(1, 'hello world', '2020-04-17T09:40:00.000Z', 'incoming');
    let fakeResponse = new Conversation('jbond', [fakeMessage]);
    conversationPromise.resolve(fakeResponse);
    $rootScope.$apply();
    expect(XucChat.conversationDisplayed).toHaveBeenCalledWith('jbond');
  });


  it('add pending message', function () {
    var message = { id: 1, content: 'a message' };
    $rootScope.$broadcast(XucChat.CHAT_SEND_PENDING_MESSAGE, message, 'jbond');
    $rootScope.$digest();
    expect(directiveScope.pendingMessages.get(1)).toEqual(message);
  });

  it('not add pending message if unexpected remote party', function () {
    var message = { id: 1, content: 'a message' };
    $rootScope.$broadcast(XucChat.CHAT_SEND_PENDING_MESSAGE, message, 'jdoe');
    $rootScope.$digest();
    expect(directiveScope.pendingMessages.size).toEqual(0);
  });

  it('displays received message when conversation is already loaded', function () {
    let olderMessage = new Message(1, 'hello world', '2020-04-17T09:40:00.000Z', 'incoming');
    directiveScope.messages = [olderMessage];

    let notForYouMessage = new Message (2, 'wrong message', '2020-04-17T12:39:00.000Z', 'incoming');
    $rootScope.$broadcast(XucChat.CHAT_RECEIVED_MESSAGE, notForYouMessage, 'bwillis');
    $rootScope.$digest();

    let newMessage = new Message (2, 'another message', '2020-04-17T12:40:00.000Z', 'incoming');
    $rootScope.$broadcast(XucChat.CHAT_RECEIVED_MESSAGE, newMessage, 'jbond');
    $rootScope.$digest();

    expect(directiveScope.messages[0]).toBe(olderMessage);
    expect(directiveScope.messages[1]).toBe(newMessage);
    expect(directiveScope.messages.length).toBe(2);
  });

  it('displays received message when conversation is not loaded yet and not part of the list of messages', function () {
    let newMessage = new Message (2, 'newer message', '2020-04-17T12:50:00.000Z', 'incoming');
    $rootScope.$broadcast(XucChat.CHAT_RECEIVED_MESSAGE, newMessage, 'jbond');
    $rootScope.$digest();

    let olderMessage = new Message(1, 'older message', '2020-04-17T09:40:00.000Z', 'incoming');
    let olderResponse = new Conversation('jbond', [olderMessage]);
    conversationPromise.resolve(olderResponse);
    $rootScope.$apply();

    expect(directiveScope.messages[0]).toBe(olderMessage);
    expect(directiveScope.messages[1]).toBe(newMessage);
    expect(directiveScope.messages.length).toBe(2);
  });

  it('merges messages without duplicates', function () {
    let message1 = new Message(1, 'new message', '2020-04-17T09:50:00.000Z', 'incoming');
    $rootScope.$broadcast(XucChat.CHAT_RECEIVED_MESSAGE, message1, 'jbond');
    $rootScope.$digest();
    let message2 = new Message(1, 'new message', '2020-04-17T09:50:00.000Z', 'incoming');
    $rootScope.$broadcast(XucChat.CHAT_RECEIVED_MESSAGE, message2, 'jbond');
    $rootScope.$digest();
    expect(directiveScope.messages[0]).toBe(message1);
    expect(directiveScope.messages.length).toBe(1);
  });

  it('merges messages without duplicates if message is received before conversation', function () {
    let newMessage = new Message(1, 'new message', '2020-04-17T09:50:00.000Z', 'incoming');
    $rootScope.$broadcast(XucChat.CHAT_RECEIVED_MESSAGE, newMessage, 'jbond');
    $rootScope.$digest();
    let oldMessage = new Message(1, 'old message', '2020-04-17T09:40:00.000Z', 'incoming');
    let conversation = new Conversation('jbond', [newMessage, oldMessage]);
    conversationPromise.resolve(conversation);
    $rootScope.$apply();

    expect(directiveScope.messages[0]).toBe(oldMessage);
    expect(directiveScope.messages[1]).toBe(newMessage);
    expect(directiveScope.messages.length).toBe(2);
  });

  it('merges messages without duplicates if message is received after conversation', function () {
    let newMessage = new Message(1, 'new message', '2020-04-17T09:50:00.000Z', 'incoming');
    let oldMessage = new Message(1, 'old message', '2020-04-17T09:40:00.000Z', 'incoming');
    let conversation = new Conversation('jbond', [newMessage, oldMessage]);
    conversationPromise.resolve(conversation);
    $rootScope.$apply();
    $rootScope.$broadcast(XucChat.CHAT_RECEIVED_MESSAGE, newMessage, 'jbond');
    $rootScope.$digest();

    expect(directiveScope.messages[0]).toBe(oldMessage);
    expect(directiveScope.messages[1]).toBe(newMessage);
    expect(directiveScope.messages.length).toBe(2);
  });

  it('show error message if sending message fails with NACK', function () {
    spyOn(XucChat, 'triggerToast');

    let pendingMessage = new Message (1, 'a message', '', 'outgoing');
    $rootScope.$broadcast(XucChat.CHAT_SEND_PENDING_MESSAGE, pendingMessage, 'jbond');
    $rootScope.$digest();

    $rootScope.$broadcast(XucChat.CHAT_SEND_MESSAGE_NACK, {sequence: 1});
    $rootScope.$digest();
    expect(XucChat.triggerToast).toHaveBeenCalledWith('ChatError', 'alert-danger');
  });

  it('show warning message if sending message to offline user', function () {
    spyOn(XucChat, 'triggerToast');

    let pendingMessage = new Message (1, 'a message', '', 'outgoing');
    $rootScope.$broadcast(XucChat.CHAT_SEND_PENDING_MESSAGE, pendingMessage, 'jbond');
    $rootScope.$digest();

    $rootScope.$broadcast(XucChat.CHAT_SEND_MESSAGE_ACK_OFFLINE, 1);
    $rootScope.$digest();
    expect(XucChat.triggerToast).toHaveBeenCalledWith('ChatOffline', 'alert-warning');
  });

  it('change pending message to the message', function () {
    let pendingMessage = new Message(1, 'new message', '', 'outgoing');
    directiveScope.pendingMessages.set(pendingMessage.id, pendingMessage);

    let message = new Message(1, 'new message', '2020-04-17T09:50:00.000Z', 'outgoing');

    $rootScope.$broadcast(XucChat.CHAT_SEND_MESSAGE_ACK, message, 'jbond');
    $rootScope.$digest();

    expect(directiveScope.pendingMessages.size).toBe(0);
    expect(directiveScope.messages[0]).toBe(message);
    expect(directiveScope.messages.length).toBe(1);
  });
});
