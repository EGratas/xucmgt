describe('display send message directive', () => {

  var scope;
  var directiveScope;
  var XucChat;
  var $compile;
  var $rootScope;
  var element;


  beforeEach(angular.mock.module('html-templates'));
  beforeEach(angular.mock.module('karma-backend'));
  beforeEach(angular.mock.module('xcChat'));
  beforeEach(angular.mock.module('xcCti'));
  beforeEach(angular.mock.module('xcHelper'));

  beforeEach(function () {
    
    angular.mock.inject(function (_$compile_, _$rootScope_, _XucChat_) {
      $rootScope = _$rootScope_;
      scope = $rootScope.$new();
      $compile = _$compile_;
      XucChat = _XucChat_;

      var elementStr = angular.element('<send-message remote-party="jbond"></send-message>');
      element = $compile(elementStr)(scope);
      scope.$digest();
      directiveScope = element.children().scope();
      scope.$digest();
    });
  });

  it('generates html when instantiated', () => {
    expect(element.html()).toContain('div');
  });

  it('forward send message values to service when called', () => {
    spyOn(XucChat, 'sendMessage');
    directiveScope.remoteParty = 'jbond';
    directiveScope.areaMessage = 'hello world';

    directiveScope.sendMessage();
    expect(XucChat.sendMessage).toHaveBeenCalledWith('jbond', 'hello world');
  });
});