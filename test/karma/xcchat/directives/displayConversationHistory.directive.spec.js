import { MessageHistory } from "../../../../app/assets/javascripts/xccti/services/XucChat.service";

describe('display conversation history directive', () => {

  var scope;
  var compiledElem;
  var XucChat;
  var $compile;
  var $rootScope;

  beforeEach(angular.mock.module('html-templates'));
  beforeEach(angular.mock.module('karma-backend'));
  beforeEach(angular.mock.module('xcChat'));
  beforeEach(angular.mock.module('xcCti'));
  beforeEach(angular.mock.module('xcHelper'));
  beforeEach(angular.mock.module('dbaq.emoji'));

  beforeEach(function () {

    angular.mock.inject(function (_$compile_, _$rootScope_, _XucChat_) {
      $rootScope = _$rootScope_;
      scope = $rootScope.$new();
      $compile = _$compile_;
      XucChat = _XucChat_;
    });
  });

  var getCompiledElement = function () {
    var element = angular.element('<display-conversation-history></display-conversation-history>');
    compiledElem = $compile(element)(scope);
    scope.$digest();
  };

  it('generates html when instantiated', function () {
    getCompiledElement();
    expect(compiledElem.html()).toContain('div');
  });

  it('displays a conversation history', function () {
    let fakeConversationHistory = new Map();
    fakeConversationHistory.set('jbond', new MessageHistory('my message from jbond', '2021-01-14T11:00:00.000Z', 0));
    spyOn(XucChat, "getConversationHistory").and.returnValue(fakeConversationHistory);
    getCompiledElement();
    expect(XucChat.getConversationHistory).toHaveBeenCalled();
  });
});
