describe('new message badge directive', () => {
  
  var scope;
  var $compile;
  var $rootScope;
  var element;
  
  beforeEach(angular.mock.module('html-templates'));
  beforeEach(angular.mock.module('karma-backend'));
  beforeEach(angular.mock.module('xcChat'));
  beforeEach(angular.mock.module('xcCti'));
  beforeEach(angular.mock.module('xcHelper'));
  beforeEach(angular.mock.module('dbaq.emoji'));
  
  beforeEach(function () {
    
    angular.mock.inject(function (_$compile_, _$rootScope_) {
      $rootScope = _$rootScope_;
      scope = $rootScope.$new();
      $compile = _$compile_;
      var elementStr = angular.element('<new-message-badge primary-color="white"></new-message-badge>');
      element = $compile(elementStr)(scope);
      scope.$digest();
      scope.$digest();
    });
  });


  it('generates html when instantiated', function () {
    expect(element.html()).toContain('<span ng-show="hasMessage" class="badge-message ng-hide"><i class="fas fa-envelope fa-envelope-offset"></i></span>');
  });
});
