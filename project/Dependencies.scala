import play.sbt.PlayImport.{ehcache, guice}
import sbt._

object Version {
  val htmlcompressor = "1.5.2"
  val scalatestplay = "5.1.0"
  val playws = "2.1.3"
  val playwsjson = "2.8.7"
  val mockito = "1.10.19"
  val scalatestmock = "3.2.10.0"
}


object Library {
  val htmlcompressor  = "com.googlecode.htmlcompressor"  % "htmlcompressor"          % Version.htmlcompressor
  val playws          = "com.typesafe.play"             %% "play-ahc-ws-standalone"  % Version.playws
  val playwsjson      = "com.typesafe.play"             %% "play-ws-standalone-json" % Version.playws
  val scalatestplay   = "org.scalatestplus.play"        %% "scalatestplus-play"      % Version.scalatestplay
  val scalatestmock   = "org.scalatestplus"             %% "mockito-3-4"             % Version.scalatestmock
  val mockito         = "org.mockito"                    % "mockito-all"             % Version.mockito
}

object Dependencies {

  import Library._

  val scalaVersion = "2.13.12"

  val resolutionRepos = Seq(
    "Local Maven Repository" at "file:///" + Path.userHome.absolutePath + "/.m2/repository",
    "Typesafe repository" at "https://repo.typesafe.com/typesafe/releases/"
  )

  val runDep: Seq[ModuleID] = run(
    htmlcompressor,
    playws,
    playwsjson,
    guice,
    ehcache
  )

  val testDep: Seq[ModuleID] = test(
    scalatestplay,
    mockito,
    scalatestmock
  )

  def run       (deps: ModuleID*): Seq[ModuleID] = deps
  def test      (deps: ModuleID*): Seq[ModuleID] = deps map (_ % "test")

}
