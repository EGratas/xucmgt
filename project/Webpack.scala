import play.sbt.PlayRunHook
import sbt.File

import scala.sys.process.Process

object Webpack {
  def apply(base: File): PlayRunHook = {
    object WebpackHook extends PlayRunHook {
      var process: Option[Process] = None

      override def afterStarted(): Unit = {
        process = Option(
           (Process("npm ci", base) ### Process("npm run build-n-watch", base)).run()
        )
      }

      override def afterStopped(): Unit = {
        process.foreach(_.destroy())
        process = None
      }
    }

    WebpackHook
  }
}
