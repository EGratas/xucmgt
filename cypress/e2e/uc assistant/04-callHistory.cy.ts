
import {Common} from './common';

let common = new Common();


describe('Call History Test', () => {
  it('should login', () => {
    common.initialSate();
  });

  it('outgoing call should be listed in call history', () => {
    common.call(102);
    cy.get('.xivo-icone-menu-historique').click();
    cy.get('div.history-container:nth-child(1) .history-detail:nth-child(1) .history-phone span').should('have.attr', 'title', 'user 1');
    cy.get('div.history-container:nth-child(1) .history-detail:nth-child(1) .history-chevron i').click();
    cy.get('div.history-container:nth-child(1) [ng-repeat="call in callList"]').then(($el) => { 
      const itemCount = Cypress.$($el).length != 0 ? Cypress.$($el).length : 0;
      common.call(105);
      cy.get('div.history-container:nth-child(1) .user-status:first-child .avatar-status').should('have.class', 'user-status4');
      cy.get('div.history-container:nth-child(1) .history-detail:first-child').find('img').should('have.attr', 'src', '/assets/images/ucassistant/call_statut_outgoing.svg');
      cy.get('div.history-container:nth-child(1) .history-detail:nth-child(1) .history-chevron i').click();
      expect(cy.get('div.history-container:nth-child(1)').find('[ng-repeat="call in callList"]').should('have.length', `${itemCount + 1}`))
    });
  });

  it('should log out', () => {
    common.logOut();
  });

  it('incoming call should be listed in call history', () => {
    cy.get('.badge').should('contain', 2);
    cy.get('.xivo-icone-menu-historique').click();
    cy.get('div.history-container:nth-child(1) .history-detail:nth-child(1) .history-phone span').should('have.attr', 'title', 'user 1');
    cy.get('div.history-container:nth-child(1) .history-detail:nth-child(1) .history-chevron i').click();
    cy.get('div.history-container:nth-child(1) [ng-repeat="call in callList"]').then(($el) => { 
      const itemCount = Cypress.$($el).length != 0 ? Cypress.$($el).length : 0;
      cy.get('div.history-container:nth-child(1) .history-detail:nth-child(1) .history-chevron i').click();
      cy.get('div.history-container:nth-child(1) .user-status:first-child .avatar-status').should('have.class', 'user-status4');
      cy.get('div.history-container:nth-child(1) .history-detail:first-child').find('img').should('have.attr', 'src', '/assets/images/ucassistant/call_statut_missed.svg');
      cy.get('div.history-container:nth-child(1) .history-detail:nth-child(1) .history-chevron i').click();
      expect(cy.get('div.history-container:nth-child(1)').find('[ng-repeat="call in callList"]').should('have.length', `${itemCount}`))
    });
  });

})