import { Common } from './common';

let common = new Common();

describe('Contact interactions Test', () => {
    beforeEach(() => {
        common.initialSate();
    })
    it('Should search for a user!', () => {
        common.searchContact('user');
        cy.get('.user-line').should('have.length', 4);
        cy.get('.contact .name').first().should('contain.text', 'user 1');
        cy.get('.contact .name').last().should('contain.text', 'user 4');
    })

    it('Should Toggle the contact Detail', () => {
        common.searchContact('user');
        cy.get('.user-line').should('exist')
        cy.get('.user-line').first().click()
        cy.get('.user-line').first().get('.user-detail').should('exist')
        cy.get('.user-line').first().click()
        cy.get('.user-line').first().get('.user-detail').should('not.exist')

    })

    it('Should fill clip board!', () => {
        common.searchContact('user');
        cy.get('.user-line').contains('user 1').click().get(`.d-flex.phone-number`).click()

        cy.get('.toast-container .animate-repeat').should('exist')

        cy.window().then((win) => {
            win.navigator.clipboard.readText().then((text) => {
                expect(text).to.eq('4032');
            });
        });

    })

//     it('should redirect to chat page', () => {
//         let helloWorld = 'hello world!'
//         common.searchContact('user');
//         setTimeout(() => {
//         }, 4000);
//         cy.get('.user-line').last().find('.comments.button').last().should('exist')
//         cy.get('.comments.button').last().should('exist')
//         cy.get('.user-line').last().find('.comments.button').last().click()
//         //cy.get('.contact .name').last().should('contain.text', 'user 4');
//
//         cy.url().should('include', '/ucassistant/conversation');
//         cy.get('.messages-header').should('contain.text', 'user 4')
//
//         cy.get('.chat-text-send').should('not.be.visible')
//         cy.get('.chat-text').type(helloWorld).should('have.value', helloWorld);
//
//         cy.get('.party-message.me').then($elemtents => {
//             let messegeNumber = 0;
//             if ($elemtents) {
//                 messegeNumber = $elemtents.length
//                 cy.wait(500)
//                 cy.get('.chat-text-send').should('be.visible')
//                 cy.get('.chat-text-send').click()
//                 cy.get('.party-message.me').should('have.length', 1 + messegeNumber)
//             } else {
//                 cy.get('.chat-text-send').should('be.visible')
//                 cy.get('.chat-text-send').click()
//                 cy.get('.party-message.me').should('have.length', 1)
//             }
//             messegeNumber++;
//             cy.get('.xivo-menu').click()
//             cy.get('.centered-logout').click()
//             cy.wait(1000);
//             common.login('user4', 'TEST', '#input_3', '#input_4');
//             cy.get('.badge-message').should('be.visible')
//             cy.get('.navigation > li').last().click()
//             cy.url().should('include', '/ucassistant/conversation');
//             cy.wait(500)
//             cy.get('.chat-party').should('exist')
//             cy.get('.chat-party').contains('user1').click()
//             cy.get('.party-message.you').should('have.length', messegeNumber);
//         })
//     })

    it('should create and delete a personal contact', () => {

        cy.get('#menu').click()
        cy.get('.menu-item').should('exist')
        cy.get('[ui-sref="interface.personalContact"]').first().click()
        cy.get('.btn.btn-primary.pull-right.ng-binding').should('exist').should('be.disabled')
        cy.get('#input_3').should('exist')
        cy.get('#input_3').type('bilbo').should('have.value', 'bilbo')
        cy.get('.btn.btn-primary.pull-right.ng-binding').should('exist').should('be.disabled')
        cy.get('#input_4').should('exist')
        cy.get('#input_4').type('4242').should('have.value', '4242')
        cy.get('.btn.btn-primary.pull-right.ng-binding').should('exist').should('not.be.disabled').click()

        common.searchContact('bilbo')
        cy.get('.user-line').should('have.length', 1)
        cy.get('.xivo-modifier').click()
        cy.get('.favorites').click()
        cy.get('.btn.btn-primary.pull-right.ng-binding').should('exist').should('not.be.disabled').click()

        cy.contains('bilbo').should('exist');
        common.searchContact('bilbo')
        cy.get('.user-line').should('have.length', 1)
        cy.get('.xivo-modifier').click()
        cy.get('.delete').click()
        cy.get('[ng-click="ctrl.remove()"]').click()
    })

})
