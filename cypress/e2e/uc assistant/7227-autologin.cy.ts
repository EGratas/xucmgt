import {Common} from './common';

let common = new Common();

describe('Auto Login process Test', () => {

    it('Should Login in UCAssistant', () => {
        cy.visit('http://edge-vdu.test.avencall.com/');
        common.login('jbond', '1234');
        cy.get('#loginError').should('exist');
        cy.url().should('include', '/ucassistant/favorites');
        cy.reload()
        cy.url().should('include', '/ucassistant/favorites');
    });

    it('Should Login in CCAgent', () => {
        cy.visit('http://edge-vdu.test.avencall.com/ccagent');
        cy.get("input[name='phoneNumber']").type('1000')
        common.login('jbond', '1234');
        cy.get('#loginError').should('exist');
        cy.url().should('include', '/main');
        cy.reload()
        cy.url().should('include', '/main');
    });

});