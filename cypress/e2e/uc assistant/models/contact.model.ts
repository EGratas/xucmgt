export interface Contact {
    contact_id: string;
    entry: Array<string>;
    favorite: boolean;
    personal: boolean;
    source: string;
    status: number;
    username: string;
    videoStatus: string;
}

export interface SearchContactResult {
 headers: Array<string>;
 entries: Array<Contact>;
}