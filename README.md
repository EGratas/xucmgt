# xucmgt

This package provides web interfaces to XiVOcc system

* CC manager
* CC Agent
* UC Assistant

Electron wrapper for these web apps is located in `electron` folder.

# Project structure :

## Front oriented files (css, js, npm packages...)
app                      → Application sources
 └ assets                → Compiled asset sources
    └ stylesheets        → LESS CSS sources files
    └ javascripts        → Our applications
       └ ccagent         → Agent web app for call centers
       └ ccmanager       → Manager web app to manage agents
       └ ucassistant     → Assistant web app for companies telephony
       └ xcchat          → Web chat for CCAgent and UCAssistant apps
       └ xccti           → Telephony-related stuff and APIs mostly linked to Xuc
       └ xchelper        → Stuff that can be re-used in CCAgent, UCAssistant, or other projects
       └ xclogin         → Login page for our applications
public                   → Public assets
 └ stylesheets           → CSS files
 └ javascripts           → Big raw libraries (lodash, sipml, jquery...)
 └ images                → Image files
 └ audio                 → Ringtones...
 └ fonts                 → Project fonts files
 └ i18n                  → Translation files used for the applications
test                     → Source folder for javascript tests
package.json             → Contains tracks of installed npm packages, custom cli build commands...

Note: all of the above projects are using AngularJS framework, and thus following officials AngularJS architecture :

    webapp                      → Our web applications
    └ controllers               → Organize and display all the pieces together
    └ directives                → Reusable pieces with DOM templates to display our services data 
    └ services                  → Pieces to do heavy logic business, data processing or client-to-server links 
    └ filters                   → ? TODO (not sure)
    └ app.config.js             → Config files for translations, routing...
    └ app.module.js             → Import and inject all of our pieces in the applications

For details, see https://docs.angularjs.org/guide/concepts

## Back or build oriented files (scala, sbt config...)

app                      → Application sources
 └ configuration         → Mostly environment variables retrieved from the server
 └ controllers           → Play files to serve html indexes and do the login part
 └ models                → ? TODO
 └ system                → ? TODO
 └ views                 → HTML views served by Play 
build.sbt                → Application build script
conf                     → Configurations files and other non-compiled resources (on classpath)
 └ application.conf      → Main configuration file
 └ routes                → Routes definition
dist                     → ? TODO
project                  → Sbt configuration files
target                   → ? TODO

# Development

sbt & npm are required to run the development version and the test suite.

To install the project dependencies

* `npm ci`

## How to launch app in dev

Create a conf file named application-*.conf like `application-dev.conf` and fill the variable : 

```
include "application.conf"

XUC_HOST = # 
XUC_PORT = # 
```

You can also directly override a configuration key if no env variable is defined.

Starting the application in play framework :

```sh
sbt run -Dhttp.port=8070 -Dconfig.file=conf/application-dev.conf
```

## To run and watch the project locally without Edge :
- XUC_HOST_IP -> The XiVOCC IP

*  `sbt run -Dhttp.port=8070 -Dxuc.host=XUC_HOST_IP -Dxuc.port=8090`

## To run and watch the project locally with Edge :
- XUC_HOST_FQDN -> The XiVOCC FQDN available in the XiVOCC custom env
- OWN_MACHINE_IP -> The IP of your machine

Note for OWN_MACHINE_IP :
- If your Edge is on Fullsave you will need to specifically use the Fullsave VPN interface IP of your host

On your edge, run the following commands :

* `docker exec -it edge_nginx_1 bash`
* `apt update && apt install vim`
* `vim /etc/nginx/conf.d/edge-proxy.conf`

Then, inside edge-proxy.conf, changes the ip for the correct xucmgt routes (in the Application section) :
- location / :                                                      https://10.181.10.3/ucassistant -> http://OWN_MACHINE_IP:8070/ucassistant
- location ~ ^/(ucassistant|ccagent|switchboard)$ :                 https://10.181.10.3             -> http://OWN_MACHINE_IP:8070
- location ~ ^/(assets|config|ucassistant|ccagent|switchboard)/ :   https://10.181.12.3             -> http://OWN_MACHINE_IP:8070

It's basically rewriting HTTPS to HTTP and the IP of the XiVOCC to your Fullsave VPN IP combined with your sbt port
Then, save the modifications and reload the nginx config file

* `nginx -s reload`

Those NGINX modifications will stay until the nginx container is recreated

Then, on your host, you can start your sbt with the xuc host pointing at your edge NGINX using the FQDN
*  `sbt run -Dhttp.port=8070 -Dxuc.host=XUC_HOST_FQDN -Dxuc.port=8090`

## Tooling

Here is a quick description of the tools used in the javascript application

* `npm` for package management
* `webpack` for building orchestration
* `eslint` for checking code style
* `karma` for testing orchestration (with firefox & chrome headless supports)
* `Jasmine` for unit testing

Each of the tools have specific configuration files:
* package.json
* webpack.config.js
* karma.config.js
* .eslintrc.js (one at the root folder, one in the test folder for specific configuration)

## Workflow

When running the application in development mode using the run command, there is a play run hook that will start webpack to build the javascript application. 

Webpack is responsible of checking the javascript application and will ensure all dependencies are merged inside the application. It will create the following files based on its configuration in the `target/web/public/main/javascripts/dist` folder:

* `agent.js`: the main agent javascript application
* `ccmanager.js`: the main ccmanager javascript application
* `ucassistant.js`: the main web assistant javascript application
* `vendor.js`: contains all the third party dependencies (angular, jquery, lodash,...)


## Test

Run the following command to start the test server: `npm it`

It will check the dependencies are available and will run test continuously.

* *Note*: By default source maps have been disabled for increasing test execution speed, for debugging purpose you can uncomment in `karma.config` : ```webpackConfig.devtool='inline-source-map'```

If you want to run only specific tests, you can run one of the commands after running the test server (`npm it`) in a separate console

* `./node_modules/karma/bin/karma run -- --grep="agent edit controller"`
* `karma run -- --grep="agent edit controller"`

The later requires the karma-cli command line tool to be in the path. It can be installed with `npm -i -g karma-cli`

Please note the test are now run inside a browser. In fact, there are two browsers: Chrome & Firefox. It allows to perform test on the rendered version of our application/controllers/directive.

Html templates that lives in the javascript application are automatically loaded using the ng-html2js plugin for karma. Other templates that lives in the Play application needs to be mocked in the `test/karma/bootstrap.js`. This last part requires that you load the following modules in your test when needed : 

``` javascript
  beforeEach(angular.mock.module('karma-backend'));
  beforeEach(angular.mock.module('html-templates'));
```

## Docker Image Build

To be able to build locally the docker image (with the `docker_build.sh` script) you'll need a self signed certificated for electron .exe, to do so:
* Install openssl package for your operating system from [here](https://www.openssl.org/related/binaries.html)
* Generate a private key: `openssl genrsa 2048 > private.pem`
* Generate the self signed certificate: `openssl req -x509 -new -key private.pem -out public.pem`
* Create PFX: `openssl pkcs12 -export -in public.pem -inkey private.pem -out mycert.pfx`
* Move .pfx file to certs folder: `mv mycert.pfx ~/.certs/avencall.pfx`
* Create a secret file containing password of the pfx : `echo mypassswd  >  ~/.certs/secret` (if you don't have secret password just create a blank file)


## ES6 & Webpack nice features

When developing javascript part, you can use ES6 features and will be transpiled to ES5 if needed. Here is a quick list of the nice features of ES6/webpack

### Arrow functions
You can now write function expression using the following syntax
``` javascript
var fn = () => {

};

// Or in case of callbacks
mypromise.then((data) => {
    console.log("Here is the data", data);
});
```

See more: [Arrow functions](https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Functions/Arrow_functions)

### Imports
Instead of manually editing your html templates to include files, you can now use the `import` statement to import specific javascript files. 

``` javascript
import _ from 'lodash';

import './mymodule/myfile.js'; // extension is not required, same as './mymodule/myfile'

```

Also they are some aliases for the root folder of our modules:
* xccti
* xchelper
* xclogin

So you can write:
``` javascript
import 'xccti/cti-webpack';
```

See more: [import keyword](https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Statements/import)

### classes

See here: [class keyword](https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Operators/class)

### export & angular

Modularizing an angular module is pretty easy with the tools we have. You can split each directive,controller,service, template in a separate file. For example, you can write a controller in a js file like this:

``` javascript
export default class MainController {
    constructor() {
        // some code...
    }
}
```

And then register it in your module:

```
import MainController from './maincontroller.js';

angular.module('MyApp', [])
    .controller('MainController', MainController);
```

See more: [export keyword](https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Statements/export), [Using angular 1.x with webpack](http://beta.angular-tips.com/blog/2015/06/using-angular-1-dot-x-with-es6-and-webpack/)


### Turning a js file into a ts file

In it's current state, the best way to do so is simply to rename your .js file into a .ts file, the linter will automatically highlight  
everything you need to correct to make it "TypeScript security compliant"

### Turning an angularjs function-as-a-class service to a proper ts class service    
The important lines are the `static $inject` one, to inject dependencies into the constructor, and the fact
that `your class needs to be the default export of the file`.

Shortened example :
```ts
import { SomeFrameworkType } from "someframework"

export default class JitsiProxy {

  static $inject = ["dependency"]

  constructor ( private dependency: TypeOfDependency ) {}

  public someMethod = (someParameter: SomeFrameworkType): boolean => {
      this.dependency.someFunction(someParameter)
      return true
  }
}
```

### Turning an angularjs function-as-a-class directive to a proper ts class directive    
The important lines are the `static $inject` one, to inject dependencies into the constructor, and the fact
that `your class needs to be the default export of the file`. You also need to make sure that your directive  
has it's restrict declaration and it's own isolated scope and that it is merged with the standard angularjs  
scope object to keep the default functionalities like `$watch`, `$on` and so on...

Shortened example :
```ts
import { IDirective, IScope } from "angular"

export default class RefreshWarning implements IDirective {

  static $inject = ["dependency"]
  public restrict: string
  public isolatedScope: {
    someProperty: string | SomeType // string type forced by binding type sadly
  }

  constructor ( private dependency: TypeOfDependency ) {
    this.isolatedScope = {
        'someProperty': '=' // some binding type -> & / @ / =
    }
    this.restrict = 'A' // 'A'ttribute, 'E'lement ...
  }

  link(scope: IScope) {
      
    this.isolatedScope = IAngularStatic.extend(scope, this.isolatedScope)

    const someMethod = (someParameter: SomeFrameworkType): boolean => {
        this.dependency.someFunction(someParameter)
        if (this.isolatedScope.someProperty) return true
    }
  }
}
```

### Put a type on a js file (ex. s1: S1 type for s1.js injected in s2.ts)

You can create a *file*.d.ts file for your javascript file containing an interface that will act as a typing contract for your javascript.  
The pitfall is that the TypeScript compiler will trust your .d.ts file even if it's wrong since it has no way to check if it's the truth.

### Primitive types syntax

The following types are primitive javascript types and **MUST** be written without an uppercase, even through they are types:  
`string, number, boolean`

### More to come...

## Cypress


# Run Cypress Tests

you can run your tests from the xivo-cypress container

```bash
 docker run -it  --network=host -v $PWD/cypress/e2e:/e2e/cypress/e2e -w /e2e xivoxc/xivo-cypress:VERSION
```

or you can run it locally by cloning the xivo.solutions/xivo-cypress repository

## Documentation

    http://xivocc.readthedocs.org

## License


This program is free software: you can redistribute it and/or modify
it under the terms of the GNU Lesser General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.
This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Lesser General Public License for more details.
You should have received a copy of the GNU Lesser General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.

See the COPYING and COPYING.LESSER files for details.
