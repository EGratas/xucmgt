package controllers

import configuration.UcConfig
import controllers.helpers.EmailTemplate
import play.api.Environment
import play.api.libs.json.Json
import play.api.mvc.InjectedController

import javax.inject.Inject

class ClientConfiguration @Inject() (config: UcConfig, env: Environment)
    extends InjectedController
    with EmailTemplate {

  def getVersion() =
    Action {
      Ok(Json.obj("value" -> config.appVersion))
    }

  def getEmailTemplate() = get(config)(env)

  def getNestedConfig(path: String) = {
    val nestedConfig = config.getNestedApplicationConf(
      path.stripSuffix("/").replace("/", "."),
      "client"
    )
    Action {
      nestedConfig match {
        case Some(nestedConfig) => Ok(Json.obj("value" -> nestedConfig))
        case None               => NotFound
      }
    }
  }

}
