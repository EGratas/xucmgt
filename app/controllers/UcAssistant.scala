package controllers

import configuration.UcConfig
import javax.inject.Inject

class UcAssistant @Inject() (config: UcConfig) extends MainController {

  val loginImage: String =
    routes.Assets.versioned("images/avencallbox.jpg").toString
  override val isAgent = false
  val title            = "XiVO UC Assistant"

  override def connect =
    Action(implicit request => {
      log.debug(s"New connection to UC assistant from ${request.remoteAddress}")
      Ok(views.html.ucassistant.index(title, config)(request.lang, request))
    })
  def loginPage =
    Action(implicit request =>
      Ok(
        views.html.ucassistant.login("ucassistant Assistant Login", loginImage)
      )
    )
  def mainPage = Action(implicit request => Ok(views.html.ucassistant.main()))
  def contacts =
    Action(implicit request => Ok(views.html.ucassistant.contacts()))
  def callcontrol =
    Action(implicit request => Ok(views.html.ucassistant.callControl()))
  def history = Action(implicit request => Ok(views.html.ucassistant.history()))
  def menu    = Action(implicit request => Ok(views.html.ucassistant.menu()))
  def personalcontact =
    Action(implicit request => Ok(views.html.ucassistant.personalContact()))
  def ringtoneDevice =
    Action(implicit request => Ok(views.html.ucassistant.ringtoneDevice()))
  def ringtoneSelection =
    Action(implicit request => Ok(views.html.ucassistant.ringtoneSelection()))
  def createmeetingroom =
    Action(implicit request => Ok(views.html.ucassistant.meetingRooms()))
  def sso =
    Action(implicit request =>
      request.getQueryString("token") match {
        case None =>
          Redirect(
            s"http://${config.hostAndPort}/xuc/sso?orig=http://${request.host}${request.path}"
          )
        case Some(token) =>
          Ok(
            views.html.ucassistant.index(title, config, Some(token))(
              request.lang,
              request
            )
          )
      }
    )

}
