package controllers

import javax.inject.Inject
import play.api.cache.SyncCacheApi
import play.api.mvc.InjectedController

class ExternalViewSample @Inject() (cache: SyncCacheApi)
    extends InjectedController {

  def connect() =
    Action { implicit request =>
      {
        Ok(
          views.html.sample.externalView()(request)
        )
      }
    }
}
