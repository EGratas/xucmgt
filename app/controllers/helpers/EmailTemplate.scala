package controllers.helpers

import configuration.EmailConfig
import play.api.Environment
import play.api.mvc.InjectedController

trait EmailTemplate extends InjectedController {

  def get(config: EmailConfig)(implicit env: Environment) =
    Action {
      config.getEmailTemplate()(env) match {
        case Some(tpl) => Ok(tpl)
        case None      => NotFound
      }
    }
}
