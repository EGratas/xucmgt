package controllers

import configuration.CcConfig
import controllers.helpers.{EmailTemplate, PrettyController}

import javax.inject.Inject
import models.ConfigurationEntry
import play.api.{Environment, Logger}
import play.api.libs.json.Json

class CcManager @Inject() (
    config: CcConfig,
    prettyCtrl: PrettyController,
    env: Environment
) extends MainController
    with EmailTemplate {

  override val log     = Logger(getClass.getName)
  override val isAgent = false
  val title            = "XiVO CC Manager"

  override def connect =
    Action { implicit request =>
      log.debug(s"New connection to CC Manager from ${request.remoteAddress}")
      Ok(
        prettyCtrl.prettify(
          views.html.ccmanager.index(title, config)(request.lang, request)
        )
      )
    }

  def content =
    Action { implicit request =>
      Ok(prettyCtrl.prettify(views.html.ccmanager.managerContent(config)))
    }

  def login =
    Action { implicit request =>
      Ok(prettyCtrl.prettify(views.html.ccmanager.login()))
    }

  def getEmailTemplate() = get(config)(env)

  def getConfig(paramName: String) =
    Action {
      config.getManagerConf(paramName) match {
        case Some(v) => Ok(Json.toJson(ConfigurationEntry(paramName, v)))
        case None    => NotFound
      }
    }
}
