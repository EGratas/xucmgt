import { AuthToken, KerberosToken, CasToken, OpenIDToken } from "xccti/services/XucLink";
import { XucAuthErrorCode, XucAuthError } from "xccti/services/XucLinkError";
import { XucCredentials, XucLink } from "xccti/services/XucLink";
import { IPromise, ILogService, ITimeoutService, IWindowService } from "angular"

export interface AuthConfiguration {
  useSso: boolean;
  casServerUrl?: string;
  casLogoutEnable?: boolean;
  openidServerUrl?: string;
  openidClientId?: string;
  openidLogoutEnable?: boolean;
  requirePhoneNumber: boolean;
}

type AuthTokenProvider<T extends AuthToken> = () => Promise<T>;
type TokenTrader<T extends AuthToken> = (t: T) => Promise<XucCredentials>;

export enum AUTH_PROVIDER {
  XUC = "XUC",
  KERBEROS = "KERBEROS",
  CAS = "CAS",
  OPENID = "OPENID"
}

export class LoginHelper {
  configuration: AuthConfiguration;
  xucLink: XucLink;
  $timeout: ITimeoutService;
  $log: ILogService;
  $window: IWindowService;
  authProvider: AUTH_PROVIDER;
  requireCredentials: boolean = true;
  loginInProgress: boolean = false;
  autoLoginInProgress: boolean = false;
  readonly retryAutoLoginInitialTimeout = 5;
  readonly retryAutoLoginMaxTimeout = 60;
  disableAutoLogin: boolean = false;
  retryAutoLogin: boolean = false;
  retryAutoLoginAfter: number = this.retryAutoLoginInitialTimeout;
  retryAutoLoginPromise?: IPromise<void>;
  retryAutoLoginCountdown: number = this.retryAutoLoginInitialTimeout;
  lastError?: XucAuthError;
  moduleName: string = "LoginHelper";
  phoneNumber: string | undefined;

  getAuthToken: AuthTokenProvider<AuthToken> = () => Promise.reject();
  tradeAuthToken: TokenTrader<AuthToken> = () => Promise.reject();
  postLogout: () => void = () => { };

  constructor(configuration: AuthConfiguration, xucLink: XucLink, $timeout: ITimeoutService, $log: ILogService, $window: IWindowService, lastError?: string) {
    this.configuration = configuration;
    this.xucLink = xucLink;
    this.$timeout = $timeout;
    this.$log = $log;
    this.$window = $window;

    this.$log.info(this.moduleName, "Starting");
    this.xucLink.setRedirectToHomeUrl(false);

    if (this.configuration.useSso) {
      this.authProvider = AUTH_PROVIDER.KERBEROS;
      this.getAuthToken = this.getKerberosToken;
      this.tradeAuthToken = this.tradeXucToken;
      this.requireCredentials = false;
    }
    else if (this.configuration.casServerUrl !== undefined) {
      this.authProvider = AUTH_PROVIDER.CAS;
      this.getAuthToken = this.getCasToken;
      this.tradeAuthToken = this.tradeCasTokenToXucToken;
      this.postLogout = this.postLogoutCas;
      this.requireCredentials = false;
    }
    else if (this.configuration.openidServerUrl !== undefined) {
      this.authProvider = AUTH_PROVIDER.OPENID;
      this.getAuthToken = this.getOpenIDToken;
      this.tradeAuthToken = this.tradeOpendIDTokenToXucToken;
      this.requireCredentials = false;
    }
    else {
      this.authProvider = AUTH_PROVIDER.XUC;
      this.getAuthToken = this.getXucCredentials;
      this.tradeAuthToken = this.tradeXucToken;
    }
    this.$log.info(this.moduleName, "Authentication provider: " + this.authProvider);
    if (lastError != undefined) {
      this.onLoginError(this.makeError(<XucAuthErrorCode>lastError));
    }

  }

  getKerberosToken: AuthTokenProvider<KerberosToken> = () => {
    return this.xucLink.getSsoCredentials();
  }

  getCasToken: AuthTokenProvider<CasToken> = () => {
    if (this.configuration.casServerUrl) {
      return this.xucLink.getCasToken(this.configuration.casServerUrl, this.phoneNumber || this.xucLink.getStoredCredentials()?.phoneNumber);
    } else {
      return Promise.reject(this.makeError(XucAuthErrorCode.InvalidConfiguration, "CAS server URL is not defined"));
    }
  }

  tradeCasTokenToXucToken: TokenTrader<CasToken> = (token: CasToken) => {
    if (this.configuration.casServerUrl) {
      return this.xucLink.tradeCasToken(this.configuration.casServerUrl, token, this.phoneNumber);
    } else {
      return Promise.reject(this.makeError(XucAuthErrorCode.InvalidConfiguration, "CAS server URL is not defined"));
    }
  }

  getOpenIDToken: AuthTokenProvider<OpenIDToken> = () => {
    if (this.configuration.openidServerUrl && this.configuration.openidClientId) {
      return this.xucLink.getOpenidToken(this.configuration.openidServerUrl, this.configuration.openidClientId, this.phoneNumber || this.xucLink.getStoredCredentials()?.phoneNumber);
    } else {
      return Promise.reject(this.makeError(XucAuthErrorCode.InvalidConfiguration, "OpendID server URL or client id is not defined"));
    }
  }

  tradeOpendIDTokenToXucToken: TokenTrader<OpenIDToken> = (token: OpenIDToken) => {
    return this.xucLink.tradeOpenidToken(token)
  }

  getXucCredentials: AuthTokenProvider<XucCredentials> = () => {
    let url = this.$window.location.href;
    if(url !== undefined && url.includes('token=')) {
        const credWithUrl = {
            username: undefined,
            phoneNumber: undefined,
            token: String(url.split('token=')[1].split('#!')[0])
        }
        return Promise.resolve(credWithUrl);
    }
    let cred = this.xucLink.getStoredCredentials();
    if (cred) {
      return Promise.resolve(cred);
    } else {
      return Promise.reject(this.makeError(XucAuthErrorCode.NoStoredCredentials));
    }
  }

  tradeXucToken: TokenTrader<AuthToken> = (token: AuthToken) => {
    this.$log.debug(this.moduleName, "Trading xuc token");
    return Promise.resolve(token as XucCredentials);
  }

  postLogoutCas: () => void = () => {
    if (this.configuration.casServerUrl && this.configuration.casLogoutEnable) {
      return this.xucLink.logoutFromCas(this.configuration.casServerUrl);
    }
  }

  autoLogin(phoneNumber?: string) {
    this.$log.info(this.moduleName, "Automatic login attempt");
    if (this.configuration.requirePhoneNumber &&
      (phoneNumber == undefined || phoneNumber.length === 0)) {
      this.onLoginError(this.makeError(XucAuthErrorCode.RequirePhoneNumber));
      return;
    }
    this.phoneNumber = phoneNumber;
    if (this.lastError != undefined) {
      switch (this.lastError.error) {
        case XucAuthErrorCode.LinkClosed:
          this.$log.warn(this.moduleName, "Link was closed, attempting relogin");
          break;
        case XucAuthErrorCode.Logout:
          this.$log.warn(this.moduleName, "Manual logout, aborting autologin");
          this.postLogout();
          return;
        default:
          this.$log.warn(this.moduleName, "Got error " + this.lastError.error + ", aborting autologin");
          return;
      }
    }
    this.loginInProgress = true;
    this.autoLoginInProgress = true;
    this.cancelAutoLoginLater();
    this.clearLastError();

    this.xucLink.checkXucIsUp()
      .then(this.getAuthToken.bind(this))
      .then(this.tradeAuthToken.bind(this))
      .then(this.finalizeLogin.bind(this))
      .catch(this.onLoginError.bind(this))

  }

  clearCasTicket() {
    window.location.search = '';
  }

  logoutFromOpenid() {
    let oidcUrl = this.configuration.openidServerUrl
    if (oidcUrl) this.xucLink.logoutFromOpenid(oidcUrl, this.phoneNumber)
  }

  onLoginError(error: XucAuthError) {
    this.$log.error(this.moduleName, "onLoginError: " + error.error);
    this.loginInProgress = false;
    this.autoLoginInProgress = false;
    this.lastError = error;
    switch (error.error) {
      case XucAuthErrorCode.NoStoredCredentials:
        this.clearLastError();
        this.fallbackToManualLogin();
        break;
      case XucAuthErrorCode.LinkClosed:
      case XucAuthErrorCode.NoResponse:
        this.retryAutoLoginLater();
        break;
      case XucAuthErrorCode.Logout:
        if (this.configuration.openidLogoutEnable) this.logoutFromOpenid();
        break;
      case XucAuthErrorCode.RequirePhoneNumber:
      case XucAuthErrorCode.LoggedInOnAnotherPhone:
        break;
      case XucAuthErrorCode.CasServerInvalidTicket:
        this.clearCasTicket();
        break;
      default:
        this.fallbackToManualLogin();
        break;
    }
  }

  login(username: string, password: string, phoneNumber?: string) {
    this.$log.info(this.moduleName, "Login attempt with credentials");
    this.loginInProgress = true;
    this.disableAutoLogin = true;
    this.cancelAutoLoginLater();
    this.clearLastError();
    if (this.configuration.requirePhoneNumber &&
      (phoneNumber == undefined || phoneNumber.length === 0)) {
      this.onLoginError(this.makeError(XucAuthErrorCode.RequirePhoneNumber));
    } else {
      this.xucLink.login(username, password, phoneNumber)
        .catch(this.onLoginError.bind(this));
    }
  }

  fallbackToManualLogin() {
    this.$log.debug(this.moduleName, "Falling back to manual login");
    this.loginInProgress = false;
    this.autoLoginInProgress = false;
    this.requireCredentials = true;
    this.retryAutoLogin = false;
    this.authProvider = AUTH_PROVIDER.XUC;
    this.getAuthToken = this.getXucCredentials;
    this.tradeAuthToken = this.tradeXucToken;
  }

  finalizeLogin(credentials: XucCredentials): Promise<any> {
    this.$log.info(this.moduleName, "Finalize login");
    if (this.phoneNumber) {
      credentials.phoneNumber = this.phoneNumber;
    }
    return this.xucLink.checkAndloginWithCredentials(credentials);
  }

  retryAutoLoginLater() {
    this.$log.debug(this.moduleName, "Retry autologin later");
    if (this.disableAutoLogin) {
      return;
    }

    if (!this.retryAutoLogin) {
      this.retryAutoLogin = true;
      this.retryAutoLoginAfter = this.retryAutoLoginInitialTimeout;
    } else {
      this.retryAutoLoginAfter = this.retryAutoLoginAfter * 2;
      if (this.retryAutoLoginAfter > this.retryAutoLoginMaxTimeout) {
        this.retryAutoLoginAfter = this.retryAutoLoginMaxTimeout;
      }
    }
    this.retryAutoLoginCountdown = this.retryAutoLoginAfter;
    this.$log.debug(this.moduleName, "Retry in " + this.retryAutoLoginCountdown);
    this.retryAutoLoginPromise = this.$timeout(this.updateAutoLoginCountdown.bind(this), 1000);
  }

  cancelAutoLoginLater() {
    if (this.retryAutoLoginPromise) {
      this.$timeout.cancel(this.retryAutoLoginPromise);
      this.retryAutoLoginPromise = undefined;
    }
  };

  updateAutoLoginCountdown() {
    this.retryAutoLoginCountdown--;
    this.$log.debug(this.moduleName, "Retry countdown " + this.retryAutoLoginCountdown);
    this.retryAutoLoginPromise = undefined;
    if (this.retryAutoLoginCountdown <= 0) {
      this.clearLastError();
      this.autoLogin(this.xucLink.getStoredCredentials()?.phoneNumber || "");
    } else {
      this.retryAutoLoginPromise = this.$timeout(this.updateAutoLoginCountdown.bind(this), 1000);
    }
  }

  makeError(code: XucAuthErrorCode, message?: string): XucAuthError {
    return { error: code, message: message } as XucAuthError;
  }

  clearLastError() {
    this.lastError = undefined;
  }

  hasError(): boolean {
    return this.lastError != undefined;
  }
}
