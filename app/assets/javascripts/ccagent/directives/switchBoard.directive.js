export default function switchBoard(XucSwitchboard) { 

  return {
    restrict: 'E',
    templateUrl: 'assets/javascripts/ccagent/directives/switchBoard.html',
    link: (scope) => {

      scope.switchboardIncomingCalls = XucSwitchboard.getSwitchboardIncomingCalls();
      scope.switchboardHoldCalls = XucSwitchboard.getSwitchboardHoldCalls();
      
      scope.getIncomingQueue = () => {
        let queue = XucSwitchboard.getIncomingQueue();

        if (queue) {
          return queue.displayName;
        }
      };

      scope.getHoldQueue = () => {
        let queue = XucSwitchboard.getHoldQueue();

        if (queue) {
          return queue.displayName;
        }
      };

      scope.retrieveByIndex = (index) => {
        scope.retrieve(scope.switchboardHoldCalls[index]);
      };

      scope.retrieve = (queueCall) => {
        XucSwitchboard.retrieveQueueCall(queueCall);
      };
    }
  };

}