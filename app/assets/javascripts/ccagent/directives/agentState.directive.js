import _ from 'lodash';
import countdown from 'countdown';

export default function agentState($translate, $log, XucAgentUser, XucLink, XucAgent, $state, $interval, ExternalEvent, electronWrapper, $window,
  CtiStatusService) {

  return {
    restrict: 'E',
    templateUrl: 'assets/javascripts/ccagent/directives/agentState.html',
    link: (scope) => {
      const logoutState = 'AgentLoggedOut';
      const onCallState = 'AgentOnCall';

      var stopTime;
      var quitApp = false;
      const stateRefreshTimeout = 1000;
      scope.agentState = {};
      scope.loggedIn = false;
      scope.ctiStatuses = [];

      scope.refreshState = () => {
        scope.agentState.timeInState = countdown(scope.agentState.momentStart);
      };
      stopTime = $interval(scope.refreshState, stateRefreshTimeout);

      scope.$on('$destroy', function() {
        $interval.cancel(stopTime);
      });

      scope.checkLogout = function(state) {
        if(state.name === logoutState) {
          if (scope.loggedIn) {
            if ($window.externalConfig.useSso) XucLink.clearCredentials();
            XucLink.logout();
            scope.loggedIn = false;
            $state.go("login", {error: "Logout"});
            if (quitApp) electronWrapper.forceQuit();
          }
        }
        else {
          scope.loggedIn = true;
        }
      };

      XucAgentUser.subscribeToAgentState(scope, function(agtState) {
        $log.debug("agentState: received "+agtState.name +" AgentStateEvent");
        scope.checkLogout(agtState);
        scope.agentState = agtState;
        let m = XucAgent.buildMoment(agtState.since);

        scope.agentState.momentStart = m.momentStart;
        scope.agentState.timeInState = m.timeInState;


        if (agtState.status !== undefined) {
          scope.ctiStatuses = CtiStatusService.getPossibleCtiStatus(agtState.status);
        } else {
          scope.ctiStatuses = CtiStatusService.getPossibleCtiStatus(CtiStatusService.mapAgentStateToCtiStatus(agtState.name));
        }


      });
      ExternalEvent.registerConfirmQuitCallback(() => {
        quitApp = true;
        scope.switchState({name: logoutState, userStatus: null});
      });

      scope.getAgentStateClassName = function(state) {
        const _state = scope.ctiStatuses.find( elem => elem.name === state.name);

        if (_state) {
          switch (_state.status) {
          case 0:
            return "fa-chevron-down state-agent-ready";
          case 1:
            return "fa-chevron-down state-agent-on-pause";
          case 2:
            return "fa-power-off state-agent-logged-out";
          }
        }
        const icon = state.name === logoutState ? "fa-power-off" : "fa-chevron-down";
        if (_.kebabCase(state.name)) {
          return icon + ' state-' + _.kebabCase(state.name) + (state.status ? " state-agent-on-pause" : " state-agent-ready");
        }
      };

      scope.switchState = function(state) {
        if (state.status !== undefined) {
          CtiStatusService.switchAgentStateCtiStatus(state.status, state.name);
        } else {
          CtiStatusService.switchAgentStateCtiStatus(CtiStatusService.mapAgentStateToCtiStatus(state.name), state.userStatus && state.userStatus.name ? state.userStatus.name : "");
        }
      };

      scope.displayTime = function(){
        return scope.agentState.name !== onCallState;
      };
    }
  };
}
