export const ELECTRON_DEFAULT_WIDTH = 362;
export const ELECTRON_DEFAULT_HEIGHT = 800;
export const ELECTRON_MINI_WIDTH = 72;
export const ELECTRON_MINI_HEIGHT = 675;
export const ELECTRON_THIRD_PARTY_MIN_WIDTH = 1000;
export const ELECTRON_QUIT_OPTIONS = {
  type: 'question',
  buttons: ['AGT_DIALOG_CANCEL', 'AGT_DIALOG_QUIT'],
  defaultId: 1,
  message: 'AGT_DIALOG_MSG',
  detail: 'AGT_DIALOG_DETAIL'
};

export default function run($rootScope, $uibModal, $state, errorModal, $window, XucLink, XucUser, XucChat, $log, $translate, $transitions, CtiProxy, ExternalEvent, electronWrapper, remoteConfiguration, externalView){
  $rootScope.$state = $state;

  remoteConfiguration.getOptional('iceGatheringTimeout', true).then(ms => {
    if (ms && ms.length > 0 && $window.SIPml) $window.SIPml.setCustomIceGatheringTimeout(ms);
  });

  $transitions.onStart({
    to: function(state) {
      return state.data !== null && state.data.requireLogin === true;
    }
  },function(trans) {
    if(!XucLink.isLogged()) {
      $log.info("Login required");
      return trans.router.stateService.target('login');
    }
    return true;
  });

  $transitions.onSuccess({ to: 'login' }, () => {
    electronWrapper.setElectronConfig({confirmQuit: {reset: true}});
    electronWrapper.setTrayIcon('logout');

    return remoteConfiguration.get('externalViewUrl').then(result => {
      externalView.setURL(result);
    });
  });

  $transitions.onSuccess({ from: 'login' }, () => {
    remoteConfiguration.getBooleanOrElse('logoffOnExit', false).then(value => {
      if (value) electronWrapper.setElectronConfig({confirmQuit: ELECTRON_QUIT_OPTIONS});
    });
    remoteConfiguration.get("version").then((serverVersion) => {
      if (serverVersion != $window.appVersion){
        $log.warn("Server version mismatch", serverVersion);
        if ($window.externalConfig.switchboard) {
          $window.location.replace("/switchboard#!/login?error=Version");
        } else {
          $window.location.replace("/ccagent#!/login?error=Version");
        }
        $window.location.reload();
      }
    });
    electronWrapper.setTrayIcon('default');
  });
}
