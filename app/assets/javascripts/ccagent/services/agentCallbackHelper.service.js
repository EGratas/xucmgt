import _ from 'lodash';
import moment from 'moment';

export default function agentCallbackHelper(XucCallback, XucAgentUser, $log, XucLink, $q) {

  this.$q = $q;

  var callbacks = [];
  var ongoingCallbackId = null;
  var ongoingCallbackTicketId = null;

  var _loadAllCallbacks = function() {
    return XucCallback.getCallbackListsAsync().then(result => _onCallbacksLoaded(result));
  };

  var _getStatus = function() {
    if(callbacks.length > 0)
      return callbacks[0].dueStatus;
    else
      return '';
  };

  var _getCallbacks = function() {
    return callbacks;
  };

  var _clearCallbacks = function() {
    callbacks = [];
  };

  var _onCallbacksLoaded = function(loadedCallbacks) {
    _clearCallbacks();
    $log.debug('onCallbacksLoaded ' + loadedCallbacks.length);

    const newCBsWithMembership = _.chain(loadedCallbacks)
      .flatMap(list => list.callbacks)
      .filter(cb => !(angular.isDefined(cb.agentId) && cb.agentId !== null))
      .filter(cb => angular.isDefined(cb.queue))
      .groupBy(cb => cb.queue.id)
      .map((cbList, queueId) => {
        return XucAgentUser.isMemberOfQueueAsync(queueId).then( (isMemberOfQueue) => {
          return {
            queueId: queueId,
            callbacks: cbList,
            isMemberOfQueue: isMemberOfQueue
          };
        });
      })
      .value();

    $q.all(newCBsWithMembership).then( (res) => {
      const newQueueIds = _.chain(res)
        .map(o => parseInt(o.queueId))
        .uniq()
        .value();

      const curCallbacks = callbacks.filter(cb => !_.includes(newQueueIds, cb.queue.id));

      const appendCb = _.chain(res)
        .filter(o => o.isMemberOfQueue)
        .flatMap(o => o.callbacks)
        .value();


      _.chain(curCallbacks)
        .concat(appendCb)
        .sortBy(cb =>  XucCallback.callbackDueStatusOrder(cb.dueStatus))
        .forEach(cb => callbacks.push(cb))
        .value();
    });

  };

  var _takeCallback = function(callback) {
    $log.debug('taking ' +callback.uuid+ ' callback');
    return XucCallback.takeCallbackAsync(callback.uuid);
  };

  var _startCallbackCall = function(callback, number) {
    $log.debug('starting ' +callback.uuid+ ' callback');
    ongoingCallbackId = callback.uuid;
    XucCallback.startCallback(callback.uuid, number);
  };

  var _releaseCallback = function(callback) {
    $log.debug('releasing ' +callback.uuid+ ' callback');
    ongoingCallbackId = null;
    ongoingCallbackTicketId = null;
    XucCallback.releaseCallback(callback.uuid);
    return {};
  };

  var _updateCallbackTicket = function(callback) {
    $log.debug('updating ' +callback.ticket.uuid+ 'ticket for ' +callback.uuid+ ' callback');

    var ticket = callback.ticket;
    if(ticket.dueDate instanceof Date) {
      ticket.dueDate = moment(ticket.dueDate).format("YYYY-MM-DD");
    }
    XucCallback.updateCallbackTicket(ticket);

    ongoingCallbackId = null;
    ongoingCallbackTicketId = null;
    return {};
  };

  var _createCallbackTicket = function() {
    return {
      uuid: null,
      status: null,
      comment: null,
      dueDate: null
    };
  };

  var _setCallbackTicket = function(callback, ticketUuid){
    if (ticketUuid != null) {
      ongoingCallbackTicketId = ticketUuid;
    }
    callback.ticket = _createCallbackTicket();
    callback.ticket.uuid = ongoingCallbackTicketId;
  };

  var _isOngoingCallback = function(callback) {
    return (callback.uuid === ongoingCallbackId && callback != null);
  };

  var _getPendingCallbacks = function(agentId) {
    var q = XucCallback
      .queryBuilder()
      .filterEq("clotured", false)
      .filterEq("agentId", agentId)
      .build();

    return XucCallback.findCallbackRequestsAsync(q);
  };

  return {
    loadAllCallbacks: _loadAllCallbacks,
    onCallbacksLoaded: _onCallbacksLoaded,
    getStatus: _getStatus,
    getCallbacks: _getCallbacks,
    takeCallback: _takeCallback,
    releaseCallback: _releaseCallback,
    getPendingCallbacks: _getPendingCallbacks,
    startCallbackCall: _startCallbackCall,
    isOngoingCallback: _isOngoingCallback,
    clearCallbacks: _clearCallbacks,
    createCallbackTicket: _createCallbackTicket,
    setCallbackTicket: _setCallbackTicket,
    updateCallbackTicket: _updateCallbackTicket
  };
}
