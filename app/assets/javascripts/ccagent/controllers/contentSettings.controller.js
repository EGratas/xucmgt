export default class ContentSettingsController {

  constructor(applicationConfiguration, CtiProxy) {
    this.applicationConfiguration = applicationConfiguration;
    this.CtiProxy = CtiProxy;
  }

  displayAudioSettings() {
    return this.CtiProxy.isUsingWebRtc();
  }

  isSwitchboard() {
    return this.applicationConfiguration.getCurrentAppConfig().switchboard;
  }

}
