import moment from 'moment';

export default class ContentHistoryController {

  constructor($scope, $state, $transitions, XucCallHistory, $translate, callContext, applicationConfiguration, callHistoryPartyFormatter) {
    this.$scope = $scope;
    this.XucCallHistory = XucCallHistory;
    this.$translate = $translate;
    this.$state = $state;
    this.$transitions = $transitions;
    this.callContext = callContext;
    this.labels = {};
    this.callHistoryPartyFormatter = callHistoryPartyFormatter;
    this.appConfig = applicationConfiguration.getCurrentAppConfig();
    this.sortByDateToggled = false;    

    $translate(['AGT_HISTORY_TODAY', 'AGT_HISTORY_YESTERDAY', 'AGT_HISTORY_LAST']).then((translations) => {
      this.labels.today = translations.AGT_HISTORY_TODAY;
      this.labels.yesterday = translations.AGT_HISTORY_YESTERDAY;
      this.labels.last = translations.AGT_HISTORY_LAST;
    });

    this.init();
  }

  init() {    
    this.$scope.isLoading = true;
    this.$transitions.onStart({}, trans => {
      if (trans.$to().name != 'content.history') {
        this.XucCallHistory.unSubscribeToAgentCallHistory();
      }
    });
    this.XucCallHistory.subscribeToAgentCallHistory(this.$scope, callHistory => {
      this.$scope.history = this.callHistoryPartyFormatter.toPartyCallsPerDay(callHistory);
      this.$scope.isLoading = false;
    });
    this.appConfig.switchboard ? this.XucCallHistory.updateAgentCallHistory(100): this.XucCallHistory.updateAgentCallHistory(20);
  }

  getCallStatus(status) {
    var baseUrl = '/assets/images/ccagent/history/call_status_';
    if (status == 'emitted') return baseUrl + 'outgoing.svg';
    else if (status == 'answered') return baseUrl + 'incoming.svg';
    else if (status == 'missed' || status == 'abandoned' || status == 'exit_with_key') return baseUrl + 'missed.svg';
    return '';
  }

  stopEventAndDial(event, number) {
    event.stopPropagation();
    this.callContext.normalizeDialOrAttTrans(number);
  }

  reverseHistory(history) {
    history.reverse();
    for (let dayAndCalls of history) {
      dayAndCalls.details.reverse();
    }
    return history;
  }

  sortByDates(history) {
    this.sortByDateToggled = !this.sortByDateToggled;
    if (history) this.$scope.history = this.reverseHistory(history);
  }

  formatHistoryDay(date) {
    return moment(date).calendar(null, {
      sameDay: this.labels.today,
      lastDay: this.labels.yesterday,
      lastWeek: this.labels.last,
      sameElse: 'DD/MM/YYYY'
    });
  }

  getDateToggleClass() { return this.sortByDateToggled ? 'fa-sort-down' : 'fa-sort-up'; }
  getChevronClass(item) { return item.display ? "fa-chevron-up" : "fa-chevron-down"; }
  getDisplayClass(item) { return item.display ? "children-show" : "children-hiddens"; }
  toggleChildrens(item) { return item.display = !item.display; }
  showChildrens(item) { return item.display; }
  formatHistoryTime(date) { return moment(date).format('HH:mm'); }
}
