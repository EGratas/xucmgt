import _ from 'lodash';

export default class ContentSearchController {

  constructor($scope, XucDirectory, focus, keyNavUtility, callContext, $rootScope) {
    this.$scope = $scope;
    this.$rootScope = $rootScope;
    this.XucDirectory = XucDirectory;
    this.$scope.searchResult = [];
    this.focus = focus;
    this.keyNavUtility = keyNavUtility;
    this.callContext = callContext;
    this.focus('search');
    this.$scope.$on('searchResultUpdated', this.onSearchResult.bind(this));
  }

  onSearchResult() {
    this.keyNavUtility.resetIndex();
    this.$scope.searchResult = _.each(this.XucDirectory.getSearchResult(), (contact) => {
      contact.hover = false;
      return contact.entry[0].toLowerCase();
    });
    _.remove(this.$scope.searchResult, (contact) => {
      return !this.isCallable(contact);
    });
    this.$scope.headers = this.XucDirectory.getHeaders().slice(0, 5);
    this.$scope.searchTerm = this.XucDirectory.getSearchTerm() ? this.XucDirectory.getSearchTerm() : '*';
    if(!this.$rootScope.$$phase) this.$scope.$apply();
  }

  getPhoneStateLabel(status, videoStatus) {
    if (videoStatus == 'Busy') return "USER_VIDEOSTATUS_" + videoStatus;
    return "USER_STATUS_" + status;
  }

  isMeetingRoom(contact) {
    return this.callContext.isMeetingRoom(contact);
  }

  getPhoneStateBackColor(contact) {
    let lighter = contact.keydown ? " lighter" : "";
    if (contact.videoStatus == "Busy") return "user-videostatus-" + contact.videoStatus;
    return "user-status" + contact.status + lighter;
  }

  getContactLabelColor(contact) {
    if (contact.keydown) return 'text-white';
  }

  isCallable(contact) {
    return _.some([1, 2, 3, 5], function(index) {
      return !_.isEmpty(contact.entry[index]);
    });
  }

  isEmpty(field) {
    if (typeof field != 'undefined') {
      if (field.length > 0) {
        return true;
      }
    }
    return false;
  }

  isSearching() {
    return this.XucDirectory.isSearching();
  }

  setHover(contact) {
    contact.hover = true;
  }

  resetHover(contact) {
    contact.hover = false;
  }

  openDropdown(contactIndex) {
    this.keyNavUtility.stop(true);
    this.$scope.searchResult[contactIndex].keydown = true;
  }
}