export default class ContentConversationController {

  constructor($scope, $state, $stateParams) {
    $scope.remoteParty = $stateParams.remoteParty;

    $scope.gotoConversationHistory = () => {
      $state.go("content.conversationHistory");
    };
  }
}
