export default class LoginController {

  constructor($window, $state, $stateParams, XucAgentUser, CtiProxy, electronWrapper, remoteConfiguration) {
    this.xucAgentUser = XucAgentUser;
    this.error = $stateParams.error;
    this.errorParam = $stateParams.phoneNumber;
    this.$state = $state;
    this.ctiProxy = CtiProxy;
    this.electronWrapper = electronWrapper;

    this.hostAndPort = $window.externalConfig.hostAndPort;
    this.useSso = $window.externalConfig.useSso;
    this.casServerUrl = $window.externalConfig.casServerUrl;
    this.casLogoutEnable = $window.externalConfig.casLogoutEnable;
    this.isSwitchboard = $window.externalConfig.switchboard;
    this.openidServerUrl = $window.externalConfig.openidServerUrl;
    this.openidClientId = $window.externalConfig.openidClientId;
    this.openidLogoutEnable = $window.externalConfig.openidLogoutEnable;
    
    let electronRelativeDownloadPath = $window.electronRelativeDownloadPath;
    
    this.electronDownloadLinkLinux = `https://mirror.xivo.solutions/${electronRelativeDownloadPath}/linux/xivo-desktop-assistant-installer.deb`; 
    this.electronDownloadLinkWindows = `https://mirror.xivo.solutions/${electronRelativeDownloadPath}/win/xivo-desktop-assistant-installer.exe`; 

    remoteConfiguration.getBooleanOrElse('showAppDownload', true).then((resp) => {
      this.displayDesktopDownload = resp;
    });
    
    if (this.isSwitchboard) remoteConfiguration.setSwitchBoard();

    let electronUpdateURL = `https://mirror.xivo.solutions/${electronRelativeDownloadPath}/win`;

    electronWrapper.setElectronConfig({confirmQuit: null});
    electronWrapper.setUpdateURL(electronUpdateURL);
  }

  onLogin() {
    this.$state.go('content');
  }  

  isElectron() {
    return this.electronWrapper.isElectron();
  }

  displayDownloadDesktop() {
    return !this.isElectron() && this.displayDesktopDownload == true;
  }
}
