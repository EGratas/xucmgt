import moment from 'moment';
import _ from 'lodash';

export default class ContentCustomerController {

  constructor($scope, $state, $translate, XucCallHistory, XucPhoneEventListener, XucPhoneState, $log, XucSheet) {
    this.$scope = $scope;
    this.$state = $state;
    this.labels = {};
    this.$scope.history = {};
    this.$scope.attachedData = [];
    this.XucCallHistory = XucCallHistory;
    this.XucSheet = XucSheet;
    this.XucPhoneState = XucPhoneState;

    $translate(['AGT_HISTORY_TODAY', 'AGT_HISTORY_YESTERDAY', 'AGT_HISTORY_LAST']).then((translations) => {
      this.labels.today = translations.AGT_HISTORY_TODAY;
      this.labels.yesterday = translations.AGT_HISTORY_YESTERDAY;
      this.labels.last = translations.AGT_HISTORY_LAST;
    });

    let activeCalls = _.filter(XucPhoneState.getCalls(), (call) => {
      return call.state === XucPhoneState.STATE_ESTABLISHED || call.state === XucPhoneState.STATE_RINGING;
    });

    if(activeCalls.length === 1) {
      $log.debug(activeCalls);
      this.updateCustomerCallInfo(activeCalls[0]);
    }

    XucSheet.subscribeToAttachedData(this.updateAttachedData.bind(this));
    XucPhoneEventListener.addRingingHandler(this.$scope, this.onRinging.bind(this));
    XucPhoneEventListener.addReleasedHandler(this.$scope, this.onRelease.bind(this));

  }

  formatHistoryDay(date) {
    return moment(date).calendar(null, {
      sameDay: this.labels.today,
      lastDay: this.labels.yesterday,
      lastWeek: this.labels.last,
      sameElse: 'DD/MM/YYYY'
    });
  }

  getCallWaitTime(call) {
    switch (call.status){
    case 'answered': return call.waitTime;
    default: return call.duration;
    }
  }

  getCallClass(call) {
    switch (call.status){
    case 'ongoing': return 'waiting-agent-answer';
    case 'answered': return 'agent-answer';
    default: return 'no-agent-answer';
    }
  }

  hasAnsweredCall(call) {
    if (call.status === 'answered') {
      return true;
    }
  }

  setActiveMenu(menu) {
    this.$state.go('content.customer.'+menu);
  }

  isActive(menu) {
    return this.$state.is('content.customer.'+menu);
  }

  formatData(data) {
    let dataStructured = _.map(data, (value, key) => { return {key: key, value: value};});
    return _.sortBy(dataStructured, ['key']);
  }

  buildDataFromSheet(data) {
    return _.map(this.formatData(data), d => { return [d.key, d.value]; });
  }

  buildDataFromDialPlan(data) {
    let filterValues = (values, startsWith) => {
      return _.map(_.filter(values, v => {return _.startsWith(v.key, startsWith);}), v => {return v.value;});
    };
    let titles = filterValues(this.formatData(data), 'USR_TITLE');
    let fields = filterValues(this.formatData(data), 'USR_DATA');

    return _.zip(titles, fields);
  }

  updateCustomerCallInfo(call) {
    // context tab
    this.updateAttachedData(call);

    // history tab
    let q = this.XucCallHistory.queryBuilder();
    q = q.filterEq("src_num", call.otherDN);
    q = q.limit(10).build();

    this.XucCallHistory.findCustomerCallHistoryAsync(q).then((result) => {
      if (this.XucPhoneState.getCalls().length > 0) this.$scope.history = result;
    });
  }

  updateAttachedData(call) {
    this.$scope.attachedData = _.concat(
      this.buildDataFromDialPlan(call.userData),
      this.buildDataFromSheet(this.XucSheet.getAttachedData())
    );
  }

  onRinging(event) {
    this.updateCustomerCallInfo(event);
  }

  onRelease() {
    this.$scope.history = {};
    this.$scope.attachedData = [];
  }

}
