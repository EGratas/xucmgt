import _ from 'lodash';
import angular, {ILogService, IScope} from "angular";
import XucThirdPartyService from "../../xccti/services/XucThirdParty.service";
import {ElectronWrapper} from "../../xchelper/services/ElectronWrapper";
import {RichWindow} from "../../RichWindow";
import {AppConfig, ApplicationConfiguration} from "../../xccti/services/applicationConfiguration.provider";

export default class ThirdPartyController {
    appConfig: AppConfig;

    constructor(
        private $scope: any,
        private $log: ILogService,
        private XucThirdPartyService: XucThirdPartyService,
        private remoteConfiguration: any,
        private electronWrapper: ElectronWrapper,
        private ExternalEvent: any,
        private $window: RichWindow,
        private applicationConfiguration: ApplicationConfiguration
    ) {
        this.appConfig = applicationConfiguration.getCurrentAppConfig();
        remoteConfiguration.get("thirdPartyWsUrl").then((value: any) => {
            if (value.length > 0) {
                this.XucThirdPartyService.setThirdPartyWs(value);

                this.ExternalEvent.registerThirdPartyCallback(this.onMessageReceived.bind(this));
                this.XucThirdPartyService.addActionHandler(this.$scope, this.onThirdPartyAction.bind(this));
                this.XucThirdPartyService.addClearHandler(this.$scope, this.onThirdPartyClearAction.bind(this));
            }
        });
    }

    isSwitchboard() {
        return this.appConfig.switchboard ? "thirdparty-switchboard" : " ";
    }

    onMessageReceived() {
        this.XucThirdPartyService.clearAction();
    }

    onThirdPartyAction(event: any) {
        switch (event.action) {
            case 'open': {
                this.$scope.action = event;
                this.electronWrapper.setElectronFullscreen();
                let btn = angular.element('#maxmin-btn');
                if (btn.length) {
                    btn.addClass('hide');
                }
                break;
            }
            case 'popup': {
                if (angular.isDefined(event.url)) {
                    let windowName = 'popup';
                    if (angular.isDefined(event.multitab) && event.multitab) {
                        windowName = windowName + _.uniqueId();
                    }
                    this.$window.open(event.url, windowName);
                }
                break;
            }
            case 'run': {
                if (angular.isDefined(event.url)) {
                    let args = [];
                    if (angular.isDefined(event.executableArgs)) {
                        args = event.executableArgs;
                    }
                    this.electronWrapper.runExecutable(event.url, args);
                }
            }
        }
    }

    onThirdPartyClearAction() {
        this.$scope.action = null;
        this.electronWrapper.setElectronConfig({
            width: this.electronWrapper.ELECTRON_AGENT_DEFAULT_WIDTH,
            height: this.electronWrapper.ELECTRON_AGENT_DEFAULT_HEIGHT
        });
        let btn = angular.element('#maxmin-btn');
        if (btn.length) {
            btn.removeClass('hide');
        }
    }
}
