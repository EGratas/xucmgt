export default class MainController {

  constructor($sce, $log, $scope, $state, $interval, $window, $translate, XucAgentUser, XucUser, XucLink, CtiProxy, XucUtils, XucDirectory, XucSheet, callContext,
    agentCallbackHelper, agentActivitiesHelper, electronWrapper, XucNotification, focus, onHold, XucQueueTotal, remoteConfiguration, applicationConfiguration,
    externalView, XucPhoneEventListener, plantronics, headset, $rootScope, JitsiProxy, BrowserService, errorModal,XucPhoneState) {
    this.$scope = $scope;
    this.$state = $state;
    this.$window = $window;
    this.XucLink = XucLink;
    this.XucUser = XucUser;
    this.XucAgentUser = XucAgentUser;
    this.CtiProxy = CtiProxy;
    this.XucUtils = XucUtils;
    this.XucDirectory = XucDirectory;
    this.XucSheet = XucSheet;
    this.XucQueueTotal = XucQueueTotal;
    this.callContext = callContext;
    this.agentCallbackHelper = agentCallbackHelper;
    this.agentActivitiesHelper = agentActivitiesHelper;
    this.electronWrapper = electronWrapper;
    this.focus = focus;
    this.onHold = onHold;
    this.plantronics = plantronics;
    this.headset = headset;
    this.$translate = $translate;
    this.$rootScope = $rootScope;
    this.jitsiProxy = JitsiProxy;
    this.showExternalViewButton = false;
    this.appConfig = applicationConfiguration.getCurrentAppConfig();

    this.electronToggledSize = {width: this.electronWrapper.ELECTRON_AGENT_MINI_WIDTH, height: this.electronWrapper.ELECTRON_AGENT_MINI_HEIGHT, minimalist: true};
    this.electronDefaultSize = { width: this.electronWrapper.ELECTRON_AGENT_DEFAULT_WIDTH, height: this.electronWrapper.ELECTRON_AGENT_DEFAULT_HEIGHT, minimalist: false};

    this.agentActivitiesHelper.init(this.$scope);

    this.agentCallbackHelper.loadAllCallbacks();
    $interval(this.agentCallbackHelper.loadAllCallbacks, 3600000, 0, true); // every hour

    XucAgentUser.loginAgent();
    XucUser.getUserAsync().then((user) => {
      $scope.user = user;
      XucNotification.enableCallNotification();
      if(this.appConfig.switchboard) {
        XucNotification.enableChatNotification();
      }

      if (BrowserService.browser === 'firefox' && CtiProxy.isUsingWebRtc()) {
        BrowserService.compatibilityAlert();
      }

    });

    XucAgentUser.whenAgentError().then(this.onAgentError.bind(this));

    if(this.appConfig.switchboard) {
      XucPhoneEventListener.addRingingHandler($scope, this.onSwitchboardRinging.bind(this));
      XucPhoneEventListener.addEstablishedHandler($scope, this.onSwitchboardEstablished.bind(this));
      XucPhoneEventListener.addReleasedHandler($scope, this.onSwitchboardRelease.bind(this));
    }

    $scope.$on('linkDisConnected', function () {
      $log.warn('InitController - Received linkDisConnected');
      if (CtiProxy.isUsingWebRtc() && XucPhoneState.getCalls().length > 0) {
        $log.warn('CTI WS closed when CtiProxy is using WebRtc');
        errorModal.showErrorModal('CTI_SOCKET_CLOSED_WHILE_USING_WEBRTC');
      } else {
        if (CtiProxy.isUsingWebRtc()) {
          CtiProxy.stopUsingWebRtc();
        }
        XucLink.logout();
        $state.go('login', { 'error': 'LinkClosed' });
      }
    });

    this.jitsiProxy.subscribeToCloseVideo($scope, () => {
      if ($scope.phoneBarIsVisible() == false) {
        JitsiProxy.togglePhoneBar();
      }
      if (!$rootScope.$$phase) $scope.$apply();
    });

    $scope.phoneBarIsVisible = () => {
      return JitsiProxy.getShowPhoneBar();
    };

    this.showExternalViewButton = externalView.isURLSet();
    CtiProxy.disableMediaKeys();

    if (this.appConfig.switchboard) {
      this.show('search');
      this.focus('search');
    } else {
      this.show('activities');
    }
  }

  showMinibarButton() {
    return !this.jitsiProxy.videoIsOngoing() && this.isElectron() && !this.appConfig.switchboard;
  }

  onAgentError(event) {
    var params = {error: event.Error};
    if (event.Error === "LoggedInOnAnotherPhone") {
      params.phoneNumber = event.phoneNb;
    }
    this.XucLink.logout();
    this.$state.go("login", params);
  }

  onKeyPress(keyCode) {

    var setPhoneClass = () => {
      angular.element('#xuc_search i').removeClass('fa-search');
      angular.element('#xuc_search i').addClass('fa-phone');
    };

    var setSearchClass = () => {
      angular.element('#xuc_search i').removeClass('fa-phone');
      angular.element('#xuc_search i').addClass('fa-search');
    };

    var normalizedValue = this.XucUtils.normalizePhoneNb(this.$scope.searchValue || '');
    (this.XucUtils.isaPhoneNb(normalizedValue)) ? setPhoneClass() : setSearchClass();

    if (keyCode == 13) {
      angular.element('#search').blur();
      this.searchOrDial();
    }
  }
  
  setInputValid() {
    this.tooltipVisible = false;
  }

  setInputInvalid() {
    this.tooltipVisible = true;
  }

  searchOrDial(){
    const input = this.XucUtils.reshapeInput(this.$scope.searchValue);
    if (input.type === 'phone') {
      this.callContext.dialOrAttTrans(input.value);
      this.$scope.searchValue = "";
      this.setInputValid();
    } else {
      let inputValue = input.value;
      if(inputValue != undefined && inputValue.length >= 2) {
        this.XucDirectory.directoryLookup(input.value);
        this.show('search');
        this.setInputValid();
      } else {
        this.setInputInvalid();
      } 
    }
  }

  isActive(contentName) {
    return this.$state.includes('content.'+contentName);
  }

  show(contentName) {
    if (this.electronWrapper.isWindowToggled()){
      this.toggleWindow();
    }
    return this.$state.go('content.'+contentName).then((tResult)=> {
      if(tResult.name === 'content.search') {
        this.focus('search');
      }
    });
  }

  getCallbackStatus() {
    return this.agentCallbackHelper.getStatus();
  }

  getWaitingCalls() {
    let stats = this.XucQueueTotal.getCalcStats();
    return this.$scope.waitingCalls = stats ? stats.sum.WaitingCalls : 0;
  }

  isElectron() {
    return this.electronWrapper.isElectron();
  }

  toggleWindow() {
    this.electronWrapper.toggleWindow(
      this.electronToggledSize,
      this.electronDefaultSize
    );
  }

  showVersion() {
    return this.$window.appVersion;
  }

  emptyInput(){
    this.$scope.searchValue = "";
    this.onKeyPress("");
  }

  onSwitchboardRinging(){
    this.$state.go('content.customer.path');
    this.focus('search');
  }

  onSwitchboardEstablished(){
    this.focus('search');
  }

  onSwitchboardRelease(){
    this.focus('search');
  }
}
