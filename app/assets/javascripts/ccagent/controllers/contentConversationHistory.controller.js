export default class ContentConversationHistoryController {

  constructor($scope, $state) {
    $scope.gotoConversation = (remoteParty) => {
      $state.go("content.conversation", {"remoteParty": remoteParty});
    };
  }
}
