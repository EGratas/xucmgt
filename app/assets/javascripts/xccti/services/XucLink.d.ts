export interface AuthToken { }
export interface KerberosToken extends AuthToken { }
export interface CasToken extends AuthToken { }
export interface OpenIDToken extends AuthToken { }

export type XucToken = string
export type PhoneNumber = string | undefined

export interface XucCredentials extends AuthToken {
  readonly username?:string
  phoneNumber: PhoneNumber
  readonly token: XucToken
}

export interface XucLink {
  login(username: string, password: string, phoneNumber?: string): Promise<any>;
  checkAndloginWithCredentials(credentials: XucCredentials): Promise<any>;
  getStoredCredentials(): XucCredentials | undefined;
  checkCredentials(credentials: XucCredentials): Promise<XucCredentials>;
  checkXucIsUp(): Promise<void>;
  storeCredentials(credentials: XucCredentials): void;
  clearCredentials(): void;
  getSsoCredentials(): Promise<XucCredentials>;
  getCasToken(casServerUrl: string, phoneNumber: string | undefined): Promise<CasToken>;
  tradeCasToken(casServerUrl: string, token: CasToken, phoneNumber: string | undefined): Promise<XucCredentials>;
  getOpenidToken(openIdServerUrl: string, clientId: string, phoneNumber: string | undefined): Promise<OpenIDToken>;
  logoutFromOpenid(openIdServerUrl: string, phoneNumber: string | undefined): void
  tradeOpenidToken(openidToken: OpenIDToken): Promise<XucCredentials>;
  getServerUrl(protocol: string): string;
  setHostAndPort(hostAndPort: string): void;
  setHomeUrl(homeUrl: string): void;
  setRedirectToHomeUrl(redirect: boolean): void;
  initCti(username: string, password: string, phoneNumber: string): void
  logout(): void;
  logoutFromCas(casServerUrl: string): void;
  isLogged(): boolean;
  whenLogged(): Promise<XucCredentials>;
  whenLoggedOut(): Promise<any>;
  getProfileRightAsync(): Promise<any>;
  isRightAllowed(profileRight: any, module: any): boolean;
  getPhoneNumber(): PhoneNumber;
  getXucToken(): XucToken | undefined;
  parseUrlParameters(urlParameter: string): any;
  getXucUser(): XucCredentials | undefined;
  getTokenRenewPromise(): Promise<any>;
  getLoginTimeoutMs(): number;
  setLoginTimeoutMs(timeout: number): void;
}
