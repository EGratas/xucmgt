type UserConfig = {
  fullName: string
  naFwdEnabled: string
  uncFwdEnabled: string
  busyFwdEnabled: string
}

export interface XucUserService {

  onUserConfig(userConfig: UserConfig): void
  getUser(): UserConfig,
  getUserAsync(): Promise<UserConfig>
}
