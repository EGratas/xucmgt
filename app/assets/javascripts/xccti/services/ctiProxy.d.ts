export interface CtiProxy {
    isUsingWebRtc: () => boolean
    dial: (number: string) => void
    hold: (uniqueId?: number) => void
    hangup: (uniqueId?: number) => void
    answer: (uniqueId?: number) => void
    conference: () => void
    completeTransfer: () => void
    toggleMicrophone: (uniqueId?: number) => void
    conferenceMuteMe: (confNumber: number) => void
    conferenceUnmuteMe: (confNumber: number) => void
    conferenceMuteAll: (confNumber: number) => void
    conferenceUnmuteAll: (confNumber: number) => void
    conferenceMute: (confNumber: number, index: number) => void
    conferenceUnmute: (confNumber: number, index: number) => void
    conferenceKick: (confNumber: number, index: number) => void
    conferenceUndeafen: (confNumber: number, index: number) => void
    isConferenceCapable:() => boolean
    getDeviceVendor: () => string,
    isUsingUa: () => boolean,
    isUsingMobileApp: () => boolean,
    isUsingCti: () => boolean,
    unregisterMobileApp: () => void,
    LINECONFIG_PROCESSED: string,
    stopUsingWebRtc: () => void
}