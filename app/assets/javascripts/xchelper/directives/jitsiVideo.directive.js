export default function jitsiVideo() {
  
  return {
    restrict: 'E',
    scope: {},
    templateUrl: 'assets/javascripts/xchelper/directives/jitsiVideo.html',
    link: () => {}
  };
}