import IAngularStatic from "angular"
import { IDirective, IScope, IWindowService } from "angular"
import { XucPhoneState } from "xccti/services/XucPhoneState"
import { ElectronWrapper } from "xchelper/services/ElectronWrapper"
import JitsiProxy from "xchelper/services/jitsiProxy.service"

export default class RefreshWarning implements IDirective {
  
  public restrict: string
  public isolatedScope: {
    eventRegistered: string | boolean
  }
  public webrtcCallOngoing: boolean
  public videoOngoing: boolean

  static $inject = [
    "XucPhoneState", "CtiProxy", "$window", "electronWrapper",
    "JitsiProxy"
  ]
  
  constructor(private XucPhoneState: XucPhoneState,
    private CtiProxy: any,
    private $window: IWindowService,
    private electronWrapper: ElectronWrapper,
    private JitsiProxy: JitsiProxy) {
      this.restrict = 'A'
      this.isolatedScope = {
        'eventRegistered': '='
      }
      this.webrtcCallOngoing = false
      this.videoOngoing = false
  }
  
  link(scope: IScope) {

    this.isolatedScope = IAngularStatic.extend(scope, this.isolatedScope);

    const beforeUnloadHandler = (event: Event) => {
      window.onpagehide = function() { afterUnloadConfirmation() }
      event.preventDefault()
      // @ts-ignore
      event.returnValue = true
    }
    
    const afterUnloadConfirmation = () => {
      if (this.CtiProxy.isUsingWebRtc()) this.CtiProxy.hangup()
      if (this.JitsiProxy.videoIsOngoing()) this.JitsiProxy.dismissVideo()
      if (this.isolatedScope.eventRegistered) {
        this.$window.removeEventListener("beforeunload", beforeUnloadHandler)
        this.electronWrapper.setElectronConfig({confirmQuit: {reset: true}})
        this.isolatedScope.eventRegistered = false
      }
    }
    
    const dialog = {
      type: 'question',
      buttons: ['DIALOG_CANCEL', 'DIALOG_QUIT'],
      defaultId: 1,
      message: 'DIALOG_MSG',
      detail: 'DIALOG_DETAIL'
    }

    const watchEventHandler = (ref: string, isOngoing: boolean): void => {
      switch(ref) {
        case 'XucPhoneState':
          this.webrtcCallOngoing = isOngoing
          break
        case 'JitsiProxy':
          this.videoOngoing = isOngoing
          break
      }

      if (this.webrtcCallOngoing || this.videoOngoing) {
        if (!this.isolatedScope.eventRegistered) {
          window.addEventListener("beforeunload", beforeUnloadHandler)
          this.electronWrapper.setElectronConfig({confirmQuit: dialog})
          this.isolatedScope.eventRegistered = true
        }
      } else {
        this.$window.removeEventListener("beforeunload", beforeUnloadHandler)
        this.electronWrapper.setElectronConfig({confirmQuit: {reset: true}})
        this.isolatedScope.eventRegistered = false
      }
    }
    
    scope.$watch(() => {
      return this.XucPhoneState.getCalls().length
    }, (newValue, oldValue) => {
      if (newValue > 0 && newValue != oldValue) watchEventHandler('XucPhoneState', true)
      else watchEventHandler('XucPhoneState', false)
    })
    
    scope.$watch(() => {
      return this.JitsiProxy.videoIsOngoing()
    }, (newValue, oldValue) => {
      if (newValue !== oldValue && newValue == true) watchEventHandler('JitsiProxy', true)
      else watchEventHandler('JitsiProxy', false)
    })
    
    scope.$on('$destroy', () => {
      afterUnloadConfirmation()
    })
    
  }
  
}