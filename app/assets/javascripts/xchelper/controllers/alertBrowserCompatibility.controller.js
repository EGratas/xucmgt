export default class AlertBrowserCompatibilityController {

  constructor($scope, $uibModalInstance) {

    $scope.cancelNotifModal = () => {
      $uibModalInstance.dismiss('cancel');
    };
  }
}