export default class NotificationModalController {

  constructor($scope, $uibModalInstance) {

    $scope.cancelNotifModal = () => {
      $uibModalInstance.dismiss('cancel');
    };
  }
}