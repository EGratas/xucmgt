import 'angular-translate-loader-partial';
import moment from 'moment';

export default function config($httpProvider, $translateProvider, $translatePartialLoaderProvider, $logProvider, $sceDelegateProvider) {
  $httpProvider.interceptors.push('preventCache');

  $sceDelegateProvider.resourceUrlWhitelist([
    // Allow same origin resource loads.
    'self',
    // Allow JSONP calls that match this pattern
    'http://127.0.0.1:32017/Spokes/DeviceServices/**',
  ]);
  
  if (moment && moment.locale) {
    moment.locale(document.body.getAttribute('data-preferredlang'));
  }

  $logProvider.debugEnabled(true);

  $translatePartialLoaderProvider.addPart('xchelper');
  $translateProvider.useLoader('$translatePartialLoader', {
    urlTemplate: 'assets/i18n/{part}-{lang}.json'
  });
  $translateProvider.registerAvailableLanguageKeys(['en','fr','de'], {
    'en_*': 'en',
    'fr_*': 'fr',
    'de_*': 'de'
  });
  $translateProvider.preferredLanguage(document.body.getAttribute('data-preferredlang'));
  $translateProvider.fallbackLanguage(['fr']);
  $translateProvider.forceAsyncReload(true);
}
