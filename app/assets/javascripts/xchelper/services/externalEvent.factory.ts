import { ILocationService, ILogProvider, IRootScopeService, IWindowService } from "angular"
import angular from 'angular';
import { XucPhoneState } from "xccti/services/XucPhoneState";

class Message {
  constructor(
    public type: string,
    public value: string) {
  }
  toJSON() {
    return {
      type: this.type,
      value: this.value
    };
  }
}

export default class ExternalEvent {

  public THIRD_PARTY_EVENT_TYPE = 'THIRD_PARTY_EVENT'
  public PHONE_EVENT_TYPE = 'PHONE_EVENT'
  public CONFIRM_EVENT_TYPE = 'CONFIRM_QUIT_EVENT'
  public FULLSCREEN_EVENT_TYPE = 'FULLSCREEN_EVENT'
  public JITSI_EVENT_TYPE = 'JITSI_EVENT'
  public ERROR_TYPE = 'ERROR'
  public targetLocation = '//'+(this.$location.host() ? this.$location.host() : 'localhost')
  public thirdPartyCallback: Function | undefined = undefined
  public confirmQuitCallback: Function | undefined = undefined
  public fullscreenCallback: Function | undefined = undefined
  public thirdPartyEvent = {type: this.THIRD_PARTY_EVENT_TYPE, value: {}}
  public unserializableMessage = new Message(this.ERROR_TYPE, 'Unable to serialize data')
  public jitsiCallback: Function | undefined
  static $inject = ['$window', '$location', '$log', 'debounce', '$rootScope', 'externalView', 'XucPhoneState', 'callContext', 'CtiProxy', 'XucUtils']

constructor(private $window: IWindowService,
  private $location: ILocationService,
  private $log: angular.ILogService,
  private debounce: any,
  private $rootScope: IRootScopeService,
  private externalView: any,
  private XucPhoneState: XucPhoneState,
  private callContext: any,
  private CtiProxy: any,
  private XucUtils: any) {
    this.registerEventListener()
  }

  registerEventListener = () => {
    this.$log.info('Register listener for HTML5 API message')
    angular.element(this.$window).on('message', (event) => {
      this.handleEvent(event.originalEvent || event)
    })
  }

  unRegisterEventListener = () => {
    this.$log.info('Unregister listener for HTML5 API message')
    angular.element(this.$window).off('message', this.registerEventListener)
  }

  registerThirdPartyCallback = (cb: Function) => {
    this.thirdPartyCallback = cb
  }

  registerConfirmQuitCallback = (cb: Function) => {
    this.confirmQuitCallback = cb
  }

  registerFullscreenCallback = (cb: Function) => {
    this.fullscreenCallback  = cb
  }

  handleEvent = (event: any) => {
    let data

    if(angular.isUndefined(event) || event === null || event.data === null || event.origin === null) {
      this.$log.error('Cannot handle event received, content or origin is empty')
      return
    }

    if (event.data === "xivo-StartCountDown") {
      return;
    }

    if(event.data === "closeThirdParty") { data = this.thirdPartyEvent }
    else {
      if (this.globalKeyEventChecks(event)) { data = event.data }
      else {
        return
      }
    }

    var message = this.deserializeData(data)

    switch (message.type) {
    case this.PHONE_EVENT_TYPE:
      this.handleEventDebounced(message.value)
      break
    case this.THIRD_PARTY_EVENT_TYPE:
      if (this.thirdPartyCallback) {
        this.thirdPartyCallback()
      }
      else {
        this.$log.error('No callback function is defined for third party event')
      }
      break
    case this.CONFIRM_EVENT_TYPE:
      if (this.confirmQuitCallback) {
        this.confirmQuitCallback()
      }
      else {
        this.$log.error('No callback function is defined for quit confirmation')
      }
      break
    case this.FULLSCREEN_EVENT_TYPE:
      if (this.fullscreenCallback) {
        this.fullscreenCallback()
      }
      else {
        this.$log.error('No callback function is defined for fullscreen')
      }
      break
    case this.JITSI_EVENT_TYPE:
      this.$rootScope.$broadcast(this.JITSI_EVENT_TYPE, message.value);
      break;
    case this.ERROR_TYPE:
      this.$log.error(message.value)
      break
    default:
      if (message.type) this.$log.error('Message type from event is not handled by this service', message.type)
    }
  }

  triggerAction = (data: string) => {
    var result = 'External action triggered: '
    var state = this.XucPhoneState.getState()
    if(data.includes("tel:") || data.includes("callto:")) {
      data = data.replace('tel:', '').replace('callto:', '')
      this.callContext.normalizeDialOrAttTrans(data)
    } else {
      if (this.XucPhoneState.isPhoneOffHook(state)) {
        this.CtiProxy.hangup()
        result += 'Hangup'
      }
      else if (this.XucPhoneState.isPhoneRinging(state)){
        this.CtiProxy.answer()
        result += 'Answer'
      }
      else {
        data = this.XucUtils.normalizePhoneNb(data)
        this.$rootScope.$broadcast("EventShowCallPlaceHolder")
        this.CtiProxy.dial(data)
        result += 'Dial ' + data
      }
    }
    this.$log.info(result)
  }

  globalKeyEventChecks = (event: any) => {
    let eventTargetIsCorrect = this.targetLocation == event.origin.split(':')[1]
    let eventComesFromTheExternalView = this.externalView.getURL() == event.origin.split(':')[1] 
    return (eventTargetIsCorrect || eventComesFromTheExternalView)
  }

  handleEventDebounced = this.debounce(this.triggerAction)

  deserializeData = (data: string) => {
    try {
      let item = angular.fromJson(data)
      return new Message(item.type, item.value)
    }
    catch (exception) {
      var message = this.unserializableMessage
      message.value += data
      return message
    }
  }

}
