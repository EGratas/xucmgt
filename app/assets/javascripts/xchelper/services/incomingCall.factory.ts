import { ILogService, IRootScopeService } from "angular"
import { XucLink } from "xccti/services/XucLink"
import XucVideoEventManager from "xccti/services/XucVideoEventManager.service"
import { ElectronWrapper } from "./ElectronWrapper"

export default class IncomingCall {

  public timeout: number = 15000
  public timer: any = undefined
  public modalInstance: any = undefined  
  public electronToggledSize: {width: number, height: number, minimalist: boolean}
  public electronDefaultSize: {width: number, height: number, minimalist: boolean}

  constructor(
    private $rootScope: IRootScopeService,
    private $uibModal: ng.ui.bootstrap.IModalService,
    private $log: ILogService,
    private XucPhoneEventListener: any,
    private XucVideoEventManager: XucVideoEventManager,
    private remoteConfiguration: any,
    private XucLink: XucLink,
    private webRtcAudio: any,
    private electronWrapper: ElectronWrapper){
    XucLink.whenLogged().then(this.init)
    this.electronToggledSize = {width: this.electronWrapper.ELECTRON_AGENT_MINI_WIDTH, height: this.electronWrapper.ELECTRON_AGENT_MINI_HEIGHT, minimalist: true}
    this.electronDefaultSize = { width: this.electronWrapper.ELECTRON_AGENT_DEFAULT_WIDTH, height: this.electronWrapper.ELECTRON_AGENT_DEFAULT_HEIGHT, minimalist: false}
    this.subscribeForEvents(!this.remoteConfiguration.isAgent())
  }

  init = () => {
    this.$log.info("Starting incomingCall service")
    this.XucLink.whenLoggedOut().then(this.uninit)
  }

  uninit = () => {
    this.$log.info("Unloading incomingCall service")
    this.modalInstance = undefined
    if (this.timer) clearTimeout(this.timer)
    this.webRtcAudio.stopRingtones()
    this.XucLink.whenLogged().then(this.init)
  }

  subscribeForEvents = (withPhoneModal: boolean) => {
    if (withPhoneModal) this.XucPhoneEventListener.addRingingHandler(this.$rootScope, this.onCallRinging)
    this.XucVideoEventManager.subscribeToVideoInviteEvent(this.$rootScope, this.onVideoInvite)
  }

  onModalExpiration = () => {
    if(this.modalInstance) this.modalInstance.close("timeout")
    this.webRtcAudio.stopRingtones()
    this.modalInstance = undefined
  }

  showModal = (backdrop: string, number: string, name: string, callType: string, uniqueId: string, requestId?: string, username?: string, token?: string) => {
    this.modalInstance = this.$uibModal.open({
      templateUrl: 'assets/javascripts/xchelper/services/incomingcall.html',
      size: 'sm',
      backdrop: backdrop,
      controller: 'IncomingCallPopup',
      resolve: {
        backdrop: function() {
          return backdrop
        },
        incomingCallNumber: function() {
          return number
        },
        incomingCallName: function() {
          return name.trim()
        },
        callType: function() {
          return callType
        },
        uniqueId: function() {
          return uniqueId
        },
        requestId: function() {
          return requestId
        },
        username: function() {
          return username
        },
        token: function() {
          return token
        },
      }
    })
    this.modalInstance.result.catch(
      () => {
        if (this.timer) clearTimeout(this.timer)
        this.$log.debug("modal closed")
        this.webRtcAudio.stopRingtones()
        this.modalInstance = undefined
      }
    )    
  }

  onCallRinging = (event: any) => {
    if(this.modalInstance) {
      return
    }
    this.showModal('true', event.otherDN, event.otherDName, event.callType, event.uniqueId)
  }

  onVideoInvite = (event: any) => {
    if(this.modalInstance) {
      this.modalInstance.close("phone modal displayed, dismiss for video invite")
      this.modalInstance = undefined
    }
    this.timer = setTimeout(this.onModalExpiration, this.timeout)
    if(this.electronWrapper.isWindowToggled()) this.electronWrapper.toggleWindow(this.electronToggledSize, this.electronDefaultSize);
    this.showModal('static', '', event.displayName, 'video', '', event.requestId, event.username, event.token)
  }

}
