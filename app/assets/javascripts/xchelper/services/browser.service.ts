import {RichWindow} from "../../RichWindow";


export default class BrowserService {

    private readonly _browser: string = 'unknown';
    public modalInstance: any = undefined;

    constructor(private $window: RichWindow, private $uibModal: ng.ui.bootstrap.IModalService) {

        let userAgent = $window.navigator.userAgent;

        switch (true) {
            case userAgent.toLowerCase().indexOf('edge') > -1:
                this._browser = 'edge';
                break;
            case userAgent.toLowerCase().indexOf('opr') > -1 && !!(<any>window).opr:
                this._browser = 'opera';
                break;
            case userAgent.toLowerCase().indexOf('chrome') > -1 && !!(<any>window).chrome:
                this._browser = 'chrome';
                break;
            case userAgent.toLowerCase().indexOf('trident') > -1:
                this._browser = 'ie';
                break;
            case userAgent.toLowerCase().indexOf('firefox') > -1:
                this._browser = 'firefox';
                break;
            case userAgent.toLowerCase().indexOf('safari') > -1:
                this._browser = 'safari';
                break;
            default:
                this._browser = 'other';
        }
    }

    get browser() {
        return this._browser;
    }

    compatibilityAlert() {
        this.modalInstance = this.$uibModal.open({
            templateUrl: 'assets/javascripts/xchelper/controllers/alertBrowserCompatibility.controller.html',
            size: 'sm',
            controller: 'NotificationModalController',
            resolve: {}
        })
        this.modalInstance.result.catch(
            () => {
                this.modalInstance = undefined
            }
        )
    }
}