import angular, { IHttpResponse } from "angular"
import { IHttpService, IIntervalService, ILogService, IPromise, IQService, IRootScopeService } from "angular"
import { XucLink } from "xccti/services/XucLink"

export default class plantronics {

  static $inject = ["$http", "XucLink", "$log", "$q", "$interval", "$rootScope"]

  constructor(
    private $http: IHttpService,
    private XucLink: XucLink,
    private $log: ILogService,
    private $q: IQService,
    private $interval: IIntervalService,
    private $rootScope: IRootScopeService
  ) {
    this.registerXucLinkLoginHandler()
  }

  private uid: number | undefined = undefined
  private sessId: number | undefined = undefined
  private state: string = "Uninitialized"
  private pollingInterval: IPromise<any> | undefined = undefined
  private canRegisterSessId = true
  private retryTimeout: NodeJS.Timeout | undefined = undefined
  public retryTimeoutSeconds: number = 2
  public retryTimeoutMax: number = 256

  public init = () => {
    this.$log.info("Starting Plantronics headset Service")
    this.tryRegistering()
  }

  public uninit = () => {
    this.$log.info("Unloading Plantronics Service")
    if (this.retryTimeout) clearTimeout(this.retryTimeout)
    this.stopPolling().then( () => {
      this.uid = undefined
      this.retryTimeoutSeconds = 2
      this.state = "Uninitialized"
    })
  }

  public registerXucLinkLoginHandler = () => {
    this.XucLink.whenLogged().then( () => {
      this.init()
      this.registerXucLinkLogoutHandler()
    })
  }

  public registerXucLinkLogoutHandler = () => {
    this.XucLink.whenLoggedOut().then( () => {
      this.uninit()
      this.registerXucLinkLoginHandler()
    })
  }

  public tryRegistering = () => {
    if (this.retryTimeoutSeconds <= this.retryTimeoutMax) {
      return this.registerUid().then( () => {
        this.state = "Paused"
      }).catch(() => {
          this.$log.warn(`Plantronics headset: Service initialization unsuccessfull. Retrying in ${this.retryTimeoutSeconds} seconds.`)
          this.startRetryTimeout()
      })
    } else this.$log.warn(`Plantronics service could not reach headset in time. No more retries.`)
  }

  public startRetryTimeout = () => {
    this.retryTimeout = setTimeout(this.tryRegistering, this.retryTimeoutSeconds * 1000)
    this.retryTimeoutSeconds *= 2
  }

  public registerUid = () => {
    if (angular.isDefined(this.uid)){
      this.$log.debug("register plantronics Uid", this.uid)
      return this.$q.resolve(this.uid)
    }
    let url = "http://127.0.0.1:32017/Spokes/DeviceServices/Info"
    let promise = this.$http.jsonp(url, {jsonpCallbackParam: 'callback'})
    return promise.then( (response: IHttpResponse<any>) => {
      if (response.data.isError == true) {
        return this.$q.reject(response.data.Err.Description)
      }
      this.uid = response.data.Result.Uid
      return this.uid      
    }).catch( (failedRequest) => {
      return this.$q.reject("Failing to register uid : "+JSON.stringify(failedRequest))
    })
  }
  
  public getUid = () => {
    return this.uid
  }

  public registerSessId = () => {
    let url = "http://127.0.0.1:32017/Spokes/DeviceServices/Attach?uid="+this.uid
    if( !(angular.isDefined(this.sessId)) && this.canRegisterSessId ){
      this.$log.debug("register sessId", this.sessId)
      this.canRegisterSessId = false
      let promise = this.$http.jsonp(url, {jsonpCallbackParam: 'callback'})
      return promise.then( (response: IHttpResponse<any>) => {
        if (response.data.isError == true) {
          return this.$q.reject(response.data.Err.Description)
        }
        this.sessId = response.data.Result
        this.canRegisterSessId = true
        return this.sessId
      }).catch( (failedRequest) => {
        this.canRegisterSessId = true
        return this.$q.reject("failing to register sessId : "+JSON.stringify(failedRequest))
      })
    }
    return this.$q.reject("avoiding second sessId registration.")
  }

  public getSessId = () => {
    return this.sessId
  }

  public startPolling = () => {
    if(this.state == "Paused"){
      this.registerSessId().then( () => {
        return this.cleanupEvents()
      }).then( () => {
        this.state = "Polling"
        if(!angular.isDefined(this.pollingInterval)){     
          this.pollingInterval = this.$interval(this.poll, 300)
        }
      }).catch( (errorMessage) => {
        this.$log.warn("Plantronics error while start polling : "+errorMessage)
      })
    }
  }

  public getPollingInterval = () => {
    return this.pollingInterval
  }

  public cleanupEvents = () => {
    let url = "http://127.0.0.1:32017/Spokes/DeviceServices/Events?sess="+this.sessId+"&queue=0"
    return  this.$http.jsonp(url, {jsonpCallbackParam: 'callback'})
  }

  public stopPolling = (): IPromise<any> => {
    if(this.state == "Polling") {
      if (this.pollingInterval) this.$interval.cancel(this.pollingInterval)
      this.pollingInterval = undefined
      let url = "http://127.0.0.1:32017/Spokes/DeviceServices/Release?sess="+this.sessId
      return this.$http.jsonp(url, {jsonpCallbackParam: 'callback'}).finally( () => {        
        this.state = "Paused"
        this.sessId = undefined
        return this.$q.resolve(this.state)
      })
    }
    return this.$q.resolve(this.state)
  }

  public getState = () => {
    return this.state
  }

  public addHeadphoneEventHandler = (callback: Function) => {
    return this.$rootScope.$on('HeadphoneEvent', (event, headphoneEventType) => {
      callback(headphoneEventType)
    })
  }

  public poll = () => {
    this.$log.debug("Plantronics : polling event")
    let url = "http://127.0.0.1:32017/Spokes/DeviceServices/Events?sess="+this.sessId+"&queue=0"
    let promise = this.$http.jsonp(url, {jsonpCallbackParam: 'callback'})
    return promise.then( (response: IHttpResponse<any>) => {
      if(response.data.isError){
        return this.$q.reject(response.data.Err)
      } else {
        return this.keypressExtraction(response.data.Result)
      }
    }).catch( (failedRequest) => {
      this.$log.error("Plantronics : Error while listening Events : "+JSON.stringify(failedRequest))
    })
  }

  public keypressExtraction = (eventChain: Array<{Event_Name: string}>) => {
    for(let plantronicEvent of eventChain){
      switch(plantronicEvent.Event_Name){
      case "Talk":
        this.$rootScope.$emit('HeadphoneEvent', 'Main')
        break
      }
    }
  }
}
