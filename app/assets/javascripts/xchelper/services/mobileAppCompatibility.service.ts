import { ILogService, IRootScopeService } from "angular"
import { RichWindow } from "RichWindow"
import { XucLink } from "xccti/services/XucLink"
import { CtiProxy } from 'xccti/services/ctiProxy'


export default class MobileAppCompatibilityService {

public modalInstance: any = undefined;

static $inject = ["$window", "$rootScope", "XucLink", "$log", "$uibModal", "CtiProxy"]
  
constructor (
  private $window: RichWindow,
  private $rootScope: IRootScopeService,
  private XucLink: XucLink,
  private $log: ILogService,
  private $uibModal : ng.ui.bootstrap.IModalService, 
  private CtiProxy: CtiProxy,
  ) {
    this.XucLink.whenLogged().then(this.init)
  }

  private init = () => {
    this.$log.info("Starting Mobile app security service")
    this.$rootScope.$on(this.CtiProxy.LINECONFIG_PROCESSED, this.onLineConfigChange.bind(this))
    this.XucLink.whenLoggedOut().then(this.unInit)
  }

  private unInit = () => {
    this.$log.info("Unloading mobile app security service")
    this.modalInstance = undefined
    this.XucLink.whenLogged().then(this.init)
  }
  
  onLineConfigChange() {
    if (this.modalInstance == undefined &&
        this.CtiProxy.isUsingMobileApp() &&
        (this.CtiProxy.isUsingUa() || this.CtiProxy.isUsingCti())
    ) {
      this.modalInstance = this.$uibModal.open({
        templateUrl: 'assets/javascripts/xchelper/services/mobileAppCompatibility.html',
        size: 'sm',
        controller: 'NotificationModalController',
        resolve: {}
      })
      this.modalInstance.result.catch(
        () => {
          this.$log.debug("modal mobileApp closed")
          this.modalInstance = undefined
        }
      )
      this.CtiProxy.unregisterMobileApp();
    }
  }
}