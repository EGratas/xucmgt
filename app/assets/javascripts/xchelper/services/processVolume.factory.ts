import angular from "angular"
import { IIntervalService, IRootScopeService, ITimeoutService } from "angular"
import { RichWindow } from "RichWindow"

class VolumeAnalyser {

  public MINIMAL_NOISE_TRESHOLD: number = 8

  constructor(public source: MediaStreamAudioSourceNode, public analyser: AnalyserNode) {}

  connectSourceToAnalyser() {
    this.source.connect(this.analyser)
  }

  disconnectSourceFromAnalyser() {
    this.source.disconnect(this.analyser)
  }

  getVolumeData() {
    let byteFrequencies = new Uint8Array(this.analyser.frequencyBinCount)
    this.analyser.getByteFrequencyData(byteFrequencies)

    let totalVolume = 0
    for (let i = 0; i < byteFrequencies.length; i++) {
      totalVolume += Math.pow(byteFrequencies[i], 2)
    }

    let average = Math.sqrt(totalVolume / byteFrequencies.length)
    
    if (average > this.MINIMAL_NOISE_TRESHOLD) return average
    else return 0
  }
}


class MonitoredCall {

  public destroyLocalStreamhandler: any
  public destroyRemoteStreamhandler: any
  public destroyAudioInterval: any
  public inputVolumeAnalyser: any
  public outputVolumeAnalyser: any

  constructor(public sipCallId: number, public audioContext: AudioContext) {}

  startInputAnalyser() {
    this.inputVolumeAnalyser.connectSourceToAnalyser()
  }
  
  startOutputAnalyser() {
    this.outputVolumeAnalyser.connectSourceToAnalyser()
  }
  
}

export class VolumeData {
  constructor(public sipCallId: number, public volume: number){}
}

export default class ProcessVolume {

  public MICROPHONE_ACTIVITY_TIMEOUT_MS: number = 5000
  public PROCESS_AUDIO_INTERVAL_MS: number = 100
  public EVENT_OUTPUT_VOLUME: string = 'OutputVolumeUpdate'
  public EVENT_INPUT_VOLUME: string = 'InputVolumeUpdate'
  public EVENT_INPUT_ACTIVE: string = 'AudioInputActive'

  public monitoredCalls: Array<any> = []
  public microphoneIsActive: boolean = false
  public microphoneActivityTimeout: any = undefined

  constructor(private $interval: IIntervalService, private $rootScope: IRootScopeService, private $timeout: ITimeoutService, private $window: RichWindow){}

  monitorCall = (sipCallId: number, MonitoredCallClass = MonitoredCall): void => {
    if (this.callIsAlreadyMonitored(sipCallId)) return
    else {
      let monitoredCall = this.createMonitoredCall(sipCallId, MonitoredCallClass)
      this.monitoredCalls.push(monitoredCall)
      monitoredCall.destroyLocalStreamhandler = this.$window.xc_webrtc.setLocalStreamHandler(this.onLocalStream.bind(null, sipCallId), sipCallId)
      monitoredCall.destroyRemoteStreamhandler = this.$window.xc_webrtc.setRemoteStreamHandler(this.onRemoteStream.bind(null, sipCallId), sipCallId)
      monitoredCall.destroyAudioInterval = this.$interval(this.processAudio.bind(null, sipCallId), this.PROCESS_AUDIO_INTERVAL_MS)
    }
  }

  callIsAlreadyMonitored = (sipCallId: number) => {
    if (this.monitoredCalls.length > 0) {
      return this.monitoredCalls
        .find(_ => _.sipCallId == sipCallId)
    } else return false
  }

  createAnalyser = (source: MediaStreamAudioSourceNode, analyser: AnalyserNode) => {
    return new VolumeAnalyser(source, analyser)
  }

  onLocalStream = (sipCallId: number, localStream: MediaStream) => {
    let monitoredCall = this.monitoredCalls.find(_ => _.sipCallId == sipCallId)
    monitoredCall.inputVolumeAnalyser = this.createAnalyser(
      monitoredCall.audioContext.createMediaStreamSource(localStream),
      monitoredCall.audioContext.createAnalyser()
    )
    monitoredCall.startInputAnalyser()
  }

  onRemoteStream = (sipCallId: number, remoteStream: MediaStream) => {
    let monitoredCall = this.monitoredCalls.find(_ => _.sipCallId == sipCallId)
    monitoredCall.outputVolumeAnalyser = this.createAnalyser(
      monitoredCall.audioContext.createMediaStreamSource(remoteStream),
      monitoredCall.audioContext.createAnalyser()
    )
    monitoredCall.startOutputAnalyser()
  }

  createMonitoredCall = (sipCallId: number, MonitoredCallClass: any) => {
    let audioContext = this.$window.xc_webrtc.getAudioContext()
    let monitoredCall = new MonitoredCallClass(sipCallId, audioContext)
    return monitoredCall
  }

  unmonitorCall = (sipCallId: number) => {
    this.monitoredCalls.find((monitoredCall, index) => {
      if (monitoredCall
        && monitoredCall.sipCallId == sipCallId
        && monitoredCall.inputVolumeAnalyser
        && monitoredCall.outputVolumeAnalyser) {

        monitoredCall.inputVolumeAnalyser.disconnectSourceFromAnalyser()
        monitoredCall.outputVolumeAnalyser.disconnectSourceFromAnalyser()

        monitoredCall.destroyLocalStreamhandler()
        monitoredCall.destroyRemoteStreamhandler()
        
        if (angular.isDefined(this.microphoneActivityTimeout)) {
          this.$timeout.cancel(this.microphoneActivityTimeout)
          this.microphoneActivityTimeout = undefined
        }
        if (angular.isDefined(monitoredCall.destroyAudioInterval)) this.$interval.cancel(monitoredCall.destroyAudioInterval)

        this.monitoredCalls.splice(index, 1)
      }
    })
    if (this.monitoredCalls.length == 0) {
      this.$timeout.cancel(this.microphoneActivityTimeout)
    }
  }

  processAudio = (sipCallId: number) => {
    let monitoredCall = this.monitoredCalls.find(m => m.sipCallId == sipCallId)
    if (monitoredCall) {
      if (monitoredCall.outputVolumeAnalyser) {
        this.$rootScope.$broadcast(this.EVENT_OUTPUT_VOLUME, new VolumeData(sipCallId, monitoredCall.outputVolumeAnalyser.getVolumeData()))
      }

      if (monitoredCall.inputVolumeAnalyser) {
        let inputVolume = monitoredCall.inputVolumeAnalyser.getVolumeData()
        this.checkForMicrophoneIssues(inputVolume)
        this.$rootScope.$broadcast(this.EVENT_INPUT_VOLUME, new VolumeData(sipCallId, inputVolume))
      }
    }
  }

  checkForMicrophoneIssues = (inputVolume: number) => {
    if (inputVolume != 0) {
      if (angular.isDefined(this.microphoneActivityTimeout)) {
        this.$timeout.cancel(this.microphoneActivityTimeout)
        this.microphoneActivityTimeout = undefined
      }
      if (!this.microphoneIsActive) this.microphoneIsActive = true
      this.$rootScope.$broadcast(this.EVENT_INPUT_ACTIVE, true)
    }
    else {
      if (this.microphoneActivityTimeout == undefined) this.microphoneActivityTimeout = this.$timeout(this.notifyMicrophoneIssue, this.MICROPHONE_ACTIVITY_TIMEOUT_MS)
    }
  }

  notifyMicrophoneIssue = () => {
    if (this.monitoredCalls.length > 0) {
      this.$rootScope.$broadcast(this.EVENT_INPUT_ACTIVE, false)
      this.microphoneIsActive = false
    }
  }
}
