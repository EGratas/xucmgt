import { ILogService, IRootScopeService } from "angular"
import { RichWindow } from "RichWindow"
import { XucLink } from "xccti/services/XucLink"

type Response = {
  responseType: string
  displayName: string
  requestId: number
  _type: string
}

type Ack = {
  ackType: string
  displayName: string
  requestId: number
  _type: string
}

export class Invitation {
  constructor(
    public requestId: number,
    public displayName: string,
    public timeout: any
    ){}
}

export default class MeetingroomsInviteManager {
  
  NEW_INVITATION_COUNT_EVENT: string = "NewInvitationCountEvent"
  DISPLAY_INVITATION_TOAST_EVENT: string = "DisplayInvitationToastEvent"

  TOAST_UNAVAILABLE_MESSAGE: string = "TOAST_UNAVAILABLE_MESSAGE"
  TOAST_ACCEPTED_MESSAGE: string =  "TOAST_ACCEPTED_MESSAGE"
  TOAST_REJECTED_MESSAGE: string = "TOAST_REJECTED_MESSAGE"
  TOAST_EXPIRATION_MESSAGE: string = "TOAST_EXPIRATION_MESSAGE"

  invitations: Array<Invitation> = []
  inviteSequence: number = 0

  timeout: number = 15000
  
  static $inject = ["$window", "$rootScope", "XucLink", "$log"]
  
  constructor (
    private $window: RichWindow,
    private $rootScope: IRootScopeService,
    private XucLink: XucLink,
    private $log: ILogService
    ) {
      this.XucLink.whenLogged().then(this.init)
      this.$window.Cti?.setHandler(this.$window.Cti.MessageType.MEETINGROOMACK, this.onAck.bind(this))
      this.$window.Cti?.setHandler(this.$window.Cti.MessageType.MEETINGROOMRESPONSE, this.onResponse.bind(this))
    }

    private init = () => {
      this.$log.info("Starting MeetingroomInviteManager service")
      this.XucLink.whenLoggedOut().then(this.unInit)
    }
  
    private unInit = () => {
      this.$log.info("Unloading MeetingroomInviteManager service")
      this.invitations = []
      this.XucLink.whenLogged().then(this.init)
    }
    
    public onAck = (ack: Ack) => {
      switch(ack.ackType) {
        case "NACK":
          this.$rootScope.$broadcast(this.DISPLAY_INVITATION_TOAST_EVENT, this.TOAST_UNAVAILABLE_MESSAGE, ack.displayName)
          this.removeInvitation(ack.requestId)
          break;
      }
    }
    
    public onResponse = (response: Response) => {
      this.removeInvitation(response.requestId)     
      switch(response.responseType) {
        case "Accept":
          this.notifyToaster(this.DISPLAY_INVITATION_TOAST_EVENT, this.TOAST_ACCEPTED_MESSAGE, response.displayName)
          break;
        case "Reject":
          this.notifyToaster(this.DISPLAY_INVITATION_TOAST_EVENT, this.TOAST_REJECTED_MESSAGE, response.displayName)
          break;
      }
    }
    
    public sendInvitation = (username: string, displayName: string, token: string): void => {
      this.inviteSequence += 1
      this.$window.Cti.inviteToMeetingRoom(this.inviteSequence, token, username)
      this.invitations.push(new Invitation(this.inviteSequence, displayName, setTimeout(this.onExpiration.bind(this, this.inviteSequence, displayName), this.timeout)))
      this.$rootScope.$broadcast(this.NEW_INVITATION_COUNT_EVENT, this.invitations.length)
    }

    public onExpiration = (requestId: number, displayName: string) => {
      this.notifyToaster(this.DISPLAY_INVITATION_TOAST_EVENT, this.TOAST_EXPIRATION_MESSAGE, displayName)
      this.removeInvitation(requestId)
    }

    public notifyToaster = (event: string, message: string, displayName: string) => {
      this.$rootScope.$broadcast(event, message, displayName)
    }

    public removeInvitation = (requestId: number) => {
      let timeoutRef = this.invitations
        .find(inv => inv.requestId == requestId)
        ?.timeout

      this.$window.clearTimeout(timeoutRef)
      this.invitations = this.invitations.filter(inv => inv.requestId != requestId)
      this.$rootScope.$broadcast(this.NEW_INVITATION_COUNT_EVENT, this.invitations.length)
    }

    acceptInvitation = (inviteSequence: number, username: string, token: string) : void => {
      this.$window.Cti.meetingRoomInviteAccept(inviteSequence, username)
    }
  
    rejectInvitation = (inviteSequence: number, username: string) : void => {
      this.$window.Cti.meetingRoomInviteReject(inviteSequence, username)
    }    
  }