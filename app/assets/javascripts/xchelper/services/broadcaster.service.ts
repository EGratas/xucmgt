import { IRootScopeService } from "angular"

export default class Broadcaster {
  
  static $inject = ['$rootScope']
  ongoingEvents: Array<string> = []
  timeoutMs: number = 500

  constructor(
    private $rootScope: IRootScopeService
    ) {}
  
  send = (event: string): void => {
    if (!this.ongoingEvents.includes(event)) {
      this.ongoingEvents.push(event)
      this.startTimeout(event)
    }
  }

  startTimeout = (event: string): void => {
    setTimeout(() => {
      this.$rootScope.$broadcast(event)
      this.ongoingEvents.splice(this.ongoingEvents.indexOf(event), 1)
    }, this.timeoutMs)
  }

}
