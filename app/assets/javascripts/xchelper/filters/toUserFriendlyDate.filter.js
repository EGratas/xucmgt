import moment from 'moment';
export default function userFriendlyDate() {
  return function (input) {
    if (!input) return "";
    return moment(input, 'YYYY-MM-DD hh:mm:ss').format('D MMMM HH:mm');
  };
}