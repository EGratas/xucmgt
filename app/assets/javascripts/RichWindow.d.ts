interface JitsiMeetExternalAPI {
  addListener: (event: string, callback: Function) => void
  dispose: () => void
  getDisplayName: (id: string) => string
  executeCommand: (command: string) => void
  isAudioMuted: () => Promise<boolean>
}

export interface RichWindow extends Window {
    externalConfig: {
        hostAndPort: string,
        host: string,
        useSso: boolean,
        casServerUrl: string,
        casLogoutEnable: boolean,
        openidServerUrl: string,
        openidClientId: string,
    }
    JitsiMeetExternalAPI: {
        new(domain: string, options: {}): JitsiMeetExternalAPI
    }
    Cti: any
    xc_webrtc: any
}
