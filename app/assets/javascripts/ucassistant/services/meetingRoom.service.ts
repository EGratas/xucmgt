import { IHttpPromise, IHttpService } from "angular";
import { XucCredentials, XucLink } from "xccti/services/XucLink";

class HTTPAuthorizationHeader {
  constructor(public Authorization: string) {}
}

class HTTPHeaders {
  constructor(
    public timeout: number,
    public headers: HTTPAuthorizationHeader
  ) {}
}

export class MeetingRoom {
  constructor(
    public id: number | undefined,
    public displayName: string,
    public userPin?: string,
    public number?: string,
    public uuid?: string
  ) {}
}

export default class MeetingRoomService {
  
  private timeout = 5000;

  constructor(
    private $http: IHttpService, 
    private XucLink: XucLink
  ) {

  }

  private getToken = (
    func: (headers: HTTPHeaders) => IHttpPromise<MeetingRoom>
  ): IHttpPromise<MeetingRoom> => {
    return this.XucLink.whenLogged().then((user: XucCredentials) => {
      let headers = new HTTPHeaders(
        this.timeout,
        new HTTPAuthorizationHeader(`Bearer ${user.token}`)
      );
      return func(headers);
    });
  };

  private getUrl = (id?: string): string => {
    let param = id ? "/" + id : "";
    return this.XucLink.getServerUrl("http") + "/xuc/api/2.0/config/meetingrooms/personal" + param;
  };

  add = (mr: MeetingRoom) => {
    let apiCall = (headers: HTTPHeaders): IHttpPromise<MeetingRoom> => {
      return this.$http.post(this.getUrl(), mr, headers);
    };

    return this.getToken(apiCall);
  };

  get = (id: string) => {
    let apiCall = (headers: HTTPHeaders): IHttpPromise<MeetingRoom> => {
      return this.$http.get(this.getUrl(id), headers);
    };

    return this.getToken(apiCall);
  };

  update = (mr: MeetingRoom) => {
    let apiCall = (headers: HTTPHeaders): IHttpPromise<MeetingRoom> => {
      return this.$http.put(this.getUrl(), mr, headers);
    };

    return this.getToken(apiCall);
  };

  remove = (id: string) => {
    let apiCall = (headers: HTTPHeaders): IHttpPromise<MeetingRoom> => {
      return this.$http.delete(this.getUrl(id), headers);
    };

    return this.getToken(apiCall);
  };
}
