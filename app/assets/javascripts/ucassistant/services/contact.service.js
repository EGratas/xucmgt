import _ from 'lodash';

export default class ContactService {

  constructor($state, $window, callContext, JitsiProxy, XucPhoneState, CtiProxy, $log) {
    this.callContext = callContext;
    this.JitsiProxy = JitsiProxy;
    this.XucPhoneState = XucPhoneState;
    this.CtiProxy = CtiProxy;
    this.$state = $state;
    this.$window = $window;
    this.$log = $log;
  }

  isMeetingRoom(contact) {
    return this.callContext.isMeetingRoom(contact);
  }

  getPhoneStateBackColor(contact) {
    if (contact.videoStatus === "Busy")
      return "user-videostatus-" + contact.videoStatus;
    return "user-status" + contact.status + (contact.keydown ? " lighter" : "");
  }

  getPhoneStateLabel(status, videoStatus) {
    if (videoStatus === 'Busy')
      return "USER_VIDEOSTATUS_" + videoStatus;
    return "USER_STATUS_" + status;
  }

  navigateToContactEditor(contact) {
    if (this.isMeetingRoom(contact)) {
      return `interface.meetingRooms({id: '${contact.contact_id}'})`;
    }
    return `interface.personalContact({id: '${contact.contact_id}'})`;
  }

  isJitsiAvailable() {
    return this.JitsiProxy.jitsiAvailable;
  }

  hasUsername(contact) {
    if (contact !== undefined) {
      return !_.isEmpty(contact.username);
    }
    return false;
  }

  generateMeetingShareLink(contact) {
    return `${this.$window.location.origin}${contact.url}`;
  }

  canShowChat(contact) {
    return this.hasUsername(contact);
  }

  canShowVideo(contact)  {
    return this.hasUsername(contact);
  }

  inviteToConference(exten, $event)  {
    $event.stopPropagation();
    let conference = this.XucPhoneState.getConference();
    if (conference !== undefined) {
      this.CtiProxy.conferenceInvite(conference.conferenceNumber, exten, 'User', true);
    }
  }

  userWithInviteToConferenceIcon(contact) {
    return this.canInviteToConference() && !contact.toggleDetails && this.isCallable(contact) && !this.isMeetingRoom(contact);
  }

  meetingRoomWithInviteToConferenceIcon(contact) {
    return this.canInviteToConference() && !contact.toggleDetails && this.isMeetingRoom(contact) && this.isJitsiAvailable();
  }

  canInviteToMeetingRoom(contact) {
    if(this.JitsiProxy.videoIsOngoing()) {
      return this.hasUsername(contact);
    }
  }

  canInviteToConference() {
    return this.XucPhoneState.getConferenceOrganizer();
  }

  isCallable(contact) {
    return _.some([1, 2, 3, 5], (index) => {
      return !_.isEmpty(contact.entry[index]);
    }) || this.callContext.isMeetingRoom(contact);
  }


  hasDetail(contact) {
    if (contact.phoneNumbers.length > 0) {
      return true;
    }
    return !_.isNil(contact.email) && contact.email !== '';
  }


  inviteToMeetingRoom(username, displayName, $event) {
    $event.stopPropagation();
    this.JitsiProxy.inviteToMeetingRoom(username, displayName);
  }

  userWithCallIcon(contact) {
    return !this.canInviteToConference()
        && !contact.toggleDetails
        && this.isCallable(contact)
        && !this.isMeetingRoom(contact)
        && !this.hasJustEmail(contact);
  }

  meetingRoomWithCallIcon(contact) {
    return !this.canInviteToConference()
        && !contact.toggleDetails
        && this.isMeetingRoom(contact)
        && this.isJitsiAvailable();
  }

  hasJustEmail(contact) {
    return contact.phoneNumbers.length === 0
        && !_.isNil(contact.email)
        && contact.email !== ''
        && !contact.toggleDetails;
  }

  startPointToPointVideoCall(username, displayName, $event) {
    $event.stopPropagation();
    this.JitsiProxy.startPointToPointVideoCall(username, displayName);
  }


  dial(number)  {
    this.callContext.normalizeDialOrAttTrans(number);
  }

  makeACall(contact, $event, phone = undefined) {
    $event.stopPropagation();
    if (this.isMeetingRoom(contact)) {
      return this.dial('**' + contact.phoneNumbers[0]);
    }
    return this.dial(phone || contact.phoneNumbers[0]);
  }

  makeAVideoCall(contact, $event) {
    $event.stopPropagation();
    if (this.isMeetingRoom(contact)) {
      return this.join(contact);
    }
    return this.startPointToPointVideoCall(contact.username, contact.displayName, $event);
  }

  _isPersonal(meetingRoomContact)  {
    return meetingRoomContact.personal ? 'personal' : 'static';
  }

  join(contact, $event) {
    $event.stopPropagation();
    if(this.isMeetingRoom(contact)) {
      this.JitsiProxy.startVideo(contact.contact_id, this._isPersonal(contact)).then(
        this.XucPhoneState.getCallsNotOnHold().forEach(call => {
          this.CtiProxy.hold(call.uniqueId);
        })
      ).catch(
        e => {
          this.$log.error("Cannot start meeting room", e);
        }
      );
    }
  }

}