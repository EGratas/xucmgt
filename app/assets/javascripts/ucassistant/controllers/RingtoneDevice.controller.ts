import angular from "angular"

type Device = {
  deviceId: string
  groupId: string
  kind: string
  label: string
}

export default class RingtoneDeviceController {
  static $inject = [
    "mediaDevices", "xcHelperPreferences", "webRtcAudio"
  ]
  devices: Array<Device> = []
  ringingDeviceId: string
  
  constructor(private mediaDevices: any, private xcHelperPreferences: any, private webRtcAudio: any) {
    this.ringingDeviceId = this.xcHelperPreferences.getRingingDeviceId()
    this.init(this.ringingDeviceId)
  }

  async init(ringingDeviceId: string) {
    this.devices = await this.mediaDevices.getAudioOutput()
    if (!this.containsDeviceId(this.devices, ringingDeviceId)) this.changeRingingDevice("default")
  }
  
  changeRingingDevice(id: any) {
    this.webRtcAudio.changeRingingDevice(id).then(() => {
    }, () => {
      angular.noop()
    })
    this.ringingDeviceId = id
    this.xcHelperPreferences.setRingingDeviceId(id)
  }
  
  containsDeviceId(devices: Array<Device>, deviceId: string) {
    return devices.find(device => device.deviceId == deviceId)
  }
  
}