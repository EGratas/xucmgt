import _ from "lodash";

export default class ContactSheetController {

  constructor($scope,contact,ContactService, $uibModalInstance, $state,applicationConfiguration, toast, $translate, XucLink) {
    console.log(contact);
    $scope['appConfig'] = applicationConfiguration.getCurrentAppConfig();
    $scope['contact'] = contact;


    $scope.contactSheetisMeetingRoom = (contact) => {
      console.log('meeting ' + ContactService.isMeetingRoom(contact));
      return ContactService.isMeetingRoom(contact);
    };

    $scope.contactSheethasDetail = (contact) => {
      console.log('detail ' + ContactService.hasDetail(contact));
      return ContactService.hasDetail(contact);
    };


    $scope.contactSheetaddFavorite = (contact, $event) => {
      $event.stopPropagation();
      Cti.addFavorite(contact.contact_id, contact.source);
      contact.favorite = true;
    };

    $scope.contactSheetremoveFavorite = (contact, $event) => {
      $event.stopPropagation();
      Cti.removeFavorite(contact.contact_id, contact.source);
      contact.favorite = false;
    };

    $scope.contactSheetgetPhoneStateBackColor = (contact) => ContactService.getPhoneStateBackColor(contact);

    $scope.contactSheetgetPhoneStateLabel = (status, videoStatus) => ContactService.getPhoneStateLabel(status, videoStatus);

    $scope.contactSheetonClose = () => $uibModalInstance.dismiss('done');

    $scope.navigateToContactEditor = (contact) => ContactService.navigateToContactEditor(contact);

    $scope.contactSheetisJitsiAvailable = () => ContactService.isJitsiAvailable();

    $scope.contactSheetgenerateMeetingShareLink = (contact) => ContactService.generateMeetingShareLink(contact);

    $scope.contactSheetgoToChat = (username, $event) => {
      $event.stopPropagation();
      $state.go($scope.appConfig.routing + '.conversation', {remoteParty: username});
      $scope.contactSheetonClose();
    };
    $scope.contactSheetcanShowChat = (contact) => ContactService.canShowChat(contact);

    $scope.contactSheetcanShowVideo = (contact) => ContactService.canShowVideo(contact);

    $scope.contactSheetinviteToConference = (exten, $event) => ContactService.inviteToConference(exten, $event);

    $scope.contactSheetuserWithInviteToConferenceIcon = (contact) => ContactService.userWithInviteToConferenceIcon(contact);

    $scope.contactSheetmeetingRoomWithInviteToConferenceIcon = (contact) => ContactService.meetingRoomWithInviteToConferenceIcon(contact);

    $scope.contactSheetcanInviteToMeetingRoom = (contact) => ContactService.canInviteToMeetingRoom(contact);

    $scope.contactSheetinviteToMeetingRoom = (username, displayName, $event) => ContactService.inviteToMeetingRoom(username, displayName, $event);

    $scope.contactSheetuserWithCallIcon = (contact) => ContactService.userWithCallIcon(contact);

    $scope.contactSheetmeetingRoomWithCallIcon = (contact) => ContactService.meetingRoomWithCallIcon(contact);

    const getTranslationType = (type) => {
      switch (type) {
      case "meetingroom":
        return "MEETINGROOM_COPIED";
      case "phone":
        return "PHONE_COPIED";
      case "email":
        return "EMAIL_COPIED";
      }
    };

    $scope.contactSheetclipboardPopup = (kind, $event) => {
      $event.stopPropagation();
      toast({
        duration: 3000,
        message: $translate.instant(getTranslationType(kind)),
        className: 'alert-copied',
        position: "center",
        container: '.toast-container'
      });
    };



    $scope.contactSheethasJustEmail = (contact) => {
      return !_.isNil(contact.email) && contact.email !== '';
    };

    $scope.contactSheetstartPointToPointVideoCall = (username, displayName, $event) => ContactService.startPointToPointVideoCall(username, displayName, $event);

    $scope.contactSheetmakeACall = (contact, $event, phoneNumber = undefined) => {
      ContactService.makeACall(contact, $event, phoneNumber);
      $scope.contactSheetonClose();
    };

    $scope.contactSheetmakeACall2 = (phoneNumber , $event) => {
      ContactService.makeACall($scope.contact, $event, phoneNumber);
      $scope.contactSheetonClose();
    };

    XucLink.whenLoggedOut().then($scope.contactSheetonClose());
  }
}