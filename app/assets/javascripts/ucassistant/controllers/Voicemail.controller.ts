import { IRootScopeService } from "angular"
import { CtiProxy } from "xccti/services/ctiProxy"
import { XucPhoneState } from "xccti/services/XucPhoneState"

export default class VoicemailController {

  constructor($scope: any, XucVoiceMail: any, CtiProxy: CtiProxy, XucPhoneState: XucPhoneState, $rootScope: IRootScopeService) {
    $scope.voiceMail = XucVoiceMail.getVoiceMail()

    $scope.$on('voicemailUpdated', function() {
      $scope.voiceMail = XucVoiceMail.getVoiceMail()
      if(!$rootScope.$$phase) $scope.$apply()
    })

    $scope.callMailBox = function() {
      if (XucPhoneState.getCalls().length > 0) CtiProxy.hold()
      $rootScope.$broadcast("EventShowCallPlaceHolder")
      CtiProxy.dial('*98')
    }
  }
}
