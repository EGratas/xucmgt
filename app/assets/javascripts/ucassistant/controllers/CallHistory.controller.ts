import {IOnInit, IScope} from 'angular'
import {XucPhoneState} from "../../xccti/services/XucPhoneState";
import {CtiProxy} from 'xccti/services/ctiProxy'
import moment from "moment";
import {StateService} from "@uirouter/core";
import {CallHistoryGroupViewModel, CallHistoryModel, CallHistoryViewModel, CallStatus, VideoEvents} from "../models/CallHistory.model";
import {CallHistoryService} from "../services/CallHistory.service";
import {sortBy} from "lodash";
import UserPreferenceService from 'xchelper/services/userPreference.service';

export class CallHistoryController {

    history?: CallHistoryGroupViewModel = undefined;
    historyIndexes: string[] = [];

    static $inject = ["callHistoryPartyFormatter", "$scope","$rootScope", "XucUtils", "$state", "XucPhoneState", "CtiProxy", "XucCallHistory", "$translate", "CallHistoryService", "callContext", "UserPreferenceService"]

    constructor(
        private callHistoryPartyFormatter: any,
        private $scope: IScope,
        private $rootScope: IScope,
        private XucUtils: any,
        private $state: StateService,
        private XucPhoneState: XucPhoneState,
        private CtiProxy: CtiProxy,
        private XucCallHistory: any,
        private $translate: any,
        private CallHistoryService: CallHistoryService,
        private callContext: any,
        private userPreferenceService: UserPreferenceService
    ) {
        this.XucCallHistory.updateUserCallHistory();
        $scope.$on('CallHistoryUpdated', () => {
            this.history = CallHistoryService.MapCallHistoryModelToCallHistoryViewModel(XucCallHistory.getRawCallHistory());
            this.historyIndexes = Object.keys(this.history) as (keyof typeof history)[];
            this.historyIndexes.sort((one, two) => (one > two ? -1 : 1));
            if (!$rootScope.$$phase) $scope.$apply();
        });
    }



    getPhoneStateBackColor(call: CallHistoryViewModel) {
        if (call.videoStatus == VideoEvents.Busy) return "user-videostatus-" + call.videoStatus;
        return "user-status" + call.phoneStatus;
    }

    statusImage(status: CallStatus) {
        const baseUrl = '/assets/images/ucassistant/call_statut_';
        if (status == CallStatus.Emitted) return baseUrl + 'outgoing.svg';
        else if (status == CallStatus.Answered) return baseUrl + 'incoming.svg';
        else if (status == CallStatus.Missed) return baseUrl + 'missed.svg';
        return '';
    }


    stopEventAndDial(event: any, number: number) {
        event.stopPropagation();
        this.callContext.normalizeDialOrAttTrans(number);
    }

    stopEventAndCreateContact(event: any, number: any, firstname: any, lastname: any) {
        event.stopPropagation();
        let contact = {
            "firstname": firstname || '',
            "lastname": lastname || '',
            "number": number || ''
        };
        this.$state.go('interface.personalContact', {"contact": contact});
    }


    formatHistoryDay(date: Date) {
        return moment(date).calendar(null, {
            sameDay: '[' + this.$translate.instant('CALENDAR_SAME_DAY') + ']',
            lastDay: '[' + this.$translate.instant('CALENDAR_LAST_DAY') + ']',
            lastWeek: 'dddd [' + this.$translate.instant('CALENDAR_LAST_WEEK') + ']',
            sameElse: 'DD/MM/YYYY'
        });
    };

    toggleChildrens(item: any) {
        item.display = !item.display;
        this.userPreferenceService.resetMissedCalls();
    };

    getCallHistorySubIndexes(index:string) {
        if (this.history) {
            let indexes = Object.keys(this.history[index]);
            (indexes as string[]).sort((one, two) => {
                if (this.history ) {
                    let temp1 = this.history[index][one as any][0] as CallHistoryViewModel;
                    let temp2 = this.history[index][two as any][0] as CallHistoryViewModel;
                    if (temp1.day && temp2.day) {
                        return (temp1.day > temp2.day ? -1 : 1)
                    }
                }
                return -1;
            });
            return indexes;
        }
    }

    getCallHistoryUserStatusClass(call: CallHistoryModel): String | void  {
      if(call.initials == "") return "externalNumber"
    }

}