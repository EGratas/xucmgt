import _ from 'lodash';

(function(){
  'use strict';

  angular
    .module('ccManager')
    .factory('AgentViewColBuilder',AgentViewColBuilder);

  AgentViewColBuilder.$inject= ['ViewColBuilder','XucAgent', 'AgentStates', 'XucGroup', '$q', '$compile', '$translate'];

  function AgentViewColBuilder(viewColBuilder, xucAgent, agentStates, XucGroup, $q, $compile, $translate) {
    const _deferredGroupData = $q.defer();

    const _getGroupData = function () {
      return _deferredGroupData.promise;
    };

    const _resolveGroupData = function () {
      const groups = _.map(XucGroup.getGroups(), function (g) {
        return {id: g.name, label: g.name};
      });
      _deferredGroupData.resolve(groups);
    };

    const _getMatchDefaultMembershipData = function () {
      return [
        {id: 'NO_DEFAULT_MEMBERSHIP', label: $translate.instant('NO_DEFAULT_MEMBERSHIP')},
        {id: 'MATCH_DEFAULT_MEMBERSHIP', label: $translate.instant('MATCH_DEFAULT_MEMBERSHIP')},
        {id: 'DOES_NOT_MATCH_DEFAULT_MEMBERSHIP', label: $translate.instant('DOES_NOT_MATCH_DEFAULT_MEMBERSHIP')}
      ];
    };

    const _multiSelectTemplate = "assets/javascripts/ccmanager/services/dropdownMultiSelect.html";
    const _buildFirstCols = function () {
      return [
        {id: 'emptyCol', show: true, showTotal: false},
        {
          id: 'selectCol',
          type: 'check',
          field: 'selected',
          show: true,
          showTotal: true,
          filter: {name: 'assets/javascripts/ccmanager/services/selectAgent.html'},
          cellClass: 'AgentSelectBox'
        },
        {
          id: 'editCol',
          type: 'edit',
          cellClass: "actionIcon text-center",
          show: true,
          showTotal: true,
          filter: {name: 'assets/javascripts/ccmanager/services/editAgent.html'}
        }
      ];
    };

    const _buildCols = function () {
      viewColBuilder.setPrefix('COL_AGENT');
      return [
        viewColBuilder.col('matchDefaultMembership').withDisplayFilter('translate').withFilter(_multiSelectTemplate).withFilterData(_getMatchDefaultMembershipData).withCellClass("colDefaultMembership").build(),
        viewColBuilder.col('number').withFieldClass('badge').withFieldDynClass('state').build(),
        viewColBuilder.col('groupName').withFilter(_multiSelectTemplate).withFilterData(_getGroupData).hide().build(),
        viewColBuilder.col('firstName').build(),
        viewColBuilder.col('lastName').build(),
        viewColBuilder.col('stats.LoginDateTime').withDisplayFilter('dateTime').withNoFilter().build(),
        viewColBuilder.col('stats.LogoutDateTime').withDisplayFilter('dateTime').withNoFilter().build(),
        {id: 'actionCol', type: 'action', cellClass: "actionIcon", show: true, showTotal: true},
        viewColBuilder.col('stateName').withCellClass('T{{agent.state}}').withFilter(_multiSelectTemplate).withDisplayFilter('translate').withFieldDynClass('state').withFilterData(agentStates.buildStates().promise).withFieldClass('state').build(),
        viewColBuilder.col('timeInState').withNoFilter().withDisplayFilter('timeInState').build(),
        viewColBuilder.col('phoneNb').build(),
        viewColBuilder.col('stats.PausedTime').withDisplayFilter('totalTime').withNoFilter().build(),
        ...xucAgent.getNotReadyStatuses().map((status) => viewColBuilder.col('stats.' + status.name).withDisplayFilter('totalTime').withNoFilter().withTitle(status.longName).build()),
        viewColBuilder.col('stats.WrapupTime').withDisplayFilter('totalTime').withNoFilter().build(),
        viewColBuilder.col('stats.InbCalls').withNoFilter().hide().build(),
        viewColBuilder.col('stats.InbAcdCalls').withNoFilter().build(),
        viewColBuilder.col('stats.InbAnsCalls').withNoFilter().hide().build(),
        viewColBuilder.col('stats.InbAnsAcdCalls').withNoFilter().build(),
        viewColBuilder.col('stats.InbAverCallTime').withDisplayFilter('totalTime').withNoFilter().hide().build(),
        viewColBuilder.col('stats.InbAverAcdCallTime').withDisplayFilter('totalTime').withNoFilter().build(),
        viewColBuilder.col('stats.InbCallTime').withDisplayFilter('totalTime').withNoFilter().hide().build(),
        viewColBuilder.col('stats.InbAcdCallTime').withDisplayFilter('totalTime').withNoFilter().build(),
        viewColBuilder.col('stats.InbUnansCalls').withNoFilter().hide().build(),
        viewColBuilder.col('stats.InbUnansAcdCalls').withNoFilter().build(),
        viewColBuilder.col('stats.InbPercUnansCalls').withNoFilter().withDisplayFilter('number:1').hide().build(),
        viewColBuilder.col('stats.InbPercUnansAcdCalls').withNoFilter().withDisplayFilter('number:1').build(),
        viewColBuilder.col('stats.OutCalls').withNoFilter().build(),
        viewColBuilder.col('stats.OutCallTime').withDisplayFilter('totalTime').withNoFilter().build()
      ];
    };

    const _buildTable = function ($scope) {
      if (XucGroup.getGroups().length > 0) {
        _resolveGroupData();
      } else {
        $scope.$on("GroupsLoaded", _resolveGroupData);
      }
      const actionItem = function (canFn, actionFn, icon, label) {
        return '<li ng-show="' + canFn + '(agent.id)" ng-click="' + actionFn + '(agent.id)" role="presentation" class="AgentMenu">' +
            '<i class="glyphicon glyphicon-' + icon + ' glyphmenu"></i>{{"' + label + '" | translate}}</li>';
      };

      const colAction = `<div ng-if="col.type==='action'" ng-controller="agentActionController" uib-dropdown class="agent-dropdown dropdown dropdown-menu-right">
          <a uib-dropdown-toggle href class="pull-right" id="dLabel">
            <i class="glyphicon glyphicon-wrench"></i>
          </a>
          <ul class="dropdown-menu" uib-dropdown-menu aria-labelledby="dLabel">
          ${actionItem("canLogIn", "login", "log-in", "ACTION_LOGIN") +
      actionItem("canLogOut", "logout", "log-out", "ACTION_LOGOUT") +
      actionItem("canPause", "pause", "pause", "ACTION_PAUSE") +
      actionItem("canUnPause", "unpause", "play", "ACTION_UNPAUSE") +
      actionItem("canListen", "listen", "bullhorn", "ACTION_LISTEN") +
      actionItem("canBeCalled", "callAgent", "earphone", "ACTION_CALL")}
            </ul>
          </div>`;


      const colEdit = `<span ng-if="col.type==='edit'" ng-controller="agentEditController" class="glyphicon glyphicon-pencil" ng-click="editAgent(agent.id)"></span>`;
      const colField = `<span ng-if="col.type==='field'" class="{{col.fieldClass}} T{{agent[col.fieldDynClass]}}" ng-bind="agent[col.field] | editableFilter:col.displayFilter"></span>`;
      const colStat = `<span ng-if="col.type==='stat'" class="{{col.fieldClass}} T{{agent[col.fieldDynClass]}}" ng-bind="agent.stats[col.field]  | editableFilter:col.displayFilter"></span>`;
      const colCheck = `<span ng-if="col.type==='check'" class="agent-check" ><input type="checkbox" ng-model="agent[col.field]"></span>`;
      const agentField = colCheck + colEdit + colField + colStat + colAction;

      let agentViewTableWithGroup = `<table ng-table-dynamic="agentDynTable with cols" cols="cols" show-filter="true" class="table table table-condensed flex-layout">
              <tbody ng-repeat="group in $groups | orderBy:'value'" ng-init="$groupRow.show = false">
                  <tr class="ng-table-group agent-table-group-data">
                      <td>
                          <a href="" ng-click="group.$hideRows = !group.$hideRows">
                              <span class="glyphicon" ng-class="{ 'glyphicon-chevron-right': group.$hideRows, 'glyphicon-chevron-down': !group.$hideRows }"></span>
                              <strong> {{ group.value }} ({{group.data.length}})</strong>
                          </a>
                      </td>
                      <td ng-if="(col.showTotal && col.show)" ng-repeat="col in cols">{{totals[group.value][col.field] | editableFilter:col.displayFilter}}</td>
                  </tr>
                  <tr ng-if="!group.$hideRows" ng-repeat="agent in group.data">
                      <td class="{{col.cellClass}} T{{agent[col.fieldClass]}}" data-header-class="tableTitle" ng-repeat="col in $columns">
                         ${agentField}
                      </td>
                  </tr>
              </tbody>
          </table>`;


      const agentViewTable = `<table ng-table-dynamic="agentDynTable with cols" show-filter="true" class="table-agent-view table table-condensed table-container" fixed-header>
                              <tr ng-repeat="agent in $data">
                                   <td class="{{col.cellClass}} T{{agent[col.fieldClass]}}" data-header-class="" ng-repeat="col in $columns">
                                      ${agentField}
                                   </td>
                              </tr>
                            </table>`;

      let newDirective = angular.element($scope.showGroup ? agentViewTableWithGroup : agentViewTable);
      let element = angular.element(document.querySelector('#agtView'));
      element.empty();
      element.append(newDirective);
      $compile(newDirective)($scope);
    };
    return {
      buildCols: _buildCols,
      buildFirstCols: _buildFirstCols,
      buildTable: _buildTable
    };
  }
})();
