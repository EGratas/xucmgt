import 'jquery';
import 'jquery-ui';
import 'angular';
import 'angular-cookies';
import 'angular-local-storage';
import 'angular-translate';
import 'angular-translate-loader-partial';
import 'angular-ui-bootstrap';
import 'angular-ui-slider';
import 'angular-ui-indeterminate';
import '@uirouter/angularjs';
import 'ng-table/bundles/ng-table';
import 'angularjs-dropdown-multiselect';
import 'isteven-angular-multiselect/isteven-multi-select';
import 'd3';
import 'angular-material';
import 'angular-material/angular-material.min.css';

/* Old libraries */
import 'xccti/cti-webpack';
import 'xchelper/helper.module';
import 'xclogin/login-webpack';

import run from './app.run';
import config from './app.config';

/* Old modules to migrate */
import './app.module';
import './controllers/addAgentGroupCtrl.controller';
import './controllers/agentActionController.controller';
import './controllers/agentEditController.controller';
import './controllers/agentsCreateDefaultConfiguration.controller';
import './controllers/agentSelectController.controller';
import './controllers/agentsUpdateController.controller';
import './controllers/ccAgentDynViewController.controller';
import './controllers/ccAgentDynViewPrefController.controller';
import './controllers/ccAgentsController.controller';
import './controllers/ccAgentViewController.controller';
import './controllers/ccCallbackController.controller';
import './controllers/ccChooseAgentCtrl.controller';
import './controllers/ccQueuesController.controller';
import './controllers/ccViewColPrefController.controller';
import './controllers/ccViewController.controller';
import './controllers/ctiLinkController.controller';
import './controllers/groupGroupsController.controller';
import './controllers/loginController.controller';
import './controllers/mainController.controller';
import './controllers/queueSelectCtrl.controller';
import './controllers/queueSliderController.controller';
import './controllers/virtualGroupController.controller';
import './controllers/qualificationResultController.controller';
import './directives/callbackListView.directive';
import './directives/d3Agent.directive';
import './directives/d3EmptyGroup.directive';
import './directives/d3Group.directive';
import './directives/d3Queue.directive';
import './directives/draggableAgent.directive';
import './directives/draggableGroup.directive';
import './directives/droppableAgent.directive';
import './directives/droppableAgentTrash.directive';
import './directives/droppableGroup.directive';
import './directives/droppableGroupTrash.directive';
import './directives/menuToggle.directive';
import './directives/queuefield.directive';
import './directives/multiSelectFilter.directive';
import './filters/agentFilter.filter';
import './filters/editableFilter.filter';
import './services/AgentViewColBuilder.factory';
import './services/CCMUtils.factory';
import './services/Preferences.factory';
import './services/Thresholds.factory';
import './services/ViewColBuilder.factory';
import './services/MetricsUtils.factory';

import ccChangeOptionController from './controllers/ccChangeOptionController.controller';
import ccQueuesController from './controllers/ccQueuesController.controller';
import ccQueueDynViewController from './controllers/ccQueueDynViewController.controller';
import ccViewAgentGroupController from './controllers/ccViewAgentGroupController.controller';
import ccViewGlobalController from './controllers/ccViewGlobalController.controller';
import queueDetailModalController from './controllers/queueDetailModalController.controller';
import optionSwitch from './directives/optionSwitch.directive';
import preferences from './services/Preferences.factory';

angular.module('ccManager')
  .config(config)
  .controller('ccChangeOptionController', ccChangeOptionController)
  .controller('ccQueuesController', ccQueuesController)
  .controller('ccQueueDynViewController', ccQueueDynViewController)
  .controller('ccViewAgentGroupController', ccViewAgentGroupController)
  .controller('ccViewGlobalController', ccViewGlobalController)
  .controller('queueDetailModalController', queueDetailModalController)
  .directive('optionSwitch', optionSwitch)
  .factory('Preferences', preferences)
  .run(run);