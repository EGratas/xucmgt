import _ from 'lodash';

(function(){
  'use strict';

  angular
    .module('ccManager')
    .controller('ccAgentDynViewController',ccAgentDynViewController);

  ccAgentDynViewController.$inject= ['$scope', 'NgTableParams','$filter','XucAgent','Preferences','AgentViewColBuilder','XucGroup','XucAgentTotal','CCMUtils', '$q', '$log'];

  function ccAgentDynViewController($scope, NgTableParams, $filter, xucAgent, preferences, agentViewColBuilder, xucGroup, xucAgentTotal, ccmUtils, $q, $log) {
    const colPrefKey = 'agentViewColumns';
    $scope.showGroup = preferences.getOption("AgentView.showGroup",{showGroup:false}).showGroup;

    $scope.agents = [];
    $scope.groups = [];
    $scope.totals = [];
    $scope.totals['default'] = {AgentPausedTotalTime:1000};
    $scope.cols = [];

    var availCols = [];
    var firstCols = agentViewColBuilder.buildFirstCols();

    var buildViewCols = function() {
      if (availCols.length === 0) {
        availCols = agentViewColBuilder.buildCols();
        $scope.cols = preferences.getViewColumns(availCols, colPrefKey, firstCols);
        agentViewColBuilder.buildTable($scope);
      }
    };

    let orderByGroupName = function(agent) {
      return agent.groupName;
    };

    $scope.tableSettings = function() {
      var calculatePage = function(params) {
        if (params.total() <= (params.page() - 1)  * params.count()) {
          var setPage = (params.page()-1) > 0 && (params.page()-1) || 1;
          params.page(setPage);
        }
      };
      return {
        filterDelay: 0,

        groupBy: $scope.showGroup ? function(item) {
          return item.groupName;
        } : undefined,
        total: $scope.agents.length, // length of data
        counts:[],
        getData: function(params) {
          $log.debug('getData');
          var getDataByGroup = function(data) {
            var orderedData = params.sorting() ?
              $filter('orderBy')(data, [orderByGroupName, params.orderBy()[0]]) : $filter('orderBy')(data, $scope.orderByGroupName);
            return (params.sorting() ? $filter('orderBy')(orderedData, params.orderBy()) : orderedData);
          };
          var getDataByList = function(data) {
            var orderedData = params.sorting() ?
              $filter('orderBy')(data, params.orderBy()) : data;
            return(orderedData.slice((params.page() - 1) * params.count(), params.page() * params.count()));
          };
          calculatePage(params);
          var data = $scope.agents;
          var filteredData = params.filter() ?
            $filter('agentFilter')(data, params.filter()) : data;
          params.total(filteredData.length);
          if($scope.showGroup) {
            $scope.displayedData = getDataByGroup(filteredData);
          }
          else {
            $scope.displayedData = getDataByList(filteredData);
          }
          $scope.displayedData = _($scope.displayedData).map(function(ag) { ag.selected=false; return ag; }).value();

          return $scope.displayedData;
        }
      };
    };

    $scope.agentDynTable = new NgTableParams({
      page: 1,
      count: $scope.showGroup ? 40: 40,
      group: $scope.showGroup ? "groupName" : "",
      sorting: {
        firstName: 'asc'
      }
    }, $scope.tableSettings()
    );

    var dfReloadTable = ccmUtils.doDeffered(function() {
      $scope.agentDynTable.reload();
    });

    var dfReloadAll = ccmUtils.doDeffered(function() {
      var qIds = preferences.getSelectedQueues();

      $q.all( _.map(xucAgent.getAgentsInQueueByIds(qIds), function (a) {
        var promise = xucGroup.getGroupAsync(a.groupId).then((group) => {a.groupName = group.name; return( a);});
        return promise;
      })).then((agents)=> {
        $scope.agents = agents;
        $scope.totals = xucAgentTotal.calculate($scope.agents);
        $scope.agentDynTable.reload();
      });

    });

    var dfCalculateTotals = ccmUtils.doDeffered(function(){
      $scope.totals = xucAgentTotal.calculate($scope.agents);
    });
    
    var isSortedByStat = function() {
      for(var key in $scope.agentDynTable.sorting()) {
        if(key.split("stats.").length > 1) return true;
      }
      return false;
    };

    $scope.$on('AgentStatisticsUpdated', function(){
      if (isSortedByStat()) {
        dfReloadTable();
      }
      dfCalculateTotals();
    });

    $scope.$on('AgentStateUpdated', function(){
      if ($scope.agentDynTable.isSortBy('stateName') ||
      (!angular.isUndefined($scope.agentDynTable.filter().stateName) && $scope.agentDynTable.filter().stateName.length > 0)) {
        dfReloadTable();
      }
    });

    $scope.$on('AgentsLoaded', function(){
      dfReloadAll();
    });

    $scope.$on('AgentConfigUpdated', function(){
      dfReloadAll();
    });

    $scope.$on('QueueMemberUpdated', function() {
      dfReloadAll();
    });

    $scope.$on('DefaultMembershipUpdated', function() {
      dfReloadAll();
    });

    $scope.$on('preferences.queueSelected', function() {
      dfReloadAll();
    });

    $scope.$on('preferences.'+colPrefKey, function() {
      $scope.cols = preferences.getViewColumns(availCols, colPrefKey, firstCols);
    });
    $scope.$on('moveTableCol', function(event,args){
      preferences.moveCol(args.colRef, args.beforeColRef, $scope.cols, colPrefKey);
    });

    buildViewCols();
    dfReloadAll();
  }
})();
