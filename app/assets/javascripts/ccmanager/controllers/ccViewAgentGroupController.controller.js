export default class ccViewAgentGroupController {

  constructor($scope, $log, XucQueueRecording, Preferences) {
    this.preferences = Preferences;
    this.$scope = $scope;

    $scope.option = {
      compactView: this.preferences.addViewOption($scope,"VIEW_OPTION_COMPACT", this.preferences.CheckboxPersistedTypeOption, false),
    };

    XucQueueRecording.getState().then(response => {
      $scope.option.recordingQueue = this.preferences.addRecordingOption($scope,"SWITCH_QUEUES_RECORDING",
        this.preferences.CheckboxFreeTypeOption, response);
    }, (error) => {
      XucQueueRecording.handleError(error);
    });

    XucQueueRecording.subscribeToQueueConfig(this.$scope, this.onQueueConfigUpdate.bind(this));

    $scope.hasError = () => {
      return XucQueueRecording.hasConfigManagerError();
    };
  }

  onQueueConfigUpdate(value) {
    this.preferences.setRecordingOptionValue(this.$scope.option.recordingQueue, value);
  }
}
