(function(){
  'use strict';

  angular
    .module('ccManager')
    .controller('ccAgentDynViewPrefController',ccAgentDynViewPrefController);

  ccAgentDynViewPrefController.$inject= ['$scope','$state','Preferences'];
  
  function ccAgentDynViewPrefController($scope, $state, preferences) {
    $scope.agentView = preferences.getOption("AgentView.showGroup",{showGroup:false});

    $scope.toggleShowGroup = function() {
      preferences.setOption("AgentView.showGroup", $scope.agentView);
      $state.reload();
    };
  }
})();
