import _ from 'lodash';

(function(){
  'use strict';

  angular
    .module('ccManager')
    .controller('agentEditController',agentEditController)
    .controller('agentEditModalCtrl',agentEditModalCtrl);

  agentEditController.$inject = ['$scope', 'XucAgent','XucQueue','XucGroup', '$uibModal', 'XucMembership', 'Preferences'];
  agentEditModalCtrl.$inject = ['$scope','$uibModalInstance','agent','NgTableParams','XucQueue','XucAgent', 'XucGroup', 'XucMembership', 'XucTableHelper'];

  function agentEditModalCtrl($scope, $uibModalInstance, agent,
    NgTableParams, xucQueue, xucAgent, xucGroup, xucMembership, xucTableHelper) {

    $scope.agent = agent;
    $scope.queues = [];
    $scope.removedQueues = [];
    $scope.agentGroups = [];
    $scope.configManagerError = false;
    $scope.newQueue = {
      queue: null,
      penalty: 0
    };

    angular.forEach($scope.agent.queueMembers, function(penalty, id) {
      var q = xucQueue.getQueue(id);
      $scope.queues.push({"id": _.parseInt(id), "displayName": q.displayName,"number": q.number, "penalty": penalty, "default": null});
    });

    xucMembership.getUserDefaultMembership($scope.agent.userId).then(function(membership) {
      var defaultQueues = [];
      _.forEach(membership, function (o) {
        defaultQueues[o.queueId] = o.penalty;
      });

      $scope.queues = _mergeQueuesMembership($scope.agent.queueMembers, defaultQueues);
      $scope.agentQueues.reload();
    }, function () {
      $scope.configManagerError = true;
    });

    function _mergeQueuesMembership(currentQueues, defaultQueues) {
      var currentQueueIds = [];
      angular.forEach(currentQueues, function(value, key){currentQueueIds.push(key);});
      var defaultQueueIds =[];
      angular.forEach(defaultQueues, function(value, key){defaultQueueIds.push(key);});

      var allQueuesIds = _.union(currentQueueIds, defaultQueueIds);
      var queues = _.map(allQueuesIds, function(id) {
        var q = xucQueue.getQueue(id);
        return {"id": _.parseInt(id), "displayName": q.displayName,"number": q.number, "penalty": _.get(currentQueues, id, null), "default": _.get(defaultQueues, id, null)};
      });
      return queues;
    }

    function refreshGroups() {
      $scope.agentGroups = xucGroup.getGroups();
    }

    $scope.$on('GroupsLoaded', refreshGroups);

    xucGroup.reload();

    $scope.availableQueues = xucQueue.getQueuesExcept($scope.queues);


    $scope.logout = function() {
      xucAgent.logout(agent.id);
    };

    $scope.agentQueues = new NgTableParams(
      {
        page: 1,
        count: 10,
        sorting: {
          penalty: 'asc'
        }
      },
      xucTableHelper.createSettings(function() { return $scope.queues;})
    );

    $scope.addQueue = function() {
      var queue = $scope.newQueue.queue;
      queue.penalty = $scope.newQueue.penalty;
      $scope.queues.push(queue);
      $scope.availableQueues = xucQueue.getQueuesExcept($scope.queues);
      $scope.newQueue.queue = null;
      $scope.agentQueues.reload();
    };

    $scope.removeQueue = function(queue) {
      $scope.queues.splice($scope.queues.indexOf(queue), 1);
      $scope.availableQueues = xucQueue.getQueuesExcept($scope.queues);
      $scope.agentQueues.reload();
    };

    $scope.ok = function () {
      var currentMembership = [];
      var defaultMembership = [];
      _.forEach($scope.queues, function(queue) {
        if (!_.isNull(queue.penalty)) {
          currentMembership.push({"id": queue.id, "penalty": queue.penalty});
        }
        if (!_.isNull(queue.default)) {
          defaultMembership.push({"queueId": queue.id, "penalty": queue.default});
        }
      });
      xucAgent.updateQueues($scope.agent.id,currentMembership);
      xucMembership.setUserDefaultMembership($scope.agent.userId, defaultMembership);
      Cti.setAgentGroup($scope.agent.id, $scope.agent.groupId);
      $uibModalInstance.close($scope.agent);
    };

    $scope.cancel = function () {
      $uibModalInstance.dismiss('cancel');
    };

    $scope.validatePenalty = function(queue) {
      var pred = function(o) {return o.id == queue.id; };
      if(_.isNull(queue.penalty) &&
      _.isNull(queue.default) &&
      !_.some($scope.removedQueues, pred)) {
        $scope.removedQueues.push(queue);
      } else {
        _.remove($scope.removedQueues, pred);
      }
    };

    $scope.classPenalty = function(penalty) {
      if (typeof penalty === 'undefined' || penalty === null) return "no-penalty";
    };

    $scope.applyDefaultMembership = function() {
      $scope.queues = _.map($scope.queues, function(value) {
        var cp = _.clone(value);
        cp.penalty = cp.default;
        return cp;
      });

      $scope.agentQueues.reload();
    };

    $scope.applyCurrentMembershipAsDefault = function() {
      $scope.queues = _.map($scope.queues, function(value) {
        var cp = _.clone(value);
        cp.default = cp.penalty;
        return cp;
      });

      $scope.agentQueues.reload();
    };



  }

  function agentEditController($scope, xucAgent, xucQueue, xucGroup, $uibModal, xucMembership, Preferences) {

    $scope.editAgent = function(agentId) {
      $scope.agent = xucAgent.getAgent(agentId);
      $uibModal.open({
        templateUrl: 'assets/javascripts/ccmanager/controllers/agentEditContent.html',
        controller: 'agentEditModalCtrl',
        resolve: {
          agent: function () {
            return $scope.agent;
          },
        }
      }).result.catch(angular.noop);
    };

    $scope.isCompactView = function() {
      return Preferences.getBoolOption('VIEW_OPTION_COMPACT');
    };

  }
})();
