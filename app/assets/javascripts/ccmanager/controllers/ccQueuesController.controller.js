export default class ccQueuesController {

  constructor(XucQueue, XucAgent, XucQueueGroup, XucQueueRecording, $scope, $rootScope, CCMUtils, Preferences, $attrs, $uibModal, remoteConfiguration, $q) {

    $scope.showCallbacks = ($attrs.showcallbacks === 'true');

    const MAXPENALTY = 20;
    // max chars allowed when queue is empty or folded in compact view
    var INITIAL_QUEUENAME_CHARSIZE = 5;
    var INC_FOLDED_QUEUENAME_CHARSIZE = 1;
    var INC_UNFOLDED_QUEUENAME_CHARSIZE = 2;

    var queueIndicators = $q.defer();

    $scope.queues = [];
    $scope.groupsArray = [];
    $scope.groupGroups = [];
    $scope.folded = false;
    $scope.agentFilter = {
      showAllAgents : true
    };

    remoteConfiguration.get("queueIndicators.toString").then((data)=>{
      $scope.$evalAsync(() => {
        queueIndicators.resolve(data.split(","));
      });
    });

    $scope.toggleShowAllAgents = function(enabled) {
      $scope.agentFilter.showAllAgents = enabled;
      buildAllGroups();
    };

    $scope.isFolded = function(penalty) {
      return (penalty === 0) && $scope.folded;
    };

    $scope.isUnFolded = function(penalty) {
      return (penalty === 0) && !$scope.folded;
    };

    $scope.fold = function() {
      $scope.folded = true;
    };

    $scope.unFold = function() {
      $scope.folded = false;
    };

    $scope.showGroup = function(penalty) {
      return ((penalty === 0) || !$scope.folded );
    };

    $scope.$on('linkDisConnected', function() {
      $scope.connected = false;
      $scope.queues.length = 0;
    });

    var agentGroupTpl = function(penalty) { return( {'penalty' : penalty, 'agents' : []});};

    var initGroupGbl = function(template, groups) {
      var initGrp = function(penalty) {
        if (typeof groups[penalty] === 'undefined') groups[penalty] = template(penalty);
        if (!groups[penalty-1] && penalty > 0) initGrp(penalty-1);
      };
      return initGrp;
    };

    var addToAgentGroup = function(group, agent) {
      group.agents.push(agent);
    };

    $scope.buildGroups = function(queueId, agents, addToGroup, template) {
      var groups = [];
      var initGroup = initGroupGbl(template, groups);
      var getGroup = function(penalty) {
        if (typeof groups[penalty] === 'undefined') initGroup(penalty);
        return groups[penalty];
      };
      var buildGroups = function() {
        angular.forEach(agents, function (agent) {
          if ($scope.agentFilter.showAllAgents || agent.state !== 'AgentLoggedOut' && typeof agent.queueMembers[queueId] !== 'undefined') {
            var group = getGroup(agent.queueMembers[queueId]);
            addToGroup(group,agent);
            if (agent.queueMembers[queueId]+1 < MAXPENALTY) initGroup(agent.queueMembers[queueId]+1);
          }
        });
      };
      initGroup(0);
      buildGroups();
      return groups;
    };

    $scope.buildGroupMember = function(queueId, agents) {
      var groups = $scope.buildGroups(queueId,agents,addToAgentGroup, agentGroupTpl);
      $scope.groupsArray[queueId] = {'idQueue' : queueId, 'groups' : groups};
    };

    $scope.buildGroupOfGroups = function(queueId) {
      XucQueueGroup.getGroupsForAQueue(queueId)
        .then( groups =>$scope.groupGroups[queueId] = {'idQueue' : queueId, 'groups' : groups});
    };

    this.buildQueueName = function(queue, isCompact){
      var separator = isCompact ? '-' : ' - ';
      return queue.number + separator + queue.displayName;
    };

    this.maxQueueNameSize = function(queueId){
      var result = INITIAL_QUEUENAME_CHARSIZE;
      if ($scope.folded) return result;
      $scope.groupsArray[queueId].groups.forEach(function(group){
        result += (group.agents.length == 0) ? INC_FOLDED_QUEUENAME_CHARSIZE : INC_UNFOLDED_QUEUENAME_CHARSIZE;
      });
      return result;
    };

    this.getRecording = (queueId) => {
      const rec = XucQueueRecording.getRecordingQueueInfo(queueId);
      let active = (rec.activated) ? 'active' : 'inactive';
      return [rec.mode, active];
    };

    this.getRecordingIcon = (queueId) => {
      let rec = this.getRecording(queueId);
      let icon = 'glyphicon';
      switch(rec[0]) {
      case 'recorded':
        return icon+'-record '+rec[1];
      case 'recordedondemand':
        return icon+'-pause '+rec[1];
      default:
        return '';
      }
    };

    this.getMetrics = () => {
      return queueIndicators.promise;
    };

    this.getRecordingLabel = (queueId) => {
      let rec = this.getRecording(queueId);
      switch(rec[0]) {
      case 'recorded':
      case 'recordedondemand':
        return rec[0]+'_'+rec[1];
      default:
        return '';
      }
    };

    this.showQueue = function(queueId) {
      $uibModal.open({
        templateUrl: 'queueDetailModal.html',
        controller: 'queueDetailModalController',
        controllerAs: 'modalCtrl',
        resolve: {
          queue: XucQueue.getQueue(queueId)
        }
      }).result.catch(angular.noop);
    };

    var initQueueGroupMembers = function() {
      for (var i = 0; i < $scope.queues.length; i++) {
        $scope.buildGroupMember($scope.queues[i].id,[]);
        $scope.buildGroupOfGroups($scope.queues[i].id,[]);
      }
    };

    var buildGroupForAQueue = function(queueId) {
      var ags = XucAgent.getAgentsInQueue(queueId);
      $scope.buildGroupMember(queueId,ags);
      $scope.buildGroupOfGroups(queueId,ags);
    };

    var buildAllGroups = function() {
      for (var i = 0; i < $scope.queues.length; i++) {
        buildGroupForAQueue($scope.queues[i].id);
      }
    };

    var selectQueue = function() {
      var isQueueSelected = function(queue) {
        return Preferences.isQueueSelected(queue.id);
      };
      $scope.queues = XucQueue.getQueues().filter(isQueueSelected);
    };

    $scope.$on('QueuesLoaded', function() {
      selectQueue();
      initQueueGroupMembers();
      buildAllGroups();
      $rootScope.$broadcast('PostQueuesLoaded');
    });

    $scope.$on('preferences.queueSelected', function() {
      dfSelectQueue();
    });

    $scope.$on('QueueMemberUpdated', function(event, queueId){
      buildGroupForAQueue(queueId);
      dfDigest();
    });

    $scope.$on('AgentStateUpdated', function(){
      dfDigest();
    });

    $scope.$on('AgentConfigUpdated', function() {
      buildAllGroups();
      dfDigest();
    });

    var dfSelectQueue = CCMUtils.doDeffered(function(){
      selectQueue();
      buildAllGroups();
    });

    var dfDigest = CCMUtils.doDeffered(function(){$scope.$digest();});
  }
}
