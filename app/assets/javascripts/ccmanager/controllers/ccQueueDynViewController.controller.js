export default class ccQueueDynViewController {

  constructor($scope, NgTableParams, $filter, $log, XucQueueTotal, ViewColBuilder, Preferences, Thresholds, XucQueueRecording, remoteConfiguration) {
    this.preferences = Preferences;
    this.thresholds = Thresholds;
    this.$scope = $scope;
    this.ViewColBuilder = ViewColBuilder;
    this.XucQueueTotal = XucQueueTotal;

    this.availCols = [];

    XucQueueRecording.getState().then(response => {
      $scope.option = {
        recordingQueue: this.preferences.addRecordingOption($scope,"SWITCH_QUEUES_RECORDING", this.preferences.CheckboxFreeTypeOption, response)
      };
    }, (error) => {
      XucQueueRecording.handleError(error);
    });

    XucQueueRecording.subscribeToQueueConfig(this.$scope, this.onQueueConfigUpdate.bind(this));

    $scope.init = () => {
      remoteConfiguration.getFromXuc('stats/queues.statTresholdsInSec').then(response => {
        XucQueueTotal.addStatsThresholds(response);
        $log.info('Adding columns with following thresholds', XucQueueTotal.getStatsThresholds());
      }, () => {
        $log.error('No custom threshold added to queue totals stats, check your configuration');
      }).finally(() => {
        this.generateTable();
        $scope.cols = this.preferences.getViewColumns(this.availCols, this.preferences.QueueViewColumnsKey);
        $scope.stats = XucQueueTotal.getCalcStats();
      });
    };

    $scope.tableSettings = function() {
      var calculatePage = function(params) {
        if (params.total() <= (params.page() - 1)  * params.count()) {
          var setPage = (params.page()-1) > 0 && (params.page()-1) || 1;
          params.page(setPage);
        }
      };
      return {
        total: $scope.queues.length, // length of data
        counts:[],
        getData: function(params) {
          var orderedData = params.sorting() ?
            $filter('orderBy')($scope.queues, params.orderBy()) :
            $scope.queues;
          params.total(orderedData.length);
          XucQueueTotal.calculate(orderedData);
          $scope.stats = XucQueueTotal.getCalcStats();
          calculatePage(params);
          return orderedData.slice((params.page() - 1) * params.count(), params.page() * params.count());
        }
      };
    };

    $scope.queueDynTable = new NgTableParams({
      page: 1,
      count: 50,
      sorting: {
        displayName: 'asc'
      }
    }, $scope.tableSettings()
    );

    $scope.$watch('queues', function(newQueues) {
      if (!newQueues) return;
      if (newQueues.length>0) $scope.queueDynTable.reload();
    },true);

    $scope.$on(this.preferences.QueueViewColumnsChanged, () => {
      $scope.cols = this.preferences.getViewColumns(this.availCols, this.preferences.QueueViewColumnsKey);
    });

    $scope.$on('moveTableCol', (event,args) => {
      this.preferences.moveCol(args.colRef, args.beforeColRef, $scope.cols, this.preferences.QueueViewColumnsKey);
    });

    $scope.thresholdClass = (field, value) => {
      return this.thresholds.thresholdClass(field,value);
    };

    $scope.hasError = () => {
      return XucQueueRecording.hasConfigManagerError();
    };

    $scope.init();
  }

  generateTable() {
    let ViewColBuilder = this.ViewColBuilder;
    ViewColBuilder.setPrefix('COL');

    this.availCols.push(ViewColBuilder.col('number').withFieldClass('badge').build());
    this.availCols.push(ViewColBuilder.col('displayName').build());
    this.availCols.push(ViewColBuilder.col('WaitingCalls').build());
    this.availCols.push(ViewColBuilder.col('LongestWaitTime').withDisplayFilter('queueTime').build());
    this.availCols.push(ViewColBuilder.col('EWT').withDisplayFilter('queueTime').build());
    this.availCols.push(ViewColBuilder.col('AvailableAgents').build());
    this.availCols.push(ViewColBuilder.col('TalkingAgents').build());
    this.availCols.push(ViewColBuilder.col('TotalNumberCallsEntered').withTooltip().build());
    this.availCols.push(ViewColBuilder.col('TotalNumberCallsAnswered').withTitleClass('tableTitle answered').withTooltip().build());
    this.availCols.push(ViewColBuilder.col('TotalNumberCallsAbandonned').withTitleClass('tableTitle abandonned').withTooltip().build());
    this.availCols.push(ViewColBuilder.col('TotalNumberCallsNotAnswered').withTitleClass('tableTitle notanswered').withTooltip().build());
    this.availCols.push(ViewColBuilder.col('TotalNumberCallsTimeout').withTitleClass('tableTitle timeout').withTooltip().build());
    this.XucQueueTotal.getStatsThresholds().forEach(t => {
      this.availCols.push(ViewColBuilder.col('PercentageAnsweredBefore'+t).withDisplayFilter('number:1').withTooltip().build());
    });
    this.XucQueueTotal.getStatsThresholds().forEach(t => {
      this.availCols.push(ViewColBuilder.col('PercentageAbandonnedAfter'+t).withDisplayFilter('number:1').withTooltip().build());
    });
    this.availCols.push(ViewColBuilder.col('TotalNumberCallsClosed').withTooltip().build());

    this.$scope.totals = {};
    this.$scope.totals.WaitingCalls = {value:'stats.sum.WaitingCalls'};
    this.$scope.totals.LongestWaitTime = {value:'stats.max.LongestWaitTime | queueTime'};
    this.$scope.totals.EWT = {value:'stats.max.EWT | queueTime'};
    this.$scope.totals.TotalNumberCallsEntered = {value:'stats.sum.TotalNumberCallsEntered'};
    this.$scope.totals.TotalNumberCallsAnswered = {value:'stats.sum.TotalNumberCallsAnswered'};
    this.XucQueueTotal.getStatsThresholds().forEach(t => {
      this.$scope.totals['PercentageAnsweredBefore'+t] = {value:'stats.global.PercentageAnsweredBefore'+t+' | number:1'};
    });
    this.$scope.totals.TotalNumberCallsAbandonned = {value:'stats.sum.TotalNumberCallsAbandonned'};
    this.XucQueueTotal.getStatsThresholds().forEach(t => {
      this.$scope.totals['PercentageAbandonnedAfter'+t] = {value:'stats.global.PercentageAbandonnedAfter'+t+' | number:1'};
    });
    this.$scope.totals.TotalNumberCallsClosed = {value:'stats.sum.TotalNumberCallsClosed'};
    this.$scope.totals.TotalNumberCallsTimeout = {value:'stats.sum.TotalNumberCallsTimeout'};
    this.$scope.totals.TotalNumberCallsNotAnswered = {value:'stats.sum.TotalNumberCallsNotAnswered'};
  }

  onQueueConfigUpdate(value) {
    this.preferences.setRecordingOptionValue(this.$scope.option.recordingQueue, value);
  }
}
