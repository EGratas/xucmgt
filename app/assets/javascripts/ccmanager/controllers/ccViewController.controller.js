(function() {
  'use strict';

  angular.module('ccManager').controller('ccViewController', ccViewController);

  ccViewController.$inject = ['$scope', 'localStorageService','$rootScope','$location','Preferences', 'XucLink',
    'userRights', '$state','$log'];

  function ccViewController($scope, localStorageService, $rootScope, $location, preferences, xucLink, userRights, $state, $log) {
    $scope.viewName = localStorageService.get('ccmanagerView') || 'global';
    $scope.helperVisibleMenu = !preferences.isIntroHelperShown('menuselection');
    $scope.showAlert = true;
    preferences.setOption("VIEW_OPTION_SHOWALL", true);

    var _manageViewParameter = function (){
      var viewName = $location.search().view;
      if(viewName){
        $scope.setView(viewName);
      }
    };

    var _manageQueuesParameter = function (){
      var queues = $location.search().queues;
      if(queues){
        preferences.setQueuesVisibility(queues,true);
        $rootScope.$broadcast('preferences.queueSelected');
      }
    };

    var locationChangeSuccessHandler = $rootScope.$on('$locationChangeSuccess',function(){
      _manageViewParameter();
      _manageQueuesParameter();
    });

    var postQueuesLoadedHandler = $rootScope.$on('PostQueuesLoaded', function() {
      _manageQueuesParameter();
    });

    $scope.isActive = function(viewName) {
      return ($scope.viewName === viewName);
    };
    var changeViewHandler = $rootScope.$on('changeView', function(event) {
      $scope.setView(event.viewName);
    });

    $scope.$on('$destroy',() => {
      locationChangeSuccessHandler();
      postQueuesLoadedHandler();
      changeViewHandler();
    });

    $scope.setView = function(viewName) {
      if ($scope.viewName != viewName) preferences.clearOptions();
      $scope.viewName = viewName;
      localStorageService.set('ccmanagerView', $scope.viewName);
    };

    xucLink.getProfileRightAsync().then(function(rightProfile) {
      $scope.userRightProfile = rightProfile;
      $log.info(`Connected profile: ${rightProfile.profile} with ${rightProfile.rights} rights`);
      if(document.body.getAttribute('enforce-security') === "true" &&
      !userRights.allowCCManager($scope.userRightProfile.profile)) {
        $state.go("login", {error: 'MissingRights'});
        xucLink.logout();
      }
    });

    $scope.hideHelper = function() {
      $scope.helperVisibleMenu = false;
      preferences.setIntroHelperShown('menuselection');
    };

    $scope.closeAlert = function() {
      $scope.showAlert = false;
    };

    $scope.displayAlert = function() {
      return $scope.showAlert && ($scope.userRightProfile == 'NoRightForUser');
    };
  }

  ccViewController.prototype.togglePinnedMenu = function() {
    if (this.menuPinned === undefined) this.menuPinned = false;
    if (this.menuState && !this.menuPinned) this.menuState = !this.menuState;
  };
})();
