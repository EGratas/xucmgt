import _ from 'lodash';

(function(){
  'use strict';

  angular
    .module('ccManager')
    .controller('qualificationResultController',qualificationResultController);

  qualificationResultController.$inject= ['$scope','XucQueue', 'XucLink'];

  function qualificationResultController($scope, XucQueue, XucLink) {

    $scope._getNextDay = function(date) {
      var nextDay = new Date(date);
      nextDay.setDate(date.getDate()+1);
      return nextDay;
    };

    $scope.init = function() {
      $scope.queues = _.sortBy(XucQueue.getQueues(),  function(o) { return o.number; });
      $scope.datePickerOpts = {
        showWeeks: true
      };
      $scope.format = 'yyyy-MM-dd';
      $scope.popupFrom = { opened: false };
      $scope.popupTo = { opened: false };
      $scope.dateFrom = new Date();
      $scope.dateTo = $scope._getNextDay($scope.dateFrom);
    };
    $scope.init();

    $scope.openFrom = function() {
      $scope.popupFrom.opened = true;
    };

    $scope.openTo = function() {
      $scope.popupTo.opened = true;
    };

    $scope.getISODate = function(date) {
      return date.toISOString().substring(0,10);
    };

    $scope.getUrl = function() {
      return XucLink.getServerUrl('http')+'/xuc/api/2.0/call_qualification/csv/' + $scope.selectedQueue.id + '/' +
        $scope.getISODate($scope.dateFrom) + '/' + $scope.getISODate($scope.dateTo);
    };

    $scope.openCsvUrl = function(){
      window.open($scope.getUrl(), '_blank');
    };

  }

})();
