export default class queueDetailModalController {

  constructor($uibModalInstance, queue, userRights) {
    this.queue = queue;
    this.$uibModalInstance = $uibModalInstance;
    this.$uibModalInstance.result.then(() => {}, () => {
      this.close();
    });
    this.userRights = userRights;
    this.showDissuasion = false;

    this.userRights.getDissuasionRight().then(value => {
      this.showDissuasion = value;
    });
  }

  close() {
    this.$uibModalInstance.close();
  }
}