(function(){
  'use strict';

  angular
    .module('ccManager')
    .directive('draggableGroup',draggableGroup);

  function draggableGroup(dragDrop){
    return {
      replace:true,
      link : function(scope, element) {
        var el = element[0];
        el.draggable = true;
        el.addEventListener(
          'dragstart',
          function(e) {
            dragDrop.onDragStartEvent(e);
            dragDrop.setDataTransfer(e,{groupId: scope.group.id, queueId: scope.queue.id, penalty: scope.groupOfGroups.penalty, clone: e.ctrlKey});
            this.style.opacity = '0.6';
            return false;
          },
          false
        );
        el.addEventListener(
          'dragend',
          function() {
            this.style.opacity = '1';
            return false;
          },
          false
        );
      }
    };
  }
})();
