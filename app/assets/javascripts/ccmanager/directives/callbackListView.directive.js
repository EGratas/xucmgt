import _ from 'lodash';
import moment from 'moment';

(function(){

  var ccManager = angular.module('ccManager');
  ccManager.directive('callbackListView', function(){
    return {
      restrict: 'E',
      templateUrl: 'assets/javascripts/ccmanager/directives/callbackListView.html',
      scope: {
        listUuid: "="
      },
      controller: ['$scope','XucCallback', 'Preferences', 'NgTableParams', 'XucAgent', '$q', '$translate',
        function($scope, xucCallback, Preferences, NgTableParams, XucAgent, $q, $translate){
          $scope.filterDataLists = $q.defer();

          $scope.filterDataClotured = [
            {id: "", title: "-"},
            {id: "true", title: $translate.instant('CB_CLOTURED_TRUE') },
            {id: "false", title: $translate.instant('CB_CLOTURED_FALSE')}
          ];

          $scope.filterDataAgentsDefer = $q.defer();
          $scope.filterDataAgents = $scope.filterDataAgentsDefer.promise;

          XucAgent.getAgentsAsync().then(function(agents) {
            var res = _.sortBy(_.map(agents, function(item) {
              return {id: item.id, title: item.firstName + " " + item.lastName};
            }), "title");

            $scope.filterDataAgentsDefer.resolve(res);
          });

          $scope.clotureI18n = function(value) {
            return $translate.instant('CB_CLOTURED_' + String(value).toUpperCase());
          };

          $scope.getCallbackStatus = function(callback) {
            return xucCallback.getCallbackStatus(callback);
          };

          $scope.callbackTable = new NgTableParams(
            {
              page: 1,
              count: 10,
              sorting: { dueDate: 'asc' },
              filter: { clotured: "false" }
            },
            {
              defaultSort: 'asc',
              getData: function(params) {
                var q = xucCallback.queryBuilder();

                _.forEach(params.filter(), function(value, key) {
                  if(angular.isDefined(value) && value !== null && value !== "") {
                    if(key === "dueDate" && value instanceof Date) {
                      value = moment(value).format("YYYY-MM-DD");
                    }
                    if(["firstname", "lastname", "phoneNumber", "mobilePhoneNumber", "company"].indexOf(key) > -1) {
                      q = q.filterIlike(key, '%' + value + '%');
                    } else {
                      q = q.filterEq(key, value);
                    }
                  }
                });
                q = q.filterEq("listUuid", $scope.listUuid);
                _.forEach(params.sorting(), function(value, key){
                  q = q.sort(key, value);
                });
                q = q.limit(params.count()).offset(params.count() * (params.page()-1)).build();
                return xucCallback.findCallbackRequestsAsync(q).then(function(result) {
                  params.total(result.total);
                  _.forEach(result.list, function(cb) {
                    cb.cloturedI18n = $scope.clotureI18n(cb.clotured);
                  });
                  return result.list;
                });
              }
            }
          );
        }]
    };
  });
})();
