(function(){
  'use strict';

  angular
    .module('ccManager')
    .directive('droppableGroupTrash',droppableGroupTrash);

  function droppableGroupTrash(dragDrop){
    return {
      replace:true,
      link: function(scope, element) {
        var el = element[0];
        el.addEventListener(
          'dragover',
          function(e) {
            e.dataTransfer.dropEffect = 'move';
            if (e.preventDefault) e.preventDefault();
            this.style.backgroundColor = "Red";
            return false;
          },
          false
        );
        el.addEventListener(
          'dragleave',
          function(e) {
            e.dataTransfer.dropEffect = 'move';
            // allows us to drop
            if (e.preventDefault) e.preventDefault();
            this.style.backgroundColor = "";
            return false;
          },
          false
        );
        el.addEventListener(
          'drop',
          function(e) {
            dragDrop.onDropEvent(e);
            this.style.backgroundColor = "";

            let data = dragDrop.getDataTransfer(e);
            scope.removeAgentGroupFromQueueGroup(data.groupId,
              data.queueId,
              data.penalty
            );
            return false;
          },
          false
        );
      }
    };
  }
})();
