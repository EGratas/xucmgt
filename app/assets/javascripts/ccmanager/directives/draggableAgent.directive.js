(function(){
  'use strict';

  angular
    .module('ccManager')
    .directive('draggableAgent',draggableAgent);

  function draggableAgent(dragDrop){
    return {
      replace:true,
      link : function(scope, element) {
        var el = element[0];
        el.draggable = true;
        el.addEventListener(
          'dragstart',
          function(e) {
            dragDrop.onDragStartEvent(e);
            dragDrop.setDataTransfer(e,{agentId: scope.agent.id, queueId: scope.queue.id, clone: e.ctrlKey});
            this.style.opacity = '0.7';
            return false;
          },
          false
        );
        el.addEventListener(
          'dragend',
          function() {
            this.style.opacity = '1';
            return false;
          },
          false
        );
      }
    };
  }
})();
