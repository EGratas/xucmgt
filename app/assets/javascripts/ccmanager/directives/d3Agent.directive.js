import d3 from 'd3';

(function () {
  'use strict';

  angular
    .module('ccManager')
    .directive('d3Agent', d3Agent);

  d3Agent.$inject = ['Preferences'];

  function d3Agent(Preferences) {
    var heightSvg = 50;
    var heightSvgCompact = 20;
    var widthSvg = 65;
    var roundCorner = 5;
    var nameMaxLen = 7;
    var fontSize = 12;
    var padding = 1;
    var agentStyle = "font-size:" + fontSize + "px;";

    var isCompactView = function () {
      return Preferences.getBoolOption('VIEW_OPTION_COMPACT');
    };

    var formatText = function (str, maxLen) {
      if (str.length > maxLen) {
        return str.substring(0, maxLen - 1) + '.';
      }
      else return str;
    };
    var drawAgentLabel = function (text, svgContainer, ypos) {
      svgContainer.append("text").attr("style", agentStyle)
        .append("tspan").attr("x", widthSvg / 2).attr("y", ypos)
        .attr("class", "AgentText").text(text);
    };
    var drawBox = function (agentState, svgContainer, boxHeight) {
      svgContainer.append("rect")
        .attr("x", padding).attr("y", 3 * padding).attr("rx", roundCorner).attr("ry", roundCorner)
        .attr("width", widthSvg - (2 * padding)).attr("height", boxHeight)
        .attr("class", agentState + " AgentBox");
    };

    return {
      restrict: 'E',
      scope: {
        data: '='
      },
      link: function (scope, element) {

        scope.render = function (data) {

          d3.select(element[0]).select("svg").remove();
          d3.select(element[0]).select("g").remove();

          var agentFirstNameToDisplay = data.firstName ? formatText(data.firstName, nameMaxLen) : "";
          var agentLastNameToDisplay = data.lastName ? formatText(data.lastName.toUpperCase(), nameMaxLen) : "";

          var svg = d3.select(element[0]).append("svg")
            .attr("height", isCompactView() ? heightSvgCompact : heightSvg).attr("width", widthSvg);
          data = data || {};

          var svgContainer = svg.append("g");

          var labelPos = {};
          labelPos.top = (2 * padding) + fontSize * 1.1;
          labelPos.middle = (2 * padding) + fontSize * 2.5;
          labelPos.bottom = (2 * padding) + fontSize * 3.5;

          if (isCompactView()) {
            drawBox(data.state, svgContainer, heightSvgCompact - (4 * padding));
            drawAgentLabel(agentFirstNameToDisplay, svgContainer, labelPos.top);
          } else {
            drawBox(data.state, svgContainer, heightSvg - (6 * padding));
            drawAgentLabel(data.phoneNb, svgContainer, labelPos.top);
            drawAgentLabel(agentLastNameToDisplay, svgContainer, labelPos.middle);
            drawAgentLabel(agentFirstNameToDisplay, svgContainer, labelPos.bottom);
          }
        };

        scope.$watch('data.state', function () {
          scope.render(scope.data);
        }, true);

        scope.$watch(isCompactView, function () {
          scope.render(scope.data);
        }, true);

      }
    };
  }
})();
