export default function goBackArrow() {
  return {
    restrict: 'E',
    templateUrl: 'assets/javascripts/xcchat/directives/goBackArrow.html',
    scope: {
      action: '='
    }
  };
}
