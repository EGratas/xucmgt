package models

sealed trait XucAuthenticationMode

case object XivoAuthentication extends XucAuthenticationMode {
  override def toString() = "xivo"
}
case object KerberosAuthentication extends XucAuthenticationMode {
  override def toString() = "kerberos"
}
case object TokenAuthentication extends XucAuthenticationMode {
  override def toString() = "token"
}

case class XucUser(
    ctiUsername: String,
    ctiPassword: String,
    phoneNumber: Option[String] = None,
    authenticationMode: XucAuthenticationMode = XivoAuthentication
) {
  override def toString() = s"XucUser($ctiUsername,****,$phoneNumber)"
}

object XucUser {
  def applyForm(
      ctiUsername: String,
      ctiPassword: String,
      phoneNumber: Option[String] = None,
      authenticationMode: Option[String]
  ) = {
    val auth = authenticationMode match {
      case Some("kerberos") => KerberosAuthentication
      case Some("token")    => TokenAuthentication
      case _                => XivoAuthentication
    }
    XucUser(ctiUsername, ctiPassword, phoneNumber, auth)
  }

  def unapplyForm(
      u: XucUser
  ): Option[(String, String, Option[String], Option[String])] =
    Some(
      (
        u.ctiUsername,
        u.ctiPassword,
        u.phoneNumber,
        Some(u.authenticationMode.toString)
      )
    )
}
