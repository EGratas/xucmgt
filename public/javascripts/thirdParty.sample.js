(function () {
  function close() {
    parent.window.postMessage("closeThirdParty", "*");
  }

  document.getElementById('close').addEventListener("click", close, false);
})();