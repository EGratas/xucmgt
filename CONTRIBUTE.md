# Requirements

You need:

1. An account on gitlab.com
2. A ssh key to be used to authenticate on gitlab
2. git installed on your personal computer configured with your ssh key

# Make changes in an existing project

To make a change in an existing project,

1. You need to have a personnal repository containing a `fork` of the project (see [Forking](#forking) below)
2. You can then clone your personnal repository on your laptop (see [Cloning](#cloning))
3. You make changes to your local repository (see [Working locally](#working-locally))
4. You can then create a merge request so your changes get to the main repository (see [Create a Merge Request](#create-a-merge-request))

Step 1 & 2 need to be done only once

## Forking

1. Go to project on gitlab (i.e. https://gitlab.com/xivo.solutions/xucmgt)
2. Click on 'Fork' to fork the project official repository
3. If needed, select your personnal namespace (i.e. your account)

You end up with a personnal fork of the project (something like https://gitlab.com/jpthomasset/xucmgt for example)

<details>
<summary>:mag: Click here to see some screenshots of the previous steps.</summary>

### Step 2
![Step 2](doc/contribute-fork.png)

### Step 3
![Step 3](doc/contribute-fork-namespace.png)

</details>

## Cloning

To work locally, you need to first clone the repository on your personnal computer. This can be done by using the url given on your personnal project, for example here:

```bash
git clone git@gitlab.com:jpthomasset/xucmgt.git
```
(Remember to use the url given on your personnal project instead of `git@gitlab.com:jpthomasset/xucmgt.git`)

Now you have a directory on your computer pointing to your personnal repo.

## Working locally

The first thing to do is to create a new branch specific to your changes

```bash
cd xucmgt
git checkout -b my-branch-fixing-something
```

Then add a file or modify an existing one. Once it's done, you can add the changes to your local repository by using the following commands:

```bash
git add my-modified-file.txt
git add my-added-file.txt
git commit -m "Modified one file and added one"
```

(Please try to use meaningful message in your commit, so not like the example ;) ).

When your are happy with your changes, you can push them on the server using the following command:
```bash
git push -u origin my-branch-fixing-something
```

## Create a Merge Request

Once you are ready to report your changes to our repo, you can create a merge request from your project and choosing our project as a target:

1. On your project, click on create merge
2. Click on `Change branches` near `Target branch`
3. Select xivo.solution project and targe branch
4. Click on `Compare branches and continue`

Congratulations, you just created your Merge Request ! Thank you for contributing, now you can have some rest and chill while we review and merge your changes in our projects ! :sunglasses:

<details>
<summary>:mag: Click here to see some screenshots of the previous steps.</summary>

### Step 1
![Step 1](doc/contribute-create-mr.png)

### Step 2
![Step 2](doc/contribute-change-branch.png)

### Step 3
![Step 3](doc/contribute-target-branch.png)

</details>
