const path = require('path');
const webpack = require('webpack');
const ESLintPlugin = require('eslint-webpack-plugin');
const TerserPlugin = require('terser-webpack-plugin');

module.exports = {
  mode: 'production',
  entry: {
    agent: ['./app/assets/javascripts/ccagent/app.js'],
    ccmanager: './app/assets/javascripts/ccmanager/app.js',
    ucassistant: './app/assets/javascripts/ucassistant/app.js',
    xuc:  ['xccti/cti-webpack']
  },
  output: {
    filename: '[name].js',
    path: path.resolve(__dirname, 'target/web/public/main/javascripts/dist/')
  },
  resolve: {
    extensions: ['.ts', '.tsx', '.js', '.service.js'],
    alias: {
      "ccagent": path.resolve(__dirname, "app/assets/javascripts/ccagent/"),
      "ccmanager": path.resolve(__dirname, "app/assets/javascripts/ccmanager/"),
      "ucassistant": path.resolve(__dirname, "app/assets/javascripts/ucassistant/"),
      "xccti": path.resolve(__dirname, "app/assets/javascripts/xccti/"),
      "xchelper": path.resolve(__dirname, "app/assets/javascripts/xchelper/"),
      "xclogin": path.resolve(__dirname, "app/assets/javascripts/xclogin/"),
      "xcchat": path.resolve(__dirname, "app/assets/javascripts/xcchat/"),
      "jquery-ui": 'jquery-ui-dist/jquery-ui.js'
    }
  },
  plugins: [
    new webpack.ProvidePlugin({
      $: 'jquery',
      jQuery: 'jquery',
      moment: 'moment',
      countdown: 'countdown',
      _: 'lodash',
      "window.jQuery":"jquery",
      "window._": "lodash"
    }),
    new ESLintPlugin(
      {
        exclude: ["node_modules", "cti.js"],
        fix: true
      }
    )
  ],
  optimization: {
    minimize: true,
    minimizer: [
      new TerserPlugin({
        include: /vendor\.js/,
        parallel: true
      })
    ],
    splitChunks: {
      cacheGroups: {
        vendor: {
          test: /node_modules/,
          chunks: 'initial',
          name: 'vendor',
          enforce: true
        },
       }
    }
  },
  module: {
    rules: [
      {
        test: /\.tsx?$/,
        use: {
            loader: 'ts-loader',
            options: {
                configFile: 'tsconfig.tests.json'
            }
        },
        exclude: {or: [/node_modules/, /electron/, /target/]},
      },
      {
        test: require.resolve('jquery'),
        loader: 'expose-loader',
        options: {
          exposes: ["$", "jQuery"],
        }
      },
      {
        test: require.resolve('angular'),
        loader: 'expose-loader',
        options: {
          exposes: 'angular'
        }
      },
      {
        test: /\.css$/,
        use: [ 'style-loader', 'css-loader' ]
      },
      {
        test: /\.html$/i,
        loader: "html-loader",
      },
      {
        test: /\.(png|jpg|gif)$/,
        use: [
          {
            loader: 'file-loader',
            options: {
              publicPath: 'assets/javascripts/dist',
              name: '[name].[ext]'
            },
          },
        ],
      }
    ]
  }
};
